package com.game.towertitans.android;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import com.google.android.gms.games.Games;
import com.google.example.games.basegameutils.GameHelper;

import game.TowerTitan;
import gservices.GServices;


public class AndroidLauncher extends AndroidApplication implements GServices {
	private GameHelper _gameHelper;
	private final static int REQUEST_CODE_UNUSED = 9002;
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useImmersiveMode = true;
		// Create the GameHelper.
		/*_gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
		_gameHelper.enableDebugLog(false);

		GameHelper.GameHelperListener gameHelperListener = new GameHelper.GameHelperListener()
		{
			@Override
			public void onSignInSucceeded()
			{
			}

			@Override
			public void onSignInFailed()
			{
			}
		};
		_gameHelper.setup(gameHelperListener);*/
		initialize(new TowerTitan(null,false), config);
	}@Override
	protected void onStart()
	{
		super.onStart();
		//_gameHelper.onStart(this);
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		//_gameHelper.onStop();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		//_gameHelper.onActivityResult(requestCode, resultCode, data);
	}
	@Override
	public void signIn()
	{
		/*try
		{
			runOnUiThread(new Runnable()
			{
				//@Override
				public void run()
				{
					_gameHelper.beginUserInitiatedSignIn();
				}
			});
		}
		catch (Exception e)
		{
			Gdx.app.log("Android", "Log in failed: " + e.getMessage() + ".");
		}*/
	}

	@Override
	public void signOut()
	{
		/*try
		{
			runOnUiThread(new Runnable()
			{
				//@Override
				public void run()
				{
					_gameHelper.signOut();
				}
			});
		}
		catch (Exception e)
		{
			Gdx.app.log("Android", "Log out failed: " + e.getMessage() + ".");
		}*/
	}
	@Override
	public void rateGame()
	{
		// Replace the end of the URL with the package of your game
		String str ="https://play.google.com/store/apps/details?id=com.game.pixelotkyle.android";
		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(str)));
	}

	@Override
	public void submitScore(long score, int id)
	{
		if (isSignedIn())
		{
			/*if(id == 1){
				Games.Leaderboards.submitScore(_gameHelper.getApiClient(), getString(R.string.leaderboard_arena_monster_challenge), score);
			}
			else {
				Games.Leaderboards.submitScore(_gameHelper.getApiClient(), getString(R.string.leaderboard_dark_tower_highest_floor), score);
			}*/
		}
		else
		{
			// Maybe sign in here then redirect to submitting score?
		}
	}

	@Override
	public void unlockAchievement(final int id) {
		if (isSignedIn())
		{

			runOnUiThread(new Runnable()
			{
				//@Override
				public void run()
				{/*int achievement = 0;
					switch (id){
						case 1 :
							achievement = R.string.achievement_rangers_trial;
							break;
						case 2 :
							achievement = R.string.achievement_level_10;
							break;
						case 3 :
							achievement = R.string.achievement_earth_crystal;
							break;
						case 4 :
							achievement = R.string.achievement_protect_20_allies;
							break;
						case 5 :
							achievement = R.string.achievement_defeat_the_mercenaries;
							break;
						case 6 :
							achievement = R.string.achievement_collect_10_heroes;
							break;
						case 7 :
							achievement = R.string.achievement_deal_200_critical_hits;
							break;
						case 8 :
							achievement = R.string.achievement_level_20;
							break;
						case 9 :
							achievement = R.string.achievement_shadow_crystal;
							break;
						case 10 :
							achievement = R.string.achievement_escape_the_sewers;
							break;
						case 11 :
							achievement = R.string.achievement_heal_5000_damage;
							break;
						case 12 :
							achievement = R.string.achievement_collect_100_items;
							break;
						case 13 :
							achievement = R.string.achievement_defeat_500_monsters;
							break;
						case 14 :
							achievement = R.string.achievement_defeat_40_unique_enemies;
							break;
						case 15 :
							achievement = R.string.achievement_level_30;
							break;
						case 16 :
							achievement = R.string.achievement_sages_trial;
							break;
						case 17 :
							achievement = R.string.achievement_restore_myria;
							break;
						case 18 :
							achievement = R.string.achievement_10000_gold;
							break;
						case 19 :
							achievement = R.string.achievement_level_40;
							break;
						case 20 :
							achievement = R.string.achievement_defeat_the_tower_king;
							break;
						case 21 :
							achievement = R.string.achievement_level_50;
							break;
						case 22 :
							achievement = R.string.achievement_level_60;
							break;
						case 23 :
							achievement = R.string.achievement_level_70;
							break;
						case 24 :
							achievement = R.string.achievement_level_80;
							break;
						case 25 :
							achievement = R.string.achievement_level_90;
							break;
						case 26 :
							achievement = R.string.achievement_level_100;
							break;
						case 27 :
							achievement = R.string.achievement_water_crystal;
							break;
						case 28 :
							achievement = R.string.achievement_storm_crystal;
							break;
						case 29 :
							achievement = R.string.achievement_maximum_upgrade;
							break;
						case 30 :
							achievement = R.string.achievement_upgrade_20_times;
							break;
						case 31 :
							achievement = R.string.achievement_orgo_chief;
							break;
						case 32 :
							achievement = R.string.achievement_mt_magmor;
							break;
						case 33 :
							achievement = R.string.achievement_1000_damage;
							break;
						case 34 :
							achievement = R.string.achievement_arena_challenge;
							break;
						case 35 :
							achievement = R.string.achievement_promote_10_units;
							break;
						case 36 :
							achievement = R.string.achievement_prisma;
							break;
						case 37 :
							achievement = R.string.achievement_defeat_20_bosses;
							break;
						case 38 :
							achievement = R.string.achievement_quest;
							break;
						case 39 :
							achievement = R.string.achievement_great_yeti;
							break;
						case 40 :
							achievement = R.string.achievement_light_crystal;
							break;
						case 41 :
							achievement = R.string.achievement_hidden_grotto;
							break;
						case 42 :
							achievement = R.string.achievement_fire_crystal;
							break;
						case 43 :
							achievement = R.string.achievement_save_pixelot;
							break;
						case 44 :
							achievement = R.string.achievement_labyrinth;
							break;
						case 45 :
							achievement = R.string.achievement_legendary;
							break;
						case 46 :
							achievement = R.string.achievement_prisma_ii;
							break;
					}
					Games.Achievements.unlock(_gameHelper.getApiClient(), getString(achievement));*/
				}
			});
		}
		else
		{
			// Maybe sign in here then redirect to submitting score?
		}
	}

	@Override
	public void showScores(int id)
	{
		if (isSignedIn()) {
			/*if(id == 1){
				startActivityForResult(Games.Leaderboards.getLeaderboardIntent(_gameHelper.getApiClient(), getString(R.string.leaderboard_arena_monster_challenge)), REQUEST_CODE_UNUSED);
			}
			else {
				startActivityForResult(Games.Leaderboards.getLeaderboardIntent(_gameHelper.getApiClient(), getString(R.string.leaderboard_dark_tower_highest_floor)), REQUEST_CODE_UNUSED);
			}*/
		}
		else
		{
			// Maybe sign in here then redirect to showing scores?
		}
	}

	@Override
	public boolean showAchievements() {
		if (isSignedIn()) {
			startActivityForResult(Games.Achievements.getAchievementsIntent(_gameHelper.getApiClient()),
					REQUEST_CODE_UNUSED);
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public boolean isSignedIn()
	{
		return false;//_gameHelper.isSignedIn();
	}


}
