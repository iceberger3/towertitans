package game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import helpers.ConfigObject;
import helpers.SaveObject;
import screens.GameScreen;
import helpers.AssetLoader;
import screens.MenuScreen;
import gservices.GServices;


public class TowerTitan extends Game {
    public static SaveObject save;
    public static ConfigObject config;
    public Music music, title, battle, map;
    public static boolean demo = false;
    private static Sound button, success,train,ding,mumble,select, back, validate, door;
    public Screen previous;
    public InputProcessor previousInput;
    public int musicId = -1;
    public int maxWidth = 128;
    public int maxHeight = 224;
    public int maxX = 0;
    public int maxY = 0;
    public boolean isIphoneX = false;
    public int iphoneXLeft = 0;
    public int iphoneXRight = 0;
    private AssetManager assetManager;
    private AssetManager musicManager;
    private int musicCount = 0;
    public static GServices googleServices;
    public static boolean isDesktop = false;
    public static boolean hasController = false;
    public TowerTitan(GServices googleServices, boolean isIphoneX, boolean isDesktop)
    {
        super();
        TowerTitan.googleServices = googleServices;
        TowerTitan.isDesktop = isDesktop;
        this.isIphoneX = isIphoneX;
        if(isIphoneX){
            iphoneXLeft = 16;
            iphoneXRight = 16;
        }

    }
    public TowerTitan(GServices googleServices, boolean isDesktop)
    {
        super();
        TowerTitan.isDesktop = isDesktop;
        TowerTitan.googleServices = googleServices;

    }
    @Override
    public void create() {
        assetManager = new AssetManager();
        musicManager = new AssetManager();
        int h = Gdx.graphics.getHeight();
        int w = Gdx.graphics.getWidth();
        float ratio = (float)w / (float)h;
        /*
        if (ratio > 128.0f / 224.0f) {
            maxWidth = 224;
            maxHeight = Math.round(maxWidth * ratio);
        } else {
            maxHeight = 128;
            maxWidth = Math.round((1 / ratio) * maxHeight);
        }*/
        maxWidth = 128;
        maxHeight = Math.round(maxWidth / ratio);
        if(ratio < .5f && !isDesktop){
            isIphoneX = true;
            iphoneXLeft = 16;
            iphoneXRight = 16;
        }
        else{
            isIphoneX = false;
            iphoneXLeft = 0;
            iphoneXRight = 0;
        }
        maxX = Gdx.graphics.getWidth();
        maxY = Gdx.graphics.getHeight();
        //AssetLoader.load();
        save = new SaveObject();
        //loadSounds();
        setScreen(new GameScreen(this,0));
        if(isDesktop){
            config = new ConfigObject();
            config.load();
            config.config.xDimension = Gdx.graphics.getWidth();
            config.config.yDimension = Gdx.graphics.getHeight();
            config.save();
        }
    }

    public void gameScreen(int map){
        setScreen(new GameScreen(this, map));
    }
    public void menuScreen(){
        setScreen(new MenuScreen(this));
    }


    public void setPrevious(Screen s){
        this.previous = s;
        this.previousInput = Gdx.input.getInputProcessor();
    }
    public void loadSounds(){
        assetManager.load("sounds/select.mp3", Sound.class);
        assetManager.load("sounds/back.mp3", Sound.class);
        assetManager.load("sounds/validate.mp3", Sound.class);
        assetManager.load("sounds/mumble.mp3", Sound.class);
        assetManager.load("sounds/ding.mp3", Sound.class);
        assetManager.load("sounds/train.mp3", Sound.class);
        assetManager.load("sounds/button.mp3", Sound.class);
        assetManager.load("sounds/success.mp3", Sound.class);
        assetManager.load("sounds/door.mp3", Sound.class);
        assetManager.finishLoading(); //Important!
        select = assetManager.get("sounds/select.mp3");
        back = assetManager.get("sounds/back.mp3");
        validate = assetManager.get("sounds/validate.mp3");
        mumble = assetManager.get("sounds/mumble.mp3");
        ding = assetManager.get("sounds/ding.mp3");
        train = assetManager.get("sounds/train.mp3");
        button = assetManager.get("sounds/button.mp3");
        success = assetManager.get("sounds/success.mp3");
        door = assetManager.get("sounds/door.mp3");
    }
    public void resetMusic(){
        musicManager.dispose();
        musicManager = new AssetManager();
    }
    @Override
    public void resize(int width,
                       int height){
        if(maxY != Gdx.graphics.getHeight() || maxX != Gdx.graphics.getWidth()) {
            int h = Gdx.graphics.getHeight();
            int w = Gdx.graphics.getWidth();
            int scale = h / 224;
            scale = Math.max(scale,3);
            if(scale > 8){
                scale = 8;
            }
            float ratio = (float)h / (float)w;
            maxWidth = Math.round(w / scale);
            maxHeight = Math.round(h / scale);
            if(maxWidth < 128){
                scale = w / 128;
                maxWidth = w / scale;
                maxHeight = h / scale;
            }
            if(ratio < .5f && !isDesktop){
                isIphoneX = true;
                iphoneXLeft = 16;
                iphoneXRight = 16;
            }
            else{
                isIphoneX = false;
                iphoneXLeft = 0;
                iphoneXRight = 0;
            }
            /*
            if (ratio > 128.0f / 224.0f) {
                maxWidth = 224;
                maxHeight = Math.round( maxWidth * ratio);
            } else {
                maxHeight = 128;
                maxWidth = Math.round((1 / ratio) * maxHeight);
            }*/
            screen.resize((int) maxWidth, (int) maxHeight);
            maxX = Gdx.graphics.getWidth();
            maxY = Gdx.graphics.getHeight();

            if(isDesktop){
                config.load();
                config.config.xDimension = Gdx.graphics.getWidth();
                config.config.yDimension = Gdx.graphics.getHeight();
                config.save();
            }
        }
    }
    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }

    public static void playSound(int id){
        if(save.isMusic()) {
            switch (id) {
                case 0:
                    mumble.play(save.master.volume);
                    break;
                case 1:
                    ding.play(save.master.volume);
                    break;
                case 2:
                    success.play(save.master.volume);
                    break;
                case 3:
                    train.play(save.master.volume);
                    break;
                case 4:
                    button.play(save.master.volume);
                    break;
                case 5:
                    validate.play(save.master.volume);
                    break;
                case 6:
                    back.play(save.master.volume);
                    break;
                case 7:
                    select.play(save.master.volume);
                    break;
                case 8:
                    door.play(save.master.volume);
                    break;
            }
        }

    }
    public static void stopSound(int id){
            switch (id) {
                case 0:
                    mumble.stop();
                    break;
                case 1:
                    ding.stop();
                    break;
                case 2:
                    success.stop();
                    break;
                case 3:
                    train.stop();
                    break;
                case 4:
                    button.stop();
                    break;
                case 5:
                    validate.stop();
                    break;
                case 6:
                    back.stop();
                    break;
                case 7:
                    select.stop();
                    break;
                case 8:
                    door.stop();
                    break;
            }
    }

    public void playMusic(int id) {
        if(save.isMusic()) {
            if (musicId != id) {
                if (music != null) {
                    music.pause();
                }
                musicCount++;
                if(musicCount > 5){
                    resetMusic();
                    musicCount = 0;
                }
                switch (id) {
                    case 0:
                        if (!musicManager.isLoaded("music/worldMap.mp3")) {
                            musicManager.load("music/worldMap.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        title = musicManager.get("music/worldMap.mp3");
                        title.setLooping(true);
                        title.setVolume(save.master.musicVolume);
                        music = title;
                        break;
                    case 1:
                        if (!musicManager.isLoaded("music/calmingDungeons.mp3")) {
                            musicManager.load("music/calmingDungeons.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/calmingDungeons.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 2:
                        if (!musicManager.isLoaded("music/journeysEnd.mp3")) {
                            musicManager.load("music/journeysEnd.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        battle = musicManager.get("music/journeysEnd.mp3");
                        battle.setLooping(true);
                        battle.setVolume(save.master.musicVolume);
                        battle.setPosition(0);
                        music = battle;
                        break;
                    case 3:
                        if (!musicManager.isLoaded("music/treacherousSlopes.mp3")) {
                            musicManager.load("music/treacherousSlopes.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/treacherousSlopes.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 4:
                        if (!musicManager.isLoaded("music/teaGarden.mp3")) {
                            musicManager.load("music/teaGarden.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/teaGarden.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 5:
                        if (!musicManager.isLoaded("music/myEnemy.mp3")) {
                            musicManager.load("music/myEnemy.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/myEnemy.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 6:
                        if (!musicManager.isLoaded("music/bloodlust.mp3")) {
                            musicManager.load("music/bloodlust.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        battle = musicManager.get("music/bloodlust.mp3");
                        battle.setLooping(true);
                        battle.setVolume(save.master.musicVolume);
                        battle.setPosition(0);
                        music = battle;
                        break;
                    case 7:
                        if (!musicManager.isLoaded("music/giftToLast.mp3")) {
                            musicManager.load("music/giftToLast.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/giftToLast.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 8:
                        if (!musicManager.isLoaded("music/brother.mp3")) {
                            musicManager.load("music/brother.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/brother.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 9:
                        if (!musicManager.isLoaded("music/adventure.mp3")) {
                            musicManager.load("music/adventure.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/adventure.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 10:
                        if (!musicManager.isLoaded("music/doxent.mp3")) {
                            musicManager.load("music/doxent.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/doxent.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 11:
                        if (!musicManager.isLoaded("music/undercover_dungeon.mp3")) {
                            musicManager.load("music/undercover_dungeon.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/undercover_dungeon.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 12:
                        if (!musicManager.isLoaded("music/undercover_boss.mp3")) {
                            musicManager.load("music/undercover_boss.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/undercover_boss.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 13:
                        if (!musicManager.isLoaded("music/rockSpace.mp3")) {
                            musicManager.load("music/rockSpace.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/rockSpace.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 14:
                        if (!musicManager.isLoaded("music/strongestDesire.mp3")) {
                            musicManager.load("music/strongestDesire.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/strongestDesire.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 15:
                        if (!musicManager.isLoaded("music/backpedal.mp3")) {
                            musicManager.load("music/backpedal.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/backpedal.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 16:
                        if (!musicManager.isLoaded("music/collapse1.mp3")) {
                            musicManager.load("music/collapse1.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/collapse1.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 17:
                        if (!musicManager.isLoaded("music/carmel.mp3")) {
                            musicManager.load("music/carmel.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/carmel.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 18:
                        if (!musicManager.isLoaded("music/empire.mp3")) {
                            musicManager.load("music/empire.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/empire.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 19:
                        if (!musicManager.isLoaded("music/collapse2.mp3")) {
                            musicManager.load("music/collapse2.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/collapse2.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 20:
                        if (!musicManager.isLoaded("music/amigos.mp3")) {
                            musicManager.load("music/amigos.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/amigos.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 21:
                        if (!musicManager.isLoaded("music/banter.mp3")) {
                            musicManager.load("music/banter.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/banter.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        map.setPosition(0);
                        music = map;
                        break;
                    case 23:
                        if (!musicManager.isLoaded("music/drive.mp3")) {
                            musicManager.load("music/drive.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/drive.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        map.setPosition(0);
                        music = map;
                        break;
                    case 24:
                        if (!musicManager.isLoaded("music/epicBattle.mp3")) {
                            musicManager.load("music/epicBattle.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/epicBattle.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 25:
                        if (!musicManager.isLoaded("music/happyPlace.mp3")) {
                            musicManager.load("music/happyPlace.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/happyPlace.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 26:
                        if (!musicManager.isLoaded("music/kaddish.mp3")) {
                            musicManager.load("music/kaddish.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/kaddish.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 27:
                        if (!musicManager.isLoaded("music/orykTaiko.mp3")) {
                            musicManager.load("music/orykTaiko.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/orykTaiko.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 28:
                        if (!musicManager.isLoaded("music/phoenix.mp3")) {
                            musicManager.load("music/phoenix.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/phoenix.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 29:
                        if (!musicManager.isLoaded("music/shiro.mp3")) {
                            musicManager.load("music/shiro.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/shiro.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 30:
                        if (!musicManager.isLoaded("music/twoSwords.mp3")) {
                            musicManager.load("music/twoSwords.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/twoSwords.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 31:
                        if (!musicManager.isLoaded("music/videoGameSoldiers.mp3")) {
                            musicManager.load("music/videoGameSoldiers.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/videoGameSoldiers.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 32:
                        if (!musicManager.isLoaded("music/forestDrama.mp3")) {
                            musicManager.load("music/forestDrama.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/forestDrama.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 33:
                        if (!musicManager.isLoaded("music/magicClockShop.mp3")) {
                            musicManager.load("music/magicClockShop.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/magicClockShop.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 34:
                        if (!musicManager.isLoaded("music/ourMountain.mp3")) {
                            musicManager.load("music/ourMountain.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/ourMountain.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 35:
                        if (!musicManager.isLoaded("music/pirates.mp3")) {
                            musicManager.load("music/pirates.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/pirates.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 36:
                        if (!musicManager.isLoaded("music/towerDefense.mp3")) {
                            musicManager.load("music/towerDefense.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/towerDefense.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 37:
                        if (!musicManager.isLoaded("music/crusadersApproaching.mp3")) {
                            musicManager.load("music/crusadersApproaching.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/crusadersApproaching.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                    case 38:
                        if (!musicManager.isLoaded("music/comradesAlways.mp3")) {
                            musicManager.load("music/comradesAlways.mp3", Music.class);
                            musicManager.finishLoading(); //Important!
                        }
                        map = musicManager.get("music/comradesAlways.mp3");
                        map.setLooping(true);
                        map.setVolume(save.master.musicVolume);
                        music = map;
                        break;
                }
                if (music != null && save.isMusic() && !music.isPlaying()) {
                    music.setVolume(music.getVolume() * (save.master.musicVolume * 2));
                    music.play();
                }
                musicId = id;
            } else {
                if (music != null && !music.isPlaying()) {
                    music.setVolume(music.getVolume() * (save.master.musicVolume * 2));
                    music.play();
                }
            }
        }
    }
}
