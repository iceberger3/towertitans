package gservices;

/**
 * Created by kyle.berger on 4/12/16.
 */
public class DesktopGServices implements GServices
{
    SteamClient steamClient = new SteamClient();
    @Override
    public void signIn()
    {
        System.out.println("DesktopGServies: signIn()");
        steamClient.initAndConnect();
    }

    @Override
    public void signOut()
    {
        System.out.println("DesktopGServies: signOut()");
    }

    @Override
    public void rateGame()
    {
        System.out.println("DesktopGServices: rateGame()");
    }

    @Override
    public void submitScore(long score,int id)
    {
        System.out.println("DesktopGServies: submitScore(" + score + ") for " +id);
    }

    @Override
    public void unlockAchievement(int id) {
        System.out.println("Im unlocking achievement: " + id);
        String achievement = "";
        switch (id){
            case 1 :
                achievement = "rangerTrial";
                break;
            case 2 :
                achievement = "level10";
                break;
            case 3 :
                achievement = "earthCrystal";
                break;
            case 4 :
                achievement = "protect";
                break;
            case 5 :
                achievement = "mercenary";
                break;
            case 6 :
                achievement = "10heroes";
                break;
            case 7 :
                achievement = "critical";
                break;
            case 8 :
                achievement = "level20";
                break;
            case 9 :
                achievement = "shadowCrystal";
                break;
            case 10 :
                achievement = "sewers";
                break;
            case 11 :
                achievement = "heal";
                break;
            case 12 :
                achievement = "items";
                break;
            case 13 :
                achievement = "monsters";
                break;
            case 14 :
                achievement = "unique";
                break;
            case 15 :
                achievement = "level30";
                break;
            case 16 :
                achievement = "sage";
                break;
            case 17 :
                achievement = "myria";
                break;
            case 18 :
                achievement = "gold";
                break;
            case 19 :
                achievement = "level40";
                break;
            case 20 :
                achievement = "tower";
                break;
            case 21 :
                achievement = "level50";
                break;
            case 22 :
                achievement = "level60";
                break;
            case 23 :
                achievement = "level70";
                break;
            case 24 :
                achievement = "level80";
                break;
            case 25 :
                achievement = "level90";
                break;
            case 26 :
                achievement = "level100";
                break;
            case 27 :
                achievement = "waterCrystal";
                break;
            case 28 :
                achievement = "stormCrystal";
                break;
            case 29 :
                achievement = "upgradeMax";
                break;
            case 30 :
                achievement = "upgrade";
                break;
            case 31 :
                achievement = "orgo";
                break;
            case 32 :
                achievement = "magmor";
                break;
            case 33 :
                achievement = "damage";
                break;
            case 34 :
                achievement = "arena";
                break;
            case 35 :
                achievement = "promote";
                break;
            case 36 :
                achievement = "prisma";
                break;
            case 37 :
                achievement = "bosses";
                break;
            case 38 :
                achievement = "quest";
                break;
            case 39 :
                achievement = "yeti";
                break;
            case 40 :
                achievement = "lightCrystal";
                break;
            case 41 :
                achievement = "grotto";
                break;
            case 42 :
                achievement = "fireCrystal";
                break;
            case 43 :
                achievement = "darkLord";
                break;
            case 44 :
                achievement = "labyrinth";
                break;
            case 45 :
                achievement = "legendary";
                break;
            case 46 :
                achievement = "prisma2";
                break;
        }
        steamClient.setAchiev(achievement);
    }

    @Override
    public void showScores(int id)
    {
        System.out.println("DesktopGServies: showScores() for " +id);
    }

    @Override
    public boolean showAchievements() {
        System.out.println("Showing Achievements");
        return false;
    }

    @Override
    public boolean isSignedIn()
    {
        System.out.println("DesktopGServies: isSignedIn()");
        return steamClient.isOnline;
    }

}
