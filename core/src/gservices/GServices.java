package gservices;

/**
 * Created by kyle.berger on 4/12/16.
 */
public interface GServices
{
    public void signIn();
    public void signOut();
    public void rateGame();
    public void submitScore(long score,int id);
    public void unlockAchievement(int id);
    public void showScores(int id);
    public boolean showAchievements();
    public boolean isSignedIn();
}
