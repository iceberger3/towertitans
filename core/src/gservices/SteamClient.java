package gservices;
import com.codedisaster.steamworks.SteamAPI;
import com.codedisaster.steamworks.SteamException;
import com.codedisaster.steamworks.SteamID;
import com.codedisaster.steamworks.SteamLeaderboardEntriesHandle;
import com.codedisaster.steamworks.SteamLeaderboardHandle;
import com.codedisaster.steamworks.SteamResult;
import com.codedisaster.steamworks.SteamUserStats;
import com.codedisaster.steamworks.SteamUserStatsCallback;
import com.codedisaster.steamworks.SteamUtils;
public class SteamClient {
    private SteamUtils utils;
    private SteamUserStats userStats;
    public boolean isOnline=false;
    static public String rangerTrialSteamId= "rangerTrial";
    SteamUserStatsCallback steamUserStatsCallback=new SteamUserStatsCallback()
    {

        @Override
        public void onUserStatsReceived(long gameId, SteamID steamIDUser,
                                        SteamResult result)
        {
        }

        @Override
        public void onUserStatsStored(long gameId, SteamResult result) {
        }

        @Override
        public void onUserStatsUnloaded(SteamID steamIDUser) {

        }

        @Override
        public void onUserAchievementStored(long gameId,
                                            boolean isGroupAchievement, String achievementName,
                                            int curProgress, int maxProgress) {
        }

        @Override
        public void onLeaderboardFindResult(SteamLeaderboardHandle leaderboard,
                                            boolean found) {
        }

        @Override
        public void onLeaderboardScoresDownloaded(
                SteamLeaderboardHandle leaderboard,
                SteamLeaderboardEntriesHandle entries, int numEntries) {
        }

        @Override
        public void onLeaderboardScoreUploaded(boolean success,
                                               SteamLeaderboardHandle leaderboard, int score,
                                               boolean scoreChanged, int globalRankNew, int globalRankPrevious) {
        }

        @Override
        public void onGlobalStatsReceived(long gameId, SteamResult result) {
        }

    };
    public boolean initAndConnect(){
        try {
            SteamAPI.loadLibraries("./steam");

            try {
                if (!SteamAPI.init())
                {
                    System.out.println("Initialisation failed");
                    isOnline=false;
                    return false ;
                }
                //Get stat object
                userStats = new SteamUserStats( steamUserStatsCallback);
                //A must before setting achievements
                userStats.requestCurrentStats();
                isOnline=true;
                return true;
            } catch (SteamException e)
            {
                e.printStackTrace();
            }
        }
        catch (SteamException e1)
        {
            System.out.println("Load libraries error");
        }
        return false;
    };
    public boolean setAchiev(String achivName) {
        try {
            //set
            userStats.setAchievement(achivName);
            //save
            boolean result=userStats.storeStats();

            return result;
        }
        catch ( Exception e) {
            isOnline=false;
            return false;
        }
    };
    public void disconnect(){
        SteamAPI.shutdown();
    };
}
