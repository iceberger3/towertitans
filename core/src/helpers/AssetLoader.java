package helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetLoader {
    public static boolean isLoaded = false;
    public static Texture menu, sprites, details, floorTiles, backgrounds, battle;


    public static TextureRegion army,armySelect,item,itemSelect,tower,towerSelect,shop,shopSelect,menuIcon,menuSelect, ladder, backButton, rightArrow,
    arrowUp,arrowDown,dmgIcon,tankIcon,purpleTankIcon,goldTankIcon,healIcon, goldIcon,gemIcon,goldGemIcon,levelUpIcon,move,moveSelect,trait,traitSelect,
    closeButton;

    public static TextureRegion paladin1,paladin2,paladin3,paladin4,paladin5,paladin6,paladin7,paladin8,paladinPortrait,slime1,slime2,slime3,
    magus1,magus2,magus3,magus4,magus5,magus6,magus7,magus8,magusPortrait,ranger1,ranger2,ranger3,ranger4,ranger5,ranger6,ranger7,ranger8,rangerPortrait,
    reaper1,reaper2,reaper3,reaper4,reaper5,reaper6,reaper7,reaper8,reaperPortrait;

    public static TextureRegion caveFloor;

    public static TextureRegion caveWall;

    public static Animation<TextureRegion> paladinAnimation, paladinBattleAnimation, paladinFlipAnimation,slimeAnimation,
    magusAnimation, magusBattleAnimation,magusFlipAnimation,rangerAnimation,rangerBattleAnimation,rangerFlipAnimation,
    reaperAnimation,reaperBattleAnimation,reaperFlipAnimation;

    public static TextureRegion blank,fire1,fire2,fire3,ice1,ice2,ice3,shock1,shock2,shock3,slash1,slash2,
            slash3,smack1,smack2,smack3,light1,light2,light3,dark1,dark2,dark3,up1,up2,up3,down1,down2,down3,
            heal1,heal2,heal3,water1,water2,water3,earth1,earth2,earth3,poison1,poison2,poison3,poison4,
            ghost1,ghost2,ghost3,ghost4,shield1,shield2,shield3,stun1,stun2,burn1,burn2,burn3,burn4,shadowUp,
            shadowAngle, shadow1, shadow2,hot1,hot2,hot3,hot4,double1,double2,double3,double4,revive1,revive2,revive3,
            revive4,revive5,revive6,revive7,snipe1,snipe2,snipe3,slashWave1,slashWave2,slashWave3,slashWave4,
            slashWave5,slashWave6,redDouble1,redDouble2,redDouble3,redDouble4,darkSlash1,darkSlash2,darkSlash3,darkSlash4,
            swordSlash1,swordSlash2,swordSlash3,swordSlash4,bomb1,bomb2,bomb3,bomb4,bomb5,bomb6, punch1, punch2, punch3,
            punch4, punch5,pierce1,pierce2,pierce3,pierce4,pierce5,bite1,bite2,bite3,bite4, sunder1, sunder2, sunder3,
            flurry1, flurry2, flurry3, flurry4,flurry5,darkFire1,darkFire2,darkFire3,powerUp1,powerUp2,powerUp3,powerUp4,
            powerUp5,powerUp6,powerUp7,healthUp1,healthUp2,healthUp3,healthUp4,sol1,sol2,sol3,sol4,sol5,sol6,sol7,sol8,
            luna1, luna2, luna3, luna4, luna5, luna6, luna7, luna8,sleet1,sleet2,sleet3,sleet4,sleet5,sleet6,
            healthUp5,healthUp6,healthUp7,agilityUp1,agilityUp2,agilityUp3,agilityUp4,reap1,reap2,reap3,reap4,reap5,reap6,reap7,
            agilityUp5,agilityUp6,agilityUp7,speedUp1,speedUp2,speedUp3,speedUp4,storm1,storm2,storm3,storm4,storm5,storm6,
            speedUp5,speedUp6,speedUp7,defenseUp1,defenseUp2,defenseUp3,defenseUp4,haunt1,haunt2,haunt3,haunt4,haunt5,haunt6,haunt7,
            defenseUp5,defenseUp6,defenseUp7,resistanceUp1,resistanceUp2,resistanceUp3,resistanceUp4,
            resistanceUp5,resistanceUp6,resistanceUp7,fireSlash1,fireSlash2,fireSlash3,fireSlash4,judgment1,judgment2,judgment3,judgment4,judgment5,
            hammer1,hammer2,hammer3,hammer4,hammer5,greenArrow31,greenArrow32,greenArrow33,greenArrow34,greenArrow35,
            greenArrow36,greenArrow37,greenArrow38,greenArrow39,barrage1,barrage2,barrage3,barrage4,barrage5,barrage6,darkfire1,darkfire2,darkfire3,
            darkfire4,darkfire5,darkfire6;
    public static Animation<TextureRegion> fireAnimation,iceAnimation,shockAnimation,slashAnimation,smackAnimation,
            lightAnimation,darkAnimation,upAnimation,downAnimation,healAnimation,waterAnimation,earthAnimation,
            poisonAnimation,ghostAnimation,shieldAnimation,stunAnimation,burnAnimation,shadowAnimation,hotAnimation,
            doubleAnimation,reviveAnimation,snipeAnimation,slashWaveAnimation,redDoubleAnimation,darkSlashAnimation,
            swordSlashAnimation,bombAnimation, punchAnimation,pierceAnimation,biteAnimation, sunderAnimation, flurryAnimation,
            darkFireAnimation,powerUpAnimation,healthUpAnimation,agilityUpAnimation,speedUpAnimation,defenseUpAnimation,
            resistanceUpAnimation,hammerAnimation,judgmentAnimation,fireSlashAnimation,greenArrow3Animation,
            barrageAnimation,darkfireAnimation,solAnimation, lunaAnimation,sleetAnimation,stormAnimation,hauntAnimation,
            reapAnimation;

    public static void load() {
        if(!isLoaded) {

            loadMenu();

            loadSprites();

            loadDetails();

            loadFloorTiles();

            loadBackgrounds();

            loadBattle();

            isLoaded = true;
        }
    }


    public static void loadMenu(){
        //load file
        menu = new Texture(Gdx.files.internal("menu.png"));
        menu.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        //load images
        army = new TextureRegion(menu, 1, 1, 26, 26);
        army.flip(false, true);

        armySelect = new TextureRegion(menu, 1, 29, 26, 26);
        armySelect.flip(false, true);

        item = new TextureRegion(menu, 29, 1, 26, 26);
        item.flip(false, true);

        itemSelect = new TextureRegion(menu, 29, 29, 26, 26);
        itemSelect.flip(false, true);

        tower = new TextureRegion(menu, 57, 1, 26, 26);
        tower.flip(false, true);

        towerSelect = new TextureRegion(menu, 57, 29, 26, 26);
        towerSelect.flip(false, true);

        shop = new TextureRegion(menu, 85, 1, 26, 26);
        shop.flip(false, true);

        shopSelect = new TextureRegion(menu, 85, 29, 26, 26);
        shopSelect.flip(false, true);

        menuIcon = new TextureRegion(menu, 113, 1, 26, 26);
        menuIcon.flip(false, true);

        menuSelect = new TextureRegion(menu, 113, 29, 26, 26);
        menuSelect.flip(false, true);

        ladder = new TextureRegion(menu, 142, 0, 9, 39);
        ladder.flip(false, true);

        dmgIcon = new TextureRegion(menu, 0, 58, 8, 8);
        dmgIcon.flip(false, true);

        tankIcon = new TextureRegion(menu, 10, 58, 8, 8);
        tankIcon.flip(false, true);

        purpleTankIcon = new TextureRegion(menu, 20, 58, 8, 8);
        purpleTankIcon.flip(false, true);

        goldTankIcon = new TextureRegion(menu, 30, 58, 8, 8);
        goldTankIcon.flip(false, true);

        healIcon = new TextureRegion(menu, 0, 68, 8, 8);
        healIcon.flip(false, true);

        arrowUp = new TextureRegion(menu, 40, 60, 7, 6);
        arrowUp.flip(false, true);

        arrowDown = new TextureRegion(menu, 49, 60, 7, 6);
        arrowDown.flip(false, true);

        goldIcon = new TextureRegion(menu, 58, 58, 8, 8);
        goldIcon.flip(false, true);

        gemIcon = new TextureRegion(menu, 68, 58, 8, 8);
        gemIcon.flip(false, true);

        goldGemIcon = new TextureRegion(menu, 78, 58, 8, 8);
        goldGemIcon.flip(false, true);

        levelUpIcon = new TextureRegion(menu, 88, 58, 9, 9);
        levelUpIcon.flip(false, true);

        move = new TextureRegion(menu, 154, 1, 26, 26);
        move.flip(false, true);

        moveSelect = new TextureRegion(menu, 154, 29, 26, 26);
        moveSelect.flip(false, true);

        trait = new TextureRegion(menu, 182, 1, 26, 26);
        trait.flip(false, true);

        traitSelect = new TextureRegion(menu, 182, 29, 26, 26);
        traitSelect.flip(false, true);

        backButton = new TextureRegion(menu, 210, 1, 50, 26);
        backButton.flip(false, true);

        rightArrow = new TextureRegion(menu, 210, 29, 32, 24);
        rightArrow.flip(false, true);

        closeButton = new TextureRegion(menu, 262, 1, 64, 24);
        closeButton.flip(false, true);


    }

    public static void loadSprites(){
        //load file
        sprites = new Texture(Gdx.files.internal("sprites.png"));
        sprites.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        paladin1 = new TextureRegion(sprites, 0, 0, 16, 16);
        paladin1.flip(false, true);

        paladin2 = new TextureRegion(sprites, 18, 0, 16, 16);
        paladin2.flip(false, true);

        paladin3 = new TextureRegion(sprites, 36, 0, 16, 16);
        paladin3.flip(false, true);

        TextureRegion[] paladin = { paladin1, paladin2, paladin2, paladin1, paladin3, paladin3 };
        paladinAnimation = new Animation<TextureRegion>(0.07f, paladin);
        paladinAnimation.setPlayMode(Animation.PlayMode.LOOP);

        paladin4 = new TextureRegion(sprites, 54, 0, 16, 16);
        paladin4.flip(false, true);

        paladin5 = new TextureRegion(sprites, 72, 0, 16, 16);
        paladin5.flip(false, true);

        TextureRegion[] paladinBattle = { paladin1, paladin4,  paladin5, paladin4, paladin1 };
        paladinBattleAnimation = new Animation<TextureRegion>(0.2f, paladinBattle);
        paladinBattleAnimation.setPlayMode(Animation.PlayMode.LOOP);

        paladin6 = new TextureRegion(sprites, 0, 18, 16, 16);
        paladin6.flip(false, true);

        paladin7 = new TextureRegion(sprites, 18, 18, 16, 16);
        paladin7.flip(false, true);

        paladin8 = new TextureRegion(sprites, 36, 18, 16, 16);
        paladin8.flip(false, true);

        TextureRegion[] paladinFlip = { paladin6, paladin7, paladin7, paladin6, paladin8, paladin8 };
        paladinFlipAnimation = new Animation<TextureRegion>(0.07f, paladinFlip);
        paladinFlipAnimation.setPlayMode(Animation.PlayMode.LOOP);


        slime1 = new TextureRegion(sprites, 180, 0, 16, 16);
        slime1.flip(false, true);

        slime2 = new TextureRegion(sprites, 198, 0, 16, 16);
        slime2.flip(false, true);

        slime3 = new TextureRegion(sprites, 216, 0, 16, 16);
        slime3.flip(false, true);

        TextureRegion[] slime = { slime1, slime2, slime3, slime2 };
        slimeAnimation = new Animation<TextureRegion>(.25f, slime);
        slimeAnimation.setPlayMode(Animation.PlayMode.LOOP);
        
        magus1 = new TextureRegion(sprites, 0, 72, 16, 16);
        magus1.flip(false, true);

        magus2 = new TextureRegion(sprites, 18, 72, 16, 16);
        magus2.flip(false, true);

        magus3 = new TextureRegion(sprites, 36, 72, 16, 16);
        magus3.flip(false, true);

        TextureRegion[] magus = { magus1, magus2, magus2, magus1, magus3, magus3 };
        magusAnimation = new Animation<TextureRegion>(0.07f, magus);
        magusAnimation.setPlayMode(Animation.PlayMode.LOOP);

        magus4 = new TextureRegion(sprites, 54, 72, 16, 16);
        magus4.flip(false, true);

        magus5 = new TextureRegion(sprites, 72, 72, 16, 16);
        magus5.flip(false, true);

        TextureRegion[] magusBattle = { magus1, magus4,  magus5, magus4, magus1 };
        magusBattleAnimation = new Animation<TextureRegion>(0.2f, magusBattle);
        magusBattleAnimation.setPlayMode(Animation.PlayMode.LOOP);

        magus6 = new TextureRegion(sprites, 0, 90, 16, 16);
        magus6.flip(false, true);

        magus7 = new TextureRegion(sprites, 18, 90, 16, 16);
        magus7.flip(false, true);

        magus8 = new TextureRegion(sprites, 36, 90, 16, 16);
        magus8.flip(false, true);

        TextureRegion[] magusFlip = { magus6, magus7, magus7, magus6, magus8, magus8 };
        magusFlipAnimation = new Animation<TextureRegion>(0.07f, magusFlip);
        magusFlipAnimation.setPlayMode(Animation.PlayMode.LOOP);
        
        ranger1 = new TextureRegion(sprites, 0, 54, 16, 16);
        ranger1.flip(false, true);

        ranger2 = new TextureRegion(sprites, 18, 54, 16, 16);
        ranger2.flip(false, true);

        ranger3 = new TextureRegion(sprites, 36, 54, 16, 16);
        ranger3.flip(false, true);

        TextureRegion[] ranger = { ranger1, ranger2, ranger2, ranger1, ranger3, ranger3 };
        rangerAnimation = new Animation<TextureRegion>(0.07f, ranger);
        rangerAnimation.setPlayMode(Animation.PlayMode.LOOP);

        ranger4 = new TextureRegion(sprites, 54, 54, 16, 16);
        ranger4.flip(false, true);

        ranger5 = new TextureRegion(sprites, 72, 54, 16, 16);
        ranger5.flip(false, true);

        TextureRegion[] rangerBattle = { ranger1, ranger4,  ranger5, ranger4, ranger1 };
        rangerBattleAnimation = new Animation<TextureRegion>(0.2f, rangerBattle);
        rangerBattleAnimation.setPlayMode(Animation.PlayMode.LOOP);

        ranger6 = new TextureRegion(sprites, 108, 18, 16, 16);
        ranger6.flip(false, true);

        ranger7 = new TextureRegion(sprites, 126, 18, 16, 16);
        ranger7.flip(false, true);

        ranger8 = new TextureRegion(sprites, 144, 18, 16, 16);
        ranger8.flip(false, true);

        TextureRegion[] rangerFlip = { ranger6, ranger7, ranger7, ranger6, ranger8, ranger8 };
        rangerFlipAnimation = new Animation<TextureRegion>(0.07f, rangerFlip);
        rangerFlipAnimation.setPlayMode(Animation.PlayMode.LOOP);
        
        reaper1 = new TextureRegion(sprites, 0, 36, 16, 16);
        reaper1.flip(false, true);

        reaper2 = new TextureRegion(sprites, 18, 36, 16, 16);
        reaper2.flip(false, true);

        reaper3 = new TextureRegion(sprites, 36, 36, 16, 16);
        reaper3.flip(false, true);

        TextureRegion[] reaper = { reaper1, reaper2, reaper2, reaper1, reaper3, reaper3 };
        reaperAnimation = new Animation<TextureRegion>(0.07f, reaper);
        reaperAnimation.setPlayMode(Animation.PlayMode.LOOP);

        reaper4 = new TextureRegion(sprites, 54, 36, 16, 16);
        reaper4.flip(false, true);

        reaper5 = new TextureRegion(sprites, 72, 36, 16, 16);
        reaper5.flip(false, true);

        TextureRegion[] reaperBattle = { reaper1, reaper4,  reaper5, reaper4, reaper1 };
        reaperBattleAnimation = new Animation<TextureRegion>(0.2f, reaperBattle);
        reaperBattleAnimation.setPlayMode(Animation.PlayMode.LOOP);

        reaper6 = new TextureRegion(sprites, 54, 18, 16, 16);
        reaper6.flip(false, true);

        reaper7 = new TextureRegion(sprites, 72, 18, 16, 16);
        reaper7.flip(false, true);

        reaper8 = new TextureRegion(sprites, 90, 18, 16, 16);
        reaper8.flip(false, true);

        TextureRegion[] reaperFlip = { reaper6, reaper7, reaper7, reaper6, reaper8, reaper8 };
        reaperFlipAnimation = new Animation<TextureRegion>(0.07f, reaperFlip);
        reaperFlipAnimation.setPlayMode(Animation.PlayMode.LOOP);

    }
    public static void loadDetails() {
        //load file
        details = new Texture(Gdx.files.internal("spriteDetails.png"));
        details.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        //load images
        paladinPortrait = new TextureRegion(details, 0, 38, 26, 26);
        paladinPortrait.flip(false, true);

        magusPortrait = new TextureRegion(details, 32, 38, 26, 26);
        magusPortrait.flip(false, true);

        rangerPortrait = new TextureRegion(details, 64, 38, 26, 26);
        rangerPortrait.flip(false, true);

        reaperPortrait = new TextureRegion(details, 96, 38, 26, 26);
        reaperPortrait.flip(false, true);
    }
    public static void loadFloorTiles() {
        //load file
        floorTiles = new Texture(Gdx.files.internal("floorTiles.png"));
        floorTiles.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        //load images
        caveFloor = new TextureRegion(floorTiles, 1, 1, 16, 8);
        caveFloor.flip(false, true);
    }
    public static void loadBackgrounds() {
        //load file
        backgrounds = new Texture(Gdx.files.internal("backgrounds.png"));
        backgrounds.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        //load images
        caveWall = new TextureRegion(backgrounds, 1, 1, 128, 32);
        caveWall.flip(false, true);
    }

    public static void loadBattle(){
        //load file
        battle = new Texture(Gdx.files.internal("battle.png"));
        battle.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        blank = new TextureRegion(battle, 90, 108, 16, 16);
        blank.flip(false, true);

        ice1 = new TextureRegion(battle, 0, 0, 16, 16);
        ice1.flip(false, true);

        ice2 = new TextureRegion(battle, 18, 0, 16, 16);
        ice2.flip(false, true);

        ice3 = new TextureRegion(battle, 36, 0, 16, 16);
        ice3.flip(false, true);

        TextureRegion[] ice = { ice1, ice2, ice3};
        iceAnimation = new Animation<TextureRegion>(.166f, ice);
        iceAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        double1 = new TextureRegion(battle, 180, 0, 16, 16);
        double1.flip(false, true);

        double2 = new TextureRegion(battle, 198, 0, 16, 16);
        double2.flip(false, true);

        double3 = new TextureRegion(battle, 216, 0, 16, 16);
        double3.flip(false, true);

        double4 = new TextureRegion(battle, 234, 0, 16, 16);
        double4.flip(false, true);

        TextureRegion[] doubleAnim = { double1, double2, double3, double4};
        doubleAnimation = new Animation<TextureRegion>(.125f, doubleAnim);
        doubleAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        swordSlash1 = new TextureRegion(battle, 180, 18, 16, 16);
        swordSlash1.flip(false, true);

        swordSlash2 = new TextureRegion(battle, 198, 18, 16, 16);
        swordSlash2.flip(false, true);

        swordSlash3 = new TextureRegion(battle, 216, 18, 16, 16);
        swordSlash3.flip(false, true);

        swordSlash4 = new TextureRegion(battle, 234, 18, 16, 16);
        swordSlash4.flip(false, true);

        TextureRegion[] swordSlashAnim = { swordSlash1, swordSlash2, swordSlash2, swordSlash3, swordSlash3, swordSlash4};
        swordSlashAnimation = new Animation<TextureRegion>(.07f, swordSlashAnim);
        swordSlashAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        redDouble1 = new TextureRegion(battle, 180, 36, 16, 16);
        redDouble1.flip(false, true);

        redDouble2 = new TextureRegion(battle, 198, 36, 16, 16);
        redDouble2.flip(false, true);

        redDouble3 = new TextureRegion(battle, 216, 36, 16, 16);
        redDouble3.flip(false, true);

        redDouble4 = new TextureRegion(battle, 234, 36, 16, 16);
        redDouble4.flip(false, true);

        TextureRegion[] redDoubleAnim = { redDouble1, redDouble2, redDouble3, redDouble4};
        redDoubleAnimation = new Animation<TextureRegion>(.125f, redDoubleAnim);
        redDoubleAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        bite1 = new TextureRegion(battle, 180, 72, 16, 16);
        bite1.flip(false, true);

        bite2 = new TextureRegion(battle, 198, 72, 16, 16);
        bite2.flip(false, true);

        bite3 = new TextureRegion(battle, 216, 72, 16, 16);
        bite3.flip(false, true);

        bite4 = new TextureRegion(battle, 234, 72, 16, 16);
        bite4.flip(false, true);

        TextureRegion[] biteAnim = { bite1, bite4,bite4,bite4,bite4, bite3, bite2,bite1};
        biteAnimation = new Animation<TextureRegion>(.06f, biteAnim);
        biteAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        darkSlash1 = new TextureRegion(battle, 180, 54, 16, 16);
        darkSlash1.flip(false, true);

        darkSlash2 = new TextureRegion(battle, 198, 54, 16, 16);
        darkSlash2.flip(false, true);

        darkSlash3 = new TextureRegion(battle, 216, 54, 16, 16);
        darkSlash3.flip(false, true);

        darkSlash4 = new TextureRegion(battle, 234, 54, 16, 16);
        darkSlash4.flip(false, true);

        TextureRegion[] darkSlashAnim = { darkSlash1, darkSlash2, darkSlash2, darkSlash3, darkSlash3, darkSlash4};
        darkSlashAnimation = new Animation<TextureRegion>(.08f, darkSlashAnim);
        darkSlashAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        punch1 = new TextureRegion(battle, 0, 108, 16, 16);
        punch1.flip(false, true);

        punch2 = new TextureRegion(battle, 18, 108, 16, 16);
        punch2.flip(false, true);

        punch3 = new TextureRegion(battle, 36, 108, 16, 16);
        punch3.flip(false, true);

        punch4 = new TextureRegion(battle, 54, 108, 16, 16);
        punch4.flip(false, true);

        punch5 = new TextureRegion(battle, 72, 108, 16, 16);
        punch5.flip(false, true);

        TextureRegion[] punchAnim = { punch1, punch2, punch3, punch4, punch5};
        punchAnimation = new Animation<TextureRegion>(.1f, punchAnim);
        punchAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        pierce1 = new TextureRegion(battle, 126, 126, 24, 24);
        pierce1.flip(false, true);

        pierce2 = new TextureRegion(battle, 152, 126, 24, 24);
        pierce2.flip(false, true);

        pierce3 = new TextureRegion(battle, 178, 126, 24, 24);
        pierce3.flip(false, true);

        pierce4 = new TextureRegion(battle, 204, 126, 24, 24);
        pierce4.flip(false, true);

        pierce5 = new TextureRegion(battle, 230, 126, 24, 24);
        pierce5.flip(false, true);

        TextureRegion[] pierceAnim = { pierce1, pierce2, pierce3, pierce4, pierce5};
        pierceAnimation = new Animation<TextureRegion>(.1f, pierceAnim);
        pierceAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        bomb1 = new TextureRegion(battle, 0, 126, 16, 16);
        bomb1.flip(false, true);

        bomb2 = new TextureRegion(battle, 18, 126, 16, 16);
        bomb2.flip(false, true);

        bomb3 = new TextureRegion(battle, 36, 126, 16, 16);
        bomb3.flip(false, true);

        bomb4 = new TextureRegion(battle, 54, 126, 16, 16);
        bomb4.flip(false, true);

        bomb5 = new TextureRegion(battle, 72, 126, 16, 16);
        bomb5.flip(false, true);

        bomb6 = new TextureRegion(battle, 90, 126, 16, 16);
        bomb6.flip(false, true);

        TextureRegion[] bombAnim = { bomb1, bomb2, bomb3, bomb4, bomb5, bomb6};
        bombAnimation = new Animation<TextureRegion>(.08f, bombAnim);
        bombAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        fire1 = new TextureRegion(battle, 0, 18, 16, 16);
        fire1.flip(false, true);

        fire2 = new TextureRegion(battle, 18, 18, 16, 16);
        fire2.flip(false, true);

        fire3 = new TextureRegion(battle, 36, 18, 16, 16);
        fire3.flip(false, true);

        TextureRegion[] fire = { fire1, fire2, fire3};
        fireAnimation = new Animation<TextureRegion>(.166f, fire);
        fireAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        light1 = new TextureRegion(battle, 0, 36, 16, 16);
        light1.flip(false, true);

        light2 = new TextureRegion(battle, 18, 36, 16, 16);
        light2.flip(false, true);

        light3 = new TextureRegion(battle, 36, 36, 16, 16);
        light3.flip(false, true);

        TextureRegion[] light = { light1, light2, light3};
        lightAnimation = new Animation<TextureRegion>(.166f, light);
        lightAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        sunder1 = new TextureRegion(battle, 162, 108, 16, 16);
        sunder1.flip(false, true);

        sunder2 = new TextureRegion(battle, 180, 108, 16, 16);
        sunder2.flip(false, true);

        sunder3 = new TextureRegion(battle, 198, 108, 16, 16);
        sunder3.flip(false, true);

        TextureRegion[] sunder = { sunder1, sunder2, sunder3};
        sunderAnimation = new Animation<TextureRegion>(.166f, sunder);
        sunderAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        shock1 = new TextureRegion(battle, 0, 54, 16, 16);
        shock1.flip(false, true);

        shock2 = new TextureRegion(battle, 18, 54, 16, 16);
        shock2.flip(false, true);

        shock3 = new TextureRegion(battle, 36, 54, 16, 16);
        shock3.flip(false, true);

        TextureRegion[] shock = { shock1, shock2, shock3,shock3};
        shockAnimation = new Animation<TextureRegion>(.125f, shock);
        shockAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        slash1 = new TextureRegion(battle, 0, 72, 16, 16);
        slash1.flip(false, true);

        slash2 = new TextureRegion(battle, 18, 72, 16, 16);
        slash2.flip(false, true);

        slash3 = new TextureRegion(battle, 36, 72, 16, 16);
        slash3.flip(false, true);

        TextureRegion[] slash = { slash1, slash2, slash3};
        slashAnimation = new Animation<TextureRegion>(.166f, slash);
        slashAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        smack1 = new TextureRegion(battle, 0, 90, 16, 16);
        smack1.flip(false, true);

        smack2 = new TextureRegion(battle, 18, 90, 16, 16);
        smack2.flip(false, true);

        smack3 = new TextureRegion(battle, 36, 90, 16, 16);
        smack3.flip(false, true);

        TextureRegion[] smack = { smack1, smack2, smack3};
        smackAnimation = new Animation<TextureRegion>(.166f, smack);
        smackAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        flurry1 = new TextureRegion(battle, 0, 198, 24, 16);
        flurry1.flip(false, true);

        flurry2 = new TextureRegion(battle, 26, 198, 24, 16);
        flurry2.flip(false, true);

        flurry3 = new TextureRegion(battle, 52, 198, 24, 16);
        flurry3.flip(false, true);

        flurry4 = new TextureRegion(battle, 78, 198, 24, 16);
        flurry4.flip(false, true);

        flurry5 = new TextureRegion(battle, 104, 198, 24, 16);
        flurry5.flip(false, true);

        TextureRegion[] flurry = { flurry1,flurry2,flurry3,flurry3,flurry4,flurry4,flurry5,flurry5};
        flurryAnimation = new Animation<TextureRegion>(.06f, flurry);
        flurryAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        snipe1 = new TextureRegion(battle, 108, 108, 16, 16);
        snipe1.flip(false, true);

        snipe2 = new TextureRegion(battle, 126, 108, 16, 16);
        snipe2.flip(false, true);

        snipe3 = new TextureRegion(battle, 144, 108, 16, 16);
        snipe3.flip(false, true);

        TextureRegion[] snipe = { snipe1, snipe2, snipe3};
        snipeAnimation = new Animation<TextureRegion>(.166f, snipe);
        snipeAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        dark1 = new TextureRegion(battle, 54, 0, 16, 16);
        dark1.flip(false, true);

        dark2 = new TextureRegion(battle, 72, 0, 16, 16);
        dark2.flip(false, true);

        dark3 = new TextureRegion(battle, 90, 0, 16, 16);
        dark3.flip(false, true);

        TextureRegion[] dark = { dark1, dark2, dark3};
        darkAnimation = new Animation<TextureRegion>(.166f, dark);
        darkAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        heal1 = new TextureRegion(battle, 54, 18, 16, 16);
        heal1.flip(false, true);

        heal2 = new TextureRegion(battle, 72, 18, 16, 16);
        heal2.flip(false, true);

        heal3 = new TextureRegion(battle, 90, 18, 16, 16);
        heal3.flip(false, true);

        TextureRegion[] heal = { heal1, heal2, heal3};
        healAnimation = new Animation<TextureRegion>(.166f, heal);
        healAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        up1 = new TextureRegion(battle, 54, 36, 16, 16);
        up1.flip(false, true);

        up2 = new TextureRegion(battle, 72, 36, 16, 16);
        up2.flip(false, true);

        up3 = new TextureRegion(battle, 90, 36, 16, 16);
        up3.flip(false, true);

        TextureRegion[] up = { up1, up2, up3,blank,blank};
        upAnimation = new Animation<TextureRegion>(.166f, up);
        upAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        down1 = new TextureRegion(battle, 54, 54, 16, 16);
        down1.flip(false, true);

        down2 = new TextureRegion(battle, 72, 54, 16, 16);
        down2.flip(false, true);

        down3 = new TextureRegion(battle, 90, 54, 16, 16);
        down3.flip(false, true);

        TextureRegion[] down = { down1, down2, down3,blank,blank};
        downAnimation = new Animation<TextureRegion>(.166f, down);
        downAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        water1 = new TextureRegion(battle, 54, 72, 16, 16);
        water1.flip(false, true);

        water2 = new TextureRegion(battle, 72, 72, 16, 16);
        water2.flip(false, true);

        water3 = new TextureRegion(battle, 90, 72, 16, 16);
        water3.flip(false, true);

        TextureRegion[] water = { water1, water2, water3};
        waterAnimation = new Animation<TextureRegion>(.166f, water);
        waterAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        earth1 = new TextureRegion(battle, 54, 90, 16, 16);
        earth1.flip(false, true);

        earth2 = new TextureRegion(battle, 72, 90, 16, 16);
        earth2.flip(false, true);

        earth3 = new TextureRegion(battle, 90, 90, 16, 16);
        earth3.flip(false, true);

        TextureRegion[] earth = { earth1, earth2, earth3};
        earthAnimation = new Animation<TextureRegion>(.166f, earth);
        earthAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        poison1 = new TextureRegion(battle, 108, 0, 16, 16);
        poison1.flip(false, true);

        poison2 = new TextureRegion(battle, 126, 0, 16, 16);
        poison2.flip(false, true);

        poison3 = new TextureRegion(battle, 144, 0, 16, 16);
        poison3.flip(false, true);

        poison4 = new TextureRegion(battle, 162, 0, 16, 16);
        poison4.flip(false, true);

        TextureRegion[] poison = { poison1, poison2, poison3, poison4};
        poisonAnimation = new Animation<TextureRegion>(.075f, poison);
        poisonAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        ghost1 = new TextureRegion(battle, 108, 18, 16, 16);
        ghost1.flip(false, true);

        ghost2 = new TextureRegion(battle, 126, 18, 16, 16);
        ghost2.flip(false, true);

        ghost3 = new TextureRegion(battle, 144, 18, 16, 16);
        ghost3.flip(false, true);

        ghost4 = new TextureRegion(battle, 162, 18, 16, 16);
        ghost4.flip(false, true);

        TextureRegion[] ghost = { ghost1, ghost2, ghost3, ghost4};
        ghostAnimation = new Animation<TextureRegion>(.075f, ghost);
        ghostAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        shield1 = new TextureRegion(battle, 108, 36, 16, 16);
        shield1.flip(false, true);

        shield2 = new TextureRegion(battle, 126, 36, 16, 16);
        shield2.flip(false, true);

        shield3 = new TextureRegion(battle, 144, 36, 16, 16);
        shield3.flip(false, true);

        TextureRegion[] shield = { shield1, shield2, shield3};
        shieldAnimation = new Animation<TextureRegion>(.166f, shield);
        shieldAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        revive1 = new TextureRegion(battle, 0, 144, 16, 16);
        revive1.flip(false, true);

        revive2 = new TextureRegion(battle, 18, 144, 16, 16);
        revive2.flip(false, true);

        revive3 = new TextureRegion(battle, 36, 144, 16, 16);
        revive3.flip(false, true);

        revive4 = new TextureRegion(battle, 54, 144, 16, 16);
        revive4.flip(false, true);

        revive5 = new TextureRegion(battle, 72, 144, 16, 16);
        revive5.flip(false, true);

        revive6 = new TextureRegion(battle, 90, 144, 16, 16);
        revive6.flip(false, true);

        revive7 = new TextureRegion(battle, 108, 144, 16, 16);
        revive7.flip(false, true);

        TextureRegion[] revive = { revive1, revive2, revive3, revive4, revive5,revive6,revive7};
        reviveAnimation = new Animation<TextureRegion>(.1f, revive);
        reviveAnimation.setPlayMode(Animation.PlayMode.NORMAL);


        slashWave1 = new TextureRegion(battle, 0, 162, 24, 16);
        slashWave1.flip(false, true);

        slashWave2 = new TextureRegion(battle, 26, 162, 24, 16);
        slashWave2.flip(false, true);

        slashWave3 = new TextureRegion(battle, 52, 162, 24, 16);
        slashWave3.flip(false, true);

        slashWave4 = new TextureRegion(battle, 78, 162, 24, 16);
        slashWave4.flip(false, true);

        slashWave5 = new TextureRegion(battle, 0, 180, 24, 16);
        slashWave5.flip(false, true);

        slashWave6 = new TextureRegion(battle, 26, 180, 24, 16);
        slashWave6.flip(false, true);

        TextureRegion[] slashWave = { slashWave1, slashWave2, slashWave3, slashWave4, slashWave5,slashWave6};
        slashWaveAnimation = new Animation<TextureRegion>(.08f, slashWave);
        slashWaveAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        stun1 = new TextureRegion(battle, 162, 36, 16, 16);
        stun1.flip(false, true);

        stun2 = new TextureRegion(battle, 162, 54, 16, 16);
        stun2.flip(false, true);

        TextureRegion[] stun = { stun1, stun2};
        stunAnimation = new Animation<TextureRegion>(.25f, stun);
        stunAnimation.setPlayMode(Animation.PlayMode.LOOP);

        burn1 = new TextureRegion(battle, 108, 72, 16, 16);
        burn1.flip(false, true);

        burn2 = new TextureRegion(battle, 126, 72, 16, 16);
        burn2.flip(false, true);

        burn3 = new TextureRegion(battle, 144, 72, 16, 16);
        burn3.flip(false, true);

        burn4 = new TextureRegion(battle, 162, 72, 16, 16);
        burn4.flip(false, true);

        TextureRegion[] burn = { burn1, burn2, burn3, burn4};
        burnAnimation = new Animation<TextureRegion>(.075f, burn);
        burnAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        shadow1 = new TextureRegion(battle, 108, 90, 16, 16);
        shadow1.flip(false, true);

        shadow2 = new TextureRegion(battle, 126, 90, 16, 16);
        shadow2.flip(false, true);

        TextureRegion[] shadow = { shadow1, shadow2};
        shadowAnimation = new Animation<TextureRegion>(.1f, shadow);
        shadowAnimation.setPlayMode(Animation.PlayMode.LOOP);

        shadowUp = new TextureRegion(battle, 144, 90, 6, 8);
        shadowUp.flip(false, true);

        shadowAngle = new TextureRegion(battle, 152, 90, 7, 7);
        shadowAngle.flip(false, true);

        hot1 = new TextureRegion(battle, 108, 54, 16, 16);
        hot1.flip(false, true);

        hot2 = new TextureRegion(battle, 126, 54, 16, 16);
        hot2.flip(false, true);

        hot3 = new TextureRegion(battle, 144, 54, 16, 16);
        hot3.flip(false, true);

        hot4 = new TextureRegion(battle, 162, 90, 16, 16);
        hot4.flip(false, true);

        TextureRegion[] hot = { hot1, hot2, hot3, hot4};
        hotAnimation = new Animation<TextureRegion>(.075f, hot);
        hotAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        darkFire1 = new TextureRegion(battle, 0, 216, 16, 16);
        darkFire1.flip(false, true);

        darkFire2 = new TextureRegion(battle, 18, 216, 16, 16);
        darkFire2.flip(false, true);

        darkFire3 = new TextureRegion(battle, 36, 216, 16, 16);
        darkFire3.flip(false, true);

        TextureRegion[] darkFire = { darkFire1, darkFire2, darkFire3};
        darkFireAnimation = new Animation<TextureRegion>(.166f, darkFire);
        darkFireAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        powerUp1 = new TextureRegion(battle, 18, 234, 16, 16);
        powerUp1.flip(false, true);

        powerUp2 = new TextureRegion(battle, 36, 234, 16, 16);
        powerUp2.flip(false, true);

        powerUp3 = new TextureRegion(battle, 54, 234, 16, 16);
        powerUp3.flip(false, true);

        powerUp4 = new TextureRegion(battle, 72, 234, 16, 16);
        powerUp4.flip(false, true);

        powerUp5 = new TextureRegion(battle, 90, 234, 16, 16);
        powerUp5.flip(false, true);

        powerUp6 = new TextureRegion(battle, 108, 234, 16, 16);
        powerUp6.flip(false, true);

        powerUp7 = new TextureRegion(battle, 126, 234, 16, 16);
        powerUp7.flip(false, true);

        TextureRegion[] powerUp = { powerUp1, powerUp2, powerUp3, powerUp4, powerUp5, powerUp6, powerUp7, powerUp7};
        powerUpAnimation = new Animation<TextureRegion>(.05f, powerUp);
        powerUpAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        healthUp1 = new TextureRegion(battle, 18, 252, 16, 16);
        healthUp1.flip(false, true);

        healthUp2 = new TextureRegion(battle, 36, 252, 16, 16);
        healthUp2.flip(false, true);

        healthUp3 = new TextureRegion(battle, 54, 252, 16, 16);
        healthUp3.flip(false, true);

        healthUp4 = new TextureRegion(battle, 72, 252, 16, 16);
        healthUp4.flip(false, true);

        healthUp5 = new TextureRegion(battle, 90, 252, 16, 16);
        healthUp5.flip(false, true);

        healthUp6 = new TextureRegion(battle, 108, 252, 16, 16);
        healthUp6.flip(false, true);

        healthUp7 = new TextureRegion(battle, 126, 252, 16, 16);
        healthUp7.flip(false, true);

        TextureRegion[] healthUp = { healthUp1, healthUp2, healthUp3, healthUp4, healthUp5, healthUp6, healthUp7, healthUp7};
        healthUpAnimation = new Animation<TextureRegion>(.05f, healthUp);
        healthUpAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        resistanceUp1 = new TextureRegion(battle, 144, 198, 16, 16);
        resistanceUp1.flip(false, true);

        resistanceUp2 = new TextureRegion(battle, 162, 198, 16, 16);
        resistanceUp2.flip(false, true);

        resistanceUp3 = new TextureRegion(battle, 180, 198, 16, 16);
        resistanceUp3.flip(false, true);

        resistanceUp4 = new TextureRegion(battle, 198, 198, 16, 16);
        resistanceUp4.flip(false, true);

        resistanceUp5 = new TextureRegion(battle, 216, 198, 16, 16);
        resistanceUp5.flip(false, true);

        resistanceUp6 = new TextureRegion(battle, 234, 198, 16, 16);
        resistanceUp6.flip(false, true);

        resistanceUp7 = new TextureRegion(battle, 252, 198, 16, 16);
        resistanceUp7.flip(false, true);

        TextureRegion[] resistanceUp = { resistanceUp1, resistanceUp2, resistanceUp3, resistanceUp4, resistanceUp5, resistanceUp6, resistanceUp7, resistanceUp7};
        resistanceUpAnimation = new Animation<TextureRegion>(.05f, resistanceUp);
        resistanceUpAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        defenseUp1 = new TextureRegion(battle, 144, 216, 16, 16);
        defenseUp1.flip(false, true);

        defenseUp2 = new TextureRegion(battle, 162, 216, 16, 16);
        defenseUp2.flip(false, true);

        defenseUp3 = new TextureRegion(battle, 180, 216, 16, 16);
        defenseUp3.flip(false, true);

        defenseUp4 = new TextureRegion(battle, 198, 216, 16, 16);
        defenseUp4.flip(false, true);

        defenseUp5 = new TextureRegion(battle, 216, 216, 16, 16);
        defenseUp5.flip(false, true);

        defenseUp6 = new TextureRegion(battle, 234, 216, 16, 16);
        defenseUp6.flip(false, true);

        defenseUp7 = new TextureRegion(battle, 252, 216, 16, 16);
        defenseUp7.flip(false, true);

        TextureRegion[] defenseUp = { defenseUp1, defenseUp2, defenseUp3, defenseUp4, defenseUp5, defenseUp6, defenseUp7, defenseUp7};
        defenseUpAnimation = new Animation<TextureRegion>(.05f, defenseUp);
        defenseUpAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        agilityUp1 = new TextureRegion(battle, 144, 234, 16, 16);
        agilityUp1.flip(false, true);

        agilityUp2 = new TextureRegion(battle, 162, 234, 16, 16);
        agilityUp2.flip(false, true);

        agilityUp3 = new TextureRegion(battle, 180, 234, 16, 16);
        agilityUp3.flip(false, true);

        agilityUp4 = new TextureRegion(battle, 198, 234, 16, 16);
        agilityUp4.flip(false, true);

        agilityUp5 = new TextureRegion(battle, 216, 234, 16, 16);
        agilityUp5.flip(false, true);

        agilityUp6 = new TextureRegion(battle, 234, 234, 16, 16);
        agilityUp6.flip(false, true);

        agilityUp7 = new TextureRegion(battle, 252, 234, 16, 16);
        agilityUp7.flip(false, true);

        TextureRegion[] agilityUp = { agilityUp1, agilityUp2, agilityUp3, agilityUp4, agilityUp5, agilityUp6, agilityUp7, agilityUp7};
        agilityUpAnimation = new Animation<TextureRegion>(.05f, agilityUp);
        agilityUpAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        speedUp1 = new TextureRegion(battle, 144, 252, 16, 16);
        speedUp1.flip(false, true);

        speedUp2 = new TextureRegion(battle, 162, 252, 16, 16);
        speedUp2.flip(false, true);

        speedUp3 = new TextureRegion(battle, 180, 252, 16, 16);
        speedUp3.flip(false, true);

        speedUp4 = new TextureRegion(battle, 198, 252, 16, 16);
        speedUp4.flip(false, true);

        speedUp5 = new TextureRegion(battle, 216, 252, 16, 16);
        speedUp5.flip(false, true);

        speedUp6 = new TextureRegion(battle, 234, 252, 16, 16);
        speedUp6.flip(false, true);

        speedUp7 = new TextureRegion(battle, 252, 252, 16, 16);
        speedUp7.flip(false, true);

        TextureRegion[] speedUp = { speedUp1, speedUp2, speedUp3, speedUp4, speedUp5, speedUp6, speedUp7, speedUp7};
        speedUpAnimation = new Animation<TextureRegion>(.05f, speedUp);
        speedUpAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        judgment1 = new TextureRegion(battle, 0, 272, 16, 16);
        judgment1.flip(false, true);

        judgment2 = new TextureRegion(battle, 18, 272, 16, 16);
        judgment2.flip(false, true);

        judgment3 = new TextureRegion(battle, 36, 272, 16, 16);
        judgment3.flip(false, true);

        judgment4 = new TextureRegion(battle, 54, 272, 16, 16);
        judgment4.flip(false, true);

        judgment5 = new TextureRegion(battle, 72, 272, 16, 16);
        judgment5.flip(false, true);

        TextureRegion[] judgment = { judgment1, judgment1, judgment2, judgment3, judgment4, judgment5, judgment5};
        judgmentAnimation = new Animation<TextureRegion>(.07f, judgment);
        judgmentAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        hammer1 = new TextureRegion(battle, 0, 290, 16, 16);
        hammer1.flip(false, true);

        hammer2 = new TextureRegion(battle, 18, 290, 16, 16);
        hammer2.flip(false, true);

        hammer3 = new TextureRegion(battle, 36, 290, 16, 16);
        hammer3.flip(false, true);

        hammer4 = new TextureRegion(battle, 54, 290, 16, 16);
        hammer4.flip(false, true);

        hammer5 = new TextureRegion(battle, 72, 290, 16, 16);
        hammer5.flip(false, true);

        TextureRegion[] hammer = { hammer1, hammer2, hammer3, hammer4, hammer5};
        hammerAnimation = new Animation<TextureRegion>(.1f, hammer);
        hammerAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        fireSlash1 = new TextureRegion(battle, 0, 308, 16, 16);
        fireSlash1.flip(false, true);

        fireSlash2 = new TextureRegion(battle, 18, 308, 16, 16);
        fireSlash2.flip(false, true);

        fireSlash3 = new TextureRegion(battle, 36, 308, 16, 16);
        fireSlash3.flip(false, true);

        fireSlash4 = new TextureRegion(battle, 54, 308, 16, 16);
        fireSlash4.flip(false, true);

        TextureRegion[] fireSlash = { fireSlash1, fireSlash2,fireSlash2, fireSlash3, fireSlash3,fireSlash4};
        fireSlashAnimation = new Animation<TextureRegion>(.08f, fireSlash);
        fireSlashAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        greenArrow31 = new TextureRegion(battle, 0, 326, 24, 16);
        greenArrow31.flip(false, true);

        greenArrow32 = new TextureRegion(battle, 26, 326, 24, 16);
        greenArrow32.flip(false, true);

        greenArrow33 = new TextureRegion(battle, 52, 326, 24, 16);
        greenArrow33.flip(false, true);

        greenArrow34 = new TextureRegion(battle, 78, 326, 24, 16);
        greenArrow34.flip(false, true);

        greenArrow35 = new TextureRegion(battle, 104, 326, 24, 16);
        greenArrow35.flip(false, true);

        greenArrow36 = new TextureRegion(battle, 0, 344, 24, 16);
        greenArrow36.flip(false, true);

        greenArrow37 = new TextureRegion(battle, 26, 344, 24, 16);
        greenArrow37.flip(false, true);

        greenArrow38 = new TextureRegion(battle, 52, 344, 24, 16);
        greenArrow38.flip(false, true);

        greenArrow39 = new TextureRegion(battle, 78, 344, 24, 16);
        greenArrow39.flip(false, true);

        TextureRegion[] greenArrow3 = { greenArrow31, greenArrow32, greenArrow33, greenArrow33, greenArrow34, greenArrow35,
                greenArrow36, greenArrow37, greenArrow37, greenArrow38, greenArrow38, greenArrow39, greenArrow39};
        greenArrow3Animation = new Animation<TextureRegion>(.05f, greenArrow3);
        greenArrow3Animation.setPlayMode(Animation.PlayMode.NORMAL);
        
        barrage1 = new TextureRegion(battle, 0, 362, 24, 16);
        barrage1.flip(false, true);

        barrage2 = new TextureRegion(battle, 26, 362, 24, 16);
        barrage2.flip(false, true);

        barrage3 = new TextureRegion(battle, 52, 362, 24, 16);
        barrage3.flip(false, true);

        barrage4 = new TextureRegion(battle, 78, 362, 24, 16);
        barrage4.flip(false, true);

        barrage5 = new TextureRegion(battle, 104, 362, 24, 16);
        barrage5.flip(false, true);

        barrage6 = new TextureRegion(battle, 130, 362, 24, 16);
        barrage6.flip(false, true);

        TextureRegion[] barrage = { barrage1, barrage2, barrage3, barrage4, barrage5, barrage6};
        barrageAnimation = new Animation<TextureRegion>(.06f, barrage);
        barrageAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        darkfire1 = new TextureRegion(battle, 0, 380, 24, 16);
        darkfire1.flip(false, true);

        darkfire2 = new TextureRegion(battle, 26, 380, 24, 16);
        darkfire2.flip(false, true);

        darkfire3 = new TextureRegion(battle, 52, 380, 24, 16);
        darkfire3.flip(false, true);

        darkfire4 = new TextureRegion(battle, 78, 380, 24, 16);
        darkfire4.flip(false, true);

        darkfire5 = new TextureRegion(battle, 104, 380, 24, 16);
        darkfire5.flip(false, true);

        darkfire6 = new TextureRegion(battle, 130, 380, 24, 16);
        darkfire6.flip(false, true);

        TextureRegion[] darkfire = { darkfire1, darkfire2, darkfire3, darkfire4, darkfire5, darkfire6};
        darkfireAnimation = new Animation<TextureRegion>(.06f, darkfire);
        darkfireAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        
        sol1 = new TextureRegion(battle, 0, 398, 32, 32);
        sol1.flip(false, true);

        sol2 = new TextureRegion(battle, 34, 398, 32, 32);
        sol2.flip(false, true);

        sol3 = new TextureRegion(battle, 68, 398, 32, 32);
        sol3.flip(false, true);

        sol4 = new TextureRegion(battle, 102, 398, 32, 32);
        sol4.flip(false, true);

        sol5 = new TextureRegion(battle, 136, 398, 32, 32);
        sol5.flip(false, true);

        sol6 = new TextureRegion(battle, 170, 398, 32, 32);
        sol6.flip(false, true);

        sol7 = new TextureRegion(battle, 204, 398, 32, 32);
        sol7.flip(false, true);

        sol8 = new TextureRegion(battle, 238, 398, 32, 32);
        sol8.flip(false, true);

        TextureRegion[] sol = { sol1, sol2, sol3, sol4, sol5,sol6,sol7,sol8};
        solAnimation = new Animation<TextureRegion>(.06f, sol);
        solAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        luna1 = new TextureRegion(battle, 0, 438, 32, 32);
        luna1.flip(false, true);

        luna2 = new TextureRegion(battle, 34, 438, 32, 32);
        luna2.flip(false, true);

        luna3 = new TextureRegion(battle, 68, 438, 32, 32);
        luna3.flip(false, true);

        luna4 = new TextureRegion(battle, 102, 438, 32, 32);
        luna4.flip(false, true);

        luna5 = new TextureRegion(battle, 136, 438, 32, 32);
        luna5.flip(false, true);

        luna6 = new TextureRegion(battle, 170, 438, 32, 32);
        luna6.flip(false, true);

        luna7 = new TextureRegion(battle, 204, 438, 32, 32);
        luna7.flip(false, true);

        luna8 = new TextureRegion(battle, 238, 438, 32, 32);
        luna8.flip(false, true);

        TextureRegion[] luna = { luna1, luna2, luna3, luna4, luna5,luna6,luna7,luna8,luna8};
        lunaAnimation = new Animation<TextureRegion>(.05f, luna);
        lunaAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        sleet1 = new TextureRegion(battle, 0, 466, 24, 16);
        sleet1.flip(false, true);

        sleet2 = new TextureRegion(battle, 26, 466, 24, 16);
        sleet2.flip(false, true);

        sleet3 = new TextureRegion(battle, 52, 466, 24, 16);
        sleet3.flip(false, true);

        sleet4 = new TextureRegion(battle, 78, 466, 24, 16);
        sleet4.flip(false, true);

        sleet5 = new TextureRegion(battle, 104, 466, 24, 16);
        sleet5.flip(false, true);

        sleet6 = new TextureRegion(battle, 130, 466, 24, 16);
        sleet6.flip(false, true);

        TextureRegion[] sleet = { sleet1, sleet2, sleet3, sleet4, sleet5,sleet6};
        sleetAnimation = new Animation<TextureRegion>(.07f, sleet);
        sleetAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        storm1 = new TextureRegion(battle, 250, 0, 16, 16);
        storm1.flip(false, true);

        storm2 = new TextureRegion(battle, 268, 0, 16, 16);
        storm2.flip(false, true);

        storm3 = new TextureRegion(battle, 286, 0, 16, 16);
        storm3.flip(false, true);

        storm4 = new TextureRegion(battle, 78, 18, 16, 16);
        storm4.flip(false, true);

        storm5 = new TextureRegion(battle, 250, 18, 16, 16);
        storm5.flip(false, true);

        storm6 = new TextureRegion(battle, 268, 18, 16, 16);
        storm6.flip(false, true);

        blank = new TextureRegion(battle, 92, 110, 16, 16);
        blank.flip(false, true);

        TextureRegion[] storm = { storm1,storm1, blank,storm2,storm2, blank,storm3,storm3, storm4,storm4, storm5,storm5,storm6,storm6}; //14
        stormAnimation = new Animation<TextureRegion>(.035f, storm);
        stormAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        haunt1 = new TextureRegion(battle, 304, 0, 16, 16);
        haunt1.flip(false, true);

        haunt2 = new TextureRegion(battle, 322, 0, 16, 16);
        haunt2.flip(false, true);

        haunt3 = new TextureRegion(battle, 340, 0, 16, 16);
        haunt3.flip(false, true);

        haunt4 = new TextureRegion(battle, 358, 0, 16, 16);
        haunt4.flip(false, true);

        haunt5 = new TextureRegion(battle, 376, 0, 16, 16);
        haunt5.flip(false, true);

        haunt6 = new TextureRegion(battle, 394, 0, 16, 16);
        haunt6.flip(false, true);

        haunt7 = new TextureRegion(battle, 412, 0, 16, 16);
        haunt7.flip(false, true);

        TextureRegion[] haunt = { haunt1, haunt2, haunt3,haunt4, haunt5, haunt6, haunt7};
        hauntAnimation = new Animation<TextureRegion>(.06f, haunt);
        hauntAnimation.setPlayMode(Animation.PlayMode.NORMAL);

        reap1 = new TextureRegion(battle, 304, 18, 16, 32);
        reap1.flip(false, true);

        reap2 = new TextureRegion(battle, 322, 18, 16, 32);
        reap2.flip(false, true);

        reap3 = new TextureRegion(battle, 340, 18, 16, 32);
        reap3.flip(false, true);

        reap4 = new TextureRegion(battle, 358, 18, 16, 32);
        reap4.flip(false, true);

        reap5 = new TextureRegion(battle, 376, 18, 16, 32);
        reap5.flip(false, true);

        reap6 = new TextureRegion(battle, 394, 18, 16, 32);
        reap6.flip(false, true);

        reap7 = new TextureRegion(battle, 412, 18, 16, 32);
        reap7.flip(false, true);

        TextureRegion[] reap = { reap1, reap2, reap3,reap4, reap5, reap6, reap7};
        reapAnimation = new Animation<TextureRegion>(.07f, reap);
        reapAnimation.setPlayMode(Animation.PlayMode.NORMAL);
    }

    public static void dispose() {
        // We must dispose of the texture when we are finished.
        //menu.dispose();
        isLoaded = false;
    }
}