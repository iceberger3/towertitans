package helpers;

/**
 * Created by kyle.berger on 2/27/17.
 */

public class CombatText {
    public String message;
    public float x;
    public float y;
    public int color;
    public float time;
    public boolean crit = false;
    public int up = 0; //-1 down, 1 up,
    public CombatText(String m, float x, float y, int c){
        this.message = m;
        this.x = x;
        this.y = y;
        this.color = c;
        time = -.2f;
    }

    public void update(float delta){
        y -= 12 * delta;
        time += delta;
    }
}
