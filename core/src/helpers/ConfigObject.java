package helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

import org.jasypt.util.text.StrongTextEncryptor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;


/**
 * Created by Berger on 11/3/2015.
 */
public class ConfigObject{
    public ConfigFile config;

    public ConfigObject(){
        try {
            load();
        }catch(Exception e) {
            newFile();
        }
    }
    public void newFile(){
        config = new ConfigFile();
    }
    public void updateSettings(){

    }
    public void save(){
        System.out.println("saving config");
        try{
            Json json = new Json();
            String saveString = json.prettyPrint(config);
            FileHandle file = new FileHandle("config.json");
            FileWriter fileWriter = new FileWriter(file.file());
            fileWriter.write(saveString);
            fileWriter.flush();
            fileWriter.close();
        }
        catch (Exception e){
        }
    }
    public void load(){
        try {

            FileHandle file = new FileHandle("config.json");
            String saveString = file.readString();
            Json json = new Json();
            config = json.fromJson(ConfigFile.class, saveString);
        } catch(Exception E){
            newFile();
        }
    }
}
