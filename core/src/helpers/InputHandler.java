package helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;


import java.util.ArrayList;

import game.TowerTitan;
import objects.Character;
import objects.Item;
import world.GameWorld;

public class InputHandler implements InputProcessor {

	private TowerTitan game;
	public GameWorld myWorld;
	private int startX, startY;
	public int pointer = -1;
	public boolean pointerLock = false;
	private float maxHeight;
	private float maxWidth;
	private int iphoneXLeft = 0;
	private int iphoneXRight = 0;
	public float wpad = 0;
	public float hpad = 0;
	private int itemCheat = 0;
	private int goldCheat = 0;
	private float dragX = 0;
	private float dragY = -1;
	private float dragDistance = 0;
    public InputHandler(TowerTitan g, GameWorld myWorld) {
		game = g;
		this.myWorld = myWorld;
		startX = 0;
		startY = 0;
		maxHeight = game.maxHeight;
		maxWidth = game.maxWidth;
		hpad = maxHeight - 224;
		wpad = maxWidth - 400;
		this.iphoneXLeft = myWorld.game.iphoneXLeft;
		this.iphoneXRight = myWorld.game.iphoneXRight;
		if(myWorld.game.isIphoneX){
			wpad -= iphoneXLeft;
			wpad -= iphoneXRight;
		}
    }

    public void resize(){
		maxHeight = game.maxHeight;
		maxWidth = game.maxWidth;
		hpad = maxHeight - 224;
		wpad = maxWidth - 128;
		this.iphoneXLeft = myWorld.game.iphoneXLeft;
		this.iphoneXRight = myWorld.game.iphoneXRight;
		if(myWorld.game.isIphoneX){
			wpad -= iphoneXLeft;
			wpad -= iphoneXRight;
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {


		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}


	public void touchDownArmyScreen(float x, float y, float hpad){
        if(myWorld.view == null) {
            for (int i = 0; i < 4; i++) {
                if (x >= i * 32 && x < 32 + i * 32 && y >= 13 + iphoneXLeft && y < 45 + iphoneXLeft) {
                    if (TowerTitan.save.save.party.size() > i) {
                        if (x >= 23 + i * 32 && x < 32 + i * 32 && y <= 22 + iphoneXLeft && TowerTitan.save.save.party.size() != 1) {
                            //remove hero
                            Character c = TowerTitan.save.save.party.get(i);
                            TowerTitan.save.save.party.remove(i);
                            TowerTitan.save.save.army.add(0, c);
                            this.myWorld.reset();
                        } else {
                            //go to hero screen
                            myWorld.view = TowerTitan.save.save.party.get(i);
                        }
                    }
                }
            }
            float armyY = 49 - myWorld.scrollY;
            for (int i = 0; i < 100; i++) {
                if (x >= (i % 4) * 32 && x < 32 + (i % 4) * 32 && y >= 45 + iphoneXLeft && y < 194 + hpad && y >= armyY + iphoneXLeft + (i / 4) * 32 && y < armyY + 32 + iphoneXLeft + (i / 4) * 32) {
                    if (TowerTitan.save.save.army.size() > i) {
                        if (x >= 23 + (i % 4) * 32 && x < 32 + (i % 4) * 32 && y <= armyY + 9 + iphoneXLeft + (i / 4) * 32 && TowerTitan.save.save.party.size() < 4) {
                            //Add hero
                            Character c = TowerTitan.save.save.army.get(i);
                            TowerTitan.save.save.army.remove(i);
                            TowerTitan.save.save.party.add(c);
                            this.myWorld.reset();
                        } else {
                            //go to hero screen
                            myWorld.view = TowerTitan.save.save.army.get(i);
                        }
                    }
                }
            }
        }
        else{
        	if(myWorld.tab == 0) {
        		if(y <= 32 + iphoneXLeft){
					if(myWorld.view.canLevelUp()){
						myWorld.view.levelUp();
						TowerTitan.save.save.gold -= Character.getLevelCost(myWorld.view.getLevel());
						TowerTitan.save.save();
					}
				}
        		if (myWorld.itemSlot == -1) {
					for (int i = 0; i < 4; i++) {
						if (y >= 53 + iphoneXLeft + i * 21 && y <= 71 + iphoneXLeft + i * 21) {
							myWorld.itemSlot = i;
							myWorld.itemList = new ArrayList<>();
							for (Item item:TowerTitan.save.save.items) {
								if(item.slot == i && (item.role == myWorld.view.role || item.role == 0)){
									int sumNew = 0;
									for(int s = 0; s < 4; s++){
										sumNew += item.stats[s];
									}
									int index = 0;
									boolean found = false;
									while(index < myWorld.itemList.size() && !found){
										int sumOld = 0;
										for(int s = 0; s < 4; s++){
											sumOld += myWorld.itemList.get(index).stats[s];
										}
										if(sumNew > sumOld){
											myWorld.itemList.add(index,item);
											found = true;
										}
										else{
											index++;
										}
									}
									if(!found) {
										myWorld.itemList.add(item);
									}
								}
							}
						}
					}
				}
        		else{
        			if(y < 36 +iphoneXLeft){
        				if(myWorld.view.items[myWorld.itemSlot] != null){
        					myWorld.itemList.add(0,myWorld.view.items[myWorld.itemSlot]);
        					TowerTitan.save.save.items.add(0,myWorld.view.items[myWorld.itemSlot]);
							myWorld.view.items[myWorld.itemSlot] = null;
						}
					}
        			for (int i = 0; i < Math.min(6,myWorld.itemList.size() - 6 * myWorld.page); i++){
        				if( y>= 36 + iphoneXLeft + i * 23 && y <= 60 + iphoneXLeft + i * 23){
							Item oldItem = myWorld.view.items[myWorld.itemSlot];

        					Item item = myWorld.itemList.get(i + 6 * myWorld.page);
							myWorld.itemList.remove(item);
							TowerTitan.save.save.items.remove(item);
							myWorld.view.items[myWorld.itemSlot] = item;
							if(oldItem != null){
								myWorld.itemList.add(0,oldItem);
								TowerTitan.save.save.items.add(0,oldItem);
							}
						}
					}
					if(myWorld.itemList.size() - 6 * myWorld.page > 6 && x >= 96 && y >= 174 + hpad && y < 198 + hpad){
						myWorld.page++;
					}
					if(myWorld.page > 0 && x >= 64 && x < 96 && y >= 174 + hpad && y < 198 + hpad ){
						myWorld.page--;
					}
					if(x < 64 && y >= 174 + hpad && y < 198 + hpad ){
						myWorld.itemSlot = -1;
						myWorld.itemList = null;
						myWorld.page = 0;
					}
				}
			}
            if(y >= 198 + hpad){
                if(x < 50){
                    resetVariables();
                    myWorld.screen = 0;
                }
                else if(x < 76){
                    myWorld.tab = 0;
					myWorld.itemSlot = -1;
					myWorld.itemList = null;
					myWorld.page = 0;
                }
                else if(x < 102){
                    myWorld.tab = 1;
					myWorld.itemSlot = -1;
					myWorld.itemList = null;
					myWorld.page = 0;
                }
                else {
                    myWorld.tab = 2;
					myWorld.itemSlot = -1;
					myWorld.itemList = null;
					myWorld.page = 0;
                }
            }
        }
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		float x = Gdx.graphics.getWidth();
		float y = Gdx.graphics.getHeight();
		x = screenX / x;
		x = x * maxWidth;
		y = screenY / y;
		y = y * maxHeight;
		float hpad = this.hpad - myWorld.game.iphoneXLeft;
		if(myWorld.screen == 0){
			if(y >= 49 + iphoneXLeft && y <= 194 + hpad) {
				myWorld.initY = y;
			}
		}
		if(myWorld.screen == 2) {
			if (y >= 158 + hpad && y <= 190 + hpad) {
				for (int i = 0; i < 4; i++) {
					if (x < (i + 1) * 32 && x >= i * 32) {
						if (myWorld.party.size() > i) {
							Character c = myWorld.party.get(i).character;
							if (c.canLevelUp()) {
								c.levelUp();
								TowerTitan.save.save.gold -= Character.getLevelCost(c.getLevel());
								TowerTitan.save.save();
							}
						}
					}
				}
			}
		}
		if(y >= 198 + hpad && myWorld.view == null){
			if(x <= 26){
                if(myWorld.screen == 0){
                    resetVariables();
                }
                else{
                    myWorld.screen = 0;
                    resetVariables();
                }
			}
			else if(x >= 25 && x <= 51){
                if(myWorld.screen == 1){
                }
                else {
                    myWorld.screen = 1;
                    resetVariables();
                }
			}
			else if(x >= 51&& x <= 76){
                if(myWorld.screen == 2){
                }
                else {
                    myWorld.screen = 2;
                    resetVariables();
                }
			}
			else if(x >= 77&& x <= 102){
                if(myWorld.screen == 3){
                }
                else {
                    myWorld.screen = 3;
                    resetVariables();
                }
			}
			if(x >= 102){
                if(myWorld.screen == 4){
                }
                else {
                    myWorld.screen = 4;
                    resetVariables();
                }
			}
		}
		return true;
	}
	
	public void resetVariables(){
        myWorld.tab = 0;
        myWorld.view = null;
        myWorld.itemSlot = -1;
        myWorld.itemList = null;
        myWorld.page = 0;
    }


	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		float x = Gdx.graphics.getWidth();
		float y = Gdx.graphics.getHeight();
		x = screenX / x;
		x = x * maxWidth;
		y = screenY / y;
		y = y * maxHeight;
		float hpad = this.hpad - myWorld.game.iphoneXLeft;

		if(myWorld.screen == 0){
			if(myWorld.lastY == -1) {
				touchDownArmyScreen(x, y, hpad);
			}
			else {
				myWorld.lastY = -1;
				myWorld.initY = -1;
			}
		}
		return true;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		float x = Gdx.graphics.getWidth();
		float y = Gdx.graphics.getHeight();
		x = screenX / x;
		x = x * maxWidth;
		y = screenY / y;
		y = y * maxHeight;
		float hpad = this.hpad - myWorld.game.iphoneXLeft;

		if(myWorld.screen == 0){
			if(myWorld.initY != -1 && TowerTitan.save.save.army.size() > 16) {
				if (myWorld.lastY == -1) {
					if (Math.abs(myWorld.initY - y) > 2)
						myWorld.lastY = myWorld.initY;
				} else {
					float delta = myWorld.lastY - y;
					System.out.print("Scroll Y: " + myWorld.scrollY + " ");
					System.out.print("delta " + delta + " ");
					myWorld.lastY = y;
					if (myWorld.scrollY + delta < 0) {
						delta = -myWorld.scrollY;
                        System.out.print("newDelta " + delta + " ");
					} else if (myWorld.scrollY + delta > 15 + (TowerTitan.save.save.army.size() / 4 - 4) * 32 + hpad) {
						delta =  (15 + (TowerTitan.save.save.army.size() / 4 - 4) * 32 + hpad) - myWorld.scrollY;
						System.out.print("newDelta " + delta + " ");
					}
					myWorld.scrollY += delta;
					System.out.println("Scroll Y: " + myWorld.scrollY);
				}
			}
		}

    	return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
