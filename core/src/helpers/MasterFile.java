package helpers;

import java.util.ArrayList;
import java.util.TreeMap;

import objects.Character;
import objects.Item;

/**
 * Created by bergerk on 3/4/16.
 */
public class MasterFile {
    private boolean music = true;
    public float volume = .25f;
    public float musicVolume = .5f;
    public boolean showHealth = true;
    public boolean showType = true;
    public boolean showDpad = true;
    public boolean fullscreen = false;
    public int battleStyle = 0;
    public SaveFile file;
    public MasterFile(){

    }

    public boolean isMusic(){
        return music;
    }

    public void setMusic(boolean b){
        music = b;
    }
}
