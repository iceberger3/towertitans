package helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

import world.MenuWorld;

public class MyTextInputListener implements Input.TextInputListener {
    public String text = "";
    public MenuWorld world;

    public MyTextInputListener(MenuWorld world){
        this.world = world;
    }

    @Override
    public void input (String text) {
        if(text.length() > 7) {
            Gdx.input.getTextInput(this, "Please Enter A Name", "", "7 chars or less");
        }
        else {
            this.text = text;
            world.textDialog = false;
            if(!this.text.equals("")){
                world.nameFlag = false;
            }
        }
    }

    @Override
    public void canceled () {
        world.textDialog = false;
    }
}