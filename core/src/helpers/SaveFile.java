package helpers;

import com.badlogic.gdx.utils.IntIntMap;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectMap;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TreeMap;

import objects.Character;
import objects.Item;

/**
 * Created by bergerk on 3/4/16.
 */
public class SaveFile implements Serializable {
    public ArrayList<Character> army;
    public ArrayList<Character> party;
    public ArrayList<Item> items;
    public int gold;
    public int gems;
    public int goldGems;
    public int towerFloor = 1;
    public double version = 0.0;
    public int[] material = new int[10];
    public String currentTime;
    public String maxDate;
    public SaveFile(){
        army = new ArrayList<>();
        items = new ArrayList<>();
        party = new ArrayList<>();
        gold = 0;
        gems = 0;

        Date currentTime = new Date();
        Date maxDate = new Date();
        this.currentTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH).format(currentTime);
        this.maxDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH).format(maxDate);
    }

    public ArrayList<Character> getArmy() {
        return army;
    }

    public void setArmy(ArrayList<Character> army) {
        this.army = army;
    }

    public ArrayList<Character> getParty() {
        return party;
    }

    public void setParty(ArrayList<Character> party) {
        this.party = party;
    }


}
