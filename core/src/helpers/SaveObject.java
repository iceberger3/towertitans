package helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

import org.jasypt.util.text.StrongTextEncryptor;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import game.TowerTitan;
import objects.Character;
import objects.jobs.Magus;
import objects.jobs.Paladin;
import objects.jobs.Ranger;
import objects.jobs.Reaper;
import screens.MenuScreen;


/**
 * Created by Berger on 11/3/2015.
 */
public class SaveObject{
    public MasterFile master;
    public SaveFile save;
    public int index = 0;
    public boolean eventPlaying = false;
    public static double latestVersion = 0.01;
    public SaveObject(){
        try {
            load();
        }catch(Exception e) {
            newFile();
        }
    }
    public void newFile(){
        master = new MasterFile();
        save = new SaveFile();
        if(TowerTitan.isDesktop){
            master.showDpad = false;
        }
        Character kyle = new Character(1, new Paladin(),"Paladin");
        save.party.add(kyle);
        save.party.add(new Character(1, new Reaper(),"Reaper"));
        save.party.add(new Character(1, new Ranger(),"Ranger"));
        save.party.add(new Character(1, new Magus(),"Magus"));
        master.file = save;
        save();
    }
    public void updateSettings(){
        Json json = new Json();
        //StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
        //textEncryptor.setPassword("P!x3|04PIXELOTpixelotpIxElOt");
        String saveString = json.prettyPrint(master);
        //saveString = textEncryptor.encrypt(saveString);
        FileHandle file = Gdx.files.local("save.json");
        file.writeString(saveString, false);
    }
    public void save(){
        try {

            Date currentTime = new Date();
            String format1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH).format(currentTime);

            save.currentTime = format1;
            master.file = save;
            Json json = new Json();
            String saveString = json.prettyPrint(master);
            FileHandle file = Gdx.files.local("save.json");
            FileWriter fileWriter = new FileWriter(file.file());
            fileWriter.write(saveString);
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception e) {
            //System.out.println("Could not save");
        }
    }
    /*
    public void saveDesktop(){
        if(!eventPlaying) {
            try {
                master.file = save;
                StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
                textEncryptor.setPassword("P!x3|04PIXELOTpixelotpIxElOt");
                Json json = new Json();
                String saveString = json.prettyPrint(master);
                saveString = textEncryptor.encrypt(saveString);
                FileHandle file = Gdx.files.local("save.json");
                FileWriter fileWriter = new FileWriter(file.file());
                fileWriter.write(saveString);
                fileWriter.flush();
                fileWriter.close();
            }
            catch (Exception e){
                //System.out.println("Could not save");
            }
        }
    }*/
    public boolean load(){
        try {
            MenuScreen.reload();
            FileHandle file = Gdx.files.local("save.json");
            String saveString = file.readString();
            Json json = new Json();
            master = json.fromJson(MasterFile.class, saveString);
            index = 0;
            loadFile(index);
            return true;
        } catch(Exception E){
                newFile();
            return  false;
        }
    }
    public void loadFile(int i){
        save = master.file;
        index = i;
        for(Character c: save.party){
            c.setJob();
        }
        for(Character c: save.army){
            c.setJob();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
        Date currentDate = new Date();
        try {
            Date saveDate = sdf.parse(save.currentTime);
            long diffInMillies = Math.abs(currentDate.getTime() - saveDate.getTime());
            long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if (diff > 10080) {
                diff = 10080;
            }
            save.currentTime = sdf.format(currentDate);
            Date maxDate = sdf.parse(save.maxDate);
            if (currentDate.getTime() - maxDate.getTime() > 0) {
                save.maxDate = sdf.format(currentDate);
            }

            int gold = 1 + save.towerFloor / 5;
            gold *= diff;
            save.gold += gold;
        }catch (Exception e){

        }
    }
    public SaveFile getSaveFile(){
        return save;
    }

    public boolean isMusic(){
        return master.isMusic();
    }

    public void setMusic(boolean b){
        master.setMusic(b);
    }
}
