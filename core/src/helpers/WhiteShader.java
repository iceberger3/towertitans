package helpers;

import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class WhiteShader {
    static String vertexShader =
            "attribute vec4 a_position; \n" +
                    "attribute vec4 a_color;\n" +
                    "attribute vec2 a_texCoord0; \n" +
                    "\n" +
                    "uniform mat4 u_projTrans; \n" +
                    "\n" +
                    "varying vec4 v_color; \n" +
                    "varying vec2 v_texCoords; \n" +
                    "\n" +
                    "void main() { \n" +
                    "v_color = a_color; \n" +
                    "v_texCoords = a_texCoord0; \n" +
                    "gl_Position = u_projTrans * a_position; \n" +
                    "}";

    static String fragmentShader = "#ifdef GL_ES\n" +
            "precision mediump float;\n" +
            "#endif\n" + "varying vec4 v_color;\n" +
            "varying vec2 v_texCoords;\n" +
            "uniform sampler2D u_texture;\n" +
            "uniform float grayscale;\n" +
            "void main()\n" +
            "{\n" +
            "vec4 texColor = texture2D(u_texture, v_texCoords);\n" +
            "float gray = dot(texColor.rgb, vec3(5, 5, 5));\n" +
            "texColor.rgb = mix(vec3(gray), texColor.rgb, grayscale);\n" +
            " gl_FragColor = v_color * texColor;\n" +
            "}";


    public static ShaderProgram whiteShader = new ShaderProgram(vertexShader,
            fragmentShader);
}