package objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;

import helpers.AssetLoader;

public class Attack {
    public int attackPower = 0;
    public int type = 4; //0 - holy, 1 - shadow, 2 - fire, 3 - water, 4 - earth, 5 - storm
    public String name = "";
    public String desc1 = "";
    public String desc2 = "";
    public float stunChance = 0;

    public TextureRegion icon;
    public float selfPower = 0;
    public float selfHealth = 0;
    public float selfDefense = 0;
    public float selfResistance = 0;
    public float enemyPower = 0;
    public float enemyHealth = 0;
    public float enemyDefense = 0;
    public float enemyResistance = 0;
    public int cooldown = 0;
    public int area = 1; //1 for 1 target, 2 for 3 person row, 3 for 3 floors
    public int targeting = 0;// -1 for allies, 0 for self, 1 for enemy
    public int id = -1;
    public int xoffset = 0;
    public int yoffset = 0;
    public AttackOverTime aot = null;
    public Animation<TextureRegion> animation = null;
    public ArrayList<Buff> buffs;

    public Attack(int id){
        buffs = new ArrayList<>();
        buildAttack(id);
        this.id = id;
    }

    private void buildAttack(int id){
        switch (id){
            case 0:
                name = "Slash";
                attackPower = 40;
                type = -1;
                desc1 = "Slash one";
                desc2 = "enemy";
                area = 1;
                targeting = 1;
                animation = AssetLoader.slashAnimation;
                break;
            case 1:
                name = "Wrath";
                attackPower = 80;
                type = -1;
                desc1 = "Double slash";
                desc2 = "one enemy";
                area = 1;
                targeting = 1;
                animation = AssetLoader.doubleAnimation;
                break;
            case 2:
                name = "Judgment";
                attackPower = 120;
                type = -1;
                desc1 = "Execute one";
                desc2 = "enemy";
                area = 1;
                targeting = 1;
                animation = AssetLoader.judgmentAnimation;
            break;
            case 3:
                name = "Law";
                attackPower = 40;
                type = 0;
                desc1 = "Lower foes";
                desc2 = "power 10%";
                enemyPower = -.1f;
                area = 2;
                targeting = 1;
                animation = AssetLoader.lightAnimation;
                break;
            case 4:
                name = "Protect";
                attackPower = 0;
                type = 0;
                desc1 = "Block 40%";
                desc2 = "allies damage";
                area = 2;
                targeting = 0;
                animation = AssetLoader.shieldAnimation;
                buffs.add(new Buff(1,.6f));
                break;
            case 5:
                name = "Heal";
                attackPower = 40;
                type = 0;
                desc1 = "Heal one";
                desc2 = "Ally";
                area  = 1;
                targeting = -1;
                animation = AssetLoader.healAnimation;
                break;
            case 6:
                name = "Barrage";
                attackPower = 40;
                type = 2;
                desc1 = "Burn all";
                desc2 = "foes";
                area = 2;
                targeting = 1;
                animation = AssetLoader.barrageAnimation;
                xoffset = -8;
                aot = new AttackOverTime(10,"Barrage",2,3,100);
                break;
            case 7:
                name = "Darkfire";
                attackPower = 40;
                type = 1;
                desc1 = "Blast all";
                desc2 = "foes";
                area = 2;
                targeting = 1;
                animation = AssetLoader.darkfireAnimation;
                xoffset = -8;
                break;
            case 8:
                name = "Snipe";
                attackPower = 100;
                type = -1;
                desc1 = "Lower foe";
                desc2 = "def 10%";
                enemyDefense = -.1f;
                area = 1;
                targeting = 1;
                animation = AssetLoader.snipeAnimation;
                break;
            case 9:
                name = "Assault";
                attackPower = 120;
                type = 4;
                desc1 = "Shoot one";
                desc2 = "foe";
                area = 1;
                targeting = 1;
                animation = AssetLoader.greenArrow3Animation;
                aot = new AttackOverTime(40,"Assault",4,3,20);
                xoffset = -8;
                break;
            case 10:
                name = "Prayer";
                attackPower = 30;
                type = 0;
                desc1 = "Heal all";
                desc2 = "allies";
                area = 2;
                targeting = -1;
                animation = AssetLoader.healAnimation;
                break;
            case 11:
                name = "Sleet";
                attackPower = 40;
                type = 3;
                desc1 = "Freeze all";
                desc2 = "foes";
                area = 2;
                targeting = 1;
                animation = AssetLoader.sleetAnimation;
                xoffset = -8;
                break;
            case 12:
                name = "Sol";
                attackPower = 120;
                type = 2;
                desc1 = "Burn one";
                desc2 = "foe";
                area = 1;
                targeting = 1;
                animation = AssetLoader.solAnimation;
                xoffset = -16;
                yoffset = -16;
                aot = new AttackOverTime(40,"Sol",2,3,20);
                break;
            case 13:
                name = "Stormbolt";
                attackPower = 40;
                type = 5;
                desc1 = "stun all";
                desc2 = "foes";
                area = 2;
                stunChance = 1;
                targeting = 1;
                animation = AssetLoader.stormAnimation;
                break;
            case 14:
                name = "Reap";
                attackPower = 120;
                type = -1;
                desc1 = "Reap one";
                desc2 = "foe soul";
                area = 1;
                targeting = 1;
                animation = AssetLoader.reapAnimation;
                yoffset = -16;
                break;
            case 15:
                name = "Haunt";
                attackPower = 20;
                type = 1;
                desc1 = "Haunt all";
                desc2 = "foes";
                area = 2;
                targeting = 1;
                animation = AssetLoader.hauntAnimation;
                aot = new AttackOverTime(20,"Haunt",1,3,100);
                break;
            case 16:
                name = "Cleave";
                attackPower = 40;
                type = -1;
                desc1 = "Slash all";
                desc2 = "enemies";
                area = 2;
                targeting = 1;
                animation = AssetLoader.slashAnimation;
                break;
            case 17:
                name = "Enrage";
                attackPower = 0;
                type = -1;
                desc1 = "Up Power";
                desc2 = "by 20%";
                selfPower = .2f;
                area = 1;
                targeting = 0;
                animation = AssetLoader.powerUpAnimation;
                break;
            case 999:
                name = "mStrike";
                attackPower = 40;
                type = -1;
                desc1 = "Strike one";
                desc2 = "enemy";
                area = 1;
                targeting = 1;
                animation = AssetLoader.smackAnimation;
                break;
        }
    }



}
