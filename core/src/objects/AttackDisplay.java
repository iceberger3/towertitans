package objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AttackDisplay {
    public Animation<TextureRegion> animation = null;
    public float time;
    public int xoffset = 0;
    public int yoffset = 0;

    public AttackDisplay(Attack a){
        animation = a.animation;
        xoffset = a.xoffset;
        yoffset = a.yoffset;
        time = 0;
    }
    public AttackDisplay(Animation a){
        animation = a;
        xoffset = 0;
        time = 0;
    }
}
