package objects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import helpers.AssetLoader;

/**
 * Created by kyle.berger on 3/2/17.
 */

public class AttackOverTime {
    private int ap; // ability power
    private String name;
    private int element = -1; // 0: holy, 1: shadow, 2: fire, 3: water/ice, 4: earth, 5: thunder\
    public int turns = 0;
    public int damage = 0;
    public int chance = 0;
    public float leech = 0;
    public Character host = null;

    public AttackOverTime(int ap, String name, int element, int turns, int chance){
        this.ap = ap;
        this.name = name;
        this.element = element;
        this.turns = turns;
        this.chance = chance;
    }

    public String getName() {
        return name;
    }

    public int getAp() {
        return ap;
    }

    public int getElement() {
        return element;
    }

    public boolean apply(){
        int random = Character.randInt(1,100);
        if(random <= chance){
            return true;
        }
        else{
            return  false;
        }
    }

    public AttackOverTime copy(int damage){
        AttackOverTime copy = new AttackOverTime(ap,name,element,turns,chance);
        copy.damage = damage;
        copy.leech = this.leech;
        return copy;
    }
    public TextureRegion getImage(float runtime){
        switch (element){
            case 0:
            default:
                return AssetLoader.hotAnimation.getKeyFrame(runtime);
            case 1:
                return AssetLoader.ghostAnimation.getKeyFrame(runtime);
            case 2:
                return AssetLoader.burnAnimation.getKeyFrame(runtime);
            case 4:
                return AssetLoader.poisonAnimation.getKeyFrame(runtime);
        }
    }
}
