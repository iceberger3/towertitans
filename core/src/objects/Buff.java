package objects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import helpers.AssetLoader;

public class Buff {
    public float effect = 1f;
    public int type = -1; //0 damage, 1 all protect, 2 melee protect, 3 magic protect
    public int turns;

    public Buff(int type, float effect){
        this.type = type;
        this.turns = 0;
        this.effect = effect;
    }

    public TextureRegion getImage(){
        switch (type){
            case 1:
                return AssetLoader.goldTankIcon;
            case 2:
                return AssetLoader.tankIcon;
            case 3:
                return AssetLoader.purpleTankIcon;
            default:
            case 0:
                return AssetLoader.dmgIcon;
        }
    }
}
