package objects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;


import java.util.ArrayList;

import game.TowerTitan;
import helpers.AssetLoader;
import objects.jobs.Magus;
import objects.jobs.Paladin;
import objects.jobs.Ranger;
import objects.jobs.Reaper;

/**
 * Created by bergerk on 12/23/15.
 */
public class Character {
    protected transient Job job;
    protected int jobId = 1;
    private int level = 0;
    private int exp;
    private String name;
    private int[] growth;
    public int[] stats;
    public transient float[] battle;
    public int currentHp;
    public Item[] items = new Item[4];
    public transient int displayHp;
    public transient int lastIndex = 0;
    public transient int stunTurns = 0;
    public int role = -1; // 1 tank, 2 dps, 3 heal
    public transient ArrayList<Buff> buffs = new ArrayList<>();
    public transient ArrayList<AttackOverTime> aots = new ArrayList<>();

    public Character() {

    }

    public Character(int level, Job job, String name) {
        this.job = job;
        this.jobId = job.getId();
        setGrowth();
        items = new Item[4];
        stats = new int[4];
        stats[0] = 100;
        stats[1] = 10;
        stats[2] = 5;
        stats[3] = 5;

        battle = new float[4];
        battle[0] = 1;
        battle[1] = 1;
        battle[2] = 1;
        battle[3] = 1;


        if(jobId > 100){
            stats[0] = 50;
        }


        this.name = name;
        exp = 0;
        this.level = 0;
        for(int i = 0; i < level; i++){
            levelUp();
        }
        this.currentHp = stats[0];
        buffs  = new ArrayList<>();
        aots = new ArrayList<>();
    }

    public void reset(){
        currentHp = getStat(0);
        displayHp = getStat(0);
        buffs.clear();
        aots.clear();
        battle = new float[4];
        battle[0] = 1;
        battle[1] = 1;
        battle[2] = 1;
        battle[3] = 1;
        stunTurns = 0;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public static int getLevelCost(int level){
        if(level > 2){
            double ceil = Math.ceil(level / 5f);
            double left = ceil * 2 * level;
            double right = ceil * 3;
            return (int)(left - right);
        }
        else if(level == 2){
            return 2;
        }
        else{
            return 1;
        }
    }

    public boolean canLevelUp(){
        if(level < 999) {
            return TowerTitan.save.save.gold >= getLevelCost(level + 1);
        }
        else{
            return false;
        }
    }

    public Job getJob() {
        return job;
    }

    public void setGrowth(){
        switch (jobId){
            case 1: //Paladin
                growth = new int[5];
                growth[0] = 1; //hp
                growth[1] = 4; //power
                growth[2] = 4; //defense
                growth[3] = 3; //resistance
                role = 1;
                break;
            case 2: //Reaper
                growth = new int[5];
                growth[0] = 1; //hp
                growth[1] = 4; //power
                growth[2] = 3; //defense
                growth[3] = 3; //resistance
                role = 2;
                break;
            case 3: //Ranger
                growth = new int[5];
                growth[0] = 1; //hp
                growth[1] = 4; //power
                growth[2] = 3; //defense
                growth[3] = 3; //resistance
                role = 2;
                break;
            case 4: //Magus
                growth = new int[5];
                growth[0] = 1; //hp
                growth[1] = 4; //power
                growth[2] = 2; //defense
                growth[3] = 4; //resistance
                role = 3;
                break;
            case 999:
                growth = new int[5];
                growth[0] = 2; //hp
                growth[1] = 4; //power
                growth[2] = 3; //defense
                growth[3] = 3; //resistance
                role = 2;
                break;
        }
    }

    public void setJob(){
        switch (jobId){
            case 1:
                job = new Paladin();
                setGrowth();
                break;
            case 2:
                job = new Reaper();
                setGrowth();
                break;
            case 3:
                job = new Ranger();
                setGrowth();
                break;
            case 4:
                job = new Magus();
                setGrowth();
                break;
        }
        this.name = job.getName();
    }

    public void levelUp(){
        stats[0] += growth[0];
        if(currentHp > 0){
            currentHp += growth[0];
        }
        stats[1] += growth[1];
        stats[2] += growth[2];
        stats[3] += growth[3];
        this.level += 1;
    }

    public int getStat(int index){
        int value = stats[index];
        for(int i = 0; i < 4; i++){
            if(items[i] != null){
                value += items[i].stats[index];
            }
        }
        return value;
    }

    public void addBattleModifier(int index, float mod){
        battle[index] += mod;
        if(battle[index] > 1.4){
            battle[index] = 1.4f;
        }
        else if(battle[index] < .7){
            battle[index] = .7f;
        }
    }

    public static int randInt(int min, int max) {
        return MathUtils.random(min, max);
    }
}
