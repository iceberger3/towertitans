package objects;

import java.util.ArrayList;

import objects.monsters.Monster;
import world.GameWorld;

public class Floor {
    public int background;
    public int floorTile;
    public int floorNum;
    public float x = 0;
    public float y;
    public ArrayList<Unit> monsters;
    public GameWorld world;

    public Floor(int b, int f, int n, float y, GameWorld w){
        this.background = b;
        this.floorTile = f;
        this.floorNum = n;
        this.y = y;
        this.world = w;
        monsters = new ArrayList<>();
        buildMonsters();
    }

    public void update(){

    }

    public void buildMonsters(){
        Unit u;
        Character c;
        int jobId = 999;
        String name = "Slime";
        for(int i = 0; i < 3; i++){
            c = new Character(floorNum,new Monster(jobId),name+i);
            u = new Unit(c,world);
            u.character.currentHp = u.character.getStat(0);
            u.character.displayHp = u.character.currentHp;
            monsters.add(u);
        }
    }
}
