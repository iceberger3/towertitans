package objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;

import helpers.AssetLoader;

/**
 * Created by bergerk on 3/4/16.
 */
public class Item {
    public transient static TextureRegion
            woodenHelm, ironHelm, cottonHat, woolHat, leatherHat, boneHelm, rangerHat, bossBandana, oakHelm, forestCap,
            hikerBandana, sageHood, wardenHelm, guardHelm, wizardHood, witchHat, whiteBandana, steelHelm, scaleHood,
            silkHat, hardHat, sniperBandana, librarianHat, gatorHood, knightHelm, cobaltHelm, salamanderHood,
            lightHat, redBandana, crystalHelm, crystalHat, crystalHood, slateHelm, obsidianHelm, ashHat, furHood,
            silverHood, goldHood, vikingHelm, vikingHat, seerBandana, parrotHood, flameHat, flameHood, flameHelm,
            lavaHelm, lavaHood, lavaHat, dragonBandana,arenaHelm,arenaHood,arenaHat,iceHelm,frostHelm,snowHat,
            starHat,sorcererHat,chainHelm,moonHelm,blueScarf,titaniumHelm,templarHelm,soulHelm,scarletHat,magusHat,
            mithrilHood,hunterHood,seerHood,astralCirclet,silverCrown,timeHat,ancientHelm,ancientMask,leafHood,
            desertHood,sunHat,hyperionHelm,rubyHelm,emeraldHood,sapphireHat,blackBandana,chaosMask,dragonBandana2,
            woodenArmor, ironArmor, cottonRobe, woolRobe, leatherArmor, boneArmor, rangerArmor, oakArmor, forestRobe,
            luckyRobe, wizardRobe, sageRobe, wardenArmor, guardArmor, witchRobe, darkGarb, lieutenantArmor, knightArmor,
            alchemistRobe, steelArmor, scaleArmor, silkRobe, hikerCoat, overalls, gatorGarb, sniperArmor, librarianRobe,
            cobaltArmor, salamanderSkin, lightRobe, paladinArmor, crystalArmor, crystalRobe, crystalScale, slateArmor,
            obsidianArmor, ashRobe, furRobe, silverArmor, goldArmor, vikingArmor, vikingVest, oracleCoat, parrotRobe,
            surfShirt, ltArmor, ltRobe, ltVest, flameArmor, flameVest, flameRobe, regalArmor, dragonCoat,regalVest,
            regalRobe,arenaRobe,arenaArmor,arenaVest,iceArmor,frostArmor,yetiArmor,snowRobe, starRobe, sorcererRobe,
            chainArmor,moonCoat,berryRobe,titaniumArmor,templarArmor,soulArmor,magusRobe,scarletRobe,mithrilArmor,
            hunterArmor,nobleCoat,seerRobe,astralRobe,princessRobe,timeRobe,leafRobe,leafGarb,ancientArmor,
            sunRobe,desertArmor,hyperionArmor,rubyArmor,emeraldArmor,sapphireRobe,secretCoat,samuraiGarb,chaosArmor,
            woodenSword, ironSword, steelSword, spiritSword, leafBlade, guardianBlade, darkBlade, giblinKnife,
            tideblade, altrius, royalBlade, crystalSword, eternity, claymore, pirateBlade, heroBlade, bigOrgoSword,
            obsidianBlade, heatBlade, magmaSword,iceFang,arenaBlade,iceSword,frostBlade,titaniumBlade, trueSteel,
            royalRapier,astralBlade,hourHand,ancientBlade,katana,excalibur, altrius2,medusa,
            woodenAxe, ironAxe, steelAxe, minerPick, hatchet, heroAxe, gatorEdge, magmaBlade, hugeAxe, crystalAxe,
            battleAxe, vikingAxe, pirateHook, obsidianAxe, magmaAxe, dragonAxe,arenaAxe,iceAxe,frostAxe,frostBane,
            sailorAxe,titaniumAxe,soulAxe,ancientAxe,treefell,souleater,
            woodenStaff, ironStaff, treeBranch, witchWand, lytesLantern, mendingStaff, steelStaff, iceRod, librarianStaff,
            crystalStaff, warStaff, fishingPole, tsunami, obsidianStaff, pyreStaff, blizzard,arenaStaff,iceStaff,
            frostStaff,thunderbolt, titaniumStaff, flowerFire,ancientStaff,stormLash,sapphireStaff,
            woodenBow, ironBow, rangerBow, skypiercer, spiderBow, steelBow, hurricane, knightBow, crystalBow, crossbow,
            cannon, heroBow, goldenHarp, obsidianBow, magmaBow, artillery,arenaBow,iceStar,moonBow,starFall,titaniumStar,
            snowStar,ancientBow,lionfall,elfnir,
            woodenSpear, ironSpear, silverSpear, forestSpear, warLance, steelLance, minerDrill, battleSpear, crystalSpear,
            halberd, harpoon, leviathan, doomscythe, obsidianSpear, magmaLance, lawmaker, oldLance, serpentLance, orgoLance,
            dragonBane, lavaLance,arenaLance, iceLance, frostLance,titaniumLance,arbiter,ancientLance,hyperionLance,dragonBane2,
            woodenHammer, ironHammer, minerShovel, heavyStone, stoneGuard, holyHammer, steelHammer, heavyHammer, goldHammer,
            crystalHammer, morningStar, aquaFist, pegLeg, heroHammer, obsidianHammer, magmaMace,stormHammer,arenaMace,
            titaniumMace, iceHammer, frostMace, fryingPan,justice,ancientHammer,deathMace,trueHeart,
            forestCloak, blanket, lumaCloak, heavyArms, hillCloak, wizardCloak, eagleCloak, guardCloak, redQuilt,
            duskCloak, giblinCloak, sewerCloak, sniperCloak, courageCloak, wisdomCloak, powerCloak, royalCloak, knightCloak,
            secretCloak, vikingCloak, seaCloak, blastCloak, charCloak, venusCloak, choirCloak, slimeCloak, spiritCloak,
            regalCloak,prismaCloak,huskyCloak, stormCloak, moonCloak,templarCloak,hunterCloak,magusCloak, soulCloak,prisma2Cloak,
            metalCloak, lightCloak, flameCloak, vangardeCloak,princessShawl,astralCover,timeCloak,arcticCloak,zapCloak,
            moltenCloak,kingsCloak,shadoCloak,chaosCloak,tigerCloak,serpentCloak,bearCloak,turtleCloak,dragonCloak,pantherCloak,
            luckyLeaf, silverChain, petFox, spiritJar, pocketShield, bearTooth, luckyDice, oldBook,
            lytesLocket, darkBook, redLeaf, toyCart, blackArrow, tinyHorror, lifeFlower, spiritFlower,
            holyBook, dragonTome, battery, steelWeight, medal, lightsMight, pocketChest, roundShield, bomb,
            blueLeaf, icebook, poisonVial, sandsOfTime, anchor,dragonFang,iceCube,hotPocket,rubberDuck,
            eagleFeather, mushroom, ghostJelly, magicLeaf, sapphire, emerald, ruby, rose, darkGem, lightGem, turtleShell,
            sandDollar, musicBook, orgoBoots, orgoHorn, viperVenom, rawObsidian, dragonScale, rainbowScale,
            huskyFang,blueBerry,letter,cloth,cookieBox, bombPowder, miniHeater,flameJar,tigerFang,soulfire,
            eagleEgg,nobleBook,pocketMirror,scarlettesRibbon,magicHourglass,timeKey,magicGem,diamondRing,
            invitation,trullHorn,royalNote,lionFang,nightfire,battleCharm,dragonOre,goldScale,magicFeather,
            magicRing,magicShield,
            dragonArmor,dragonHelm,eagleArmor,eagleHelm,wildHelm,wildArmor,titanHelm,titanArmor,dragonHood,
            dragonGarb,wildHood,wildGarb,eagleHood,eagleGarb,titanHood,titanGarb,dragonCap,dragonRobe,eagleRobe,
            eagleCap,wildCap,wildRobe,titanCap,titanRobe,mimikingu,

    bone, cotton, fur, gold, iron, leather, obsidian, scale, silk, silver, slate, steel, wood, wool, ash,
            snow, ice, chain,mithril,scarlet,titanium,darkMatter;
    public transient static Texture load;
    public int id = 0;
    public int role = 0; //0 all, 1 tank, 2 dps, 3 heal
    public int[] stats = new int[4];
    public int slot = 0; //0 helm, 1 cloak, 2 armor, 3 weapon
    public int rarity = 0;
    public String name = "";
    public String message = "";
    public float crit = 0;
    public float melee = 0;
    public float spell = 0;
    public float resistence = 0;
    public float defense = 0;
    public float speed = 0;
    public float power = 0;
    public float health = 0;
    public float agility = 0;
    public float piercing = 0;
    public float reflect = 0;
    public float elementResistance = 0; //none
    public int elementType = -2; //no type -3 == all types of magic
    public int upgradeCount = 0;
    public int tl = 0;
    public Item(){

    }
    public Item(int id, int towerLevel) {
        this.id = id;
        setValues(id,towerLevel);
    }

    public void setValues(int id, int towerLevel) {
        //get tier of equipment. ex. tower level 5 -> 20, 35 -> 40
        tl = ((towerLevel / 20)) * 20;
        int tl2 = tl / 2;
        int tl5 = tl / 5;
        int tl10 = tl / 10;
        for (int i = 0; i < 4; i++) {
            stats[i] = 0; // 0 hp, 1 pow, 2 def, 3 res
        }
        switch (id) {
            case 1:
                name = "Wood Armor";
                stats[2] = 2 * (tl5);
                role = 1;
                slot = 2;
                break;
            case 2:
                name = "Wood Helm";
                stats[2] = tl5;
                role = 1;
                slot = 0;
                break;
            case 3:
                name = "Wood Sword";
                stats[1] = tl2;
                role = 1;
                slot = 3;
                break;
            case 4:
                name = "Wood Axe";
                stats[1] = tl2;
                role = 2;
                slot = 3;
                break;
            case 5:
                name = "Iron Sword";
                stats[1] = tl2;
                role = 1;
                slot = 3;
                break;
            case 6:
                name = "Iron Axe";
                stats[1] = tl2 ;
                role = 2;
                slot = 3;
                break;
            case 7:
                name = "Steel Sword";
                stats[1] = tl2;
                role = 1;
                slot = 3;
                break;
            case 8:
                name = "Steel Axe";
                stats[1] = tl2;
                role = 2;
                slot = 3;
                break;
            case 9:
                name = "Woods Cloak";
                stats[1] = tl5;
                stats[0] = tl5;
                slot = 1;
                rarity = 1;
                break;
            case 11:
                name = "Iron Armor";
                stats[2] = 3 * (tl10);
                stats[3] = tl10;
                role = 1;
                slot = 2;
                break;
            case 12:
                name = "Cotton Robe";
                stats[2] = 2 * (tl5);
                role = 3;
                slot = 2;
                break;
            case 13:
                name = "Wool Robe";
                stats[2] = tl10;
                stats[3] = 3 * (tl10);
                role = 3;
                slot = 2;
                break;
            case 14:
                name = "Leather Coat";
                stats[2] = tl5;
                stats[3] = tl5;
                role = 2;
                slot = 2;
                break;
            case 15:
                name = "Bone Armor";
                stats[2] = tl5;
                stats[3] = tl5;
                role = 2;
                slot = 2;
                break;
            case 16:
                name = "Iron Helm";
                stats[2] = tl5;
                role = 1;
                slot = 0;
                break;
            case 17:
                name = "Cotton Hat";
                stats[3] = tl5;
                role = 3;
                slot = 0;
                break;
            case 18:
                name = "Wool Hat";
                stats[3] = tl5;
                role = 3;
                slot = 0;
                break;
            case 19:
                name = "Leather Hat";
                stats[2] = tl10;
                stats[3] = tl10;
                role = 2;
                slot = 0;
                break;
            case 20:
                name = "Bone Helm";
                stats[2] = tl10;
                stats[3] = tl10;
                role = 2;
                slot = 0;
                break;
            case 21:
                name = "Wood Staff";
                stats[1] = tl2;
                role = 3;
                slot = 3;
                break;
            case 22:
                name = "Iron Staff";
                stats[1] = tl2;
                role = 3;
                slot = 3;
                break;
            case 23:
                name = "Wood Bow";
                stats[1] = tl2;
                role = 2;
                slot = 3;
                break;
            case 24:
                name = "Iron Bow";
                stats[1] = tl2;
                role = 2;
                slot = 3;
                break;
            case 25:
                name = "Wood Spear";
                stats[1] = tl2;
                role = 1;
                slot = 3;
                break;
            case 26:
                name = "Iron Spear";
                stats[1] = tl2;
                role = 1;
                slot = 3;
                break;
            case 27:
                name = "Wood Hammer";
                stats[1] = tl2;
                role = 3;
                slot = 3;
                break;
            case 28:
                name = "Iron Hammer";
                stats[1] = tl2;
                role = 3;
                slot = 3;
                break;
            case 30:
                name = "Blanket";
                stats[0] = tl5;
                slot = 1;
                break;
            case 31:
                name = "Spirit Sword";
                stats[1] = (tl2) + (3 * (tl5));
                stats[2] = tl5;
                stats[3] = 2 * (tl5);
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 32:
                name = "Ranger Bow";
                stats[1] = (tl2) + (tl5);
                stats[0] = tl5;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 33:
                name = "Ranger Hat";
                stats[2] = tl5;
                stats[3] = tl5;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 34:
                name = "Ranger Coat";
                stats[0] = tl5;
                stats[2] = 3 * (tl10);
                stats[3] = 3 * (tl10);
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 35:
                name = "Miner Shovel";
                stats[1] = (tl2) + 2 * (tl5);
                stats[0] = tl5;
                stats[2] = tl5;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 36:
                name = "Miner Pick";
                stats[1] = (tl2) + 2 * (tl5);
                stats[0] = 2 * (tl5);
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 37:
                name = "Boss Bandana";
                stats[0] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 0;
                slot = 0;
                rarity = 2;
                break;
            case 38:
                name = "Silver Lance";
                stats[1] = (tl2) + 2 *(tl5);
                stats[3] = 2 *(tl5);
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 40:
                name = "Luma Cloak";
                stats[0] = tl5;
                stats[3] = tl5;
                stats[5] = tl5;
                slot = 1;
                rarity = 2;
                break;
            case 42:
                name = "Oak Armor";
                stats[2] = 3 * tl5;
                stats[0] = tl5;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 43:
                name = "Oak Helm";
                stats[2] = 3 * tl10;
                stats[0] = tl10;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 44:
                name = "Spirit Robe";
                stats[3] = 3 * tl5;
                stats[0] = tl5;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 45:
                name = "Spirit Cap";
                stats[3] = 3 * tl10;
                stats[0] = tl10;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 46:
                name = "Leaf Blade";
                stats[1] = (tl2) + (tl5);
                stats[0] = tl5;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 47:
                name = "Hatchet";
                stats[1] = (tl2) + 2 * (tl5);
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 48:
                name = "Tree Branch";
                stats[1] = (tl2) + (tl5);
                stats[0] = tl5;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 49:
                name = "Heavy Stone";
                stats[1] = (tl2) + (tl5);
                stats[2] = tl5;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 50:
                name = "Pointy Stick";
                stats[1] = (tl2) + (tl5);
                stats[3] = tl5;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 52:
                name = "HeavyArms";
                stats[0] = tl5;
                stats[1] = 3 * (tl5);
                rarity = 3;
                slot = 1;
                break;
            case 55:
                name = "Stoneguard";
                stats[1] = tl2 + 2*tl5;
                stats[0] = tl5;
                stats[2] = tl5;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 56:
                name = "Skypiercer";
                stats[1] = tl2 + 3*tl5;
                stats[3] = tl5;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 57:
                name = "Hiker Bandana";
                stats[2] = tl10;
                stats[1] = tl5;
                stats[3] = tl10;
                role = 0;
                slot = 0;
                rarity = 1;
                break;
            case 58:
                name = "Hill Cloak";
                stats[0] = tl5;
                stats[2] = tl5;
                role = 0;
                slot = 1;
                rarity = 1;
                break;
            case 60:
                name = "Lucky Robe";
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 61:
                name = "Wizard Cloak";
                stats[0] = tl5;
                stats[1] = tl5 * 2;
                stats[3] = tl5;
                rarity = 3;
                slot = 1;
                break;
            case 62:
                name = "Wizard Robe";
                stats[1] = tl5;
                stats[2] = tl5;
                stats[3] = tl5 * 2;
                role = 3;
                slot = 2;
                rarity = 2;
                break;
            case 66:
                name = "Eagle Cloak";
                stats[0] = tl5;
                stats[1] = tl5;
                role = 0;
                slot = 1;
                rarity = 1;
                break;
            case 68:
                name = "Warden Helm";
                stats[2] = tl5;
                stats[3] = tl5;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 69:
                name = "Warden Armor";
                stats[1] = tl5;
                stats[2] = tl5 + tl10;
                stats[3] = tl5 + tl10;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 70:
                name = "Sage Hood";
                stats[0] = tl5;
                stats[2] = tl10;
                stats[3] = tl5 + tl10;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 71:
                name = "Sage Robe";
                stats[0] = tl5;
                stats[2] = tl5;
                stats[3] = 2 * tl5;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 72:
                name = "Guard Helm";
                stats[0] = tl10;
                stats[2] = tl5;
                stats[3] = tl10;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 73:
                name = "Guard Armor";
                stats[0] = tl5;
                stats[2] = 2 * tl5;
                stats[3] = tl5;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 75:
                name = "Wizard Hood";
                stats[1] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 3;
                slot = 0;
                rarity = 2;
                break;
            case 76:
                name = "Guard Cloak";
                stats[0] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 77:
                name = "Guardian Blade";
                stats[2] = tl5;
                stats[1] = tl2 + tl5;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 78:
                name = "Hero Axe";
                stats[1] = tl2 + 2 * tl5;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 79:
                name = "Witch's Wand";
                stats[1] = tl2 + tl5;
                stats[0] = tl5;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 80:
                name = "Lyte's Lantern";
                stats[1] = tl2 + 2 * tl5;
                stats[0] = 2 * tl5;
                stats[3] = 2 * tl5;
                role = 3;
                slot = 3;
                rarity = 3;
                break;
            case 81:
                name = "Spider Bow";
                stats[1] = tl2 + tl5;
                stats[3] = tl5;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 82:
                name = "War Lance";
                stats[1] = tl2 + tl5;
                stats[0] = tl5;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 83:
                name = "Holy Hammer";
                stats[1] = tl2 + tl5;
                stats[2] = tl5;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 86:
                name = "Dark Book";
                message = "Up dark Dmg 20%";
                role = 0;
                slot = 4;
                rarity = 1;
                break;
            case 87:
                name = "Witch Hat";
                stats[2] = tl10;
                stats[3] = tl5 + tl10;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 88:
                name = "Witch Robe";
                stats[2] = tl5;
                stats[3] = tl5 * 3;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 89:
                name = "Dusk Cloak";
                stats[0] = tl5;
                stats[3] = tl5;
                role = 0;
                slot = 1;
                rarity = 1;
                break;
            case 90:
                name = "Dark Blade";
                stats[1] = tl2 + tl5 * 2;
                stats[0] = tl5;
                stats[3] = tl5;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 91:
                name = "Corporal Armor";
                stats[0] = tl5 * 2;
                stats[2] = tl5 * 3;
                stats[3] = tl5;
                role = 1;
                slot = 2;
                rarity = 2;
                break;
            case 92:
                name = "Dark Garb";
                stats[1] = tl5 * 2;
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 94:
                name = "Alchemist Robe";
                stats[0] = tl5 * 2;
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 95:
                name = "White Bandana";
                stats[0] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 0;
                slot = 0;
                rarity = 2;
                break;
            case 96:
                name = "Mending Staff";
                stats[1] = tl2 + tl5*2;
                stats[0] = tl5;
                stats[3] = tl5;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 98:
                name = "Steel Staff";
                stats[1] = tl2;
                role = 3;
                slot = 3;
                break;
            case 99:
                name = "Steel Bow";
                stats[1] = tl2;
                role = 2;
                slot = 3;
                break;
            case 100:
                name = "Steel Spear";
                stats[1] = tl2;
                role = 1;
                slot = 3;
                break;
            case 101:
                name = "Steel Hammer";
                stats[1] = tl2;
                role = 3;
                slot = 3;
                break;
            case 102:
                name = "Silk Hat";
                stats[3] = tl5;
                role = 3;
                slot = 0;
                break;
            case 103:
                name = "Silk Robe";
                stats[2] = tl10;
                stats[3] = tl5 + tl10;
                role = 3;
                slot = 2;
                break;
            case 104:
                name = "Scale Hood";
                stats[2] = tl10;
                stats[3] = tl10;
                role = 2;
                slot = 0;
                break;
            case 105:
                name = "Scale Armor";
                stats[2] = tl5;
                stats[3] = tl5;
                role = 2;
                slot = 2;
                break;
            case 106:
                name = "Steel Helm";
                stats[2] = tl5;
                role = 1;
                slot = 0;
                break;
            case 107:
                name = "Steel Armor";
                stats[2] = tl5 + tl10;
                stats[3] = tl10;
                role = 1;
                slot = 2;
                break;
            case 108:
                name = "Giblin Knife";
                stats[1] = tl2 + tl5;
                stats[0] = tl5;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 109:
                name = "Giblin Cloak";
                stats[1] = tl5;
                stats[0] = tl5;
                role = 0;
                slot = 1;
                rarity = 1;
                break;
            case 110:
                name = "Hiker Coat";
                stats[2] = 2 * tl5;
                stats[3] = 2 * tl5;
                role = 0;
                slot = 2;
                rarity = 1;
                break;
            case 114:
                name = "Overalls";
                stats[0] = tl5;
                stats[2] = tl5 + tl10;
                stats[3] = tl5 + tl10;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 115:
                name = "Hard Hat";
                stats[2] = tl5;
                stats[3] = tl5;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 116:
                name = "Miner Drill";
                stats[1] = tl2 + 2* tl5;
                stats[0] = tl5;
                stats[2] = tl5;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 119:
                name = "Tideblade";
                stats[1] = tl2 + 2* tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 120:
                name = "Sewer Cloak";
                stats[1] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 121:
                name = "Gator Edge";
                stats[1] = tl2 + 2 * tl5;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 122:
                name = "Gator Garb";
                stats[1] = tl10;
                stats[2] = tl5 + tl10;
                stats[3] = tl5 + tl10;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 124:
                name = "Hurricane";
                stats[1] = tl2 + 2 * tl5;
                stats[2] = tl5 * 2;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 125:
                name = "Sniper Coat";
                stats[1] = tl5 * 2;
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 126:
                name = "Sniper Bandana";
                stats[1] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 0;
                slot = 0;
                rarity = 2;
                break;
            case 127:
                name = "Sniper Cloak";
                stats[1] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 129:
                name = "Courage Cloak";
                stats[0] = tl5 * 2;
                stats[1] = tl5;
                stats[2] = tl5;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 130:
                name = "Wisdom Cloak";
                stats[0] = tl5;
                stats[1] = tl5;
                stats[3] = tl5 * 2;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 131:
                name = "Power Cloak";
                stats[0] = tl5 * 2;
                stats[1] = tl5 * 2;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 137:
                name = "Librarian Robe";
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 138:
                name = "Librarian Hat";
                stats[2] = tl5;
                stats[3] = tl5;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 139:
                name = "Gator Hood";
                stats[2] = tl5;
                stats[3] = tl5;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 140:
                name = "Knight Helm";
                stats[0] = tl10;
                stats[2] = tl5;
                stats[3] = tl10;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 141:
                name = "Knight Armor";
                stats[0] = tl5;
                stats[2] = tl5 * 2;
                stats[3] = tl5;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 142:
                name = "Magma Edge";
                stats[1] = tl2 + 3 * tl5;
                stats[0] = 2 * tl5;
                role = 2;
                slot = 3;
                rarity = 3;
                break;
            case 143:
                name = "Ice Rod";
                stats[1] = tl2 + 2* tl5;
                stats[0] = tl5;
                stats[2] = tl5;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 144:
                name = "Librarian Staff";
                stats[1] = tl2 + tl5;
                stats[3] = tl5;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 145:
                name = "Knight Bow";
                stats[1] = tl2 + tl5;
                stats[2] = tl5;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 146:
                name = "Battle Lance";
                stats[1] = tl2 + tl5;
                stats[0] = tl5;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 147:
                name = "Heavy Hammer";
                stats[1] = tl2 + tl5;
                stats[2] = tl5;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 148:
                name = "Gold Hammer";
                stats[1] = tl2 + 2* tl5;
                stats[2] = 2* tl5;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 150:
                name = "Cobalt Armor";
                stats[2] = tl5 * 3;
                stats[3] = tl5 * 3;
                role = 1;
                slot = 2;
                rarity = 2;
                break;
            case 151:
                name = "Cobalt Helm";
                stats[0] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 1;
                slot = 0;
                rarity = 2;
                break;
            case 152:
                name = "Samander Skin";
                stats[1] = tl5 * 2;
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 153:
                name = "Samander Hood";
                stats[1] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 2;
                slot = 0;
                rarity = 2;
                break;
            case 154:
                name = "Light Robe";
                stats[2] = tl5;
                stats[3] = tl5 * 3;
                stats[0] = tl5 * 2;
                role = 3;
                slot = 2;
                rarity = 2;
                break;
            case 155:
                name = "Light Hat";
                stats[2] = tl10;
                stats[3] = tl5 + tl10;
                stats[5] = tl5;
                role = 3;
                slot = 0;
                rarity = 2;
                break;
            case 156:
                name = "Red Bandana";
                stats[0] = tl5;
                stats[2] = tl5 + tl10;
                stats[3] = tl5 + tl10;
                role = 0;
                slot = 0;
                rarity = 3;
                break;
            case 157:
                name = "Paladin Armor";
                stats[0] = tl5 * 2;
                stats[2] = tl5 * 4;
                stats[3] = tl5 * 2;
                role = 1;
                slot = 2;
                rarity = 3;
                break;
            case 158:
                name = "Huge Axe";
                stats[1] = tl2 + 3 * tl5;
                stats[2] = tl5;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 159:
                name = "Royal Blade";
                stats[1] = tl2 + 2* tl5;
                stats[3] = tl5 * 2;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 160:
                name = "Altrius";
                stats[1] = tl2 + tl5 * 4;
                stats[0] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 1;
                slot = 3;
                rarity = 4;
                break;
            case 161:
                name = "Knight Cloak";
                stats[0] = tl5;
                stats[2] = tl5;
                stats[3] = tl5;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 162:
                name = "Royal Cloak";
                stats[0] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 167:
                name = "Crystal Helm";
                stats[2] = tl5 * 3;
                stats[3] = tl5;
                role = 1;
                slot = 0;
                rarity = 3;
                break;
            case 168:
                name = "Crystal Armor";
                stats[2] = tl5 * 6;
                stats[3] = tl5 * 2;
                role = 1;
                slot = 2;
                rarity = 3;
                break;
            case 169:
                name = "Crystal Hood";
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 2;
                slot = 0;
                rarity = 3;
                break;
            case 170:
                name = "Crystal Scale";
                stats[2] = tl5 * 4;
                stats[3] = tl5 * 4;
                role = 2;
                slot = 2;
                rarity = 3;
                break;
            case 171:
                name = "Crystal Hat";
                stats[2] = tl5;
                stats[3] = tl5 * 3;
                role = 3;
                slot = 0;
                rarity = 3;
                break;
            case 172:
                name = "Crystal Robe";
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 6;
                role = 3;
                slot = 2;
                rarity = 3;
                break;
            case 173:
                name = "Crystal Staff";
                stats[1] = tl2 + tl5 * 3;
                stats[3] = tl5 * 3;
                role = 3;
                slot = 3;
                rarity = 3;
                break;
            case 174:
                name = "Crystal Axe";
                stats[1] = tl2 + tl5 * 4;
                stats[0] = tl5 * 2;
                role = 2;
                slot = 3;
                rarity = 3;
                break;
            case 175:
                name = "Crystal Bow";
                stats[1] = tl2 + tl5 * 3;
                stats[3] = tl5 * 3;
                role = 2;
                slot = 3;
                rarity = 3;
                break;
            case 176:
                name = "Crystal Hammer";
                stats[1] = tl2 + tl5 * 3;
                stats[2] = tl5 * 3;
                role = 3;
                slot = 3;
                rarity = 3;
                break;
            case 177:
                name = "Crystal Spear";
                stats[1] = tl2 + tl5 * 3;
                stats[3] = tl5 * 3;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 178:
                name = "Crystal Sword";
                stats[1] = tl2 + tl5 * 3;
                stats[0] = tl5 * 3;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 180:
                name = "Eternity";
                stats[1] = tl2 + tl5 * 4;
                stats[2] = tl5 * 2;
                stats[3] = tl5 * 2;
                role = 0;
                slot = 3;
                rarity = 4;
                break;
            case 181:
                name = "Slate Armor"; // 18 total, +6 each rarity
                stats[3] = 12;
                stats[5] = 6;
                role = 1;
                slot = 2;
                rarity = 0;
                break;
            case 182:
                name = "Slate Helm";
                stats[3] = 6;
                stats[5] = 3;
                role = 1;
                slot = 0;
                rarity = 0;
                break;
            case 183:
                name = "Silver Armor"; // 18 total, +6 each rarity
                stats[3] = 9;
                stats[5] = 9;
                role = 2;
                slot = 2;
                rarity = 0;
                break;
            case 184:
                name = "Silver Hood";
                stats[3] = 5;
                stats[5] = 4;
                role = 2;
                slot = 0;
                rarity = 0;
                break;
            case 185:
                name = "Fur Robe"; // 18 total, +6 each rarity
                stats[3] = 6;
                stats[5] = 12;
                role = 3;
                slot = 2;
                rarity = 0;
                break;
            case 186:
                name = "Fur Hood";
                stats[3] = 3;
                stats[5] = 6;
                role = 3;
                slot = 0;
                rarity = 0;
                break;
            case 187:
                name = "Obsidian Armor"; // 30 total, +10 each rarity
                stats[3] = 20;
                stats[5] = 10;
                role = 1;
                slot = 2;
                rarity = 0;
                break;
            case 188:
                name = "Obsidian Helm";
                stats[3] = 10;
                stats[5] = 5;
                role = 1;
                slot = 0;
                rarity = 0;
                break;
            case 189:
                name = "Gold Armor"; // 30 total, +10 each rarity
                stats[3] = 15;
                stats[5] = 15;
                role = 2;
                slot = 2;
                rarity = 0;
                break;
            case 190:
                name = "Gold Hood";
                stats[3] = 8;
                stats[5] = 7;
                role = 2;
                slot = 0;
                rarity = 0;
                break;
            case 191:
                name = "Ash Robe"; // 30 total, +10 each rarity
                stats[3] = 10;
                stats[5] = 20;
                role = 3;
                slot = 2;
                rarity = 0;
                break;
            case 192:
                name = "Ash Hat";
                stats[3] = 5;
                stats[5] = 10;
                role = 3;
                slot = 0;
                rarity = 0;
                break;
            case 193:
                name = "Claymore";
                stats[1] = 16;
                role = 1;
                slot = 3;
                break;
            case 194:
                name = "Battle Axe";
                stats[1] = 16;
                role = 2;
                slot = 3;
                break;
            case 195:
                name = "War Staff";
                stats[1] = 16;
                role = 3;
                slot = 3;
                break;
            case 196:
                name = "Crossbow";
                stats[1] = 16;
                role = 2;
                slot = 3;
                break;
            case 197:
                name = "Halberd";
                stats[1] = 16;
                role = 1;
                slot = 3;
                break;
            case 198:
                name = "Morning Star";
                stats[1] = 16;
                role = 3;
                slot = 3;
                break;
            case 199:
                name = "Secret Cloak"; //Tier 4 Green cloak 18 total, blue +9, purple +18
                stats[1] = 6;
                stats[2] = 9;
                stats[3] = 12;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 200:
                name = "Viking Armor"; // 18 total, +6 each rarity
                stats[0] = 8;
                stats[3] = 16;
                stats[5] = 8;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 201:
                name = "Viking Helm";
                stats[0] = 4;
                stats[3] = 8;
                stats[5] = 4;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 202:
                name = "Viking Vest"; // 18 total, +6 each rarity
                stats[2] = 8;
                stats[3] = 12;
                stats[5] = 12;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 203:
                name = "Viking Cap";
                stats[2] = 4;
                stats[3] = 6;
                stats[5] = 6;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 204:
                name = "Round Shield";
                role = 0;
                slot = 4;
                rarity = 3;
                message = "30% reduced damage";
                break;
            case 205:
                name = "Viking Axe";
                stats[1] = 20;
                stats[2] = 8;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 206:
                name = "Viking Cloak"; //Tier 4 Green cloak 18 total, blue +9, purple +18
                stats[0] = 6;
                stats[1] = 8;
                stats[2] = 4;
                role = 0;
                slot = 1;
                rarity = 1;
                break;
            case 207:
                name = "Oracle Coat"; // 18 total, +6 each rarity
                stats[3] = 12;
                stats[5] = 20;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 208:
                name = "Sea Bandana";
                stats[1] = 5;
                stats[3] = 9;
                stats[5] = 9;
                role = 0;
                slot = 0;
                rarity = 2;
                break;
            case 209:
                name = "Sea Cloak"; //Tier 4 Green cloak 18 total, blue +9, purple +18
                stats[0] = 6;
                stats[3] = 9;
                stats[5] = 12;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 210:
                name = "Blue Leaf";
                message = "Up Def/Res 15%";
                role = 0;
                slot = 4;
                rarity = 2;
                defense = .15f;
                resistence = .15f;
                break;
            case 211:
                name = "Aqua Fist";
                stats[1] = 24;
                stats[3] = 6;
                stats[5] = 10;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 212:
                name = "Pirate Hook";
                stats[1] = 20;
                stats[2] = 8;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 213:
                name = "Pirate Blade";
                stats[1] = 20;
                stats[2] = 8;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 214:
                name = "Fishing Pole";
                stats[1] = 20;
                stats[2] = 8;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 215:
                name = "Harpoon";
                stats[1] = 20;
                stats[2] = 8;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 216:
                name = "Peg Leg";
                stats[1] = 20;
                stats[2] = 8;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 217:
                name = "Cannon";
                stats[1] = 20;
                stats[2] = 8;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 218:
                name = "Tsunami";
                stats[1] = 24;
                stats[0] = 8;
                stats[5] = 8;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 219:
                name = "Leviathan";
                stats[1] = 24;
                stats[0] = 8;
                stats[2] = 8;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 220:
                name = "Parrot Robe"; // 18 total, +6 each rarity
                stats[2] = 8;
                stats[3] = 8;
                stats[5] = 16;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 221:
                name = "Parrot Hood"; // 18 total, +6 each rarity
                stats[0] = 4;
                stats[3] = 4;
                stats[5] = 8;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 222:
                name = "Cannonball";
                message = "Hits all 15% Fire";
                role = 0;
                slot = 4;
                rarity = 3;
                break;
            case 223:
                name = "Bone";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 224:
                name = "Cotton";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 225:
                name = "Fur";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 226:
                name = "Raw Gold";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 227:
                name = "Iron";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 228:
                name = "Leather";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 229:
                name = "Obsidian";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 230:
                name = "Scale";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 231:
                name = "Silk";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 232:
                name = "Raw Silver";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 233:
                name = "Slate";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 234:
                name = "Steel";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 235:
                name = "Wood";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 236:
                name = "Wool";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 237:
                name = "Hero Blade";
                stats[1] = 2;
                stats[3] = 1;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 238:
                name = "Hero Bow";
                stats[1] = 2;
                stats[5] = 1;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 239:
                name = "Hero Hammer";
                stats[1] = 2;
                stats[0] = 1;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 240:
                name = "Surf Shirt";
                stats[2] = 16;
                stats[3] = 20;
                stats[5] = 10;
                role = 0;
                slot = 2;
                rarity = 2;
                break;
            case 241:
                name = "Turtle Shell";
                message = "Great for soup.";
                rarity = 1;
                slot = 5;
                break;
            case 242:
                name = "Venus Cloak"; //Tier 5 Green cloak 24 total, blue +12, purple +24
                stats[0] = 16; // hp
                stats[1] = 16; // pow
                stats[5] = 16; // res
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 243:
                name = "Char Cloak"; //Tier 5 Green cloak 24 total, blue +12, purple +24
                stats[0] = 24; // hp
                stats[1] = 16; // pow
                stats[2] = 8; // agi
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 244:
                name = "Blast Cloak"; //Tier 5 Green cloak 24 total, blue +12, purple +24
                stats[1] = 16; // pow
                stats[3] = 24; // def
                stats[5] = 8; // res
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 245:
                name = "Ice Tome";
                message = "+40% Ice/Water Spells";
                rarity = 2;
                slot = 4;
                break;
            case 246:
                name = "Lt. Armor";
                stats[2] = 16;
                stats[3] = 20;
                stats[5] = 10;
                role = 1;
                slot = 2;
                rarity = 2;
                break;
            case 247:
                name = "Lt. Garb";
                stats[2] = 16;
                stats[3] = 15;
                stats[5] = 15;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 248:
                name = "Lt. Robe";
                stats[2] = 16;
                stats[3] = 10;
                stats[5] = 20;
                role = 3;
                slot = 2;
                rarity = 2;
                break;
            case 249:
                name = "Sand Dollar";
                message = "Seas the day!";
                rarity = 1;
                slot = 5;
                break;
            case 250:
                name = "Poison Vial";
                message = "Poison 10%, 3 turns";
                rarity = 2;
                slot = 4;
                break;
            case 251:
                name = "Music Book";
                message = "Sought by Bards";
                rarity = 2;
                slot = 5;
                break;
            case 252:
                name = "Orgo Boots";
                message = "Walk on hot sand";
                rarity = 3;
                slot = 5;
                break;
            case 253:
                name = "Big Orgo Sword";
                stats[1] = 28;
                stats[0] = 14;
                stats[3] = 10;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 254:
                name = "Music Book";
                message = "+5% stat changes";
                rarity = 2;
                slot = 4;
                break;
            case 255:
                name = "Choir Cloak"; //Tier 4 Green cloak 18 total, blue +9, purple +18
                stats[0] = 12;
                stats[2] = 12;
                stats[5] = 12;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 256:
                name = "Golden Harp";
                stats[1] = 24;
                stats[0] = 8;
                stats[2] = 8;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 257:
                name = "Orgo Horn";
                message = "Waylan needs this.";
                rarity = 2;
                slot = 5;
                break;
            case 258:
                name = "Slime Cloak";
                stats[0] = 2;
                stats[2] = 2;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 259:
                name = "Spirit Cloak";
                stats[0] = 2;
                stats[1] = 2;
                stats[5] = 2;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 260:
                name = "Doomscythe";
                stats[1] = 10;
                stats[2] = 6;
                stats[5] = 4;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 261:
                name = "Sands of Time";
                message = "Always act first";
                slot = 4;
                rarity = 4;
                break;
            case 262:
                name = "Obsidian Sword";
                stats[1] = 25;
                role = 1;
                slot = 3;
                break;
            case 263:
                name = "Obsidian Axe";
                stats[1] = 25;
                role = 2;
                slot = 3;
                break;
            case 264:
                name = "Obsidian Staff";
                stats[1] = 25;
                role = 3;
                slot = 3;
                break;
            case 265:
                name = "Obsidian Bow";
                stats[1] = 25;
                role = 2;
                slot = 3;
                break;
            case 266:
                name = "Obsidian Spear";
                stats[1] = 25;
                role = 1;
                slot = 3;
                break;
            case 267:
                name = "Obsidian Hammer";
                stats[1] = 25;
                role = 3;
                slot = 3;
                break;
            case 268:
                name = "Magma Mace";
                stats[1] = 30;
                stats[2] = 10;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 269:
                name = "Pyre Staff";
                stats[1] = 30;
                stats[0] = 10;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 270:
                name = "Ash";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 271:
                name = "Magma Blade";
                stats[1] = 30;
                stats[0] = 10;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 272:
                name = "Magma Axe";
                stats[1] = 30;
                stats[5] = 10;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 273:
                name = "Magma Bow";
                stats[1] = 30;
                stats[2] = 10;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 274:
                name = "Magma Lance";
                stats[1] = 30;
                stats[3] = 10;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 275:
                name = "Artillery";
                stats[1] = 35;
                stats[2] = 8;
                stats[0] = 12;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 276:
                name = "Heat Blade";
                stats[1] = 35;
                stats[5] = 8;
                stats[0] = 12;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 277:
                name = "Flame Armor"; // 30 total, +10 each rarity
                stats[0] = 10;
                stats[3] = 26;
                stats[5] = 14;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 278:
                name = "Flame Helm";
                stats[1] = 5;
                stats[3] = 13;
                stats[5] = 7;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 279:
                name = "Flame Vest"; // 30 total, +10 each rarity
                stats[0] = 10;
                stats[3] = 20;
                stats[5] = 20;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 280:
                name = "Flame Hood";
                stats[1] = 5;
                stats[3] = 10;
                stats[5] = 10;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 281:
                name = "Flame Robe"; // 30 total, +10 each rarity
                stats[0] = 10;
                stats[3] = 14;
                stats[5] = 26;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 282:
                name = "Flame Hat";
                stats[1] = 5;
                stats[3] = 7;
                stats[5] = 13;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 283:
                name = "Lava Helm";
                stats[1] = 10;
                stats[3] = 16;
                stats[5] = 9;
                role = 1;
                slot = 0;
                rarity = 2;
                break;
            case 284:
                name = "Lava Hood";
                stats[1] = 10;
                stats[3] = 13;
                stats[5] = 12;
                role = 2;
                slot = 0;
                rarity = 2;
                break;
            case 285:
                name = "Lava Hat";
                stats[1] = 10;
                stats[3] = 9;
                stats[5] = 16;
                role = 3;
                slot = 0;
                rarity = 2;
                break;
            case 286:
                name = "Dragon Band"; //15 total def + res, +5 green, +5 blue, +5 red, +5 legendary, +20 power
                message = "10% critical chance";
                stats[1] = 21;
                stats[3] = 17;
                stats[5] = 17;
                crit = .1f;
                role = 0;
                slot = 0;
                rarity = 4;
                break;
            case 287:
                name = "Regal Armor"; // 30 total, +10 each rarity
                stats[0] = 20;
                stats[3] = 32;
                stats[5] = 18;
                role = 1;
                slot = 2;
                rarity = 2;
                break;
            case 288:
                name = "Trusty Anchor";
                message = "Hp/Res+20% Spd-10%";
                role = 0;
                slot = 4;
                rarity = 2;
                health = .2f;
                resistence = .2f;
                speed = -.1f;
                break;
            case 289:
                name = "Lawmaker";
                stats[1] = 35;
                stats[3] = 10;
                stats[5] = 10;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 290:
                name = "Regal Cloak"; //Tier 5 Green cloak 24 total, blue +12
                stats[1] = 8;
                stats[3] = 16;
                stats[5] = 12;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 291:
                name = "Old Lance";
                message = "Waylan needs this.";
                rarity = 0;
                slot = 5;
                break;
            case 292:
                name = "Serpent Lance";
                stats[1] = 30;
                stats[2] = 10;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 293:
                name = "Orgo Lance";
                stats[1] = 35;
                stats[0] = 8;
                stats[2] = 12;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 294:
                name = "Lava Lance";
                stats[1] = 40;
                stats[0] = 12;
                stats[2] = 18;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 295:
                name = "Dragon Bane";
                message = "+40% dmg to dragon";
                stats[1] = 45;
                stats[0] = 16;
                stats[2] = 24;
                role = 1;
                slot = 3;
                rarity = 4;
                break;
            case 296:
                name = "Viper Venom";
                message = "Waylan needs this.";
                rarity = 1;
                slot = 5;
                break;
            case 297:
                name = "Raw Obsidian";
                message = "Waylan needs this.";
                rarity = 3;
                slot = 5;
                break;
            case 298:
                name = "Dragon Scale";
                message = "Waylan needs this.";
                rarity = 4;
                slot = 5;
                break;
            case 299:
                name = "Dragon Axe";
                stats[1] = 35;
                stats[2] = 12;
                stats[3] = 8;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 300:
                name = "Dragon Coat"; // 30 total, +10 each rarity
                stats[1] = 30;
                stats[3] = 30;
                stats[5] = 30;
                role = 0;
                slot = 2;
                rarity = 3;
                break;
            case 301:
                name = "Blizzard";
                stats[1] = 35;
                stats[0] = 8;
                stats[3] = 12;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 302:
                name = "Regal Robe"; // 30 total, +10 each rarity
                stats[2] = 20;
                stats[3] = 18;
                stats[5] = 32;
                role = 3;
                slot = 2;
                rarity = 2;
                break;
            case 303:
                name = "Regal Vest"; // 30 total, +10 each rarity
                stats[2] = 20;
                stats[3] = 26;
                stats[5] = 24;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 304:
                name = "Icefang";
                stats[1] = 40;
                stats[0] = 12;
                stats[3] = 18;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 305:
                name = "Storm Hammer";
                stats[1] = 35;
                stats[2] = 8;
                stats[2] = 12;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 306:
                name = "Dragon Fang";
                role = 0;
                slot = 4;
                rarity = 3;
                piercing = .3f;
                message = "Ignore 30% def/res";
                break;
            case 307:
                name = "Rubber Duck";
                role = 0;
                slot = 4;
                rarity = 2;
                elementResistance = .4f;
                elementType = 2;
                message = "Enemy storm dmg -40%";
                break;
            case 308:
                name = "Ice Cube";
                role = 0;
                slot = 4;
                rarity = 2;
                elementResistance = .4f;
                elementType = 2;
                message = "Enemy fire dmg -40%";
                break;
            case 309:
                name = "Hot Pocket";
                role = 0;
                slot = 4;
                rarity = 2;
                elementResistance = .4f;
                elementType = 3;
                message = "Enemy ice dmg -40%";
                break;
            case 310:
                name = "Rainbow Scale";
                role = 0;
                slot = 4;
                rarity = 3;
                elementResistance = .3f;
                elementType = -3;
                message = "Enemy magic -30%";
                break;
            case 311:
                name = "Arena Armor"; // 30 total, +10 each rarity
                stats[0] = 30;
                stats[3] = 40;
                stats[5] = 20;
                role = 1;
                slot = 2;
                rarity = 3;
                break;
            case 312:
                name = "Arena Vest"; // 30 total, +10 each rarity
                stats[0] = 30;
                stats[3] = 30;
                stats[5] = 30;
                role = 2;
                slot = 2;
                rarity = 3;
                break;
            case 313:
                name = "Arena Robe"; // 30 total, +10 each rarity
                stats[0] = 30;
                stats[3] = 20;
                stats[5] = 40;
                role = 3;
                slot = 2;
                rarity = 3;
                break;
            case 314:
                name = "Arena Helm"; // 15 total, +5 each rarity
                stats[0] = 15;
                stats[3] = 20;
                stats[5] = 10;
                role = 1;
                slot = 0;
                rarity = 3;
                break;
            case 315:
                name = "Arena Hood"; // 15 total, +5 each rarity
                stats[0] = 15;
                stats[3] = 15;
                stats[5] = 15;
                role = 2;
                slot = 0;
                rarity = 3;
                break;
            case 316:
                name = "Arena Hat"; // 15 total, +5 each rarity
                stats[0] = 15;
                stats[3] = 10;
                stats[5] = 20;
                role = 3;
                slot = 0;
                rarity = 3;
                break;
            case 317:
                name = "Arena Blade";
                stats[1] = 40;
                stats[2] = 18;
                stats[3] = 12;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 318:
                name = "Arena Axe";
                stats[1] = 40;
                stats[2] = 18;
                stats[0] = 12;
                role = 2;
                slot = 3;
                rarity = 3;
                break;
            case 319:
                name = "Arena Hammer";
                stats[1] = 40;
                stats[3] = 12;
                stats[0] = 18;
                role = 3;
                slot = 3;
                rarity = 3;
                break;
            case 320:
                name = "Arena Staff";
                stats[1] = 40;
                stats[2] = 12;
                stats[5] = 18;
                role = 3;
                slot = 3;
                rarity = 3;
                break;
            case 321:
                name = "Arena Bow";
                stats[1] = 40;
                stats[0] = 12;
                stats[2] = 18;
                role = 2;
                slot = 3;
                rarity = 3;
                break;
            case 322:
                name = "Arena Lance";
                stats[1] = 40;
                stats[3] = 18;
                stats[5] = 12;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 323:
                name = "Prisma Cloak"; //Tier 5 Green cloak 24 total, blue +12,
                stats[1] = 28;
                stats[3] = 16;
                stats[5] = 16;
                message = "+3% stats per turn.";
                role = 0;
                slot = 1;
                rarity = 4;
                break;
            case 324:
                name = "Snow";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 325:
                name = "Chain";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 326:
                name = "Ice";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 327:
                name = "Ice Sword";
                stats[1] = 36;
                role = 1;
                slot = 3;
                break;
            case 328:
                name = "Ice Axe";
                stats[1] = 36;
                role = 2;
                slot = 3;
                break;
            case 329:
                name = "Ice Staff";
                stats[1] = 36;
                role = 3;
                slot = 3;
                break;
            case 330:
                name = "Ice Star";
                stats[1] = 36;
                role = 2;
                slot = 3;
                break;
            case 331:
                name = "Ice Lance";
                stats[1] = 36;
                role = 1;
                slot = 3;
                break;
            case 332:
                name = "Ice Hammer";
                stats[1] = 36;
                role = 3;
                slot = 3;
                break;
            case 333:
                name = "Frost Blade";
                stats[1] = 44;
                stats[3] = 10;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 334:
                name = "Frost Axe";
                stats[1] = 44;
                stats[3] = 10;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 335:
                name = "Frost Staff";
                stats[1] = 44;
                stats[3] = 10;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 336:
                name = "Moon Bow";
                stats[1] = 44;
                stats[5] = 10;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 337:
                name = "Frost Lance";
                stats[1] = 44;
                stats[3] = 10;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 338:
                name = "Frost Mace";
                stats[1] = 44;
                stats[3] = 10;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 339:
                name = "Ice Armor"; // 48 total, +12 each rarity
                stats[3] = 32;
                stats[5] = 16;
                role = 1;
                slot = 2;
                rarity = 0;
                break;
            case 340:
                name = "Ice Helm";
                stats[3] = 16;
                stats[5] = 8;
                role = 1;
                slot = 0;
                rarity = 0;
                break;
            case 341:
                name = "Chain Armor";
                stats[3] = 24;
                stats[5] = 24;
                role = 2;
                slot = 2;
                rarity = 0;
                break;
            case 342:
                name = "Chain Hood";
                stats[3] = 12;
                stats[5] = 12;
                role = 2;
                slot = 0;
                rarity = 0;
                break;
            case 343:
                name = "Snow Robe";
                stats[3] = 16;
                stats[5] = 32;
                role = 3;
                slot = 2;
                rarity = 0;
                break;
            case 344:
                name = "Snow Hat";
                stats[3] = 8;
                stats[5] = 16;
                role = 3;
                slot = 0;
                rarity = 0;
                break;
            case 345:
                name = "Frost Armor";  // 48 total, +12 each rarity
                stats[0] = 12;
                stats[3] = 40;
                stats[5] = 20;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 346:
                name = "Frost Helm";
                stats[2] = 6;
                stats[3] = 20;
                stats[5] = 10;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 347:
                name = "Moon Coat";
                stats[2] = 12;
                stats[3] = 30;
                stats[5] = 30;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 348:
                name = "Moon Helm";
                stats[0] = 6;
                stats[3] = 15;
                stats[5] = 15;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 349:
                name = "Star Robe";
                stats[2] = 12;
                stats[3] = 20;
                stats[5] = 40;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 350:
                name = "Star Hat";
                stats[0] = 6;
                stats[3] = 10;
                stats[5] = 20;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 351:
                name = "Yeti Armor"; // 48 total, +12 each rarity
                stats[0] = 24;
                stats[3] = 48;
                stats[5] = 24;
                role = 1;
                slot = 2;
                rarity = 2;
                break;
            case 352:
                name = "Sorcerer Robe"; // 30 total, +10 each rarity
                stats[2] = 12;
                stats[3] = 20;
                stats[5] = 40;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 353:
                name = "Sorcerer Hat";
                stats[1] = 6;
                stats[3] = 10;
                stats[5] = 20;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 354:
                name = "Starfall";
                stats[1] = 52;
                stats[0] = 8;
                stats[5] = 12;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 355:
                name = "Lost Scarf";
                message = "Scarlett misses this.";
                rarity = 2;
                slot = 5;
                break;
            case 356:
                name = "Blue Scarf";
                stats[0] = 12;
                stats[3] = 18;
                stats[5] = 18;
                role = 0;
                slot = 0;
                rarity = 2;
                break;
            case 357:
                name = "Frostbane";
                stats[1] = 60;
                stats[2] = 20;
                stats[3] = 10;
                role = 2;
                slot = 3;
                rarity = 3;
                break;
            case 358:
                name = "Ice Wulf Cloak"; //Tier 6 Green cloak 32 total, blue +16
                stats[1] = 24;
                stats[2] = 16;
                stats[2] = 8;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 359:
                name = "Ice Fang";
                message = "Bring this to Tanner";
                rarity = 1;
                slot = 5;
                break;
            case 360:
                name = "Storm Cloak"; //Tier 6 Green cloak 32 total, blue +16
                stats[0] = 8;
                stats[1] = 16;
                stats[2] = 24;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 361:
                name = "Berry Robe"; // 48 total, +12 each rarity
                stats[0] = 24;
                stats[3] = 24;
                stats[5] = 48;
                role = 3;
                slot = 2;
                rarity = 2;
                break;
            case 362:
                name = "Moon Cloak"; //Tier 6 Green cloak 32 total, blue +16
                stats[2] = 8;
                stats[0] = 16;
                stats[5] = 24;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 363:
                name = "Blue Berry";
                message = "Bring to Snowfall";
                rarity = 1;
                slot = 5;
                break;
            case 364:
                name = "Jeff's Letter";
                message = "Bring to Myria";
                rarity = 2;
                slot = 5;
                break;
            case 365:
                name = "Azure Cloth";
                message = "Bring to Harbor Town";
                rarity = 2;
                slot = 5;
                break;
            case 366:
                name = "Sailor Axe";
                stats[1] = 24;
                stats[2] = 10;
                stats[3] = 6;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 367:
                name = "Cookie Box";
                message = "Bring to Wyrm";
                rarity = 2;
                slot = 5;
                break;
            case 368:
                name = "Bomb Powder";
                message = "Tim needs this.";
                rarity = 2;
                slot = 5;
                break;
            case 369:
                name = "Red Quilt"; //Tier 6 Green cloak 32 total, blue +16
                stats[0] = 28;
                stats[5] = 20;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 370:
                name = "Mini Heater";
                message = "Bring to Snowfall.";
                rarity = 2;
                slot = 5;
                break;
            case 371:
                name = "Flame Jar";
                role = 0;
                slot = 4;
                rarity = 3;
                message = "2x Burn Damage";
                break;
            case 372:
                name = "Thunderbolt";
                stats[1] = 52;
                stats[2] = 12;
                stats[2] = 8;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 373:
                name = "Tiger Fang";
                role = 0;
                slot = 4;
                rarity = 3;
                message = "30% Critical Chance";
                crit = .3f;
                break;
            case 374:
                name = "Scarlet";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 375:
                name = "Mithril";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 376:
                name = "Titanium";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 377:
                name = "Titanium Armor"; // 66 total, +14 each rarity
                stats[3] = 44;
                stats[5] = 22;
                role = 1;
                slot = 2;
                rarity = 0;
                break;
            case 378:
                name = "Titanium Helm";
                stats[3] = 22;
                stats[5] = 11;
                role = 1;
                slot = 0;
                rarity = 0;
                break;
            case 379:
                name = "Mithril Armor";
                stats[3] = 33;
                stats[5] = 33;
                role = 2;
                slot = 2;
                rarity = 0;
                break;
            case 380:
                name = "Mithril Hood";
                stats[3] = 17;
                stats[5] = 16;
                role = 2;
                slot = 0;
                rarity = 0;
                break;
            case 381:
                name = "Scarlet Robe";
                stats[3] = 22;
                stats[5] = 44;
                role = 3;
                slot = 2;
                rarity = 0;
                break;
            case 382:
                name = "Scarlet Hat";
                stats[3] = 11;
                stats[5] = 22;
                role = 3;
                slot = 0;
                rarity = 0;
                break;
            case 383:
                name = "Templar Armor";  // 66 total, +14 each rarity
                stats[0] = 14;
                stats[3] = 52;
                stats[5] = 28;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 384:
                name = "Templar Helm";
                stats[0] = 7;
                stats[3] = 26;
                stats[5] = 14;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 385:
                name = "Hunter Armor";
                stats[2] = 14;
                stats[3] = 40;
                stats[5] = 40;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 386:
                name = "Hunter Hood";
                stats[0] = 7;
                stats[3] = 20;
                stats[5] = 20;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 387:
                name = "Magus Robe";
                stats[1] = 14;
                stats[3] = 28;
                stats[5] = 52;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 388:
                name = "Magus Hat";
                stats[1] = 7;
                stats[3] = 14;
                stats[5] = 26;
                role = 3;
                slot = 0;
                rarity = 1;
                break;
            case 389:
                name = "Soul Armor"; // 30 total, +10 each rarity
                stats[2] = 28;
                stats[3] = 62;
                stats[5] = 32;
                role = 1;
                slot = 2;
                rarity = 2;
                break;
            case 390:
                name = "Soul Helm";
                stats[1] = 14;
                stats[3] = 31;
                stats[5] = 16;
                role = 1;
                slot = 0;
                rarity = 2;
                break;
            case 391:
                name = "Titanium Blade";
                stats[1] = 64;
                stats[2] = 12;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 392:
                name = "Titanium Axe";
                stats[1] = 64;
                stats[2] = 12;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 393:
                name = "Titanium Staff";
                stats[1] = 64;
                stats[0] = 12;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 394:
                name = "Titanium Star";
                stats[1] = 64;
                stats[2] = 12;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 395:
                name = "Titanium Lance";
                stats[1] = 64;
                stats[3] = 12;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 396:
                name = "Titanium Mace";
                stats[1] = 64;
                stats[2] = 12;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 397:
                name = "Templar Cloak"; //Tier 7 Green cloak 44 total, blue +20
                stats[1] = 8;
                stats[0] = 24;
                stats[3] = 12;
                role = 0;
                slot = 1;
                rarity = 1;
                break;
            case 398:
                name = "Hunter Cloak"; //Tier 7 Green cloak 44 total, blue +20
                stats[1] = 8;
                stats[2] = 24;
                stats[0] = 12;
                role = 0;
                slot = 1;
                rarity = 1;
                break;
            case 399:
                name = "Magus Cloak"; //Tier 7 Green cloak 44 total, blue +20
                stats[1] = 8;
                stats[5] = 24;
                stats[0] = 12;
                role = 0;
                slot = 1;
                rarity = 1;
                break;
            case 400:
                name = "Soul Axe";
                stats[1] = 76;
                stats[0] = 24;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 401:
                name = "Soul Cloak"; //Tier 7 Green cloak 44 total, blue +20
                stats[1] = 24;
                stats[0] = 24;
                stats[2] = 16;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 402:
                name = "Metal Cloak"; //Tier 7 Green cloak 44 total, blue +20
                stats[0] = 24;
                stats[3] = 40;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 403:
                name = "Light Cloak"; //Tier 7 Green cloak 44 total, blue +20
                stats[0] = 24;
                stats[5] = 40;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 404:
                name = "Flame Cloak"; //Tier 7 Green cloak 44 total, blue +20
                stats[1] = 24;
                stats[0] = 20;
                stats[2] = 20;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 405:
                name = "Flowerfire";
                stats[1] = 76;
                stats[0] = 16;
                stats[2] = 8;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 406:
                name = "Snowstar";
                stats[1] = 76;
                stats[2] = 16;
                stats[3] = 8;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 407:
                name = "Soulfire";
                message = "+ 30% Power";
                rarity = 3;
                power = .3f;
                slot = 4;
                break;
            case 408:
                name = "Eagle Egg";
                message = "eggxactly 1 pound";
                rarity = 1;
                slot = 5;
                break;
            case 409:
                name = "Frying Pan";
                stats[1] = 76;
                stats[5] = 24;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 410:
                name = "Noble Book";
                message = "Aledgar's notebook";
                rarity = 2;
                slot = 5;
                break;
            case 411:
                name = "Noble Coat"; // 30 total, +10 each rarity
                stats[0] = 28;
                stats[3] = 48;
                stats[5] = 46;
                role = 0;
                slot = 2;
                rarity = 2;
                break;
            case 412:
                name = "Truesteel";
                stats[1] = 76;
                stats[3] = 24;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 413:
                name = "Justice";
                stats[1] = 76;
                stats[0] = 24;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 414:
                name = "Seer Armor";
                stats[2] = 28;
                stats[3] = 46;
                stats[5] = 48;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 415:
                name = "Seer Hood";
                stats[0] = 14;
                stats[3] = 23;
                stats[5] = 24;
                role = 2;
                slot = 0;
                rarity = 2;
                break;
            case 416:
                name = "Arbiter";
                stats[1] = 76;
                stats[3] = 24;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 417:
                name = "Vangarde Cloak"; //Tier 7 Green cloak 44 total, blue +20, 84
                stats[1] = 32;
                stats[0] = 26;
                stats[3] = 26;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 418:
                name = "Astral Blade";
                stats[1] = 76;
                stats[0] = 8;
                stats[5] = 16;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 419:
                name = "Royal Rapier";
                stats[1] = 88; //36
                stats[3] = 12;
                stats[5] = 24;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 420:
                name = "Princess Robe";
                stats[1] = 42;
                stats[3] = 36;
                stats[5] = 72;
                role = 3;
                slot = 2;
                rarity = 3;
                break;
            case 421:
                name = "Silver Crown";
                stats[0] = 21;
                stats[3] = 26;
                stats[5] = 28;
                role = 0;
                slot = 0;
                rarity = 3;
                break;
            case 422:
                name = "Astral Garb";
                stats[2] = 28;
                stats[3] = 32;
                stats[5] = 62;
                role = 3;
                slot = 2;
                rarity = 2;
                break;
            case 423:
                name = "Astral Circlet";
                stats[2] = 14;
                stats[3] = 16;
                stats[5] = 31;
                role = 3;
                slot = 0;
                rarity = 2;
                break;
            case 424:
                name = "Royal Shawl"; //Tier 7 Green cloak 44 total, blue +20, 84
                stats[1] = 32;
                stats[3] = 26;
                stats[5] = 26;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 425:
                name = "Astral Cover"; //Tier 7 Green cloak 44 total, blue +20
                stats[1] = 24;
                stats[2] = 20;
                stats[2] = 20;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 426:
                name = "Pocket Mirror";
                message = "Reflect 20% dmg";
                rarity = 3;
                reflect = .2f;
                slot = 4;
                break;
            case 427:
                name = "Scarlette Ribbon";
                message = "+ 20% Hp & Res";
                rarity = 3;
                health = .2f;
                resistence = .2f;
                slot = 4;
                break;
            case 428:
                name = "Hourglass";
                message = "bring to tommy";
                rarity = 2;
                slot = 5;
                break;
            case 429:
                name = "Hourglass";
                message = "+ 30% Speed";
                rarity = 3;
                speed = .3f;
                slot = 4;
                break;
            case 430:
                name = "Time Key";
                message = "Vangarde Time Tower";
                rarity = 2;
                slot = 5;
                break;
            case 431:
                name = "Time Robe";
                stats[2] = 28;
                stats[3] = 32;
                stats[5] = 62;
                role = 3;
                slot = 2;
                rarity = 2;
                break;
            case 432:
                name = "Time Hat";
                stats[2] = 14;
                stats[3] = 16;
                stats[5] = 31;
                role = 3;
                slot = 0;
                rarity = 2;
                break;
            case 433:
                name = "Time Cloak"; //Tier 7 Green cloak 44 total, blue +20
                stats[2] = 24;
                stats[0] = 20;
                stats[1] = 20;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 434:
                name = "Hour Hand";
                stats[1] = 76;
                stats[2] = 24;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 435:
                name = "Sapphire";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 436:
                name = "Emerald";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 437:
                name = "Ruby";
                message = "Upgrade Material";
                rarity = 0;
                slot = 5;
                break;
            case 438:
                name = "Ancient Blade";
                stats[1] = 90;
                stats[2] = 16;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 439:
                name = "Ancient Axe";
                stats[1] = 90;
                stats[5] = 16;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 440:
                name = "Ancient Staff";
                stats[1] = 90;
                stats[0] = 16;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 441:
                name = "Ancient Bow";
                stats[1] = 90;
                stats[2] = 16;
                role = 2;
                slot = 3;
                rarity = 1;
                break;
            case 442:
                name = "Ancient Lance";
                stats[1] = 90;
                stats[3] = 16;
                role = 1;
                slot = 3;
                rarity = 1;
                break;
            case 443:
                name = "Ancient Hammer";
                stats[1] = 90;
                stats[3] = 16;
                role = 3;
                slot = 3;
                rarity = 1;
                break;
            case 444:
                name = "Ancient Armor";  // 94 total, +16 each rarity
                stats[2] = 16;
                stats[3] = 72;
                stats[5] = 38;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 445:
                name = "Ancient Helm";
                stats[0] = 8;
                stats[3] = 36;
                stats[5] = 19;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 446:
                name = "Leaf Garb";
                stats[2] = 16;
                stats[3] = 55;
                stats[5] = 55;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 447:
                name = "Leaf Hood";
                stats[0] = 8;
                stats[3] = 23;
                stats[5] = 22;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 448:
                name = "Leaf Robe";
                stats[2] = 16;
                stats[3] = 38;
                stats[5] = 72;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 449:
                name = "Ancient Mask";
                stats[1] = 8;
                stats[3] = 19;
                stats[5] = 36;
                role = 3;
                slot = 0;
                rarity = 1;
                break;

            case 450:
                name = "Treefell";
                stats[1] = 106;
                stats[2] = 32;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 451:
                name = "Hyperion Armor";  // 94 total, +16 each rarity
                stats[3] = 84;
                stats[5] = 42;
                role = 1;
                slot = 2;
                rarity = 1;
                break;
            case 452:
                name = "Hyperion Helm";
                stats[3] = 42;
                stats[5] = 21;
                role = 1;
                slot = 0;
                rarity = 1;
                break;
            case 453:
                name = "Desert Armor";
                stats[3] = 63;
                stats[5] = 63;
                role = 2;
                slot = 2;
                rarity = 1;
                break;
            case 454:
                name = "Desert Hood";
                stats[3] = 32;
                stats[5] = 31;
                role = 2;
                slot = 0;
                rarity = 1;
                break;
            case 455:
                name = "Sun Robe";
                stats[3] = 42;
                stats[5] = 84;
                role = 3;
                slot = 2;
                rarity = 1;
                break;
            case 456:
                name = "Sun Hat";
                stats[3] = 21;
                stats[5] = 42;
                role = 3;
                slot = 0;
                rarity = 1;
                break;

            case 457:
                name = "Ruby Armor";  // 94 total, +16 each rarity
                stats[0] = 32;
                stats[3] = 84;
                stats[5] = 42;
                role = 1;
                slot = 2;
                rarity = 2;
                break;
            case 458:
                name = "Ruby Helm";
                stats[0] = 16;
                stats[3] = 42;
                stats[5] = 21;
                role = 1;
                slot = 0;
                rarity = 2;
                break;
            case 459:
                name = "Emerald Armor";
                stats[2] = 32;
                stats[3] = 63;
                stats[5] = 63;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 460:
                name = "Emerald Hood";
                stats[2] = 16;
                stats[3] = 32;
                stats[5] = 31;
                role = 2;
                slot = 0;
                rarity = 2;
                break;
            case 461:
                name = "Sapphire Robe";
                stats[2] = 32;
                stats[3] = 42;
                stats[5] = 84;
                role = 3;
                slot = 2;
                rarity = 2;
                break;
            case 462:
                name = "Sapphire Hat";
                stats[2] = 16;
                stats[3] = 21;
                stats[5] = 42;
                role = 3;
                slot = 0;
                rarity = 2;
                break;
            case 463:
                name = "Gold Key";
                message = "Hyperion Desert SE";
                rarity = 4;
                slot = 5;
                break;
            case 464:
                name = "Stormlash";
                stats[1] = 180;
                stats[2] = 80;
                stats[0] = 80;
                role = 3;
                slot = 3;
                rarity = 4;
                message = "Crit up 20%";
                crit = .2f;
                break;
            case 465:
                name = "Dark Matter";
                message = "Upgrade Material";
                rarity = 3;
                slot = 5;
                role = 0;
                break;
            case 466:
                name = "Sapphire Staff";
                stats[1] = 106;
                stats[3] = 12;
                stats[5] = 20;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 467:
                name = "Magic Gem";
                message = "bring to jemma";
                rarity = 1;
                slot = 5;
                break;
            case 468:
                name = "Arctic Cloak"; //Tier 7
                stats[0] = 36;
                stats[3] = 36;
                stats[5] = 36;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 469:
                name = "Zap Cloak"; //Tier 7
                stats[1] = 48;
                stats[2] = 36;
                stats[2] = 24;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 470:
                name = "Molten Cloak"; //Tier 7
                stats[0] = 48;
                stats[1] = 36;
                stats[2] = 24;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 471:
                name = "Diamond Ring";
                message = "Bring to Vangarde.";
                rarity = 2;
                slot = 5;
                break;
            case 472:
                name = "Invitation";
                message = "Bring to Hyperion.";
                rarity = 2;
                slot = 5;
                break;
            case 473:
                name = "Trull Horn";
                message = "Bring to Hyperion.";
                rarity = 2;
                slot = 5;
                break;
            case 475:
                name = "Royal Note";
                message = "Bring to Myria.";
                rarity = 3;
                slot = 5;
                break;
            case 476:
                name = "King's Cloak"; //Tier 8
                stats[0] = 42;
                stats[1] = 56;
                stats[3] = 72;
                message = "Max HP + 10%";
                health = .1f;
                role = 0;
                slot = 1;
                rarity = 4;
                break;
            case 477:
                name = "Lion Fang";
                message = "Bring this to Tanner";
                rarity = 1;
                slot = 5;
                break;
            case 478:
                name = "Lionfall";
                stats[1] = 106;
                stats[0] = 20;
                stats[2] = 12;
                role = 2;
                slot = 3;
                rarity = 2;
                break;
            case 479:
                name = "Secret Coat";
                stats[0] = 32;
                stats[3] = 63;
                stats[5] = 63;
                role = 0;
                slot = 2;
                rarity = 2;
                break;
            case 480:
                name = "Katana";
                stats[1] = 106;
                stats[2] = 32;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 481:
                name = "Samurai Garb";
                stats[1] = 32;
                stats[3] = 63;
                stats[5] = 63;
                role = 2;
                slot = 2;
                rarity = 2;
                break;
            case 482:
                name = "Black Bandana";
                stats[2] = 16;
                stats[3] = 32;
                stats[5] = 31;
                role = 0;
                slot = 0;
                rarity = 2;
                break;
            case 483:
                name = "Shadow Cloak"; //Tier 7
                stats[1] = 32;
                stats[2] = 28;
                stats[0] = 24;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 484:
                name = "Nightfire";
                message = "+ 60% hide dmg";
                rarity = 3;
                slot = 4;
                break;
            case 485:
                name = "Hyperion Lance";
                stats[1] = 106;
                stats[0] = 32;
                role = 1;
                slot = 3;
                rarity = 2;
                break;
            case 486:
                name = "Chaos Cloak"; //Tier 7
                stats[1] = 28;
                stats[2] = 32;
                stats[5] = 24;
                role = 0;
                slot = 1;
                rarity = 2;
                break;
            case 487:
                name = "Death Mace";
                stats[1] = 106;
                stats[2] = 32;
                role = 3;
                slot = 3;
                rarity = 2;
                break;
            case 488:
                name = "Battle Charm";
                message = "+15% party dmg";
                rarity = 3;
                slot = 4;
                break;
            case 489:
                name = "Chaos Armor";  // 94 total, +16 each rarity
                stats[2] = 32;
                stats[3] = 84;
                stats[5] = 42;
                role = 1;
                slot = 2;
                rarity = 2;
                break;
            case 490:
                name = "Chaos Mask";
                stats[1] = 16;
                stats[3] = 42;
                stats[5] = 21;
                role = 1;
                slot = 0;
                rarity = 2;
                break;
            case 491:
                name = "Excalibur";
                stats[1] = 122;
                stats[0] = 28;
                stats[3] = 20;
                role = 1;
                slot = 3;
                rarity = 3;
                break;
            case 492:
                name = "Altrius II";
                stats[1] = 180;
                stats[0] = 92;
                stats[5] = 68;
                role = 11;
                slot = 3;
                rarity = 4;
                message = "Heal 10% hp";
                break;
            case 493:
                name = "Dragon Ore";
                message = "Waylan needs this.";
                rarity = 4;
                slot = 5;
                break;
            case 494:
                name = "Gold Scale";
                message = "Waylan needs this.";
                rarity = 4;
                slot = 5;
                break;
            case 495:
                name = "Dragon Band II";
                message = "10% critical chance";
                crit = .1f;
                stats[1] = 40;
                stats[3] = 50;
                stats[5] = 50;
                role = 0;
                slot = 0;
                rarity = 4;
                break;
            case 496:
                name = "Dragon Bane II";
                stats[1] = 180;
                stats[0] = 64;
                stats[2] = 96;
                role = 1;
                slot = 3;
                rarity = 4;
                message = "+40% dmg to dragon";
                break;
            case 497:
                name = "Elfnir";
                stats[1] = 180;
                stats[2] = 160;
                role = 2;
                slot = 3;
                rarity = 4;
                piercing = .1f;
                message = "Ignore 10% def/res";
                break;
            case 498:
                name = "Trueheart";
                stats[1] = 180;
                stats[0] = 60;
                stats[3] = 100;
                role = 3;
                slot = 3;
                rarity = 4;
                message = "20% reduced damage";
                break;
            case 499:
                name = "Souleater";
                stats[1] = 180;
                stats[2] = 98;
                stats[5] = 62;
                role = 2;
                slot = 3;
                rarity = 4;
                message = "+40% Dark Damage";
                break;
            case 500:
                name = "Magic Feather";
                message = "+40% critical chance";
                rarity = 4;
                crit = .4f;
                slot = 4;
                break;
            case 501:
                name = "Magic Ring";
                message = "+40% resistance";
                rarity = 4;
                resistence = .4f;
                slot = 4;
                break;
            case 502:
                name = "Magic Shield";
                message = "+40% defense";
                rarity = 4;
                defense = .4f;
                slot = 4;
                break;
            case 575:
                name = "Tiger Mantle"; //Tier 8
                stats[0] = 36;
                stats[1] = 60;
                stats[2] = 44;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 576:
                name = "Bear Hide"; //Tier 8
                stats[0] = 44;
                stats[1] = 36;
                stats[3] = 64;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 577:
                name = "Serpent Scales"; //Tier 8
                stats[1] = 46;
                stats[2] = 42;
                stats[5] = 52;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 578:
                name = "Turtle Shell"; //Tier 8
                stats[0] = 48;
                stats[3] = 46;
                stats[5] = 46;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 579:
                name = "Panther Pelt"; //Tier 8
                stats[1] = 44;
                stats[2] = 60;
                stats[5] = 36;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 580:
                name = "Dragon Wings"; //Tier 8
                stats[0] = 44;
                stats[1] = 64;
                stats[5] = 36;
                role = 0;
                slot = 1;
                rarity = 3;
                break;
            case 581:
                name = "Medusa";
                stats[1] = 180;
                stats[2] = 92;
                stats[5] = 68;
                role = 1;
                slot = 3;
                rarity = 4;
                message = "Poison foe 10%";
                break;
            case 582:
                name = "Mimikingu";
                stats[0] = 80;
                stats[3] = 100;
                stats[5] = 100;
                health = .05f;
                power = .05f;
                agility = .05f;
                speed = .05f;
                resistence = .05f;
                defense = .05f;
                role = 0;
                slot = 2;
                rarity = 4;
                message = "All stats +5%";
                break;
            case 583:
                name = "Prisma II Cloak"; //Tier 5 Green cloak 24 total, blue +12,
                stats[1] = 74;
                stats[3] = 48;
                stats[5] = 48;
                message = "+3% stats per turn.";
                role = 0;
                slot = 1;
                rarity = 4;
                break;
        }
        //handle upgrade count
        if (upgradeCount > 0) {
            int bonus = 0;
            for (int i = 0; i < stats.length; i++) {
                if (stats[i] > 0) {
                    for (int x = 0; x < upgradeCount; x++) {
                        bonus += Math.ceil(stats[i] * .2);
                    }
                    stats[i] += bonus;
                    bonus = 0;
                }
            }
        }
    }

    public TextureRegion getImage() {
        switch (id) {
            case 1:
                if (woodenArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/wooden/woodenArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woodenArmor = new TextureRegion(load, 0, 0, 14, 14);
                    woodenArmor.flip(false, true);
                }
                return woodenArmor;
            case 2:
                if (woodenHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/wooden/woodenHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woodenHelm = new TextureRegion(load, 0, 0, 14, 14);
                    woodenHelm.flip(false, true);
                }
                return woodenHelm;
            case 3:
                if (woodenSword == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/woodenSword.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woodenSword = new TextureRegion(load, 0, 0, 14, 14);
                    woodenSword.flip(false, true);
                }
                return woodenSword;
            case 4:
                if (woodenAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/woodenAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woodenAxe = new TextureRegion(load, 0, 0, 14, 14);
                    woodenAxe.flip(false, true);
                }
                return woodenAxe;
            case 5:
                if (ironSword == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/iron/ironSword.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ironSword = new TextureRegion(load, 0, 0, 14, 14);
                    ironSword.flip(false, true);
                }
                return ironSword;
            case 6:
                if (ironAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/iron/ironAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ironAxe = new TextureRegion(load, 0, 0, 14, 14);
                    ironAxe.flip(false, true);
                }
                return ironAxe;
            case 7:
                if (steelSword == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/steel/steelSword.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelSword = new TextureRegion(load, 0, 0, 14, 14);
                    steelSword.flip(false, true);
                }
                return steelSword;
            case 8:
                if (steelAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/steel/steelAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelAxe = new TextureRegion(load, 0, 0, 14, 14);
                    steelAxe.flip(false, true);
                }
                return steelAxe;
            case 9:
                if (forestCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/forestCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    forestCloak = new TextureRegion(load, 0, 0, 14, 14);
                    forestCloak.flip(false, true);
                }
                return forestCloak;
            case 10:
                if (luckyLeaf == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/luckyLeaf.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    luckyLeaf = new TextureRegion(load, 0, 0, 14, 14);
                    luckyLeaf.flip(false, true);
                }
                return luckyLeaf;
            case 11:
                if (ironArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/iron/ironArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ironArmor = new TextureRegion(load, 0, 0, 14, 14);
                    ironArmor.flip(false, true);
                }
                return ironArmor;
            case 12:
                if (cottonRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/cotton/cottonRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    cottonRobe = new TextureRegion(load, 0, 0, 14, 14);
                    cottonRobe.flip(false, true);
                }
                return cottonRobe;
            case 13:
                if (woolRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/woolRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woolRobe = new TextureRegion(load, 0, 0, 14, 14);
                    woolRobe.flip(false, true);
                }
                return woolRobe;
            case 14:
                if (leatherArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/leather/leatherArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    leatherArmor = new TextureRegion(load, 0, 0, 14, 14);
                    leatherArmor.flip(false, true);
                }
                return leatherArmor;
            case 15:
                if (boneArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/bone/boneArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    boneArmor = new TextureRegion(load, 0, 0, 14, 14);
                    boneArmor.flip(false, true);
                }
                return boneArmor;
            case 16:
                if (ironHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/iron/ironHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ironHelm = new TextureRegion(load, 0, 0, 14, 14);
                    ironHelm.flip(false, true);
                }
                return ironHelm;
            case 17:
                if (cottonHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/cotton/cottonHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    cottonHat = new TextureRegion(load, 0, 0, 14, 14);
                    cottonHat.flip(false, true);
                }
                return cottonHat;
            case 18:
                if (woolHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/woolHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woolHat = new TextureRegion(load, 0, 0, 14, 14);
                    woolHat.flip(false, true);
                }
                return woolHat;
            case 19:
                if (leatherHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/leather/leatherHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    leatherHat = new TextureRegion(load, 0, 0, 14, 14);
                    leatherHat.flip(false, true);
                }
                return leatherHat;
            case 20:
                if (boneHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/bone/boneHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    boneHelm = new TextureRegion(load, 0, 0, 14, 14);
                    boneHelm.flip(false, true);
                }
                return boneHelm;
            case 21:
                if (woodenStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/woodenStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woodenStaff = new TextureRegion(load, 0, 0, 14, 14);
                    woodenStaff.flip(false, true);
                }
                return woodenStaff;
            case 22:
                if (ironStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/iron/ironStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ironStaff = new TextureRegion(load, 0, 0, 14, 14);
                    ironStaff.flip(false, true);
                }
                return ironStaff;
            case 23:
                if (woodenBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/woodenBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woodenBow = new TextureRegion(load, 0, 0, 14, 14);
                    woodenBow.flip(false, true);
                }
                return woodenBow;
            case 24:
                if (ironBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/iron/ironBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ironBow = new TextureRegion(load, 0, 0, 14, 14);
                    ironBow.flip(false, true);
                }
                return ironBow;
            case 25:
                if (woodenSpear == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/woodenSpear.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woodenSpear = new TextureRegion(load, 0, 0, 14, 14);
                    woodenSpear.flip(false, true);
                }
                return woodenSpear;
            case 26:
                if (ironSpear == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/iron/ironSpear.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ironSpear = new TextureRegion(load, 0, 0, 14, 14);
                    ironSpear.flip(false, true);
                }
                return ironSpear;
            case 27:
                if (woodenHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/woodenHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    woodenHammer = new TextureRegion(load, 0, 0, 14, 14);
                    woodenHammer.flip(false, true);
                }
                return woodenHammer;
            case 28:
                if (ironHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/iron/ironHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ironHammer = new TextureRegion(load, 0, 0, 14, 14);
                    ironHammer.flip(false, true);
                }
                return ironHammer;
            case 29:
                if (silverChain == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/silverChain.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silverChain = new TextureRegion(load, 0, 0, 14, 14);
                    silverChain.flip(false, true);
                }
                return silverChain;
            case 30:
                if (blanket == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/blanket.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blanket = new TextureRegion(load, 0, 0, 14, 14);
                    blanket.flip(false, true);
                }
                return blanket;
            case 31:
                if (spiritSword == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/spiritSword.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    spiritSword = new TextureRegion(load, 0, 0, 14, 14);
                    spiritSword.flip(false, true);
                }
                return spiritSword;
            case 32:
                if (rangerBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/rangerBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rangerBow = new TextureRegion(load, 0, 0, 14, 14);
                    rangerBow.flip(false, true);
                }
                return rangerBow;
            case 33:
                if (rangerHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/leather/rangerHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rangerHat = new TextureRegion(load, 0, 0, 14, 14);
                    rangerHat.flip(false, true);
                }
                return rangerHat;
            case 34:
                if (rangerArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/leather/rangerArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rangerArmor = new TextureRegion(load, 0, 0, 14, 14);
                    rangerArmor.flip(false, true);
                }
                return rangerArmor;
            case 35:
                if (minerShovel == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/minerShovel.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    minerShovel = new TextureRegion(load, 0, 0, 14, 14);
                    minerShovel.flip(false, true);
                }
                return minerShovel;
            case 36:
                if (minerPick == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/minerPick.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    minerPick = new TextureRegion(load, 0, 0, 14, 14);
                    minerPick.flip(false, true);
                }
                return minerPick;
            case 37:
                if (bossBandana == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/bossBandana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    bossBandana = new TextureRegion(load, 0, 0, 14, 14);
                    bossBandana.flip(false, true);
                }
                return bossBandana;
            case 38:
                if (silverSpear == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/silver/silverSpear.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silverSpear = new TextureRegion(load, 0, 0, 14, 14);
                    silverSpear.flip(false, true);
                }
                return silverSpear;
            case 39:
                if (petFox == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/petFox.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    petFox = new TextureRegion(load, 0, 0, 14, 14);
                    petFox.flip(false, true);
                }
                return petFox;
            case 40:
                if (lumaCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/lumaCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lumaCloak = new TextureRegion(load, 0, 0, 14, 14);
                    lumaCloak.flip(false, true);
                }
                return lumaCloak;
            case 41:
                if (spiritJar == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/spiritJar.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    spiritJar = new TextureRegion(load, 0, 0, 14, 14);
                    spiritJar.flip(false, true);
                }
                return spiritJar;
            case 42:
                if (oakArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/wooden/oakArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    oakArmor = new TextureRegion(load, 0, 0, 14, 14);
                    oakArmor.flip(false, true);
                }
                return oakArmor;
            case 43:
                if (oakHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/wooden/oakHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    oakHelm = new TextureRegion(load, 0, 0, 14, 14);
                    oakHelm.flip(false, true);
                }
                return oakHelm;
            case 44:
                if (forestRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/cotton/spiritRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    forestRobe = new TextureRegion(load, 0, 0, 14, 14);
                    forestRobe.flip(false, true);
                }
                return forestRobe;
            case 45:
                if (forestCap == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/cotton/spiritCap.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    forestCap = new TextureRegion(load, 0, 0, 14, 14);
                    forestCap.flip(false, true);
                }
                return forestCap;
            case 46:
                if (leafBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/leafBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    leafBlade = new TextureRegion(load, 0, 0, 14, 14);
                    leafBlade.flip(false, true);
                }
                return leafBlade;
            case 47:
                if (hatchet == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/hatchet.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hatchet = new TextureRegion(load, 0, 0, 14, 14);
                    hatchet.flip(false, true);
                }
                return hatchet;
            case 48:
                if (treeBranch == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/treeBranch.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    treeBranch = new TextureRegion(load, 0, 0, 14, 14);
                    treeBranch.flip(false, true);
                }
                return treeBranch;
            case 49:
                if (heavyStone == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/heavyStone.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    heavyStone = new TextureRegion(load, 0, 0, 14, 14);
                    heavyStone.flip(false, true);
                }
                return heavyStone;
            case 50:
                if (forestSpear == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/forestSpear.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    forestSpear = new TextureRegion(load, 0, 0, 14, 14);
                    forestSpear.flip(false, true);
                }
                return forestSpear;
            case 51:
                return null;//AssetLoader.silverOre;
            case 52:
                if (heavyArms == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/heavyArms.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    heavyArms = new TextureRegion(load, 0, 0, 14, 14);
                    heavyArms.flip(false, true);
                }
                return heavyArms;
            case 53:
                if (pocketShield == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/pocketShield.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    pocketShield = new TextureRegion(load, 0, 0, 14, 14);
                    pocketShield.flip(false, true);
                }
                return pocketShield;
            case 123:
            case 54:
                if (bearTooth == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/bearTooth.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    bearTooth = new TextureRegion(load, 0, 0, 14, 14);
                    bearTooth.flip(false, true);
                }
                return bearTooth;
            case 55:
                if (stoneGuard == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/stoneGuard.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    stoneGuard = new TextureRegion(load, 0, 0, 14, 14);
                    stoneGuard.flip(false, true);
                }
                return stoneGuard;
            case 56:
                if (skypiercer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/skypiercer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    skypiercer = new TextureRegion(load, 0, 0, 14, 14);
                    skypiercer.flip(false, true);
                }
                return skypiercer;
            case 57:
                if (hikerBandana == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/hikerBandana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hikerBandana = new TextureRegion(load, 0, 0, 14, 14);
                    hikerBandana.flip(false, true);
                }
                return hikerBandana;
            case 58:
                if (hillCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/hillCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hillCloak = new TextureRegion(load, 0, 0, 14, 14);
                    hillCloak.flip(false, true);
                }
                return hillCloak;
            case 59:
                if (luckyDice == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/luckyDice.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    luckyDice = new TextureRegion(load, 0, 0, 14, 14);
                    luckyDice.flip(false, true);
                }
                return luckyDice;
            case 60:
                if (luckyRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/luckyRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    luckyRobe = new TextureRegion(load, 0, 0, 14, 14);
                    luckyRobe.flip(false, true);
                }
                return luckyRobe;
            case 61:
                if (wizardCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/wizardCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wizardCloak = new TextureRegion(load, 0, 0, 14, 14);
                    wizardCloak.flip(false, true);
                }
                return wizardCloak;
            case 62:
                if (wizardRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/wizardRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wizardRobe = new TextureRegion(load, 0, 0, 14, 14);
                    wizardRobe.flip(false, true);
                }
                return wizardRobe;
            case 63:
                if (oldBook == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/oldBook.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    oldBook = new TextureRegion(load, 0, 0, 14, 14);
                    oldBook.flip(false, true);
                }
                return oldBook;
            case 64:
                return null;// AssetLoader.blackCatItem;
            case 65:
                if (lytesLocket == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/lytesLocket.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lytesLocket = new TextureRegion(load, 0, 0, 14, 14);
                    lytesLocket.flip(false, true);
                }
                return lytesLocket;
            case 66:
                if (eagleCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/eagleCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleCloak = new TextureRegion(load, 0, 0, 14, 14);
                    eagleCloak.flip(false, true);
                }
                return eagleCloak;
            case 67:
                if (eagleFeather == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/eagleFeather.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleFeather = new TextureRegion(load, 0, 0, 14, 14);
                    eagleFeather.flip(false, true);
                }
                return eagleFeather;
            case 68:
                if (wardenHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/bone/wardenHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wardenHelm = new TextureRegion(load, 0, 0, 14, 14);
                    wardenHelm.flip(false, true);
                }
                return wardenHelm;
            case 69:
                if (wardenArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/bone/wardenArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wardenArmor = new TextureRegion(load, 0, 0, 14, 14);
                    wardenArmor.flip(false, true);
                }
                return wardenArmor;
            case 70:
                if (sageHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/sageHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sageHood = new TextureRegion(load, 0, 0, 14, 14);
                    sageHood.flip(false, true);
                }
                return sageHood;
            case 71:
                if (sageRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/sageRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sageRobe = new TextureRegion(load, 0, 0, 14, 14);
                    sageRobe.flip(false, true);
                }
                return sageRobe;
            case 72:
                if (guardHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/iron/guardHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    guardHelm = new TextureRegion(load, 0, 0, 14, 14);
                    guardHelm.flip(false, true);
                }
                return guardHelm;
            case 73:
                if (guardArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/iron/guardArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    guardArmor = new TextureRegion(load, 0, 0, 14, 14);
                    guardArmor.flip(false, true);
                }
                return guardArmor;
            case 74:
                if (mushroom == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/mushroom.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    mushroom = new TextureRegion(load, 0, 0, 14, 14);
                    mushroom.flip(false, true);
                }
                return mushroom;
            case 75:
                if (wizardHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/wizardHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wizardHood = new TextureRegion(load, 0, 0, 14, 14);
                    wizardHood.flip(false, true);
                }
                return wizardHood;
            case 76:
                if (guardCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/guardCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    guardCloak = new TextureRegion(load, 0, 0, 14, 14);
                    guardCloak.flip(false, true);
                }
                return guardCloak;
            case 77:
                if (guardianBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/guardianBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    guardianBlade = new TextureRegion(load, 0, 0, 14, 14);
                    guardianBlade.flip(false, true);
                }
                return guardianBlade;
            case 78:
                if (heroAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/heroAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    heroAxe = new TextureRegion(load, 0, 0, 14, 14);
                    heroAxe.flip(false, true);
                }
                return heroAxe;
            case 79:
                if (witchWand == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/witchWand.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    witchWand = new TextureRegion(load, 0, 0, 14, 14);
                    witchWand.flip(false, true);
                }
                return witchWand;
            case 80:
                if (lytesLantern == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/lytesLantern.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lytesLantern = new TextureRegion(load, 0, 0, 14, 14);
                    lytesLantern.flip(false, true);
                }
                return lytesLantern;
            case 81:
                if (spiderBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/spiderBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    spiderBow = new TextureRegion(load, 0, 0, 14, 14);
                    spiderBow.flip(false, true);
                }
                return spiderBow;
            case 82:
                if (warLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/warLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    warLance = new TextureRegion(load, 0, 0, 14, 14);
                    warLance.flip(false, true);
                }
                return warLance;
            case 83:
                if (holyHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/holyHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    holyHammer = new TextureRegion(load, 0, 0, 14, 14);
                    holyHammer.flip(false, true);
                }
                return holyHammer;
            case 84:
            case 85:
                if (ghostJelly == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/ghostJelly.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ghostJelly = new TextureRegion(load, 0, 0, 14, 14);
                    ghostJelly.flip(false, true);
                }
                return ghostJelly;
            case 86:
                if (darkBook == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/darkBook.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    darkBook = new TextureRegion(load, 0, 0, 14, 14);
                    darkBook.flip(false, true);
                }
                return darkBook;
            case 87:
                if (witchHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/witchHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    witchHat = new TextureRegion(load, 0, 0, 14, 14);
                    witchHat.flip(false, true);
                }
                return witchHat;
            case 88:
                if (witchRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/wool/witchRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    witchRobe = new TextureRegion(load, 0, 0, 14, 14);
                    witchRobe.flip(false, true);
                }
                return witchRobe;
            case 89:
                if (duskCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/duskCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    duskCloak = new TextureRegion(load, 0, 0, 14, 14);
                    duskCloak.flip(false, true);
                }
                return duskCloak;
            case 90:
                if (darkBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/darkBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    darkBlade = new TextureRegion(load, 0, 0, 14, 14);
                    darkBlade.flip(false, true);
                }
                return darkBlade;
            case 91:
                if (lieutenantArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/iron/lieutenantArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lieutenantArmor = new TextureRegion(load, 0, 0, 14, 14);
                    lieutenantArmor.flip(false, true);
                }
                return lieutenantArmor;
            case 92:
                if (darkGarb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/bone/darkGarb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    darkGarb = new TextureRegion(load, 0, 0, 14, 14);
                    darkGarb.flip(false, true);
                }
                return darkGarb;
            case 93:
                if (magicLeaf == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/magicLeaf.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magicLeaf = new TextureRegion(load, 0, 0, 14, 14);
                    magicLeaf.flip(false, true);
                }
                return magicLeaf;
            case 94:
                if (alchemistRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/bone/alchemistRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    alchemistRobe = new TextureRegion(load, 0, 0, 14, 14);
                    alchemistRobe.flip(false, true);
                }
                return alchemistRobe;
            case 95:
                if (whiteBandana == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/whiteBandana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    whiteBandana = new TextureRegion(load, 0, 0, 14, 14);
                    whiteBandana.flip(false, true);
                }
                return whiteBandana;
            case 96:
                if (mendingStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/mendingStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    mendingStaff = new TextureRegion(load, 0, 0, 14, 14);
                    mendingStaff.flip(false, true);
                }
                return mendingStaff;
            case 97:
                if (redLeaf == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/redLeaf.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    redLeaf = new TextureRegion(load, 0, 0, 14, 14);
                    redLeaf.flip(false, true);
                }
                return redLeaf;
            case 98:
                if (steelStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/steel/steelStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelStaff = new TextureRegion(load, 0, 0, 14, 14);
                    steelStaff.flip(false, true);
                }
                return steelStaff;
            case 99:
                if (steelBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/steel/steelBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelBow = new TextureRegion(load, 0, 0, 14, 14);
                    steelBow.flip(false, true);
                }
                return steelBow;
            case 100:
                if (steelLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/steel/steelSpear.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelLance = new TextureRegion(load, 0, 0, 14, 14);
                    steelLance.flip(false, true);
                }
                return steelLance;
            case 101:
                if (steelHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/steel/steelHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelHammer = new TextureRegion(load, 0, 0, 14, 14);
                    steelHammer.flip(false, true);
                }
                return steelHammer;
            case 102:
                if (silkHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/silk/silkHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silkHat = new TextureRegion(load, 0, 0, 14, 14);
                    silkHat.flip(false, true);
                }
                return silkHat;
            case 103:
                if (silkRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/silk/silkRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silkRobe = new TextureRegion(load, 0, 0, 14, 14);
                    silkRobe.flip(false, true);
                }
                return silkRobe;
            case 104:
                if (scaleHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/scaleHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    scaleHood = new TextureRegion(load, 0, 0, 14, 14);
                    scaleHood.flip(false, true);
                }
                return scaleHood;
            case 105:
                if (scaleArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/scaleArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    scaleArmor = new TextureRegion(load, 0, 0, 14, 14);
                    scaleArmor.flip(false, true);
                }
                return scaleArmor;
            case 106:
                if (steelHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/steelHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelHelm = new TextureRegion(load, 0, 0, 14, 14);
                    steelHelm.flip(false, true);
                }
                return steelHelm;
            case 107:
                if (steelArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/steelArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelArmor = new TextureRegion(load, 0, 0, 14, 14);
                    steelArmor.flip(false, true);
                }
                return steelArmor;
            case 108:
                if (giblinKnife == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/giblinKnife.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    giblinKnife = new TextureRegion(load, 0, 0, 14, 14);
                    giblinKnife.flip(false, true);
                }
                return giblinKnife;
            case 109:
                if (giblinCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/giblinCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    giblinCloak = new TextureRegion(load, 0, 0, 14, 14);
                    giblinCloak.flip(false, true);
                }
                return giblinCloak;
            case 110:
                if (hikerCoat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/hikerCoat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hikerCoat = new TextureRegion(load, 0, 0, 14, 14);
                    hikerCoat.flip(false, true);
                }
                return hikerCoat;
            case 111:
            case 435:
                if (sapphire == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/sapphire.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sapphire = new TextureRegion(load, 0, 0, 14, 14);
                    sapphire.flip(false, true);
                }
                return sapphire;
            case 112:
            case 436:
                if (emerald == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/emerald.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    emerald = new TextureRegion(load, 0, 0, 14, 14);
                    emerald.flip(false, true);
                }
                return emerald;
            case 113:
            case 437:
                if (ruby == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/ruby.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ruby = new TextureRegion(load, 0, 0, 14, 14);
                    ruby.flip(false, true);
                }
                return ruby;
            case 114:
                if (overalls == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/overalls.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    overalls = new TextureRegion(load, 0, 0, 14, 14);
                    overalls.flip(false, true);
                }
                return overalls;
            case 115:
                if (hardHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/hardHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hardHat = new TextureRegion(load, 0, 0, 14, 14);
                    hardHat.flip(false, true);
                }
                return hardHat;
            case 116:
                if (minerDrill == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/minerDrill.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    minerDrill = new TextureRegion(load, 0, 0, 14, 14);
                    minerDrill.flip(false, true);
                }
                return minerDrill;
            case 117:
                if (toyCart == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/toyCart.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    toyCart = new TextureRegion(load, 0, 0, 14, 14);
                    toyCart.flip(false, true);
                }
                return toyCart;
            case 118:
                if (tinyHorror == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/tinyHorror.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    tinyHorror = new TextureRegion(load, 0, 0, 14, 14);
                    tinyHorror.flip(false, true);
                }
                return tinyHorror;
            case 119:
                if (tideblade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/tideblade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    tideblade = new TextureRegion(load, 0, 0, 14, 14);
                    tideblade.flip(false, true);
                }
                return tideblade;
            case 120:
                if (sewerCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/sewerCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sewerCloak = new TextureRegion(load, 0, 0, 14, 14);
                    sewerCloak.flip(false, true);
                }
                return sewerCloak;
            case 121:
                if (gatorEdge == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/gatorEdge.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    gatorEdge = new TextureRegion(load, 0, 0, 14, 14);
                    gatorEdge.flip(false, true);
                }
                return gatorEdge;
            case 122:
                if (gatorGarb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/gatorGarb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    gatorGarb = new TextureRegion(load, 0, 0, 14, 14);
                    gatorGarb.flip(false, true);
                }
                return gatorGarb;
            case 124:
                if (hurricane == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/hurricane.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hurricane = new TextureRegion(load, 0, 0, 14, 14);
                    hurricane.flip(false, true);
                }
                return hurricane;
            case 125:
                if (sniperArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/sniperArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sniperArmor = new TextureRegion(load, 0, 0, 14, 14);
                    sniperArmor.flip(false, true);
                }
                return sniperArmor;
            case 126:
                if (sniperBandana == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/sniperBandana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sniperBandana = new TextureRegion(load, 0, 0, 14, 14);
                    sniperBandana.flip(false, true);
                }
                return sniperBandana;
            case 127:
                if (sniperCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/sniperCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sniperCloak = new TextureRegion(load, 0, 0, 14, 14);
                    sniperCloak.flip(false, true);
                }
                return sniperCloak;
            case 128:
                if (blackArrow == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/blackArrow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blackArrow = new TextureRegion(load, 0, 0, 14, 14);
                    blackArrow.flip(false, true);
                }
                return blackArrow;
            case 129:
                if (courageCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/courageCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    courageCloak = new TextureRegion(load, 0, 0, 14, 14);
                    courageCloak.flip(false, true);
                }
                return courageCloak;
            case 130:
                if (wisdomCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/wisdomCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wisdomCloak = new TextureRegion(load, 0, 0, 14, 14);
                    wisdomCloak.flip(false, true);
                }
                return wisdomCloak;
            case 131:
                if (powerCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/powerCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    powerCloak = new TextureRegion(load, 0, 0, 14, 14);
                    powerCloak.flip(false, true);
                }
                return powerCloak;
            case 132:
                if (lifeFlower == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/lifeFlower.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lifeFlower = new TextureRegion(load, 0, 0, 14, 14);
                    lifeFlower.flip(false, true);
                }
                return lifeFlower;
            case 133:
                if (spiritFlower == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/spiritFlower.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    spiritFlower = new TextureRegion(load, 0, 0, 14, 14);
                    spiritFlower.flip(false, true);
                }
                return spiritFlower;
            case 134:
                if (rose == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/rose.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rose = new TextureRegion(load, 0, 0, 14, 14);
                    rose.flip(false, true);
                }
                return rose;
            case 135:
                if (holyBook == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/holyBook.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    holyBook = new TextureRegion(load, 0, 0, 14, 14);
                    holyBook.flip(false, true);
                }
                return holyBook;
            case 136:
                if (dragonTome == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/dragonTome.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonTome = new TextureRegion(load, 0, 0, 14, 14);
                    dragonTome.flip(false, true);
                }
                return dragonTome;
            case 137:
                if (librarianRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/silk/librarianRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    librarianRobe = new TextureRegion(load, 0, 0, 14, 14);
                    librarianRobe.flip(false, true);
                }
                return librarianRobe;
            case 138:
                if (librarianHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/silk/librarianHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    librarianHat = new TextureRegion(load, 0, 0, 14, 14);
                    librarianHat.flip(false, true);
                }
                return librarianHat;
            case 139:
                if (gatorHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/gatorHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    gatorHood = new TextureRegion(load, 0, 0, 14, 14);
                    gatorHood.flip(false, true);
                }
                return gatorHood;
            case 140:
                if (knightHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/knightHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    knightHelm = new TextureRegion(load, 0, 0, 14, 14);
                    knightHelm.flip(false, true);
                }
                return knightHelm;
            case 141:
                if (knightArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/knightArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    knightArmor = new TextureRegion(load, 0, 0, 14, 14);
                    knightArmor.flip(false, true);
                }
                return knightArmor;
            case 142:
                if (magmaBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/magmaBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magmaBlade = new TextureRegion(load, 0, 0, 14, 14);
                    magmaBlade.flip(false, true);
                }
                return magmaBlade;
            case 143:
                if (iceRod == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/iceRod.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceRod = new TextureRegion(load, 0, 0, 14, 14);
                    iceRod.flip(false, true);
                }
                return iceRod;
            case 144:
                if (librarianStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/librarianStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    librarianStaff = new TextureRegion(load, 0, 0, 14, 14);
                    librarianStaff.flip(false, true);
                }
                return librarianStaff;
            case 145:
                if (knightBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/knightBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    knightBow = new TextureRegion(load, 0, 0, 14, 14);
                    knightBow.flip(false, true);
                }
                return knightBow;
            case 146:
                if (battleSpear == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/battleSpear.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    battleSpear = new TextureRegion(load, 0, 0, 14, 14);
                    battleSpear.flip(false, true);
                }
                return battleSpear;
            case 147:
                if (heavyHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/uncommon/heavyHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    heavyHammer = new TextureRegion(load, 0, 0, 14, 14);
                    heavyHammer.flip(false, true);
                }
                return heavyHammer;
            case 148:
                if (goldHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/goldHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    goldHammer = new TextureRegion(load, 0, 0, 14, 14);
                    goldHammer.flip(false, true);
                }
                return goldHammer;
            case 149:
                if (battery == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/battery.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    battery = new TextureRegion(load, 0, 0, 14, 14);
                    battery.flip(false, true);
                }
                return battery;
            case 150:
                if (cobaltArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/cobaltArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    cobaltArmor = new TextureRegion(load, 0, 0, 14, 14);
                    cobaltArmor.flip(false, true);
                }
                return cobaltArmor;
            case 151:
                if (cobaltHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/cobaltHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    cobaltHelm = new TextureRegion(load, 0, 0, 14, 14);
                    cobaltHelm.flip(false, true);
                }
                return cobaltHelm;
            case 152:
                if (salamanderSkin == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/salamanderSkin.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    salamanderSkin = new TextureRegion(load, 0, 0, 14, 14);
                    salamanderSkin.flip(false, true);
                }
                return salamanderSkin;
            case 153:
                if (salamanderHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/salamanderHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    salamanderHood = new TextureRegion(load, 0, 0, 14, 14);
                    salamanderHood.flip(false, true);
                }
                return salamanderHood;
            case 154:
                if (lightRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/silk/lightRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lightRobe = new TextureRegion(load, 0, 0, 14, 14);
                    lightRobe.flip(false, true);
                }
                return lightRobe;
            case 155:
                if (lightHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/silk/lightHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lightHat = new TextureRegion(load, 0, 0, 14, 14);
                    lightHat.flip(false, true);
                }
                return lightHat;
            case 156:
                if (redBandana == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/redBandana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    redBandana = new TextureRegion(load, 0, 0, 14, 14);
                    redBandana.flip(false, true);
                }
                return redBandana;
            case 157:
                if (paladinArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/paladinArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    paladinArmor = new TextureRegion(load, 0, 0, 14, 14);
                    paladinArmor.flip(false, true);
                }
                return paladinArmor;
            case 158:
                if (hugeAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/hugeAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hugeAxe = new TextureRegion(load, 0, 0, 14, 14);
                    hugeAxe.flip(false, true);
                }
                return hugeAxe;
            case 159:
                if (royalBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/rare/royalBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    royalBlade = new TextureRegion(load, 0, 0, 14, 14);
                    royalBlade.flip(false, true);
                }
                return royalBlade;
            case 160:
                if (altrius == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/altrius.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    altrius = new TextureRegion(load, 0, 0, 14, 14);
                    altrius.flip(false, true);
                }
                return altrius;
            case 161:
                if (knightCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/knightCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    knightCloak = new TextureRegion(load, 0, 0, 14, 14);
                    knightCloak.flip(false, true);
                }
                return knightCloak;
            case 162:
                if (royalCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/royalCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    royalCloak = new TextureRegion(load, 0, 0, 14, 14);
                    royalCloak.flip(false, true);
                }
                return royalCloak;
            case 163:
                if (lightsMight == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/lightsMight.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lightsMight = new TextureRegion(load, 0, 0, 14, 14);
                    lightsMight.flip(false, true);
                }
                return lightsMight;
            case 164:
                if (steelWeight == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/steelWeight.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steelWeight = new TextureRegion(load, 0, 0, 14, 14);
                    steelWeight.flip(false, true);
                }
                return steelWeight;
            case 165:
                if (medal == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/medal.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    medal = new TextureRegion(load, 0, 0, 14, 14);
                    medal.flip(false, true);
                }
                return medal;
            case 166:
                if (darkGem == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/darkGem.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    darkGem = new TextureRegion(load, 0, 0, 14, 14);
                    darkGem.flip(false, true);
                }
                return darkGem;
            case 167:
                if (crystalHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/crystalHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalHelm = new TextureRegion(load, 0, 0, 14, 14);
                    crystalHelm.flip(false, true);
                }
                return crystalHelm;
            case 168:
                if (crystalArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/steel/crystalArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalArmor = new TextureRegion(load, 0, 0, 14, 14);
                    crystalArmor.flip(false, true);
                }
                return crystalArmor;
            case 169:
                if (crystalHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/crystalHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalHood = new TextureRegion(load, 0, 0, 14, 14);
                    crystalHood.flip(false, true);
                }
                return crystalHood;
            case 170:
                if (crystalScale == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/scale/crystalScale.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalScale = new TextureRegion(load, 0, 0, 14, 14);
                    crystalScale.flip(false, true);
                }
                return crystalScale;
            case 171:
                if (crystalHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/silk/crystalHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalHat = new TextureRegion(load, 0, 0, 14, 14);
                    crystalHat.flip(false, true);
                }
                return crystalHat;
            case 172:
                if (crystalRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/silk/crystalRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalRobe = new TextureRegion(load, 0, 0, 14, 14);
                    crystalRobe.flip(false, true);
                }
                return crystalRobe;
            case 173:
                if (crystalStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/crystalStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalStaff = new TextureRegion(load, 0, 0, 14, 14);
                    crystalStaff.flip(false, true);
                }
                return crystalStaff;
            case 174:
                if (crystalAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/crystalAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalAxe = new TextureRegion(load, 0, 0, 14, 14);
                    crystalAxe.flip(false, true);
                }
                return crystalAxe;
            case 175:
                if (crystalBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/crystalBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalBow = new TextureRegion(load, 0, 0, 14, 14);
                    crystalBow.flip(false, true);
                }
                return crystalBow;
            case 176:
                if (crystalHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/crystalHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalHammer = new TextureRegion(load, 0, 0, 14, 14);
                    crystalHammer.flip(false, true);
                }
                return crystalHammer;
            case 177:
                if (crystalSpear == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/crystalSpear.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalSpear = new TextureRegion(load, 0, 0, 14, 14);
                    crystalSpear.flip(false, true);
                }
                return crystalSpear;
            case 178:
                if (crystalSword == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/crystalSword.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crystalSword = new TextureRegion(load, 0, 0, 14, 14);
                    crystalSword.flip(false, true);
                }
                return crystalSword;
            case 179:
                if (pocketChest == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/pocketChest.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    pocketChest = new TextureRegion(load, 0, 0, 14, 14);
                    pocketChest.flip(false, true);
                }
                return pocketChest;
            case 180:
                if (eternity == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/eternity.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eternity = new TextureRegion(load, 0, 0, 14, 14);
                    eternity.flip(false, true);
                }
                return eternity;
            case 181:
                if (slateArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/slate/slateArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    slateArmor = new TextureRegion(load, 0, 0, 14, 14);
                    slateArmor.flip(false, true);
                }
                return slateArmor;
            case 182:
                if (slateHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/slate/slateHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    slateHelm = new TextureRegion(load, 0, 0, 14, 14);
                    slateHelm.flip(false, true);
                }
                return slateHelm;
            case 183:
                if (silverArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/silver/silverArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silverArmor = new TextureRegion(load, 0, 0, 14, 14);
                    silverArmor.flip(false, true);
                }
                return silverArmor;
            case 184:
                if (silverHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/silver/silverHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silverHood = new TextureRegion(load, 0, 0, 14, 14);
                    silverHood.flip(false, true);
                }
                return silverHood;
            case 185:
                if (furRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/fur/furRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    furRobe = new TextureRegion(load, 0, 0, 14, 14);
                    furRobe.flip(false, true);
                }
                return furRobe;
            case 186:
                if (furHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/fur/furHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    furHood = new TextureRegion(load, 0, 0, 14, 14);
                    furHood.flip(false, true);
                }
                return furHood;
            case 187:
                if (obsidianArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/obsidian/obsidianArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidianArmor = new TextureRegion(load, 0, 0, 14, 14);
                    obsidianArmor.flip(false, true);
                }
                return obsidianArmor;
            case 188:
                if (obsidianHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/obsidian/obsidianHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidianHelm = new TextureRegion(load, 0, 0, 14, 14);
                    obsidianHelm.flip(false, true);
                }
                return obsidianHelm;
            case 189:
                if (goldArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/gold/goldArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    goldArmor = new TextureRegion(load, 0, 0, 14, 14);
                    goldArmor.flip(false, true);
                }
                return goldArmor;
            case 190:
                if (goldHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/gold/goldHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    goldHood = new TextureRegion(load, 0, 0, 14, 14);
                    goldHood.flip(false, true);
                }
                return goldHood;
            case 191:
                if (ashRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/ash/ashRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ashRobe = new TextureRegion(load, 0, 0, 14, 14);
                    ashRobe.flip(false, true);
                }
                return ashRobe;
            case 192:
                if (ashHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/ash/ashHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ashHat = new TextureRegion(load, 0, 0, 14, 14);
                    ashHat.flip(false, true);
                }
                return ashHat;
            case 193:
                if (claymore == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/claymore.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    claymore = new TextureRegion(load, 0, 0, 14, 14);
                    claymore.flip(false, true);
                }
                return claymore;
            case 194:
                if (battleAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/battleAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    battleAxe = new TextureRegion(load, 0, 0, 14, 14);
                    battleAxe.flip(false, true);
                }
                return battleAxe;
            case 195:
                if (warStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/warStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    warStaff = new TextureRegion(load, 0, 0, 14, 14);
                    warStaff.flip(false, true);
                }
                return warStaff;
            case 196:
                if (crossbow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/crossbow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    crossbow = new TextureRegion(load, 0, 0, 14, 14);
                    crossbow.flip(false, true);
                }
                return crossbow;
            case 197:
                if (halberd == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/halberd.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    halberd = new TextureRegion(load, 0, 0, 14, 14);
                    halberd.flip(false, true);
                }
                return halberd;
            case 198:
                if (morningStar == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/morningstar.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    morningStar = new TextureRegion(load, 0, 0, 14, 14);
                    morningStar.flip(false, true);
                }
                return morningStar;
            case 199:
                if (secretCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/secretCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    secretCloak = new TextureRegion(load, 0, 0, 14, 14);
                    secretCloak.flip(false, true);
                }
                return secretCloak;
            case 200:
                if (vikingArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/slate/vikingArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    vikingArmor = new TextureRegion(load, 0, 0, 14, 14);
                    vikingArmor.flip(false, true);
                }
                return vikingArmor;
            case 201:
                if (vikingHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/slate/vikingHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    vikingHelm = new TextureRegion(load, 0, 0, 14, 14);
                    vikingHelm.flip(false, true);
                }
                return vikingHelm;
            case 202:
                if (vikingVest == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/silver/vikingVest.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    vikingVest = new TextureRegion(load, 0, 0, 14, 14);
                    vikingVest.flip(false, true);
                }
                return vikingVest;
            case 203:
                if (vikingHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/silver/vikingCap.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    vikingHat = new TextureRegion(load, 0, 0, 14, 14);
                    vikingHat.flip(false, true);
                }
                return vikingHat;
            case 204:
                if (roundShield == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/roundShield.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    roundShield = new TextureRegion(load, 0, 0, 14, 14);
                    roundShield.flip(false, true);
                }
                return roundShield;
            case 205:
                if (vikingAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/vikingAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    vikingAxe = new TextureRegion(load, 0, 0, 14, 14);
                    vikingAxe.flip(false, true);
                }
                return vikingAxe;
            case 206:
                if (vikingCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/vikingCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    vikingCloak = new TextureRegion(load, 0, 0, 14, 14);
                    vikingCloak.flip(false, true);
                }
                return vikingCloak;
            case 207:
                if (oracleCoat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/silver/oracleCoat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    oracleCoat = new TextureRegion(load, 0, 0, 14, 14);
                    oracleCoat.flip(false, true);
                }
                return oracleCoat;
            case 208:
                if (seerBandana == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/seerBandana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    seerBandana = new TextureRegion(load, 0, 0, 14, 14);
                    seerBandana.flip(false, true);
                }
                return seerBandana;
            case 209:
                if (seaCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/aquaCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    seaCloak = new TextureRegion(load, 0, 0, 14, 14);
                    seaCloak.flip(false, true);
                }
                return seaCloak;
            case 210:
                if (blueLeaf == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/blueLeaf.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blueLeaf = new TextureRegion(load, 0, 0, 14, 14);
                    blueLeaf.flip(false, true);
                }
                return blueLeaf;
            case 211:
                if (aquaFist == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/aquaFist.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    aquaFist = new TextureRegion(load, 0, 0, 14, 14);
                    aquaFist.flip(false, true);
                }
                return aquaFist;
            case 212:
                if (pirateHook == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/pirateHook.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    pirateHook = new TextureRegion(load, 0, 0, 14, 14);
                    pirateHook.flip(false, true);
                }
                return pirateHook;
            case 213:
                if (pirateBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/pirateBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    pirateBlade = new TextureRegion(load, 0, 0, 14, 14);
                    pirateBlade.flip(false, true);
                }
                return pirateBlade;
            case 214:
                if (fishingPole == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/oldRod.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    fishingPole = new TextureRegion(load, 0, 0, 14, 14);
                    fishingPole.flip(false, true);
                }
                return fishingPole;
            case 215:
                if (harpoon == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/harpoon.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    harpoon = new TextureRegion(load, 0, 0, 14, 14);
                    harpoon.flip(false, true);
                }
                return harpoon;
            case 216:
                if (pegLeg == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/pegLeg.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    pegLeg = new TextureRegion(load, 0, 0, 14, 14);
                    pegLeg.flip(false, true);
                }
                return pegLeg;
            case 217:
                if (cannon == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/cannon.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    cannon = new TextureRegion(load, 0, 0, 14, 14);
                    cannon.flip(false, true);
                }
                return cannon;
            case 218:
                if (tsunami == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/tsunami.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    tsunami = new TextureRegion(load, 0, 0, 14, 14);
                    tsunami.flip(false, true);
                }
                return tsunami;
            case 219:
                if (leviathan == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/leviathan.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    leviathan = new TextureRegion(load, 0, 0, 14, 14);
                    leviathan.flip(false, true);
                }
                return leviathan;
            case 220:
                if (parrotRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/fur/parrotRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    parrotRobe = new TextureRegion(load, 0, 0, 14, 14);
                    parrotRobe.flip(false, true);
                }
                return parrotRobe;
            case 221:
                if (parrotHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/fur/parrotHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    parrotHood = new TextureRegion(load, 0, 0, 14, 14);
                    parrotHood.flip(false, true);
                }
                return parrotHood;
            case 222:
                if (bomb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/bomb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    bomb = new TextureRegion(load, 0, 0, 14, 14);
                    bomb.flip(false, true);
                }
                return bomb;
            case 223:
                if (bone == null) {
                    load = new Texture(Gdx.files.internal("items/materials/bone.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    bone = new TextureRegion(load, 0, 0, 14, 14);
                    bone.flip(false, true);
                }
                return bone;
            case 224:
                if (cotton == null) {
                    load = new Texture(Gdx.files.internal("items/materials/cotton.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    cotton = new TextureRegion(load, 0, 0, 14, 14);
                    cotton.flip(false, true);
                }
                return cotton;
            case 225:
                if (fur == null) {
                    load = new Texture(Gdx.files.internal("items/materials/fur.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    fur = new TextureRegion(load, 0, 0, 14, 14);
                    fur.flip(false, true);
                }
                return fur;
            case 226:
                if (gold == null) {
                    load = new Texture(Gdx.files.internal("items/materials/gold.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    gold = new TextureRegion(load, 0, 0, 14, 14);
                    gold.flip(false, true);
                }
                return gold;
            case 227:
                if (iron == null) {
                    load = new Texture(Gdx.files.internal("items/materials/iron.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iron = new TextureRegion(load, 0, 0, 14, 14);
                    iron.flip(false, true);
                }
                return iron;
            case 228:
                if (leather == null) {
                    load = new Texture(Gdx.files.internal("items/materials/leather.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    leather = new TextureRegion(load, 0, 0, 14, 14);
                    leather.flip(false, true);
                }
                return leather;
            case 229:
                if (obsidian == null) {
                    load = new Texture(Gdx.files.internal("items/materials/obsidian.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidian = new TextureRegion(load, 0, 0, 14, 14);
                    obsidian.flip(false, true);
                }
                return obsidian;
            case 230:
                if (scale == null) {
                    load = new Texture(Gdx.files.internal("items/materials/scale.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    scale = new TextureRegion(load, 0, 0, 14, 14);
                    scale.flip(false, true);
                }
                return scale;
            case 231:
                if (silk == null) {
                    load = new Texture(Gdx.files.internal("items/materials/silk.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silk = new TextureRegion(load, 0, 0, 14, 14);
                    silk.flip(false, true);
                }
                return silk;
            case 232:
                if (silver == null) {
                    load = new Texture(Gdx.files.internal("items/materials/silver.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silver = new TextureRegion(load, 0, 0, 14, 14);
                    silver.flip(false, true);
                }
                return silver;
            case 233:
                if (slate == null) {
                    load = new Texture(Gdx.files.internal("items/materials/slate.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    slate = new TextureRegion(load, 0, 0, 14, 14);
                    slate.flip(false, true);
                }
                return slate;
            case 234:
                if (steel == null) {
                    load = new Texture(Gdx.files.internal("items/materials/steel.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    steel = new TextureRegion(load, 0, 0, 14, 14);
                    steel.flip(false, true);
                }
                return steel;
            default:
            case 235:
                if (wood == null) {
                    load = new Texture(Gdx.files.internal("items/materials/wood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wood = new TextureRegion(load, 0, 0, 14, 14);
                    wood.flip(false, true);
                }
                return wood;
            case 236:
                if (wool == null) {
                    load = new Texture(Gdx.files.internal("items/materials/wool.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wool = new TextureRegion(load, 0, 0, 14, 14);
                    wool.flip(false, true);
                }
                return wool;
            case 237:
                if (heroBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/heroBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    heroBlade = new TextureRegion(load, 0, 0, 14, 14);
                    heroBlade.flip(false, true);
                }
                return heroBlade;
            case 238:
                if (heroBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/heroBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    heroBow = new TextureRegion(load, 0, 0, 14, 14);
                    heroBow.flip(false, true);
                }
                return heroBow;
            case 239:
                if (heroHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/wooden/heroHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    heroHammer = new TextureRegion(load, 0, 0, 14, 14);
                    heroHammer.flip(false, true);
                }
                return heroHammer;
            case 240:
                if (surfShirt == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/surfShirt.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    surfShirt = new TextureRegion(load, 0, 0, 14, 14);
                    surfShirt.flip(false, true);
                }
                return surfShirt;
            case 241:
                if (turtleShell == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/turtleShell.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    turtleShell = new TextureRegion(load, 0, 0, 14, 14);
                    turtleShell.flip(false, true);
                }
                return turtleShell;
            case 242:
                if (venusCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/venusCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    venusCloak = new TextureRegion(load, 0, 0, 14, 14);
                    venusCloak.flip(false, true);
                }
                return venusCloak;
            case 243:
                if (charCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/charCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    charCloak = new TextureRegion(load, 0, 0, 14, 14);
                    charCloak.flip(false, true);
                }
                return charCloak;
            case 244:
                if (blastCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/blastCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blastCloak = new TextureRegion(load, 0, 0, 14, 14);
                    blastCloak.flip(false, true);
                }
                return blastCloak;
            case 245:
                if (icebook == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/icebook.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    icebook = new TextureRegion(load, 0, 0, 14, 14);
                    icebook.flip(false, true);
                }
                return icebook;
            case 246:
                if (ltArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/slate/ltArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ltArmor = new TextureRegion(load, 0, 0, 14, 14);
                    ltArmor.flip(false, true);
                }
                return ltArmor;
            case 247:
                if (ltVest == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/silver/ltVest.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ltVest = new TextureRegion(load, 0, 0, 14, 14);
                    ltVest.flip(false, true);
                }
                return ltVest;
            case 248:
                if (ltRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/fur/ltRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ltRobe = new TextureRegion(load, 0, 0, 14, 14);
                    ltRobe.flip(false, true);
                }
                return ltRobe;
            case 249:
                if (sandDollar == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/sandDollar.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sandDollar = new TextureRegion(load, 0, 0, 14, 14);
                    sandDollar.flip(false, true);
                }
                return sandDollar;
            case 250:
                if (poisonVial == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/poisonVial.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    poisonVial = new TextureRegion(load, 0, 0, 14, 14);
                    poisonVial.flip(false, true);
                }
                return poisonVial;
            case 251:
                if (musicBook == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/musicBook.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    musicBook = new TextureRegion(load, 0, 0, 14, 14);
                    musicBook.flip(false, true);
                }
                return musicBook;
            case 252:
                if (orgoBoots == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/orgoBoots.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    orgoBoots = new TextureRegion(load, 0, 0, 14, 14);
                    orgoBoots.flip(false, true);
                }
                return orgoBoots;
            case 253:
                if (bigOrgoSword == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/bigOrgoSword.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    bigOrgoSword = new TextureRegion(load, 0, 0, 14, 14);
                    bigOrgoSword.flip(false, true);
                }
                return bigOrgoSword;
            case 254:
                if (musicBook == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/musicBook.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    musicBook = new TextureRegion(load, 0, 0, 14, 14);
                    musicBook.flip(false, true);
                }
                return musicBook;
            case 255:
                if (choirCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/choirCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    choirCloak = new TextureRegion(load, 0, 0, 14, 14);
                    choirCloak.flip(false, true);
                }
                return choirCloak;
            case 256:
                if (goldenHarp == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/goldenHarp.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    goldenHarp = new TextureRegion(load, 0, 0, 14, 14);
                    goldenHarp.flip(false, true);
                }
                return goldenHarp;
            case 257:
                if (orgoHorn == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/orgoHorn.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    orgoHorn = new TextureRegion(load, 0, 0, 14, 14);
                    orgoHorn.flip(false, true);
                }
                return orgoHorn;
            case 258:
                if (slimeCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/slimeCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    slimeCloak = new TextureRegion(load, 0, 0, 14, 14);
                    slimeCloak.flip(false, true);
                }
                return slimeCloak;
            case 259:
                if (spiritCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/spiritCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    spiritCloak = new TextureRegion(load, 0, 0, 14, 14);
                    spiritCloak.flip(false, true);
                }
                return spiritCloak;
            case 260:
                if (doomscythe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/epic/doomscythe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    doomscythe = new TextureRegion(load, 0, 0, 14, 14);
                    doomscythe.flip(false, true);
                }
                return doomscythe;
            case 261:
                if (sandsOfTime == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/sandsOfTime.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sandsOfTime = new TextureRegion(load, 0, 0, 14, 14);
                    sandsOfTime.flip(false, true);
                }
                return sandsOfTime;
            case 262:
                if (obsidianBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/obsidianBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidianBlade = new TextureRegion(load, 0, 0, 14, 14);
                    obsidianBlade.flip(false, true);
                }
                return obsidianBlade;
            case 263:
                if (obsidianAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/obsidianAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidianAxe = new TextureRegion(load, 0, 0, 14, 14);
                    obsidianAxe.flip(false, true);
                }
                return obsidianAxe;
            case 264:
                if (obsidianStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/obsidianStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidianStaff = new TextureRegion(load, 0, 0, 14, 14);
                    obsidianStaff.flip(false, true);
                }
                return obsidianStaff;
            case 265:
                if (obsidianBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/obsidianBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidianBow = new TextureRegion(load, 0, 0, 14, 14);
                    obsidianBow.flip(false, true);
                }
                return obsidianBow;
            case 266:
                if (obsidianSpear == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/obsidianSpear.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidianSpear = new TextureRegion(load, 0, 0, 14, 14);
                    obsidianSpear.flip(false, true);
                }
                return obsidianSpear;
            case 267:
                if (obsidianHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/obsidianHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    obsidianHammer = new TextureRegion(load, 0, 0, 14, 14);
                    obsidianHammer.flip(false, true);
                }
                return obsidianHammer;
            case 268:
                if (magmaMace == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/magmaMace.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magmaMace = new TextureRegion(load, 0, 0, 14, 14);
                    magmaMace.flip(false, true);
                }
                return magmaMace;
            case 269:
                if (pyreStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/pyreStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    pyreStaff = new TextureRegion(load, 0, 0, 14, 14);
                    pyreStaff.flip(false, true);
                }
                return pyreStaff;
            case 270:
                if (ash == null) {
                    load = new Texture(Gdx.files.internal("items/materials/ash.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ash = new TextureRegion(load, 0, 0, 14, 14);
                    ash.flip(false, true);
                }
                return ash;
            case 271:
                if (magmaSword == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/magmaSword.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magmaSword = new TextureRegion(load, 0, 0, 14, 14);
                    magmaSword.flip(false, true);
                }
                return magmaSword;
            case 272:
                if (magmaAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/magmaAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magmaAxe = new TextureRegion(load, 0, 0, 14, 14);
                    magmaAxe.flip(false, true);
                }
                return magmaAxe;
            case 273:
                if (magmaBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/magmaBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magmaBow = new TextureRegion(load, 0, 0, 14, 14);
                    magmaBow.flip(false, true);
                }
                return magmaBow;
            case 274:
                if (magmaLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/magmaLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magmaLance = new TextureRegion(load, 0, 0, 14, 14);
                    magmaLance.flip(false, true);
                }
                return magmaLance;
            case 275:
                if (artillery == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/artillery.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    artillery = new TextureRegion(load, 0, 0, 14, 14);
                    artillery.flip(false, true);
                }
                return artillery;
            case 276:
                if (heatBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/heatBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    heatBlade = new TextureRegion(load, 0, 0, 14, 14);
                    heatBlade.flip(false, true);
                }
                return heatBlade;
            case 277:
                if (flameArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/obsidian/flameArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flameArmor = new TextureRegion(load, 0, 0, 14, 14);
                    flameArmor.flip(false, true);
                }
                return flameArmor;
            case 278:
                if (flameHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/obsidian/flameHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flameHelm = new TextureRegion(load, 0, 0, 14, 14);
                    flameHelm.flip(false, true);
                }
                return flameHelm;
            case 279:
                if (flameVest == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/gold/flameVest.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flameVest = new TextureRegion(load, 0, 0, 14, 14);
                    flameVest.flip(false, true);
                }
                return flameVest;
            case 280:
                if (flameHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/gold/flameHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flameHood = new TextureRegion(load, 0, 0, 14, 14);
                    flameHood.flip(false, true);
                }
                return flameHood;
            case 281:
                if (flameRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/ash/flameRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flameRobe = new TextureRegion(load, 0, 0, 14, 14);
                    flameRobe.flip(false, true);
                }
                return flameRobe;
            case 282:
                if (flameHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/ash/flameHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flameHat = new TextureRegion(load, 0, 0, 14, 14);
                    flameHat.flip(false, true);
                }
                return flameHat;
            case 283:
                if (lavaHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/obsidian/lavaHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lavaHelm = new TextureRegion(load, 0, 0, 14, 14);
                    lavaHelm.flip(false, true);
                }
                return lavaHelm;
            case 284:
                if (lavaHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/gold/lavaHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lavaHood = new TextureRegion(load, 0, 0, 14, 14);
                    lavaHood.flip(false, true);
                }
                return lavaHood;
            case 285:
                if (lavaHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/ash/lavaHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lavaHat = new TextureRegion(load, 0, 0, 14, 14);
                    lavaHat.flip(false, true);
                }
                return lavaHat;
            case 286:
                if (dragonBandana == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/dragonBandana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonBandana = new TextureRegion(load, 0, 0, 14, 14);
                    dragonBandana.flip(false, true);
                }
                return dragonBandana;
            case 287:
                if (regalArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/obsidian/regalArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    regalArmor = new TextureRegion(load, 0, 0, 14, 14);
                    regalArmor.flip(false, true);
                }
                return regalArmor;
            case 288:
                if (anchor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/anchor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    anchor = new TextureRegion(load, 0, 0, 14, 14);
                    anchor.flip(false, true);
                }
                return anchor;
            case 289:
                if (lawmaker == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/lawmaker.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lawmaker = new TextureRegion(load, 0, 0, 14, 14);
                    lawmaker.flip(false, true);
                }
                return lawmaker;
            case 290:
                if (regalCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/regalCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    regalCloak = new TextureRegion(load, 0, 0, 14, 14);
                    regalCloak.flip(false, true);
                }
                return regalCloak;
            case 291:
                if (oldLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/oldLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    oldLance = new TextureRegion(load, 0, 0, 14, 14);
                    oldLance.flip(false, true);
                }
                return oldLance;
            case 292:
                if (serpentLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/serpentLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    serpentLance = new TextureRegion(load, 0, 0, 14, 14);
                    serpentLance.flip(false, true);
                }
                return serpentLance;
            case 293:
                if (orgoLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/orgoLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    orgoLance = new TextureRegion(load, 0, 0, 14, 14);
                    orgoLance.flip(false, true);
                }
                return orgoLance;
            case 294:
                if (lavaLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/lavaLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lavaLance = new TextureRegion(load, 0, 0, 14, 14);
                    lavaLance.flip(false, true);
                }
                return lavaLance;
            case 295:
                if (dragonBane == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/dragonBane.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonBane = new TextureRegion(load, 0, 0, 14, 14);
                    dragonBane.flip(false, true);
                }
                return dragonBane;
            case 296:
                if (viperVenom == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/viperVenom.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    viperVenom = new TextureRegion(load, 0, 0, 14, 14);
                    viperVenom.flip(false, true);
                }
                return viperVenom;
            case 297:
                if (rawObsidian == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/rawObsidian.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rawObsidian = new TextureRegion(load, 0, 0, 14, 14);
                    rawObsidian.flip(false, true);
                }
                return rawObsidian;
            case 298:
                if (dragonScale == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/dragonScale.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonScale = new TextureRegion(load, 0, 0, 14, 14);
                    dragonScale.flip(false, true);
                }
                return dragonScale;
            case 299:
                if (dragonAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/dragonAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonAxe = new TextureRegion(load, 0, 0, 14, 14);
                    dragonAxe.flip(false, true);
                }
                return dragonAxe;
            case 300:
                if (dragonCoat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/dragonCoat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonCoat = new TextureRegion(load, 0, 0, 14, 14);
                    dragonCoat.flip(false, true);
                }
                return dragonCoat;
            case 301:
                if (blizzard == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/blizzard.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blizzard = new TextureRegion(load, 0, 0, 14, 14);
                    blizzard.flip(false, true);
                }
                return blizzard;
            case 302:
                if (regalRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/ash/regalRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    regalRobe = new TextureRegion(load, 0, 0, 14, 14);
                    regalRobe.flip(false, true);
                }
                return regalRobe;
            case 303:
                if (regalVest == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/gold/regalVest.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    regalVest = new TextureRegion(load, 0, 0, 14, 14);
                    regalVest.flip(false, true);
                }
                return regalVest;
            case 304:
                if (iceFang == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/iceFang.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceFang = new TextureRegion(load, 0, 0, 14, 14);
                    iceFang.flip(false, true);
                }
                return iceFang;
            case 305:
                if (stormHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/stormHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    stormHammer = new TextureRegion(load, 0, 0, 14, 14);
                    stormHammer.flip(false, true);
                }
                return stormHammer;
            case 306:
                if (dragonFang == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/dragonFang.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonFang = new TextureRegion(load, 0, 0, 14, 14);
                    dragonFang.flip(false, true);
                }
                return dragonFang;
            case 307:
                if (rubberDuck == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/rubberDuck.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rubberDuck = new TextureRegion(load, 0, 0, 14, 14);
                    rubberDuck.flip(false, true);
                }
                return rubberDuck;
            case 308:
                if (iceCube == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/iceCube.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceCube = new TextureRegion(load, 0, 0, 14, 14);
                    iceCube.flip(false, true);
                }
                return iceCube;
            case 309:
                if (hotPocket == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/hotPocket.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hotPocket = new TextureRegion(load, 0, 0, 14, 14);
                    hotPocket.flip(false, true);
                }
                return hotPocket;
            case 310:
                if (rainbowScale == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/rainbowScale.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rainbowScale = new TextureRegion(load, 0, 0, 14, 14);
                    rainbowScale.flip(false, true);
                }
                return rainbowScale;
            case 311:
                if (arenaArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/obsidian/arenaArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaArmor = new TextureRegion(load, 0, 0, 14, 14);
                    arenaArmor.flip(false, true);
                }
                return arenaArmor;
            case 312:
                if (arenaVest == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/gold/arenaVest.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaVest = new TextureRegion(load, 0, 0, 14, 14);
                    arenaVest.flip(false, true);
                }
                return arenaVest;
            case 313:
                if (arenaRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/ash/arenaRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaRobe = new TextureRegion(load, 0, 0, 14, 14);
                    arenaRobe.flip(false, true);
                }
                return arenaRobe;
            case 314:
                if (arenaHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/obsidian/arenaHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaHelm = new TextureRegion(load, 0, 0, 14, 14);
                    arenaHelm.flip(false, true);
                }
                return arenaHelm;
            case 315:
                if (arenaHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/gold/arenaHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaHood = new TextureRegion(load, 0, 0, 14, 14);
                    arenaHood.flip(false, true);
                }
                return arenaHood;
            case 316:
                if (arenaHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/ash/arenaHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaHat = new TextureRegion(load, 0, 0, 14, 14);
                    arenaHat.flip(false, true);
                }
                return arenaHat;
            case 317:
                if (arenaBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/arenaBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaBlade = new TextureRegion(load, 0, 0, 14, 14);
                    arenaBlade.flip(false, true);
                }
                return arenaBlade;
            case 318:
                if (arenaAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/arenaAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaAxe = new TextureRegion(load, 0, 0, 14, 14);
                    arenaAxe.flip(false, true);
                }
                return arenaAxe;
            case 319:
                if (arenaMace == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/arenaMace.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaMace = new TextureRegion(load, 0, 0, 14, 14);
                    arenaMace.flip(false, true);
                }
                return arenaMace;
            case 320:
                if (arenaStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/arenaStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaStaff = new TextureRegion(load, 0, 0, 14, 14);
                    arenaStaff.flip(false, true);
                }
                return arenaStaff;
            case 321:
                if (arenaBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/arenaBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaBow = new TextureRegion(load, 0, 0, 14, 14);
                    arenaBow.flip(false, true);
                }
                return arenaBow;
            case 322:
                if (arenaLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/obsidian/arenaLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arenaLance = new TextureRegion(load, 0, 0, 14, 14);
                    arenaLance.flip(false, true);
                }
                return arenaLance;
            case 323:
                if (prismaCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/prismaCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    prismaCloak = new TextureRegion(load, 0, 0, 14, 14);
                    prismaCloak.flip(false, true);
                }
                return prismaCloak;
            case 324:
                if (snow == null) {
                    load = new Texture(Gdx.files.internal("items/materials/snow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    snow = new TextureRegion(load, 0, 0, 14, 14);
                    snow.flip(false, true);
                }
                return snow;
            case 325:
                if (chain == null) {
                    load = new Texture(Gdx.files.internal("items/materials/chain.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    chain = new TextureRegion(load, 0, 0, 14, 14);
                    chain.flip(false, true);
                }
                return chain;
            case 326:
                if (ice == null) {
                    load = new Texture(Gdx.files.internal("items/materials/ice.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ice = new TextureRegion(load, 0, 0, 14, 14);
                    ice.flip(false, true);
                }
                return ice;
            case 327:
                if (iceSword == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/iceSword.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceSword = new TextureRegion(load, 0, 0, 14, 14);
                    iceSword.flip(false, true);
                }
                return iceSword;
            case 328:
                if (iceAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/iceAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceAxe = new TextureRegion(load, 0, 0, 14, 14);
                    iceAxe.flip(false, true);
                }
                return iceAxe;
            case 329:
                if (iceStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/iceStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceStaff = new TextureRegion(load, 0, 0, 14, 14);
                    iceStaff.flip(false, true);
                }
                return iceStaff;
            case 330:
                if (iceStar == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/iceStar.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceStar = new TextureRegion(load, 0, 0, 14, 14);
                    iceStar.flip(false, true);
                }
                return iceStar;
            case 331:
                if (iceLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/iceLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceLance = new TextureRegion(load, 0, 0, 14, 14);
                    iceLance.flip(false, true);
                }
                return iceLance;
            case 332:
                if (iceHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/iceHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceHammer = new TextureRegion(load, 0, 0, 14, 14);
                    iceHammer.flip(false, true);
                }
                return iceHammer;
            case 333:
                if (frostBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/frostBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    frostBlade = new TextureRegion(load, 0, 0, 14, 14);
                    frostBlade.flip(false, true);
                }
                return frostBlade;
            case 334:
                if (frostAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/frostAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    frostAxe = new TextureRegion(load, 0, 0, 14, 14);
                    frostAxe.flip(false, true);
                }
                return frostAxe;
            case 335:
                if (frostStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/frostStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    frostStaff = new TextureRegion(load, 0, 0, 14, 14);
                    frostStaff.flip(false, true);
                }
                return frostStaff;
            case 336:
                if (moonBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/moonBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    moonBow = new TextureRegion(load, 0, 0, 14, 14);
                    moonBow.flip(false, true);
                }
                return moonBow;
            case 337:
                if (frostLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/frostLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    frostLance = new TextureRegion(load, 0, 0, 14, 14);
                    frostLance.flip(false, true);
                }
                return frostLance;
            case 338:
                if (frostMace == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/frostMace.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    frostMace = new TextureRegion(load, 0, 0, 14, 14);
                    frostMace.flip(false, true);
                }
                return frostMace;
            case 339:
                if (iceArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ice/iceArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceArmor = new TextureRegion(load, 0, 0, 14, 14);
                    iceArmor.flip(false, true);
                }
                return iceArmor;
            case 340:
                if (iceHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ice/iceHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    iceHelm = new TextureRegion(load, 0, 0, 14, 14);
                    iceHelm.flip(false, true);
                }
                return iceHelm;
            case 341:
                if (chainArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/chain/chainArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    chainArmor = new TextureRegion(load, 0, 0, 14, 14);
                    chainArmor.flip(false, true);
                }
                return chainArmor;
            case 342:
                if (chainHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/chain/chainHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    chainHelm = new TextureRegion(load, 0, 0, 14, 14);
                    chainHelm.flip(false, true);
                }
                return chainHelm;
            case 343:
                if (snowRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/snow/snowRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    snowRobe = new TextureRegion(load, 0, 0, 14, 14);
                    snowRobe.flip(false, true);
                }
                return snowRobe;
            case 344:
                if (snowHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/snow/snowHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    snowHat = new TextureRegion(load, 0, 0, 14, 14);
                    snowHat.flip(false, true);
                }
                return snowHat;
            case 345:
                if (frostArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ice/frostArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    frostArmor = new TextureRegion(load, 0, 0, 14, 14);
                    frostArmor.flip(false, true);
                }
                return frostArmor;
            case 346:
                if (frostHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ice/frostHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    frostHelm = new TextureRegion(load, 0, 0, 14, 14);
                    frostHelm.flip(false, true);
                }
                return frostHelm;
            case 347:
                if (moonCoat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/chain/moonCoat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    moonCoat = new TextureRegion(load, 0, 0, 14, 14);
                    moonCoat.flip(false, true);
                }
                return moonCoat;
            case 348:
                if (moonHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/chain/moonHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    moonHelm = new TextureRegion(load, 0, 0, 14, 14);
                    moonHelm.flip(false, true);
                }
                return moonHelm;
            case 349:
                if (starRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/snow/starRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    starRobe = new TextureRegion(load, 0, 0, 14, 14);
                    starRobe.flip(false, true);
                }
                return starRobe;
            case 350:
                if (starHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/snow/starHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    starHat = new TextureRegion(load, 0, 0, 14, 14);
                    starHat.flip(false, true);
                }
                return starHat;
            case 351:
                if (yetiArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ice/yetiArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    yetiArmor = new TextureRegion(load, 0, 0, 14, 14);
                    yetiArmor.flip(false, true);
                }
                return yetiArmor;
            case 352:
                if (sorcererRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/snow/sorcererRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sorcererRobe = new TextureRegion(load, 0, 0, 14, 14);
                    sorcererRobe.flip(false, true);
                }
                return sorcererRobe;
            case 353:
                if (sorcererHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/snow/sorcererHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sorcererHat = new TextureRegion(load, 0, 0, 14, 14);
                    sorcererHat.flip(false, true);
                }
                return sorcererHat;
            case 354:
                if (starFall == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/starFall.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    starFall = new TextureRegion(load, 0, 0, 14, 14);
                    starFall.flip(false, true);
                }
                return starFall;
            case 355:
                if (blueScarf == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/blueScarf.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blueScarf = new TextureRegion(load, 0, 0, 14, 14);
                    blueScarf.flip(false, true);
                }
                return blueScarf;
            case 356:
                if (blueScarf == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/blueScarf.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blueScarf = new TextureRegion(load, 0, 0, 14, 14);
                    blueScarf.flip(false, true);
                }
                return blueScarf;
            case 357:
                if (frostBane == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/frostBane.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    frostBane = new TextureRegion(load, 0, 0, 14, 14);
                    frostBane.flip(false, true);
                }
                return frostBane;
            case 358:
                if (huskyCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/huskyCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    huskyCloak = new TextureRegion(load, 0, 0, 14, 14);
                    huskyCloak.flip(false, true);
                }
                return huskyCloak;
            case 359:
                if (huskyFang == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/huskyFang.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    huskyFang = new TextureRegion(load, 0, 0, 14, 14);
                    huskyFang.flip(false, true);
                }
                return huskyFang;
            case 360:
                if (stormCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/stormCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    stormCloak = new TextureRegion(load, 0, 0, 14, 14);
                    stormCloak.flip(false, true);
                }
                return stormCloak;
            case 361:
                if (berryRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/snow/berryRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    berryRobe = new TextureRegion(load, 0, 0, 14, 14);
                    berryRobe.flip(false, true);
                }
                return berryRobe;
            case 362:
                if (moonCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/moonCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    moonCloak = new TextureRegion(load, 0, 0, 14, 14);
                    moonCloak.flip(false, true);
                }
                return moonCloak;
            case 363:
                if (blueBerry == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/blueBerry.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blueBerry = new TextureRegion(load, 0, 0, 14, 14);
                    blueBerry.flip(false, true);
                }
                return blueBerry;
            case 364:
                if (letter == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/letter.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    letter = new TextureRegion(load, 0, 0, 14, 14);
                    letter.flip(false, true);
                }
                return letter;
            case 365:
                if (cloth == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/cloth.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    cloth = new TextureRegion(load, 0, 0, 14, 14);
                    cloth.flip(false, true);
                }
                return cloth;
            case 366:
                if (sailorAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/pirate/sailorAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sailorAxe = new TextureRegion(load, 0, 0, 14, 14);
                    sailorAxe.flip(false, true);
                }
                return sailorAxe;
            case 367:
                if (cookieBox == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/cookieBox.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    cookieBox = new TextureRegion(load, 0, 0, 14, 14);
                    cookieBox.flip(false, true);
                }
                return cookieBox;
            case 368:
                if (bombPowder == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/bombPowder.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    bombPowder = new TextureRegion(load, 0, 0, 14, 14);
                    bombPowder.flip(false, true);
                }
                return bombPowder;
            case 369:
                if (redQuilt == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/redQuilt.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    redQuilt = new TextureRegion(load, 0, 0, 14, 14);
                    redQuilt.flip(false, true);
                }
                return redQuilt;
            case 370:
                if (miniHeater == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/miniHeater.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    miniHeater = new TextureRegion(load, 0, 0, 14, 14);
                    miniHeater.flip(false, true);
                }
                return miniHeater;
            case 371:
                if (flameJar == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/flameJar.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flameJar = new TextureRegion(load, 0, 0, 14, 14);
                    flameJar.flip(false, true);
                }
                return flameJar;
            case 372:
                if (thunderbolt == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ice/thunderBolt.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    thunderbolt = new TextureRegion(load, 0, 0, 14, 14);
                    thunderbolt.flip(false, true);
                }
                return thunderbolt;
            case 373:
                if (tigerFang == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/tigerFang.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    tigerFang = new TextureRegion(load, 0, 0, 14, 14);
                    tigerFang.flip(false, true);
                }
                return tigerFang;
            case 374:
                if (scarlet == null) {
                    load = new Texture(Gdx.files.internal("items/materials/scarlet.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    scarlet = new TextureRegion(load, 0, 0, 14, 14);
                    scarlet.flip(false, true);
                }
                return scarlet;
            case 375:
                if (mithril == null) {
                    load = new Texture(Gdx.files.internal("items/materials/mithril.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    mithril = new TextureRegion(load, 0, 0, 14, 14);
                    mithril.flip(false, true);
                }
                return mithril;
            case 376:
                if (titanium == null) {
                    load = new Texture(Gdx.files.internal("items/materials/titanium.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titanium = new TextureRegion(load, 0, 0, 14, 14);
                    titanium.flip(false, true);
                }
                return titanium;
            case 377:
                if (titaniumArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/titanium/titaniumArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titaniumArmor = new TextureRegion(load, 0, 0, 14, 14);
                    titaniumArmor.flip(false, true);
                }
                return titaniumArmor;
            case 378:
                if (titaniumHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/titanium/titaniumHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titaniumHelm = new TextureRegion(load, 0, 0, 14, 14);
                    titaniumHelm.flip(false, true);
                }
                return titaniumHelm;
            case 379:
                if (mithrilArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/mithril/mithrilArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    mithrilArmor = new TextureRegion(load, 0, 0, 14, 14);
                    mithrilArmor.flip(false, true);
                }
                return mithrilArmor;
            case 380:
                if (mithrilHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/mithril/mithrilHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    mithrilHood = new TextureRegion(load, 0, 0, 14, 14);
                    mithrilHood.flip(false, true);
                }
                return mithrilHood;
            case 381:
                if (scarletRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/scarletRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    scarletRobe = new TextureRegion(load, 0, 0, 14, 14);
                    scarletRobe.flip(false, true);
                }
                return scarletRobe;
            case 382:
                if (scarletHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/scarletHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    scarletHat = new TextureRegion(load, 0, 0, 14, 14);
                    scarletHat.flip(false, true);
                }
                return scarletHat;
            case 383:
                if (templarArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/titanium/templarArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    templarArmor = new TextureRegion(load, 0, 0, 14, 14);
                    templarArmor.flip(false, true);
                }
                return templarArmor;
            case 384:
                if (templarHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/titanium/templarHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    templarHelm = new TextureRegion(load, 0, 0, 14, 14);
                    templarHelm.flip(false, true);
                }
                return templarHelm;
            case 385:
                if (hunterArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/mithril/hunterArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hunterArmor = new TextureRegion(load, 0, 0, 14, 14);
                    hunterArmor.flip(false, true);
                }
                return hunterArmor;
            case 386:
                if (hunterHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/mithril/hunterHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hunterHood = new TextureRegion(load, 0, 0, 14, 14);
                    hunterHood.flip(false, true);
                }
                return hunterHood;
            case 387:
                if (magusRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/magusRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magusRobe = new TextureRegion(load, 0, 0, 14, 14);
                    magusRobe.flip(false, true);
                }
                return magusRobe;
            case 388:
                if (magusHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/magusHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magusHat = new TextureRegion(load, 0, 0, 14, 14);
                    magusHat.flip(false, true);
                }
                return magusHat;
            case 389:
                if (soulArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/titanium/soulArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    soulArmor = new TextureRegion(load, 0, 0, 14, 14);
                    soulArmor.flip(false, true);
                }
                return soulArmor;
            case 390:
                if (soulHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/titanium/soulHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    soulHelm = new TextureRegion(load, 0, 0, 14, 14);
                    soulHelm.flip(false, true);
                }
                return soulHelm;
            case 391:
                if (titaniumBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/titaniumBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titaniumBlade = new TextureRegion(load, 0, 0, 14, 14);
                    titaniumBlade.flip(false, true);
                }
                return titaniumBlade;
            case 392:
                if (titaniumAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/titaniumAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titaniumAxe = new TextureRegion(load, 0, 0, 14, 14);
                    titaniumAxe.flip(false, true);
                }
                return titaniumAxe;
            case 393:
                if (titaniumStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/titaniumStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titaniumStaff = new TextureRegion(load, 0, 0, 14, 14);
                    titaniumStaff.flip(false, true);
                }
                return titaniumStaff;
            case 394:
                if (titaniumStar == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/titaniumStar.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titaniumStar = new TextureRegion(load, 0, 0, 14, 14);
                    titaniumStar.flip(false, true);
                }
                return titaniumStar;
            case 395:
                if (titaniumLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/titaniumLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titaniumLance = new TextureRegion(load, 0, 0, 14, 14);
                    titaniumLance.flip(false, true);
                }
                return titaniumLance;
            case 396:
                if (titaniumMace == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/titaniumMace.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titaniumMace = new TextureRegion(load, 0, 0, 14, 14);
                    titaniumMace.flip(false, true);
                }
                return titaniumMace;
            case 397:
                if (templarCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/templarCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    templarCloak = new TextureRegion(load, 0, 0, 14, 14);
                    templarCloak.flip(false, true);
                }
                return templarCloak;
            case 398:
                if (hunterCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/hunterCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hunterCloak = new TextureRegion(load, 0, 0, 14, 14);
                    hunterCloak.flip(false, true);
                }
                return hunterCloak;
            case 399:
                if (magusCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/magusCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magusCloak = new TextureRegion(load, 0, 0, 14, 14);
                    magusCloak.flip(false, true);
                }
                return magusCloak;
            case 400:
                if (soulAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/soulAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    soulAxe = new TextureRegion(load, 0, 0, 14, 14);
                    soulAxe.flip(false, true);
                }
                return soulAxe;
            case 401:
                if (soulCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/soulCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    soulCloak = new TextureRegion(load, 0, 0, 14, 14);
                    soulCloak.flip(false, true);
                }
                return soulCloak;
            case 402:
                if (metalCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/metalCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    metalCloak = new TextureRegion(load, 0, 0, 14, 14);
                    metalCloak.flip(false, true);
                }
                return metalCloak;
            case 403:
                if (lightCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/lightCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lightCloak = new TextureRegion(load, 0, 0, 14, 14);
                    lightCloak.flip(false, true);
                }
                return lightCloak;
            case 404:
                if (flameCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/fireCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flameCloak = new TextureRegion(load, 0, 0, 14, 14);
                    flameCloak.flip(false, true);
                }
                return flameCloak;
            case 405:
                if (flowerFire == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/flowerFire.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    flowerFire = new TextureRegion(load, 0, 0, 14, 14);
                    flowerFire.flip(false, true);
                }
                return flowerFire;
            case 406:
                if (snowStar == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/snowStar.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    snowStar = new TextureRegion(load, 0, 0, 14, 14);
                    snowStar.flip(false, true);
                }
                return snowStar;
            case 407:
                if (soulfire == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/soulfire.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    soulfire = new TextureRegion(load, 0, 0, 14, 14);
                    soulfire.flip(false, true);
                }
                return soulfire;
            case 408:
                if (eagleEgg == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/eagleEgg.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleEgg = new TextureRegion(load, 0, 0, 14, 14);
                    eagleEgg.flip(false, true);
                }
                return eagleEgg;
            case 409:
                if (fryingPan == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/fryingPan.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    fryingPan = new TextureRegion(load, 0, 0, 14, 14);
                    fryingPan.flip(false, true);
                }
                return fryingPan;
            case 410:
                if (nobleBook == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/nobleBook.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    nobleBook = new TextureRegion(load, 0, 0, 14, 14);
                    nobleBook.flip(false, true);
                }
                return nobleBook;
            case 411:
                if (nobleCoat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/nobleCoat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    nobleCoat = new TextureRegion(load, 0, 0, 14, 14);
                    nobleCoat.flip(false, true);
                }
                return nobleCoat;
            case 412:
                if (trueSteel == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/trueSteel.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    trueSteel = new TextureRegion(load, 0, 0, 14, 14);
                    trueSteel.flip(false, true);
                }
                return trueSteel;
            case 413:
                if (justice == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/justice.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    justice = new TextureRegion(load, 0, 0, 14, 14);
                    justice.flip(false, true);
                }
                return justice;
            case 414:
                if (seerRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/mithril/seerRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    seerRobe = new TextureRegion(load, 0, 0, 14, 14);
                    seerRobe.flip(false, true);
                }
                return seerRobe;
            case 415:
                if (seerHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/mithril/seerHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    seerHood = new TextureRegion(load, 0, 0, 14, 14);
                    seerHood.flip(false, true);
                }
                return seerHood;
            case 416:
                if (arbiter == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/arbiter.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arbiter = new TextureRegion(load, 0, 0, 14, 14);
                    arbiter.flip(false, true);
                }
                return arbiter;
            case 417:
                if (vangardeCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/vangardeCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    vangardeCloak = new TextureRegion(load, 0, 0, 14, 14);
                    vangardeCloak.flip(false, true);
                }
                return vangardeCloak;
            case 418:
                if (astralBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/astralBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    astralBlade = new TextureRegion(load, 0, 0, 14, 14);
                    astralBlade.flip(false, true);
                }
                return astralBlade;
            case 419:
                if (royalRapier == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/royalRapier.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    royalRapier = new TextureRegion(load, 0, 0, 14, 14);
                    royalRapier.flip(false, true);
                }
                return royalRapier;
            case 420:
                if (princessRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/princessRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    princessRobe = new TextureRegion(load, 0, 0, 14, 14);
                    princessRobe.flip(false, true);
                }
                return princessRobe;
            case 421:
                if (silverCrown == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/silverCrown.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    silverCrown = new TextureRegion(load, 0, 0, 14, 14);
                    silverCrown.flip(false, true);
                }
                return silverCrown;
            case 422:
                if (astralRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/astralRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    astralRobe = new TextureRegion(load, 0, 0, 14, 14);
                    astralRobe.flip(false, true);
                }
                return astralRobe;
            case 423:
                if (astralCirclet == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/astralCirclet.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    astralCirclet = new TextureRegion(load, 0, 0, 14, 14);
                    astralCirclet.flip(false, true);
                }
                return astralCirclet;
            case 424:
                if (princessShawl == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/princessShawl.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    princessShawl = new TextureRegion(load, 0, 0, 14, 14);
                    princessShawl.flip(false, true);
                }
                return princessShawl;
            case 425:
                if (astralCover == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/astralCover.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    astralCover = new TextureRegion(load, 0, 0, 14, 14);
                    astralCover.flip(false, true);
                }
                return astralCover;
            case 426:
                if (pocketMirror == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/pocketMirror.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    pocketMirror = new TextureRegion(load, 0, 0, 14, 14);
                    pocketMirror.flip(false, true);
                }
                return pocketMirror;
            case 427:
                if (scarlettesRibbon == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/scarlettesRibbon.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    scarlettesRibbon = new TextureRegion(load, 0, 0, 14, 14);
                    scarlettesRibbon.flip(false, true);
                }
                return scarlettesRibbon;
            case 428:
            case 429:
                if (magicHourglass == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/hourglass.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magicHourglass = new TextureRegion(load, 0, 0, 14, 14);
                    magicHourglass.flip(false, true);
                }
                return magicHourglass;
            case 430:
                if (timeKey == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/timeKey.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    timeKey = new TextureRegion(load, 0, 0, 14, 14);
                    timeKey.flip(false, true);
                }
                return timeKey;
            case 431:
                if (timeRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/timeRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    timeRobe = new TextureRegion(load, 0, 0, 14, 14);
                    timeRobe.flip(false, true);
                }
                return timeRobe;
            case 432:
                if (timeHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/scarlet/timeHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    timeHat = new TextureRegion(load, 0, 0, 14, 14);
                    timeHat.flip(false, true);
                }
                return timeHat;
            case 433:
                if (timeCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier6/timeCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    timeCloak = new TextureRegion(load, 0, 0, 14, 14);
                    timeCloak.flip(false, true);
                }
                return timeCloak;
            case 434:
                if (hourHand == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/titanium/hourHand.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hourHand = new TextureRegion(load, 0, 0, 14, 14);
                    hourHand.flip(false, true);
                }
                return hourHand;
            case 438:
                if (ancientBlade == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/ancientBlade.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientBlade = new TextureRegion(load, 0, 0, 14, 14);
                    ancientBlade.flip(false, true);
                }
                return ancientBlade;
            case 439:
                if (ancientAxe == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/ancientAxe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientAxe = new TextureRegion(load, 0, 0, 14, 14);
                    ancientAxe.flip(false, true);
                }
                return ancientAxe;
            case 440:
                if (ancientStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/ancientStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientStaff = new TextureRegion(load, 0, 0, 14, 14);
                    ancientStaff.flip(false, true);
                }
                return ancientStaff;
            case 441:
                if (ancientBow == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/ancientBow.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientBow = new TextureRegion(load, 0, 0, 14, 14);
                    ancientBow.flip(false, true);
                }
                return ancientBow;
            case 442:
                if (ancientLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/ancientLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientLance = new TextureRegion(load, 0, 0, 14, 14);
                    ancientLance.flip(false, true);
                }
                return ancientLance;
            case 443:
                if (ancientHammer == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/ancientHammer.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientHammer = new TextureRegion(load, 0, 0, 14, 14);
                    ancientHammer.flip(false, true);
                }
                return ancientHammer;
            case 444:
                if (ancientArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ruby/ancientArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientArmor = new TextureRegion(load, 0, 0, 14, 14);
                    ancientArmor.flip(false, true);
                }
                return ancientArmor;
            case 445:
                if (ancientHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ruby/ancientHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientHelm = new TextureRegion(load, 0, 0, 14, 14);
                    ancientHelm.flip(false, true);
                }
                return ancientHelm;
            case 446:
                if (leafGarb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/emerald/leafGarb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    leafGarb = new TextureRegion(load, 0, 0, 14, 14);
                    leafGarb.flip(false, true);
                }
                return leafGarb;
            case 447:
                if (leafHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/emerald/leafHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    leafHood = new TextureRegion(load, 0, 0, 14, 14);
                    leafHood.flip(false, true);
                }
                return leafHood;
            case 448:
                if (leafRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/sapphire/leafRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    leafRobe = new TextureRegion(load, 0, 0, 14, 14);
                    leafRobe.flip(false, true);
                }
                return leafRobe;
            case 449:
                if (ancientMask == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/sapphire/ancientMask.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    ancientMask = new TextureRegion(load, 0, 0, 14, 14);
                    ancientMask.flip(false, true);
                }
                return ancientMask;
            case 450:
                if (treefell == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/treefell.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    treefell = new TextureRegion(load, 0, 0, 14, 14);
                    treefell.flip(false, true);
                }
                return treefell;
            case 451:
                if (hyperionArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ruby/hyperionArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hyperionArmor = new TextureRegion(load, 0, 0, 14, 14);
                    hyperionArmor.flip(false, true);
                }
                return hyperionArmor;
            case 452:
                if (hyperionHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ruby/hyperionHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hyperionHelm = new TextureRegion(load, 0, 0, 14, 14);
                    hyperionHelm.flip(false, true);
                }
                return hyperionHelm;
            case 453:
                if (desertArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/emerald/desertArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    desertArmor = new TextureRegion(load, 0, 0, 14, 14);
                    desertArmor.flip(false, true);
                }
                return desertArmor;
            case 454:
                if (desertHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/emerald/desertHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    desertHood = new TextureRegion(load, 0, 0, 14, 14);
                    desertHood.flip(false, true);
                }
                return desertHood;
            case 455:
                if (sunRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/sapphire/sunRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sunRobe = new TextureRegion(load, 0, 0, 14, 14);
                    sunRobe.flip(false, true);
                }
                return sunRobe;
            case 456:
                if (sunHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/sapphire/sunHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sunHat = new TextureRegion(load, 0, 0, 14, 14);
                    sunHat.flip(false, true);
                }
                return sunHat;
            case 457:
                if (rubyArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ruby/rubyArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rubyArmor = new TextureRegion(load, 0, 0, 14, 14);
                    rubyArmor.flip(false, true);
                }
                return rubyArmor;
            case 458:
                if (rubyHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ruby/rubyHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    rubyHelm = new TextureRegion(load, 0, 0, 14, 14);
                    rubyHelm.flip(false, true);
                }
                return rubyHelm;
            case 459:
                if (emeraldArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/emerald/emeraldArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    emeraldArmor = new TextureRegion(load, 0, 0, 14, 14);
                    emeraldArmor.flip(false, true);
                }
                return emeraldArmor;
            case 460:
                if (emeraldHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/emerald/emeraldHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    emeraldHood = new TextureRegion(load, 0, 0, 14, 14);
                    emeraldHood.flip(false, true);
                }
                return emeraldHood;
            case 461:
                if (sapphireRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/sapphire/sapphireRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sapphireRobe = new TextureRegion(load, 0, 0, 14, 14);
                    sapphireRobe.flip(false, true);
                }
                return sapphireRobe;
            case 462:
                if (sapphireHat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/sapphire/sapphireHat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sapphireHat = new TextureRegion(load, 0, 0, 14, 14);
                    sapphireHat.flip(false, true);
                }
                return sapphireHat;
            case 463:
                return null; //AssetLoader.keyGold;
            case 464:
                if (stormLash == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/stormLash.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    stormLash = new TextureRegion(load, 0, 0, 14, 14);
                    stormLash.flip(false, true);
                }
                return stormLash;
            case 465:
                if (darkMatter == null) {
                    load = new Texture(Gdx.files.internal("items/materials/darkMatter.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    darkMatter = new TextureRegion(load, 0, 0, 14, 14);
                    darkMatter.flip(false, true);
                }
                return darkMatter;
            case 466:
                if (sapphireStaff == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/sapphireStaff.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    sapphireStaff = new TextureRegion(load, 0, 0, 14, 14);
                    sapphireStaff.flip(false, true);
                }
                return sapphireStaff;
            case 467:
                if (magicGem == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/magicGem.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magicGem = new TextureRegion(load, 0, 0, 14, 14);
                    magicGem.flip(false, true);
                }
                return magicGem;
            case 468:
                if (arcticCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier7/arcticCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    arcticCloak = new TextureRegion(load, 0, 0, 14, 14);
                    arcticCloak.flip(false, true);
                }
                return arcticCloak;
            case 469:
                if (zapCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier7/zapCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    zapCloak = new TextureRegion(load, 0, 0, 14, 14);
                    zapCloak.flip(false, true);
                }
                return zapCloak;
            case 470:
                if (moltenCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier7/moltenCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    moltenCloak = new TextureRegion(load, 0, 0, 14, 14);
                    moltenCloak.flip(false, true);
                }
                return moltenCloak;
            case 471:
                if (diamondRing == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/diamondRing.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    diamondRing = new TextureRegion(load, 0, 0, 14, 14);
                    diamondRing.flip(false, true);
                }
                return diamondRing;
            case 472:
                if (invitation == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/invitation.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    invitation = new TextureRegion(load, 0, 0, 14, 14);
                    invitation.flip(false, true);
                }
                return invitation;
            case 473:
                if (trullHorn == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/trullHorn.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    trullHorn = new TextureRegion(load, 0, 0, 14, 14);
                    trullHorn.flip(false, true);
                }
                return trullHorn;
            case 475:
                if (royalNote == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/royalNote.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    royalNote = new TextureRegion(load, 0, 0, 14, 14);
                    royalNote.flip(false, true);
                }
                return royalNote;
            case 476:
                if (kingsCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier8/kingsCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    kingsCloak = new TextureRegion(load, 0, 0, 14, 14);
                    kingsCloak.flip(false, true);
                }
                return kingsCloak;
            case 477:
                if (lionFang == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/lionFang.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lionFang = new TextureRegion(load, 0, 0, 14, 14);
                    lionFang.flip(false, true);
                }
                return lionFang;
            case 478:
                if (lionfall == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/lionfall.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lionfall = new TextureRegion(load, 0, 0, 14, 14);
                    lionfall.flip(false, true);
                }
                return lionfall;
            case 479:
                if (secretCoat == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/secretCoat.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    secretCoat = new TextureRegion(load, 0, 0, 14, 14);
                    secretCoat.flip(false, true);
                }
                return secretCoat;
            case 480:
                if (katana == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/katana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    katana = new TextureRegion(load, 0, 0, 14, 14);
                    katana.flip(false, true);
                }
                return katana;
            case 481:
                if (samuraiGarb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/emerald/samuraiGarb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    samuraiGarb = new TextureRegion(load, 0, 0, 14, 14);
                    samuraiGarb.flip(false, true);
                }
                return samuraiGarb;
            case 482:
                if (blackBandana == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/blackBandana.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    blackBandana = new TextureRegion(load, 0, 0, 14, 14);
                    blackBandana.flip(false, true);
                }
                return blackBandana;
            case 483:
                if (shadoCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier7/shadoCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    shadoCloak = new TextureRegion(load, 0, 0, 14, 14);
                    shadoCloak.flip(false, true);
                }
                return shadoCloak;
            case 484:
                if (nightfire == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/nightfire.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    nightfire = new TextureRegion(load, 0, 0, 14, 14);
                    nightfire.flip(false, true);
                }
                return nightfire;
            case 485:
                if (hyperionLance == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/hyperionLance.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    hyperionLance = new TextureRegion(load, 0, 0, 14, 14);
                    hyperionLance.flip(false, true);
                }
                return hyperionLance;
            case 486:
                if (chaosCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier7/chaosCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    chaosCloak = new TextureRegion(load, 0, 0, 14, 14);
                    chaosCloak.flip(false, true);
                }
                return chaosCloak;
            case 487:
                if (deathMace == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/deathMace.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    deathMace = new TextureRegion(load, 0, 0, 14, 14);
                    deathMace.flip(false, true);
                }
                return deathMace;
            case 488:
                if (battleCharm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/battleCharm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    battleCharm = new TextureRegion(load, 0, 0, 14, 14);
                    battleCharm.flip(false, true);
                }
                return battleCharm;
            case 489:
                if (chaosArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ruby/chaosArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    chaosArmor = new TextureRegion(load, 0, 0, 14, 14);
                    chaosArmor.flip(false, true);
                }
                return chaosArmor;
            case 490:
                if (chaosMask == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/ruby/chaosMask.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    chaosMask = new TextureRegion(load, 0, 0, 14, 14);
                    chaosMask.flip(false, true);
                }
                return chaosMask;
            case 491:
                if (excalibur == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/ruby/excalibur.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    excalibur = new TextureRegion(load, 0, 0, 14, 14);
                    excalibur.flip(false, true);
                }
                return excalibur;
            case 492:
                if (altrius2 == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/altrius2.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    altrius2 = new TextureRegion(load, 0, 0, 14, 14);
                    altrius2.flip(false, true);
                }
                return altrius2;
            case 493:
                if (dragonOre == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/dragonOre.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonOre = new TextureRegion(load, 0, 0, 14, 14);
                    dragonOre.flip(false, true);
                }
                return dragonOre;
            case 494:
                if (goldScale == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/goldScale.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    goldScale = new TextureRegion(load, 0, 0, 14, 14);
                    goldScale.flip(false, true);
                }
                return goldScale;
            case 495:
                if (dragonBandana2 == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/dragonBandana2.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonBandana2 = new TextureRegion(load, 0, 0, 14, 14);
                    dragonBandana2.flip(false, true);
                }
                return dragonBandana2;
            case 496:
                if (dragonBane2 == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/dragonBane2.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonBane2 = new TextureRegion(load, 0, 0, 14, 14);
                    dragonBane2.flip(false, true);
                }
                return dragonBane2;
            case 497:
                if (elfnir == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/elfnir.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    elfnir = new TextureRegion(load, 0, 0, 14, 14);
                    elfnir.flip(false, true);
                }
                return elfnir;
            case 498:
                if (trueHeart == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/trueHeart.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    trueHeart = new TextureRegion(load, 0, 0, 14, 14);
                    trueHeart.flip(false, true);
                }
                return trueHeart;
            case 499:
                if (souleater == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/souleater.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    souleater = new TextureRegion(load, 0, 0, 14, 14);
                    souleater.flip(false, true);
                }
                return souleater;
            case 500:
                if (magicFeather == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/magicFeather.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magicFeather = new TextureRegion(load, 0, 0, 14, 14);
                    magicFeather.flip(false, true);
                }
                return magicFeather;
            case 501:
                if (magicRing == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/magicRing.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magicRing = new TextureRegion(load, 0, 0, 14, 14);
                    magicRing.flip(false, true);
                }
                return magicRing;
            case 502:
                if (magicShield == null) {
                    load = new Texture(Gdx.files.internal("items/armor/relics/magicShield.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    magicShield = new TextureRegion(load, 0, 0, 14, 14);
                    magicShield.flip(false, true);
                }
                return magicShield;
            case 510:
            case 518:
            case 526:
                if (titanHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/titanHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titanHelm = new TextureRegion(load, 0, 0, 14, 14);
                    titanHelm.flip(false, true);
                }
                return titanHelm;
            case 503:
            case 511:
            case 519:
                if (titanArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/titanArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titanArmor = new TextureRegion(load, 0, 0, 14, 14);
                    titanArmor.flip(false, true);
                }
                return titanArmor;
            case 504:
            case 512:
            case 520:
                if (dragonHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/dragonHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonHelm = new TextureRegion(load, 0, 0, 14, 14);
                    dragonHelm.flip(false, true);
                }
                return dragonHelm;
            case 505:
            case 513:
            case 521:
                if (dragonArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/dragonArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonArmor = new TextureRegion(load, 0, 0, 14, 14);
                    dragonArmor.flip(false, true);
                }
                return dragonArmor;
            case 506:
            case 514:
            case 522:
                if (wildHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/wildHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wildHelm = new TextureRegion(load, 0, 0, 14, 14);
                    wildHelm.flip(false, true);
                }
                return wildHelm;
            case 507:
            case 515:
            case 523:
                if (wildArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/wildArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wildArmor = new TextureRegion(load, 0, 0, 14, 14);
                    wildArmor.flip(false, true);
                }
                return wildArmor;
            case 508:
            case 516:
            case 524:
                if (eagleHelm == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/eagleHelm.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleHelm = new TextureRegion(load, 0, 0, 14, 14);
                    eagleHelm.flip(false, true);
                }
                return eagleHelm;
            case 509:
            case 517:
            case 525:
                if (eagleArmor == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/eagleArmor.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleArmor = new TextureRegion(load, 0, 0, 14, 14);
                    eagleArmor.flip(false, true);
                }
                return eagleArmor;

            case 534:
            case 542:
            case 550:
                if (titanHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/final/titanHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titanHood = new TextureRegion(load, 0, 0, 14, 14);
                    titanHood.flip(false, true);
                }
                return titanHood;
            case 527:
            case 535:
            case 543:
                if (titanGarb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/final/titanGarb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titanGarb = new TextureRegion(load, 0, 0, 14, 14);
                    titanGarb.flip(false, true);
                }
                return titanGarb;
            case 528:
            case 536:
            case 544:
                if (dragonHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/final/dragonHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonHood = new TextureRegion(load, 0, 0, 14, 14);
                    dragonHood.flip(false, true);
                }
                return dragonHood;
            case 529:
            case 537:
            case 545:
                if (dragonGarb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/final/dragonGarb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonGarb = new TextureRegion(load, 0, 0, 14, 14);
                    dragonGarb.flip(false, true);
                }
                return dragonGarb;
            case 530:
            case 538:
            case 546:
                if (wildHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/final/wildHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wildHood = new TextureRegion(load, 0, 0, 14, 14);
                    wildHood.flip(false, true);
                }
                return wildHood;
            case 531:
            case 539:
            case 547:
                if (wildGarb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/final/wildGarb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wildGarb = new TextureRegion(load, 0, 0, 14, 14);
                    wildGarb.flip(false, true);
                }
                return wildGarb;
            case 532:
            case 540:
            case 548:
                if (eagleHood == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/final/eagleHood.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleHood = new TextureRegion(load, 0, 0, 14, 14);
                    eagleHood.flip(false, true);
                }
                return eagleHood;
            case 533:
            case 541:
            case 549:
                if (eagleGarb == null) {
                    load = new Texture(Gdx.files.internal("items/armor/medium/final/eagleGarb.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleGarb = new TextureRegion(load, 0, 0, 14, 14);
                    eagleGarb.flip(false, true);
                }
                return eagleGarb;

            case 558:
            case 566:
            case 574:
                if (titanCap == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/final/titanCap.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titanCap = new TextureRegion(load, 0, 0, 14, 14);
                    titanCap.flip(false, true);
                }
                return titanCap;
            case 551:
            case 559:
            case 567:
                if (titanRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/final/titanRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    titanRobe = new TextureRegion(load, 0, 0, 14, 14);
                    titanRobe.flip(false, true);
                }
                return titanRobe;
            case 552:
            case 560:
            case 568:
                if (dragonCap == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/final/dragonCap.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonCap = new TextureRegion(load, 0, 0, 14, 14);
                    dragonCap.flip(false, true);
                }
                return dragonCap;
            case 553:
            case 561:
            case 569:
                if (dragonRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/final/dragonRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonRobe = new TextureRegion(load, 0, 0, 14, 14);
                    dragonRobe.flip(false, true);
                }
                return dragonRobe;
            case 554:
            case 562:
            case 570:
                if (wildCap == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/final/wildCap.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wildCap = new TextureRegion(load, 0, 0, 14, 14);
                    wildCap.flip(false, true);
                }
                return wildCap;
            case 555:
            case 563:
            case 571:
                if (wildRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/final/wildRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    wildRobe = new TextureRegion(load, 0, 0, 14, 14);
                    wildRobe.flip(false, true);
                }
                return wildRobe;
            case 556:
            case 564:
            case 572:
                if (eagleCap == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/final/eagleCap.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleCap = new TextureRegion(load, 0, 0, 14, 14);
                    eagleCap.flip(false, true);
                }
                return eagleCap;
            case 557:
            case 565:
            case 573:
                if (eagleRobe == null) {
                    load = new Texture(Gdx.files.internal("items/armor/light/final/eagleRobe.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    eagleRobe = new TextureRegion(load, 0, 0, 14, 14);
                    eagleRobe.flip(false, true);
                }
                return eagleRobe;
            case 575:
                if (tigerCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier8/tigerCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    tigerCloak = new TextureRegion(load, 0, 0, 14, 14);
                    tigerCloak.flip(false, true);
                }
                return tigerCloak;
            case 576:
                if (bearCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier8/bearCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    bearCloak = new TextureRegion(load, 0, 0, 14, 14);
                    bearCloak.flip(false, true);
                }
                return bearCloak;
            case 577:
                if (serpentCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier8/serpentCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    serpentCloak = new TextureRegion(load, 0, 0, 14, 14);
                    serpentCloak.flip(false, true);
                }
                return serpentCloak;
            case 578:
                if (turtleCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier8/turtleCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    turtleCloak = new TextureRegion(load, 0, 0, 14, 14);
                    turtleCloak.flip(false, true);
                }
                return turtleCloak;
            case 579:
                if (pantherCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier8/pantherCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    pantherCloak = new TextureRegion(load, 0, 0, 14, 14);
                    pantherCloak.flip(false, true);
                }
                return pantherCloak;
            case 580:
                if (dragonCloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier8/dragonCloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    dragonCloak = new TextureRegion(load, 0, 0, 14, 14);
                    dragonCloak.flip(false, true);
                }
                return dragonCloak;
            case 581:
                if (medusa == null) {
                    load = new Texture(Gdx.files.internal("items/weapons/legendary/medusa.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    medusa = new TextureRegion(load, 0, 0, 14, 14);
                    medusa.flip(false, true);
                }
                return medusa;
            case 582:
                if (mimikingu == null) {
                    load = new Texture(Gdx.files.internal("items/armor/heavy/final/mimikingu.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    mimikingu = new TextureRegion(load, 0, 0, 14, 14);
                    mimikingu.flip(false, true);
                }
                return mimikingu;
            case 583:
                if (prisma2Cloak == null) {
                    load = new Texture(Gdx.files.internal("items/armor/cloaks/tier8/prisma2Cloak.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    prisma2Cloak = new TextureRegion(load, 0, 0, 14, 14);
                    prisma2Cloak.flip(false, true);
                }
                return prisma2Cloak;
            case 584:
                if (lightGem == null) {
                    load = new Texture(Gdx.files.internal("items/armor/all/lightGem.png"));
                    load.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
                    lightGem = new TextureRegion(load, 0, 0, 14, 14);
                    lightGem.flip(false, true);
                }
                return lightGem;
        }
    }

    public static Item getItem(int towerLevel){
        int id = 1;
        ArrayList<Integer> ids = new ArrayList<>();
        if(towerLevel <= 20){
            ids.add(1);ids.add(2);ids.add(3);ids.add(4);ids.add(9);ids.add(12);ids.add(14);ids.add(17);
            ids.add(19);ids.add(21);ids.add(22);ids.add(23);ids.add(25);ids.add(27);ids.add(30);ids.add(31);
            ids.add(32);ids.add(34);ids.add(35);ids.add(36);ids.add(37);ids.add(38);ids.add(40);ids.add(42);
            ids.add(43);ids.add(44);ids.add(45);ids.add(46);ids.add(47);ids.add(48);ids.add(49);ids.add(50);
        }
        else if(towerLevel <= 40){
            ids.add(52);ids.add(55);ids.add(56);ids.add(57);ids.add(58);ids.add(60);ids.add(61);ids.add(62);
            ids.add(66);ids.add(68);ids.add(69);ids.add(70);ids.add(71);ids.add(72);ids.add(73);ids.add(75);
            ids.add(76);ids.add(77);ids.add(78);ids.add(79);ids.add(80);ids.add(81);ids.add(82);ids.add(83);
            ids.add(86);ids.add(87);ids.add(88);ids.add(89);ids.add(90);ids.add(91);ids.add(92);ids.add(94);
            ids.add(95);ids.add(96);
        }
        else if(towerLevel <= 60){
            ids.add(98);ids.add(99);ids.add(100);ids.add(101);ids.add(102);ids.add(103);ids.add(104);ids.add(105);
            ids.add(106);ids.add(107);ids.add(108);ids.add(109);ids.add(110);ids.add(114);
            ids.add(115);ids.add(116);ids.add(119);ids.add(120);ids.add(121);ids.add(124);ids.add(125);
            ids.add(126);ids.add(127);ids.add(129);ids.add(130);ids.add(131);
        }
        else if(towerLevel <= 80){
            ids.add(122);ids.add(137);ids.add(138);ids.add(139);
            ids.add(140);ids.add(141);ids.add(142);ids.add(143);ids.add(144);ids.add(145);ids.add(146);ids.add(147);
            ids.add(148);ids.add(150);ids.add(151);ids.add(152);ids.add(153);ids.add(154);ids.add(155);ids.add(156);
            ids.add(157);ids.add(158);ids.add(159);ids.add(160);ids.add(161);ids.add(162);ids.add(167);ids.add(168);
            ids.add(169);ids.add(170);ids.add(171);ids.add(172);ids.add(173);ids.add(174);ids.add(175);ids.add(176);
            ids.add(177);ids.add(178);ids.add(180);
        }
        if(ids.size() > 0){
            id = ids.get(Character.randInt(0, ids.size()-1));
        }
        return new Item(id, towerLevel);
    }

    public static Item getMaterial(int towerLevel){
        int id = 235;
        if(towerLevel <= 20){
            id = 235;
        }
        else if(towerLevel <= 40){
            id = 227;
        }
        else if(towerLevel <= 80){
            id = 234;
        }
        return new Item(id,towerLevel);
    }

}
