package objects;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by bergerk on 12/23/15.
 */
public interface Job {

    public String getName();

    public int getId();

    public int getRarity(); // 1 - common, 2 - rare, 3 - epic, 4 - legendary

    public Attack getAttack(int index);

    public TextureRegion getBattleAnimation(float delta);

    public TextureRegion getSwingAnimation(float delta);

    public TextureRegion getWalkAnimation(float delta);

    public TextureRegion getFlipAnimation(float delta);

    public TextureRegion getDetailImage();
}
