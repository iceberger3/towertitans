package objects;

import com.badlogic.gdx.Game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import objects.Character;
import world.GameWorld;

/**
 * Created by kyle.berger on 3/3/17.
 */

public class Unit {
    public float x;
    public float y;
    public GameWorld world;
    public int id;
    public String name = "";
    public Character character;
    public ArrayList<AttackDisplay> displays;

    public Unit(Character c, GameWorld world){
        name = c.getName();
        character = c;
        this.world = world;
        displays = new ArrayList<>();
    }

    public void update() {
    }

    public void move(float x, float y){
        this.x += x;
        this.y += y;
    }

}
