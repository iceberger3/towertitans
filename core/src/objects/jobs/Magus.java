package objects.jobs;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


import java.util.ArrayList;

import helpers.AssetLoader;
import objects.Attack;
import objects.Job;

/**
 * Created by bergerk on 12/26/15.
 */
public class Magus implements Job {
    public static boolean loaded = false;
    public static TextureRegion swing1, swing2, swing3, swing4, swing5;
    public ArrayList<Attack> attacks;
    public static Animation<TextureRegion> swingAnimation;

    public Magus(){
        load();
        attacks = new ArrayList<>();
        attacks.add(new Attack(12));
        attacks.add(new Attack(10));
        attacks.add(new Attack(11));
        attacks.add(new Attack(13));
    }

    @Override
    public String getName() {
        return "Magus";
    }

    @Override
    public int getId(){
        return 4;
    }

    @Override
    public int getRarity(){
        return 3;
    }

    @Override
    public TextureRegion getBattleAnimation(float delta){
        return AssetLoader.magusBattleAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getWalkAnimation(float delta){
        return AssetLoader.magusAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getFlipAnimation(float delta) {
        return AssetLoader.magusFlipAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getSwingAnimation(float delta){
        return swingAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getDetailImage(){
        return AssetLoader.magusPortrait;

    }

    @Override
    public Attack getAttack(int index) {
        return attacks.get(index);
    }


    public static void load(){
        if(!loaded) {
            swing1 = new TextureRegion(AssetLoader.sprites, 90, 72, 16, 16);
            swing1.flip(false, true);

            swing2 = new TextureRegion(AssetLoader.sprites, 108, 72, 16, 16);
            swing2.flip(false, true);

            swing3 = new TextureRegion(AssetLoader.sprites, 126, 72, 16, 16);
            swing3.flip(false, true);

            swing4 = new TextureRegion(AssetLoader.sprites, 144, 72, 16, 16);
            swing4.flip(false, true);

            swing5 = new TextureRegion(AssetLoader.sprites, 162, 72, 16, 16);
            swing5.flip(false, true);

            TextureRegion[] swing = { swing1, swing2,  swing3, swing4, swing5};
            swingAnimation = new Animation<TextureRegion>(0.12f, swing);
            swingAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        }
    }
}
