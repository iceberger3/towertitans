package objects.jobs;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


import java.util.ArrayList;

import helpers.AssetLoader;
import objects.Attack;
import objects.Job;

/**
 * Created by bergerk on 12/26/15.
 */
public class Paladin implements Job {
    public static boolean loaded = false;
    public static TextureRegion swing1, swing2, swing3, swing4, swing5;
    public ArrayList<Attack> attacks;
    public static Animation<TextureRegion> swingAnimation;

    public Paladin(){
        load();
        attacks = new ArrayList<>();
        attacks.add(new Attack(1));
        attacks.add(new Attack(3));
        attacks.add(new Attack(4));
        attacks.add(new Attack(5));
    }

    @Override
    public String getName() {
        return "Paladin";
    }

    @Override
    public int getId(){
        return 1;
    }

    @Override
    public int getRarity(){
        return 3;
    }

    @Override
    public TextureRegion getBattleAnimation(float delta){
        return AssetLoader.paladinBattleAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getWalkAnimation(float delta){
        return AssetLoader.paladinAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getFlipAnimation(float delta) {
        return AssetLoader.paladinFlipAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getSwingAnimation(float delta){
        return swingAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getDetailImage(){
        return AssetLoader.paladinPortrait;

    }

    @Override
    public Attack getAttack(int index) {
        return attacks.get(index);
    }


    public static void load(){
        if(!loaded) {
            swing1 = new TextureRegion(AssetLoader.sprites, 90, 0, 16, 16);
            swing1.flip(false, true);

            swing2 = new TextureRegion(AssetLoader.sprites, 108, 0, 16, 16);
            swing2.flip(false, true);

            swing3 = new TextureRegion(AssetLoader.sprites, 126, 0, 16, 16);
            swing3.flip(false, true);

            swing4 = new TextureRegion(AssetLoader.sprites, 144, 0, 16, 16);
            swing4.flip(false, true);

            swing5 = new TextureRegion(AssetLoader.sprites, 162, 0, 16, 16);
            swing5.flip(false, true);
            TextureRegion[] swing = {AssetLoader.paladin5, swing1, swing2, swing2, swing3, swing3, swing4, swing5};
            swingAnimation = new Animation<TextureRegion>(0.075f, swing);
            swingAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        }
    }
}
