package objects.jobs;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


import java.util.ArrayList;

import helpers.AssetLoader;
import objects.Attack;
import objects.Job;

/**
 * Created by bergerk on 12/26/15.
 */
public class Ranger implements Job {
    public static boolean loaded = false;
    public static TextureRegion swing1, swing2, swing3, swing4, swing5;
    public ArrayList<Attack> attacks;
    public static Animation<TextureRegion> swingAnimation;

    public Ranger(){
        load();
        attacks = new ArrayList<>();
        attacks.add(new Attack(6));
        attacks.add(new Attack(9));
        attacks.add(new Attack(7));
        attacks.add(new Attack(8));
    }

    @Override
    public String getName() {
        return "Ranger";
    }

    @Override
    public int getId(){
        return 3;
    }

    @Override
    public int getRarity(){
        return 3;
    }

    @Override
    public TextureRegion getBattleAnimation(float delta){
        return AssetLoader.rangerBattleAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getWalkAnimation(float delta){
        return AssetLoader.rangerAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getFlipAnimation(float delta) {
        return AssetLoader.rangerFlipAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getSwingAnimation(float delta){
        return swingAnimation.getKeyFrame(delta);
    }

    @Override
    public TextureRegion getDetailImage(){
        return AssetLoader.rangerPortrait;

    }

    @Override
    public Attack getAttack(int index) {
        return attacks.get(index);
    }


    public static void load(){
        if(!loaded) {
            swing1 = new TextureRegion(AssetLoader.sprites, 90, 54, 16, 16);
            swing1.flip(false, true);

            swing2 = new TextureRegion(AssetLoader.sprites, 108, 54, 16, 16);
            swing2.flip(false, true);

            swing3 = new TextureRegion(AssetLoader.sprites, 126, 54, 16, 16);
            swing3.flip(false, true);

            swing4 = new TextureRegion(AssetLoader.sprites, 144, 54, 16, 16);
            swing4.flip(false, true);

            swing5 = new TextureRegion(AssetLoader.sprites, 162, 54, 16, 16);
            swing5.flip(false, true);
            TextureRegion[] swing = {swing1, swing2, swing2, swing3, swing3, swing4, swing5, swing1};
            swingAnimation = new Animation<TextureRegion>(0.075f, swing);
            swingAnimation.setPlayMode(Animation.PlayMode.NORMAL);
        }
    }
}
