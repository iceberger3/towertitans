package objects.monsters;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.ArrayList;

import helpers.AssetLoader;
import objects.Attack;
import objects.Job;

/**
 * Created by bergerk on 3/4/16.
 */
public class Monster implements Job{


    private int id;
    private String name;
    private Animation<TextureRegion> battle = null;
    private Animation<TextureRegion> walk = null;
    private Animation<TextureRegion> swing = null;
    private ArrayList<Attack> attacks;
    public Monster(int id){
        this.id = id;
        attacks = new ArrayList<>();
        buildMonster(id);
    }

    private void buildMonster(int id){
        switch (id){
            case 999:
                name = "Slime";
                battle = AssetLoader.slimeAnimation;
                walk = AssetLoader.slimeAnimation;
                swing = battle;
                attacks.add(new Attack(999));
                break;
        }
    }




    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getRarity(){
        return 1;
    }

    @Override
    public Attack getAttack(int index) {
        return attacks.get(index % attacks.size());
    }

    @Override
    public TextureRegion getBattleAnimation(float delta){
        if(battle != null){
            return battle.getKeyFrame(delta);
        }
        else {
            return null;
        }
    }

    @Override
    public TextureRegion getWalkAnimation(float delta){
        if(walk != null){
            return walk.getKeyFrame(delta);
        }
        else {
            return null;
        }
    }

    @Override
    public TextureRegion getFlipAnimation(float delta) {
        if(walk != null){
            return walk.getKeyFrame(delta);
        }
        else {
            return null;
        }
    }

    @Override
    public TextureRegion getSwingAnimation(float delta){
        if(swing != null){
            return swing.getKeyFrame(delta);
        }
        else if(battle != null){
            return battle.getKeyFrame(delta);
        }
        else {
            return null;
        }
    }

    @Override
    public TextureRegion getDetailImage(){
        return null;
    }
}
