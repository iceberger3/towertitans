package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

import game.TowerTitan;
import helpers.AssetLoader;
import world.GameRenderer;
import world.GameWorld;
import helpers.InputHandler;

/**
 * Created by Kyle on 4/22/2015.
 */
public class GameScreen  implements Screen{
        private GameWorld world;
        private GameRenderer renderer;
        private float runTime;
        private TowerTitan game;
        private static int music;
        public InputHandler inputHandler;

        public GameScreen(TowerTitan g, int map) {
            //System.out.println("Game Screen Start: " + System.currentTimeMillis());
            game = g;
            world = new GameWorld(g,map,this);
            //System.out.println("world: " + System.currentTimeMillis());
            renderer = new GameRenderer(world);
            //System.out.println("renderer: " + System.currentTimeMillis());
            inputHandler = new InputHandler(g,world);
            Gdx.input.setInputProcessor(inputHandler);
            Gdx.input.setCatchBackKey(true);
            //System.out.println("handler: " + System.currentTimeMillis());

            music = setMusic(map);
            //System.out.println("end: " + System.currentTimeMillis());

        }

        public static int setMusic(int map){

            switch (map){
                case 2:
                case 121:
                case 165:
                    music = 7; // town theme
                    break;
                case 20:
                        music = 5;
                    break;
                case 251:
                case 252:
                case 253:
                case 254:
                case 255:
                case 256:
                case 257:
                case 258:
                    music = 5;
                    break;
                case 0:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 71:
                case 72:
                case 73:
                case 74:
                case 75:
                case 208:
                case 209:
                case 210:
                case 211:
                case 275:
                case 276:
                case 277:
                case 243:
                case 244:
                    music = 1; // spritwood theme
                    break;
                case 1:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 19:
                case 34:
                case 35:
                case 36:
                case 37:
                case 76:
                case 77:
                case 78:
                case 79:
                case 80://cave theme
                case 133:
                    music = 3;
                    break;
                case 18:
                case 33:
                case 38:
                case 81:
                case 82:
                case 83:
                case 84:
                case 85:
                case 122:
                case 177:
                case 178:
                case 179:
                case 207:
                    music = 9; // adventure
                    break;
                case 21:
                case 22:
                case 86:
                case 87:
                case 88:
                case 89:
                case 90:
                    music = 10; // doxent
                    break;
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 91:
                case 92:
                case 93:
                case 94:
                case 95:
                case 269:
                case 270:
                case 271:
                case 245:
                case 246:
                case 247:
                    music = 11; // undercover
                    break;
                case 40:
                case 41:
                case 42://sewer
                case 96:
                case 97:
                case 98:
                case 99:
                case 100:
                case 272:
                case 273:
                case 274:
                    music = 13;
                    break;
                case 3:
                case 4:
                case 39:
                case 43:
                case 44:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                case 68:
                case 69:
                case 106:
                case 107:
                case 108:
                case 109:
                case 110://castle
                case 259:
                case 263:
                case 264:
                    music = 14;
                    break;
                case 45://myria
                    music = 15;
                    break;
                case 59:
                case 60:
                case 61:
                case 62:
                case 70:
                case 111:
                case 112:
                case 113:
                case 114:
                case 115:
                case 116:
                case 117:
                case 118:
                case 119:
                case 120://darktower
                    music = 16;
                    break;
                case 46:
                case 47:
                case 48:
                case 101:
                case 102:
                case 103:
                case 104:
                case 105://library
                    music = 17;
                    break;
                case 123:
                case 124:
                case 125:
                case 126:
                case 127:
                case 128:
                case 129:
                case 130://epic battle
                    music = 24;
                    break;
                case 131: //sandy shore
                case 212:
                case 213:
                case 214:
                case 215:
                case 216:
                case 217:
                case 218:
                case 219:
                    music = 20;
                    break;
                case 132: //happy place
                    music = 5;
                    break;
                case 134:
                case 135:
                case 136:
                case 137: //twoswords
                case 164:
                case 220:
                case 221:
                case 222:
                case 223:
                case 224:
                case 225:
                case 226:
                case 228:
                case 229:
                    music = 30;
                    break;
                case 138:
                case 139:
                case 140:
                case 141: //orykTaiko
                    music = 27;
                    break;
                case 142:
                case 143:
                case 144:
                case 145:
                case 146:
                case 147:
                case 148:
                case 149:
                case 150: //kadish
                case 266:
                case 267:
                    music = 26;
                    break;
                case 151: //videoGameSoldiers
                case 280:
                case 281:
                case 282:
                case 283:
                case 284:
                    music = 31;
                    break;
                case 152:
                case 153:
                case 154:
                case 155:
                case 156:
                case 157:
                case 158:
                case 159: //shiro
                case 169:
                case 170:
                case 171:
                case 172:
                case 173:
                case 174:
                case 175:
                case 176:
                case 248:
                case 249:
                case 250:
                    music = 29;
                    break;
                case 160: //phoenix
                case 161: //phoenix
                case 162: //phoenix
                case 163: //phoenix
                case 265:
                case 240:
                case 241:
                    music = 28;
                    break;
                case 166:
                case 167:
                case 168:
                    music = 32;
                    break;
                case 200:
                case 201:
                case 202:
                case 203:
                case 204:
                case 205:
                case 206:
                    music = 33;
                    break;
                case 227:
                case 279:
                    music = 35;
                    break;
                case 230:
                case 231:
                case 232:
                case 233:
                case 234:
                case 235:
                case 236:
                case 237:
                case 238:
                case 239:
                case 268:
                    music = 36;
                    break;
                case 187:
                case 188:
                case 189:
                case 190:
                case 191:
                case 192:
                case 193:
                case 194:
                case 195:
                case 196:
                case 197:
                case 198:
                case 199:
                case 260:
                case 261:
                case 262:
                    music = 37;
                    break;

                case 180:
                case 181:
                case 182:
                case 183:
                case 184:
                case 185:
                case 186:
                    music = 38;
                    break;

                default:
                    music = 1;
                    break;
            }
            return music;
        }

        public void render(float delta) {
            try {
                runTime += delta;
                world.update(delta);
                renderer.render(runTime);
            }
            catch (Exception e){
                e.printStackTrace();
                TowerTitan.save.save();
                Gdx.app.exit();
            }
        }

        @Override
        public void resize(int width, int height) {

        }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(inputHandler);
         AssetLoader.load();


        resize(game.maxX,game.maxY);

    }

    @Override
    public void pause() {

        game.save.save();

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

            game.save.save();

    }
    @Override
    public void dispose() {

        game.save.save();

    }

    }
