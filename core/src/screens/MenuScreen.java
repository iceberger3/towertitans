package screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controllers;


import game.TowerTitan;
import helpers.AssetLoader;
import helpers.MenuInputHandler;
import world.MenuRenderer;
import world.MenuWorld;

/**
 * Created by Kyle on 6/16/2015.
 */
public class MenuScreen implements Screen {

    private MenuWorld world;
    private MenuRenderer renderer;
    private MenuInputHandler inputHandler;
    private TowerTitan game;
    private float runTime;

    public MenuScreen(TowerTitan game){
        this.game = game;
        game.save.load();
        if(game.save.isMusic()) {
            game.playMusic(0);
        }
        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();
        float gameWidth = 224;
        float gameHeight = 128;

        int midPointY = (int) (gameHeight / 2);

        world = new MenuWorld(midPointY, game);
        renderer = new MenuRenderer(world, (int) gameHeight, midPointY);
        inputHandler = new MenuInputHandler(world);
        Controllers.clearListeners();

        Gdx.input.setInputProcessor(inputHandler);
        Gdx.input.setCatchBackKey(true);
        //TowerTitan.googleServices.signIn();
    }

    @Override
    public void render(float delta) {
        runTime += delta;
        world.update(delta);
        renderer.render(runTime);
    }

    @Override
    public void resize(int width, int height) {
        if(game.maxY != Gdx.graphics.getHeight() || game.maxX != Gdx.graphics.getWidth()) {
            System.out.println("resized");
            renderer.resize();
            game.maxX = Gdx.graphics.getWidth();
            game.maxY = Gdx.graphics.getHeight();
        }
    }

    public static void reload(){
        MenuWorld.loaded = false;
        AssetLoader.isLoaded = false;
        AssetLoader.load();
    }

    @Override
    public void show() {
        Controllers.clearListeners();
        Gdx.input.setInputProcessor(inputHandler);
        AssetLoader.load();
        if(game.save.isMusic() && game.music != null && !game.music.isPlaying()) {
            game.music.play();
        }
        renderer.resize();
        System.out.println("resized");
    }

    @Override
    public void pause() {
        if(game.save.isMusic() && game.music != null) {
            game.music.pause();
        }
    }

    @Override
    public void resume() {
        if(game.save.isMusic() && game.music != null && !game.music.isPlaying()) {
            game.music.play();
        }
    }

    @Override
    public void hide() {
        if(game.save.isMusic()) {
            game.music.pause();
        }
    }

    @Override
    public void dispose() {
        if(game.save.isMusic() && game.title != null){
            game.title.stop();
            game.title.dispose();
        }
    }
}
