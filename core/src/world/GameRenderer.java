package world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.tools.bmfont.BitmapFontWriter;

import java.text.DecimalFormat;

import game.TowerTitan;
import helpers.AssetLoader;
import helpers.CombatText;
import objects.AttackOverTime;
import objects.Character;
import objects.Floor;
import objects.Item;
import objects.Unit;

public class GameRenderer {

	public GameWorld myWorld;
	private OrthographicCamera cam;
	private ShapeRenderer shapeRenderer;
	private SpriteBatch batcher;
    BitmapFont font, font2, fontTitle, fontTitleBack,fontText;
	public float maxWidth = 224;
	public float maxHeight = 168;
	public float centerX = 112;
	public float centerY = 64;
	public float wpad = 0;
	public float hpad = 0;
	private int rotate = 0;
	private float runtime = 0;
	private int iphoneXLeft = 0;
	private int iphoneXRight = 0;

	public GameRenderer(GameWorld world) {
		generateFonts();
        myWorld = world;
		maxWidth = world.game.maxWidth;
		maxHeight = world.game.maxHeight;
		hpad = maxHeight - 224;
		wpad = maxWidth - 128;
		if(myWorld.game.isIphoneX){
			iphoneXLeft = myWorld.game.iphoneXLeft;
			iphoneXRight = myWorld.game.iphoneXRight;
		}
		centerX = maxWidth / 2.0f;
		centerY = maxHeight / 2.0f;
		cam = new OrthographicCamera();
		cam.setToOrtho(true, maxWidth, maxHeight);


		batcher = new SpriteBatch();
		// Attach batcher to camera
		batcher.setProjectionMatrix(cam.combined);

		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(cam.combined);
	}

	public void resize(){
		maxWidth = myWorld.game.maxWidth;
		maxHeight = myWorld.game.maxHeight;
		hpad = maxHeight - 224;
		wpad = maxWidth - 128;
		if(myWorld.game.isIphoneX){
			iphoneXLeft = myWorld.game.iphoneXLeft;
			iphoneXRight = myWorld.game.iphoneXRight;
			wpad -= iphoneXLeft;
			wpad -= iphoneXRight;
		}
		centerX = maxWidth / 2.0f;
		centerY = maxHeight / 2.0f;
		cam = new OrthographicCamera();
		cam.setToOrtho(true, maxWidth, maxHeight);
		batcher = new SpriteBatch();
		batcher.setProjectionMatrix(cam.combined);
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(cam.combined);
	}

	public void render(float runTime) {
		runtime = runTime;
		// Fill the entire screen with black, to prevent potential flickering.
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl20.glEnable(GL20.GL_BLEND);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(new Color(153 /255f, 128 / 255f, 99 /255f, 1));
		shapeRenderer.rect(0,0,maxWidth,maxHeight);
		shapeRenderer.end();
		Gdx.gl20.glDisable(GL20.GL_BLEND);
		if(myWorld.screen == 2) {
			drawFloors(runTime);
			drawSprite(runTime);
			drawCombatText();
		}
		else if(myWorld.screen == 0){
			drawArmyScreen();
		}
		drawInterface(runTime);
	}

	private void generateFonts(){
		FreeTypeFontGenerator generator = null;
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = null;
		try{
			font = new BitmapFont(Gdx.files.internal("fonts/fontBig.fnt"), true);
			font.setColor(Color.valueOf("ffffff"));
			//System.out.println("Font1 loaded successfully");
		}catch(Exception e) {
			generator = new FreeTypeFontGenerator(Gdx.files.internal("visitor1.ttf"));
			parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.size = 20;
			parameter.flip = true;
			parameter.shadowOffsetX = 0;
			parameter.shadowOffsetY = 0;
			parameter.shadowColor = Color.BLACK;
			parameter.packer = new PixmapPacker(512, 512, Pixmap.Format.RGBA8888, 2, false, new PixmapPacker.SkylineStrategy());
			font = generator.generateFont(parameter);
			font.setColor(Color.valueOf("ffffff"));

			//save big font
			FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(parameter);
			BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
			info.padding = new BitmapFontWriter.Padding(0, 0, 0, 0);
			BitmapFontWriter.writeFont(data, new String[]{"fontBig.png"},
					Gdx.files.internal("fonts/fontBig.fnt"), info, 512, 512);
			BitmapFontWriter.writePixmaps(parameter.packer.getPages(), Gdx.files.internal("fonts"), "fontBig");
			//System.out.println("FontBig generated");
		}
		try{
			fontText = new BitmapFont(Gdx.files.internal("fonts/fontText.fnt"), true);
			fontText.setColor(Color.valueOf("000000"));
			//System.out.println("Font1 loaded successfully");
		}catch(Exception e) {
			generator = new FreeTypeFontGenerator(Gdx.files.internal("m5x7.ttf"));
			parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.size = 16;
			parameter.flip = true;
			parameter.shadowOffsetX = 0;
			parameter.shadowOffsetY = 0;
			parameter.shadowColor = Color.BLACK;
			parameter.packer = new PixmapPacker(512, 512, Pixmap.Format.RGBA8888, 2, false, new PixmapPacker.SkylineStrategy());
			fontText = generator.generateFont(parameter);
			fontText.setColor(Color.valueOf("000000"));

			//save big font
			FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(parameter);
			BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
			info.padding = new BitmapFontWriter.Padding(0, 0, 0, 0);
			BitmapFontWriter.writeFont(data, new String[]{"fontText.png"},
					Gdx.files.internal("fonts/fontText.fnt"), info, 512, 512);
			BitmapFontWriter.writePixmaps(parameter.packer.getPages(), Gdx.files.internal("fonts"), "fontText");
			//System.out.println("FontBig generated");
		}
		try{
			fontTitle = new BitmapFont(Gdx.files.internal("fonts/fontTitle.fnt"), true);
			fontTitle.setColor(Color.valueOf("000000"));
			//System.out.println("Font1 loaded successfully");
		}catch(Exception e) {
			generator = new FreeTypeFontGenerator(Gdx.files.internal("thirteen.ttf"));
			parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.size = 16;
			parameter.flip = true;
			parameter.shadowOffsetX = 0;
			parameter.shadowOffsetY = 0;
			parameter.shadowColor = Color.BLACK;
			parameter.packer = new PixmapPacker(512, 512, Pixmap.Format.RGBA8888, 2, false, new PixmapPacker.SkylineStrategy());
			fontTitle = generator.generateFont(parameter);
			fontTitle.setColor(Color.valueOf("000000"));

			//save big font
			FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(parameter);
			BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
			info.padding = new BitmapFontWriter.Padding(0, 0, 0, 0);
			BitmapFontWriter.writeFont(data, new String[]{"fontTitle.png"},
					Gdx.files.internal("fonts/fontTitle.fnt"), info, 512, 512);
			BitmapFontWriter.writePixmaps(parameter.packer.getPages(), Gdx.files.internal("fonts"), "fontTitle");
			//System.out.println("FontBig generated");
		}
		try{
			fontTitleBack = new BitmapFont(Gdx.files.internal("fonts/fontTitleBack.fnt"), true);
			fontTitleBack.setColor(Color.valueOf("ffffff"));
			//System.out.println("Font1 loaded successfully");
		}catch(Exception e) {
			generator = new FreeTypeFontGenerator(Gdx.files.internal("thirteen.ttf"));
			parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.size = 16;
			parameter.flip = true;
			parameter.shadowOffsetX = 0;
			parameter.shadowOffsetY = 0;
			parameter.shadowColor = Color.BLACK;
			parameter.packer = new PixmapPacker(512, 512, Pixmap.Format.RGBA8888, 2, false, new PixmapPacker.SkylineStrategy());
			fontTitle = generator.generateFont(parameter);
			fontTitle.setColor(Color.valueOf("ffffff"));

			//save big font
			FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(parameter);
			BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
			info.padding = new BitmapFontWriter.Padding(0, 0, 0, 0);
			BitmapFontWriter.writeFont(data, new String[]{"fontTitleBack.png"},
					Gdx.files.internal("fonts/fontTitleBack.fnt"), info, 512, 512);
			BitmapFontWriter.writePixmaps(parameter.packer.getPages(), Gdx.files.internal("fonts"), "fontTitleBack");
			//System.out.println("FontBig generated");
		}
		//System.out.println("font1: " + System.currentTimeMillis());
		try{
			font2 = new BitmapFont(Gdx.files.internal("fonts/font.fnt"), true);
			//System.out.println("Font2 loaded successfully");
		}catch(Exception e){
			if(generator == null){
				generator = new FreeTypeFontGenerator(Gdx.files.internal("visitor1.ttf"));
				parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			}
			parameter.flip = true;
			parameter.shadowOffsetX = 0;
			parameter.shadowOffsetY = 0;
			parameter.shadowColor = Color.BLACK;
			parameter.size = 10;
			parameter.packer = new PixmapPacker(512, 512, Pixmap.Format.RGBA8888, 2, false, new PixmapPacker.SkylineStrategy());
			font2 = generator.generateFont(parameter);
			font2.setColor(.1f,.1f,.1f,1);
			FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(parameter);

			//save small font
			BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
			info.padding = new BitmapFontWriter.Padding(0, 0, 0, 0);
			BitmapFontWriter.writeFont(data, new String[] {"font.png"},
					Gdx.files.internal("fonts/font.fnt"), info, 512, 512);
			BitmapFontWriter.writePixmaps(parameter.packer.getPages(), Gdx.files.internal("fonts"), "font");
			//System.out.println("Font generated");
		}
		font2.setColor(.1f,.1f,.1f,1);
		fontText.setColor(.1f,.1f,.1f,1);
		fontText = font2;
	}

	public void drawTiles(){
		/*batcher.begin();
		int width = myWorld.map.width + 6;
		//Tile border = new Tile(myWorld.map.getBorder(),0,0,0,myWorld);
		for(Tile t: myWorld.map.tiles){
			batcher.draw(t.getImage(),t.getX(),t.getY());
		}
		batcher.end();
		Gdx.gl20.glEnable(GL20.GL_BLEND);
		shapeRenderer.begin(ShapeType.Filled);
		if(myWorld.selected != null){
			float shade = runtime % 2;
			if(shade < 1){
				shade = 2 - shade;
			}
			shade /= 2f;
			shade /= 2f;
			shade = .2f + shade;
			for(Tile tile : myWorld.selected.movementTiles){
				shapeRenderer.setColor(new Color(36 /255f, 131 / 255f, 179 /255f, shade));
				shapeRenderer.rect(tile.getX(),tile.getY(),16,16);
				shapeRenderer.setColor(new Color(34 /255f, 113 / 255f, 153 /255f, 1));
				if(!myWorld.map.tiles.get(tile.getNum()-1).moveable){
					shapeRenderer.rect(tile.getX(),tile.getY(),1,16);
				}
				if(!myWorld.map.tiles.get(tile.getNum()+1).moveable){
					shapeRenderer.rect(tile.getX() + 15,tile.getY(),1,16);
				}
				if(!myWorld.map.tiles.get(tile.getNum()-myWorld.map.width).moveable){
					shapeRenderer.rect(tile.getX(),tile.getY(),16,1);
				}
				if(!myWorld.map.tiles.get(tile.getNum()+myWorld.map.width).moveable){
					shapeRenderer.rect(tile.getX(),tile.getY()+15,16,1);
				}

			}
		}
		shapeRenderer.end();
		Gdx.gl20.glDisable(GL20.GL_BLEND);
		*/
	}

	public void drawSprite(float runTime){
		float hpad = this.hpad - iphoneXLeft;
		batcher.begin();
		int i = 0;
		for(Unit u: myWorld.party){
			if(myWorld.floors.get(0).floorNum % 2 == 0) {
				if (u.character.currentHp > 0) {
					if (myWorld.moving) {
						float uniqueTime = myWorld.movingTime + i;
						if(uniqueTime >= 5.5f){
							uniqueTime -= 5.5f;
							batcher.draw(u.character.getJob().getWalkAnimation(runTime), 112 - uniqueTime * 16, myWorld.floors.get(0).y + 16 + hpad);
						}
						else if(uniqueTime >= 3){
							uniqueTime -= 3f;
							batcher.draw(u.character.getJob().getFlipAnimation(runTime), 112, myWorld.floors.get(0).y + 16 - (2.5f - uniqueTime) * 16 + hpad);
						}
						else{
							u.character.getJob().getWalkAnimation(runTime).flip(true,false);
							batcher.draw(u.character.getJob().getWalkAnimation(runTime), 64 + uniqueTime * 16, myWorld.floors.get(1).y + 16 + hpad);
							u.character.getJob().getWalkAnimation(runTime).flip(true,false);
						}
					}
					else if (u.character == myWorld.attacking) {
						batcher.draw(u.character.getJob().getSwingAnimation(myWorld.attackingTime), u.x, u.y + hpad);
					} else {
						batcher.draw(u.character.getJob().getBattleAnimation(myWorld.runtime), u.x, u.y + hpad);
					}
					if(u.character.stunTurns > 0){
						batcher.draw(AssetLoader.stunAnimation.getKeyFrame(myWorld.runtime), u.x, u.y + hpad);
					}
					if(u.character.aots.size() > 0){
						if(myWorld.runtime%1 <= .3f){
							batcher.draw(u.character.aots.get(0).getImage(myWorld.runtime%1), u.x, u.y + hpad);
						}
						else if(myWorld.runtime % 1 <= .6f && u.character.aots.size() > 1){
							batcher.draw(u.character.aots.get(1).getImage(myWorld.runtime%1), u.x, u.y + hpad);
						}
						else if(myWorld.runtime % 2 <= .9f && u.character.aots.size() > 2){
							batcher.draw(u.character.aots.get(2).getImage(myWorld.runtime%1), u.x, u.y + hpad);
						}
					}
					if(u.character.buffs.size() > 0){
						batcher.draw(u.character.buffs.get(0).getImage(),u.x + 8, u.y + 10 + hpad);
					}
					if(u.displays.size() > 0){
						batcher.draw(u.displays.get(0).animation.getKeyFrame(u.displays.get(0).time),u.x, u.y + hpad);
					}
				} else if (myWorld.reviveTime > 0) {
					batcher.draw(AssetLoader.reviveAnimation.getKeyFrame(.7f - myWorld.reviveTime), u.x, u.y + hpad);
				}
			}
			else{
				if (u.character.currentHp > 0) {
					if (myWorld.moving) {
						float uniqueTime = myWorld.movingTime + i;
						if(uniqueTime >= 5.5f){
							uniqueTime -= 5.5f;
							u.character.getJob().getWalkAnimation(runTime).flip(true,false);
							batcher.draw(u.character.getJob().getWalkAnimation(runTime), 0 + uniqueTime * 16, myWorld.floors.get(0).y + 16 + hpad);
							u.character.getJob().getWalkAnimation(runTime).flip(true,false);
						}
						else if(uniqueTime >= 3){
							uniqueTime -= 3f;
							batcher.draw(u.character.getJob().getFlipAnimation(runTime), 0, myWorld.floors.get(0).y + 16 - (2.5f - uniqueTime) * 16 + hpad);
						}
						else{
							batcher.draw(u.character.getJob().getWalkAnimation(runTime), 0 + (3 - uniqueTime) * 16, myWorld.floors.get(1).y + 16 + hpad);
						}
					}
					else if (u.character == myWorld.attacking) {
						u.character.getJob().getSwingAnimation(myWorld.attackingTime).flip(true,false);
						batcher.draw(u.character.getJob().getSwingAnimation(myWorld.attackingTime), u.x, u.y + hpad);
						u.character.getJob().getSwingAnimation(myWorld.attackingTime).flip(true,false);
					} else {
						u.character.getJob().getBattleAnimation(myWorld.runtime).flip(true,false);
						batcher.draw(u.character.getJob().getBattleAnimation(myWorld.runtime), u.x, u.y + hpad);
						u.character.getJob().getBattleAnimation(myWorld.runtime).flip(true,false);
					}
					if(u.character.stunTurns > 0){
						batcher.draw(AssetLoader.stunAnimation.getKeyFrame(myWorld.runtime), u.x, u.y + hpad);
					}
					if(u.character.aots.size() > 0){
						if(myWorld.runtime%1 <= .3f){
							batcher.draw(u.character.aots.get(0).getImage(myWorld.runtime%1), u.x, u.y + hpad);
						}
						else if(myWorld.runtime % 1 <= .6f && u.character.aots.size() > 1){
							batcher.draw(u.character.aots.get(1).getImage(myWorld.runtime%1), u.x, u.y + hpad);
						}
						else if(myWorld.runtime % 2 <= .9f && u.character.aots.size() > 2){
							batcher.draw(u.character.aots.get(2).getImage(myWorld.runtime%1), u.x, u.y + hpad);
						}
					}
					if(u.character.buffs.size() > 0){
						batcher.draw(u.character.buffs.get(0).getImage(),u.x + 8, u.y + 10 + hpad);
					}
                    if(u.displays.size() > 0){
                        batcher.draw(u.displays.get(0).animation.getKeyFrame(u.displays.get(0).time),u.x, u.y + hpad);
                    }
				} else if (myWorld.reviveTime > 0) {
						AssetLoader.reviveAnimation.getKeyFrame(.7f - myWorld.reviveTime).flip(true,false);
					batcher.draw(AssetLoader.reviveAnimation.getKeyFrame(.7f - myWorld.reviveTime), u.x, u.y + hpad);
						AssetLoader.reviveAnimation.getKeyFrame(.7f - myWorld.reviveTime).flip(true,false);
				}
			}
			i++;
		}
		batcher.end();
	}

	public void drawFloors(float runtime){
		float hpad = this.hpad - iphoneXLeft;
		batcher.begin();
		for(int f = myWorld.floors.size() - 1; f >= 0; f --){
			Floor floor = myWorld.floors.get(f);
			batcher.draw(getBackground(floor.background),0,floor.y + hpad);
			TextureRegion floorTile = getFloorTile(floor.floorTile);
			for(int i = 0; i < 8; i++){
				batcher.draw(floorTile,i * 16, floor.y + 32 + hpad);
			}
			for(int i = 0; i < floor.monsters.size(); i++){
				Unit u = floor.monsters.get(i);
				float xmod = 0;
				if(u.character == myWorld.attacking){
					xmod = myWorld.attackingTime;
					if(xmod > .25){
						xmod = .5f - xmod;
					}
					if(u.character.stunTurns > 0){
						xmod = 0;
					}
				}
				if(floor.floorNum % 2 == 0) {
					u.character.getJob().getBattleAnimation(runtime).flip(true,false);
					batcher.draw(u.character.getJob().getBattleAnimation(runtime), 112 + (i - floor.monsters.size()) * 16 - xmod * 16, floor.y + 16 + hpad);
					u.character.getJob().getBattleAnimation(runtime).flip(true,false);
					if(u.character.stunTurns > 0){
						batcher.draw(AssetLoader.stunAnimation.getKeyFrame(myWorld.runtime), 112 + (i - floor.monsters.size()) * 16 - xmod * 16, floor.y + 16 + hpad);
					}
					if(u.character.aots.size() > 0){
						if(myWorld.runtime%1 <= .3f){
							batcher.draw(u.character.aots.get(0).getImage(myWorld.runtime%1), 112 + (i - floor.monsters.size()) * 16 - xmod * 16, floor.y + 16 + hpad);
						}
						else if(myWorld.runtime % 1 <= .6f && u.character.aots.size() > 1){
							batcher.draw(u.character.aots.get(1).getImage(myWorld.runtime%1), 112 + (i - floor.monsters.size()) * 16 - xmod * 16, floor.y + 16 + hpad);
						}
						else if(myWorld.runtime % 2 <= .9f && u.character.aots.size() > 2){
							batcher.draw(u.character.aots.get(2).getImage(myWorld.runtime%1), 112 + (i - floor.monsters.size()) * 16 - xmod * 16, floor.y + 16 + hpad);
						}
					}
                    if(u.displays.size() > 0){
                        batcher.draw(u.displays.get(0).animation.getKeyFrame(u.displays.get(0).time),112 + (i - floor.monsters.size()) * 16 + u.displays.get(0).xoffset, floor.y + 16 + hpad + u.displays.get(0).yoffset);
                    }
				}
				else{
					batcher.draw(u.character.getJob().getBattleAnimation(runtime), 0 - (i - floor.monsters.size()) * 16 + xmod * 16, floor.y + 16 + hpad);
					if(u.character.stunTurns > 0){
						batcher.draw(AssetLoader.stunAnimation.getKeyFrame(myWorld.runtime), 0 - (i - floor.monsters.size()) * 16 + xmod * 16, floor.y + 16 + hpad);
					}
					if(u.character.aots.size() > 0){
						if(myWorld.runtime%1 <= .3f){
							batcher.draw(u.character.aots.get(0).getImage(myWorld.runtime%1), 0 - (i - floor.monsters.size()) * 16 + xmod * 16, floor.y + 16 + hpad);
						}
						else if(myWorld.runtime % 1 <= .6f && u.character.aots.size() > 1){
							batcher.draw(u.character.aots.get(1).getImage(myWorld.runtime%1), 0 - (i - floor.monsters.size()) * 16 + xmod * 16, floor.y + 16 + hpad);
						}
						else if(myWorld.runtime % 2 <= .9f && u.character.aots.size() > 2){
							batcher.draw(u.character.aots.get(2).getImage(myWorld.runtime%1), 0 - (i - floor.monsters.size()) * 16 + xmod * 16, floor.y + 16 + hpad);
						}
					}
                    if(u.displays.size() > 0){
						u.displays.get(0).animation.getKeyFrame(u.displays.get(0).time).flip(true,false);
                        batcher.draw(u.displays.get(0).animation.getKeyFrame(u.displays.get(0).time),0 - (i - floor.monsters.size()) * 16, floor.y + 16 + hpad + u.displays.get(0).yoffset);
						u.displays.get(0).animation.getKeyFrame(u.displays.get(0).time).flip(true,false);
                    }
				}
			}
			if(floor.floorNum % 2 == 0) {
				batcher.draw(AssetLoader.ladder,116,floor.y - 7 + hpad);
				String floorText = floor.floorNum + "F";
				int length = getTextX(floorText);
				drawTextOutline(floor.floorNum + "F",127 - length,floor.y + 34 + hpad);
			}
			else{
				batcher.draw(AssetLoader.ladder,4,floor.y - 7 + hpad);
				drawTextOutline(floor.floorNum + "F",3,floor.y + 34 + hpad);
			}
		}
		batcher.end();
		shapeRenderer.begin(ShapeType.Filled);
		for(int f = myWorld.floors.size() - 1; f >= 0; f --){
			Floor floor = myWorld.floors.get(f);
			for(int i = 0; i < floor.monsters.size(); i++) {
				Unit u = floor.monsters.get(i);
				if(floor.floorNum % 2 == 0) {
					u.x = 112 + (i - floor.monsters.size()) * 16;
				}
				else{
					u.x = 0 - (i - floor.monsters.size()) * 16;
				}
				u.y = floor.y + 32 + hpad;
				shapeRenderer.setColor(new Color(15 /255f, 15 / 255f, 15 /255f, 1));
				shapeRenderer.rect(u.x + 1,u.y,14,3);
				shapeRenderer.setColor(new Color(60 /255f, 60 / 255f, 60 /255f, 1));
				shapeRenderer.rect(u.x + 2,u.y + 1,12,1);
				shapeRenderer.setColor(getHpColor(u.character.displayHp,u.character.getStat(0)));
				shapeRenderer.rect(u.x + 2,u.y + 1,(u.character.displayHp / ((float)u.character.getStat(0))) * 12,1);
			}
		}
		shapeRenderer.end();
	}

	public TextureRegion getBackground(int b){
		switch (b){
			case 0:
				return AssetLoader.caveWall;
		}
		return AssetLoader.caveWall;
	}
	public TextureRegion getFloorTile(int f){
		switch (f){
			case 0:
				return AssetLoader.caveFloor;
		}
		return AssetLoader.caveWall;
	}

	public void drawSelect(float x, float y, int w, int h, float runtime){
			x-= 1;
			y-= 1;
			Gdx.gl20.glEnable(GL20.GL_BLEND);
			shapeRenderer.begin(ShapeType.Filled);
			float shade = runtime % 2;
			if (shade > 1) {
				shade = 2 - shade;
			}
			shade += .25f;
			shapeRenderer.setColor(.1f, .1f, .1f, shade);

			//top left
			shapeRenderer.rect(x, y - 1, 6, 3);
			shapeRenderer.rect(x, y, 3, 5);

			//top right
			shapeRenderer.rect(x + w - 4, y - 1, 6, 3);
			shapeRenderer.rect(x + w - 1, y, 3, 5);

			//bottom left
			shapeRenderer.rect(x, y + h, 6, 3);
			shapeRenderer.rect(x, y + h - 3, 3, 5);

			//bottom right
			shapeRenderer.rect(x + w - 4, y + h, 6, 3);
			shapeRenderer.rect(x + w - 1, y + h - 3, 3, 5);
			shapeRenderer.setColor(1, 1, 1, shade);

			//top left
			shapeRenderer.rect(x+1, y, 4, 1);
			shapeRenderer.rect(x+1, y + 1, 1, 3);

			//top right
			shapeRenderer.rect(x + w - 3, y, 4, 1);
			shapeRenderer.rect(x + w, y + 1, 1, 3);

			//bottom left
			shapeRenderer.rect(x+1, y + h + 1, 4, 1);
			shapeRenderer.rect(x+1, y + h - 2, 1, 3);

			//bottom right
			shapeRenderer.rect(x + w - 3, y + h + 1, 4, 1);
			shapeRenderer.rect(x + w, y + h - 2, 1, 3);
			shapeRenderer.end();
			Gdx.gl20.glDisable(GL20.GL_BLEND);
	}

	public void drawCombatText(){
		float hpad = this.hpad - iphoneXLeft;
		batcher.begin();
		for (CombatText ct : myWorld.floatingText) {
			if(ct.time >= 0) {
				setFontColor(ct.color);
				int len = ct.message.length();
				int offset = len * 6;
				for (char c : ct.message.toCharArray()) {
					if (c == '1') {
						offset -= 3;
					}
				}
				offset /= 2f;
				Color c = font2.getColor();
				c = new Color(c.r, c.g, c.b, 1);
				drawTextOutline(ct.message + (ct.crit ? "!" : ""), ct.x + 8 - offset, ct.y + hpad, c);
				if (ct.up == 1) {
					batcher.draw(AssetLoader.arrowUp, ct.x + 8 - offset + (len * 6), ct.y + .5f + hpad);
				} else if (ct.up == -1) {
					batcher.draw(AssetLoader.arrowDown, ct.x + 8 - offset + (len * 6), ct.y + .5f + hpad);
				}
			}
		}
		batcher.end();
	}
	private void setFontColor(int id){
		switch (id){
			case -1:
				font2.setColor(Color.valueOf("ffffff"));
				break;
			case 0:
				font2.setColor(Color.valueOf("ffecb3"));
				break;
			case 1:
				font2.setColor(Color.valueOf("c57acc"));
				break;
			case 2:
				font2.setColor(Color.valueOf("ff9633"));
				break;
			case 3:
				font2.setColor(Color.valueOf("aadaf2"));
				break;
			case 4:
				font2.setColor(Color.valueOf("6cb336"));
				break;
			case 5:
				font2.setColor(Color.valueOf("ffe047"));
				break;
			case 6:
				font2.setColor(Color.valueOf("cc2929"));
				break;
		}
	}



	public void drawInterface(float runtime) {
		float hpad = this.hpad - iphoneXLeft;
		if (myWorld.screen == 2 && myWorld.itemDisplay > 0) {
			shapeRenderer.begin(ShapeType.Filled);
			float display = myWorld.itemDisplay;
			if (display > 3.5) {
				display = 4 - display;
				display *= 2;
			} else if (display < .5) {
				display *= 2;
			} else {
				display = 1;
			}
			shapeRenderer.setColor(.1f, .1f, .1f, 1);
			shapeRenderer.rect(0, 12 + iphoneXLeft - 20 + 20 * display, maxWidth, 22);
			shapeRenderer.setColor(.5f, .5f, .5f, 1);
			shapeRenderer.rect(1, 13 + iphoneXLeft - 20 + 20 * display, maxWidth - 2, 20);
			drawItemSquare(myWorld.newItem.rarity, 2, 14 + iphoneXLeft - 20 + 20 * display);
			shapeRenderer.end();
			batcher.begin();
			drawItem(myWorld.newItem,3, 15 + iphoneXLeft - 20 + 20 * display);
			batcher.end();
		}
		shapeRenderer.begin(ShapeType.Filled);
		//draw top
		shapeRenderer.setColor(new Color(32 /255f, 32 / 255f, 32 /255f, 1));
		shapeRenderer.rect(0,0,maxWidth,12 + iphoneXLeft);
		shapeRenderer.setColor(new Color(15 /255f, 15 / 255f, 15 /255f, 1));
		shapeRenderer.rect(0,12 + iphoneXLeft,maxWidth,1);
		if(myWorld.screen == 2) {
			//draw bottom
			shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
			shapeRenderer.rect(0, 157 + hpad, maxWidth, maxHeight);
			shapeRenderer.setColor(new Color(60 / 255f, 60 / 255f, 60 / 255f, 1));
			//blank hp bars
			for (int i = 0; i < 4; i++) {
				shapeRenderer.rect(1 + i * 32, 191 + hpad, 30, 2);
				shapeRenderer.rect(1 + i * 32, 195 + hpad, 30, 2);
			}
			for (int i = 0; i < 4; i++) {
				Character c;
				if (TowerTitan.save.save.party.size() > i) {
					c = TowerTitan.save.save.party.get(i);
					drawCharacterIcon(c, 0 + i * 32, 158 + hpad);
					shapeRenderer.setColor(getHpColor(c.displayHp, c.getStat(0)));
					shapeRenderer.rect(1 + i * 32, 191 + hpad, (c.displayHp / ((float) c.getStat(0))) * 30, 2);
					shapeRenderer.setColor(Color.valueOf("cca329"));
					float levelRatio = TowerTitan.save.save.gold / (float) Character.getLevelCost(c.getLevel() + 1);
					if (levelRatio > 1) {
						levelRatio = 1;
					}
					shapeRenderer.rect(1 + i * 32, 195 + hpad, (int) Math.floor(levelRatio * 30), 2);
				} else {
					drawCharacterIcon(null, i * 32, 158 + hpad);
				}
			}
		}
		shapeRenderer.setColor(new Color(30 /255f, 30 / 255f, 30 /255f, 1));
		shapeRenderer.rect(0,198 + 26 + hpad,maxWidth,maxHeight);
		shapeRenderer.end();
		batcher.begin();
		//draw top
		font2.setColor(1,1,1,1);
		batcher.draw(AssetLoader.goldIcon,2,2 + iphoneXLeft);
		int gold = TowerTitan.save.save.gold;
		String goldDisplay = "";
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		if(gold > 999999){
			goldDisplay = String.valueOf(df.format(gold / 1000000f));
			goldDisplay += "M";
			if(goldDisplay.length() > 9){
				DecimalFormat df1 = new DecimalFormat();
				df.setMaximumFractionDigits(1);
				goldDisplay = String.valueOf(df1.format(gold / 1000000f));
				goldDisplay += "M";
				if(goldDisplay.length() > 9) {
					goldDisplay = String.valueOf(df1.format(gold / 1000000));
				}
			}
		}
		else if(gold > 999){
			goldDisplay = String.valueOf(df.format(gold / 1000f));
			goldDisplay += "K";
		}
		else{
			goldDisplay = String.valueOf(gold);
		}
		font2.draw(batcher,goldDisplay,12,3.5f + iphoneXLeft);
		batcher.draw(AssetLoader.gemIcon,64,2 + iphoneXLeft);
		font2.draw(batcher,String.valueOf(TowerTitan.save.save.gems),74,3.5f + iphoneXLeft);
		batcher.draw(AssetLoader.goldGemIcon,96,2 + iphoneXLeft);
		font2.draw(batcher,String.valueOf(TowerTitan.save.save.goldGems),106,3.5f + iphoneXLeft);

		if(myWorld.screen == 2) {
			for (int i = 0; i < 4; i++) {
				Character c;
				if (TowerTitan.save.save.party.size() > i) {
					c = TowerTitan.save.save.party.get(i);
					if (c.getJob().getDetailImage() != null) {
						batcher.draw(c.getJob().getDetailImage(), 3 + i * 32, 161 + hpad);
					}
					switch (c.role) {
						case 1:
							batcher.draw(AssetLoader.tankIcon, i * 32, 176 + hpad);
							break;
						case 2:
							batcher.draw(AssetLoader.dmgIcon, i * 32, 176 + hpad);
							break;
						case 3:
							batcher.draw(AssetLoader.healIcon, i * 32, 176 + hpad);
							break;
					}
					String levelText = "";
					if (c.getLevel() < 200) {
						levelText = "Lv." + String.valueOf(c.getLevel());
					} else {
						levelText = "Lv" + String.valueOf(c.getLevel());
					}
					if (c.canLevelUp()) {
						if (getTextX(levelText) <= 27) {
							drawTextOutline("+", getTextX(levelText) + i * 32, 184 + hpad, Color.valueOf("e6b530"));
						} else {
							drawTextOutline("+", 25 + i * 32, 178 + hpad, Color.valueOf("e6b530"));
						}
					}
					drawTextOutline(levelText, 1 + i * 32, 184 + hpad);
				}
			}
		}
		if(myWorld.view == null) {
			if (myWorld.screen == 0) {
				batcher.draw(AssetLoader.armySelect, 0, 198 + hpad);
			} else {
				batcher.draw(AssetLoader.army, 0, 198 + hpad);
			}
			if (myWorld.screen == 1) {
				batcher.draw(AssetLoader.itemSelect, 25, 198 + hpad);
			} else {
				batcher.draw(AssetLoader.item, 25, 198 + hpad);
			}
			if (myWorld.screen == 2) {
				batcher.draw(AssetLoader.towerSelect, 51, 198 + hpad);
			} else {
				batcher.draw(AssetLoader.tower, 51, 198 + hpad);
			}
			if (myWorld.screen == 3) {
				batcher.draw(AssetLoader.shopSelect, 77, 198 + hpad);
			} else {
				batcher.draw(AssetLoader.shop, 77, 198 + hpad);
			}
			if (myWorld.screen == 4) {
				batcher.draw(AssetLoader.menuSelect, 102, 198 + hpad);
			} else {
				batcher.draw(AssetLoader.menuIcon, 102, 198 + hpad);
			}
		}
		batcher.end();
    }
	public Color getHpColor(int hp, int max){
		float ratio = ((float) hp) / ((float) max);
		Color color = null;
		if (ratio > .6) {
			color = Color.valueOf("0a8035");
		} else if (ratio > .3) {
			color = Color.valueOf("cca12b");
		} else {
			color = Color.valueOf("b32424");
		}
		return color;
	}
    public void drawCharacterIcon(Character c, float x, float y){
		Color one, two, three;
		if(c != null) {
			switch (c.getJob().getRarity()) {
				case 2:
					one = new Color(20 /255f, 46 / 255f, 102 /255f, 1);
					two = new Color(38 /255f, 87 / 255f, 191 /255f, 1);
					three = new Color(87 /255f, 130 / 255f, 217 /255f, 1);
					break;
				case 3:
					one = new Color(102 /255f, 10 / 255f, 10 /255f, 1);
					two = new Color(153 /255f, 31 / 255f, 31 /255f, 1);
					three = new Color(204 /255f, 63 / 255f, 63 /255f, 1);
					break;
				case 4:
					one = new Color(102 /255f, 68 / 255f, 20 /255f, 1);
					two = new Color(179 /255f, 119 / 255f, 36 /255f, 1);
					three = new Color(204 /255f, 162 / 255f, 102 /255f, 1);
					break;
				default:
					one = new Color(4 /255f, 89 / 255f, 43 /255f, 1);
					two = new Color(10 /255f, 128 / 255f, 53 /255f, 1);
					three = new Color(97 /255f, 194 / 255f, 107 /255f, 1);
					break;
			}
		}
		else{
			one = new Color(32 /255f, 32 / 255f, 32 /255f, 1);
			two = new Color(64 /255f, 64 / 255f, 64 /255f, 1);
			three = new Color(96 /255f, 96 / 255f, 96 /255f, 1);
		}
		shapeRenderer.setColor(one);
		shapeRenderer.rect(x,y,32,32);
		shapeRenderer.setColor(two);
		shapeRenderer.rect(x+1,y+1,30,30);
		shapeRenderer.setColor(one);
		shapeRenderer.rect(x+2,y+2,28,28);
		shapeRenderer.setColor(three);
		shapeRenderer.rect(x+3,y+3,26,26);
	}

	public void drawTextOutline(String text, float x, float y){
		drawTextOutline(text, x, y, new Color(1,1,1,1));
	}
	public void drawTextOutline(String text, float x, float y, Color c){
		font2.setColor(.1f,.1f,.1f,1);
		font2.draw(batcher, text, x-1, y-1);
		font2.draw(batcher, text, x, y-1);
		font2.draw(batcher, text, x+1, y-1);
		font2.draw(batcher, text, x-1, y);
		font2.draw(batcher, text, x+1, y);
		font2.draw(batcher, text, x-1, y+1);
		font2.draw(batcher, text, x, y+1);
		font2.draw(batcher, text, x+1, y+1);
		font2.setColor(c);
		font2.draw(batcher, text, x,y);
		font2.setColor(.1f,.1f,.1f,1);
	}

	public void drawArmyScreen(){
		float hpad = this.hpad - iphoneXLeft;
		if(myWorld.view == null) {
			shapeRenderer.begin(ShapeType.Filled);
			shapeRenderer.setColor(new Color(90 / 255f, 90 / 255f, 90 / 255f, 1));
			shapeRenderer.rect(0, 49 + iphoneXLeft, maxWidth, maxHeight);
			float armyY = 49 - myWorld.scrollY;
			for (int i = 0; i < 100; i++) {
				Character c;
				if (TowerTitan.save.save.army.size() > i) {
					c = TowerTitan.save.save.army.get(i);
					drawCharacterIcon(c, (i % 4) * 32, armyY + iphoneXLeft + (i / 4) * 32);
				} else {
					drawCharacterIcon(null, (i % 4) * 32, armyY + iphoneXLeft + (i / 4) * 32);
				}
			}
			shapeRenderer.end();
			batcher.begin();
			for (int i = 0; i < 100; i++) {
				Character c;
				if (TowerTitan.save.save.army.size() > i) {
					c = TowerTitan.save.save.army.get(i);
					if (c.getJob().getDetailImage() != null) {
						batcher.draw(c.getJob().getDetailImage(), 3 + (i % 4) * 32, armyY + 3 + iphoneXLeft + (i / 4) * 32);
					}
					switch (c.role) {
						case 1:
							batcher.draw(AssetLoader.tankIcon, (i % 4) * 32, armyY + 18 + iphoneXLeft + (i / 4) * 32);
							break;
						case 2:
							batcher.draw(AssetLoader.dmgIcon, (i % 4) * 32, armyY + 18 + iphoneXLeft + (i / 4) * 32);
							break;
						case 3:
							batcher.draw(AssetLoader.healIcon, (i % 4) * 32, armyY + 18 + iphoneXLeft + (i / 4) * 32);
							break;
					}
					String levelText = "";
					if (c.getLevel() < 200) {
						levelText = "Lv." + String.valueOf(c.getLevel());
					} else {
						levelText = "Lv" + String.valueOf(c.getLevel());
					}
					drawTextOutline(levelText, 1 + (i % 4) * 32, armyY + 26 + iphoneXLeft + (i / 4) * 32);
				}
			}
			batcher.end();
			shapeRenderer.begin(ShapeType.Filled);
			if (TowerTitan.save.save.party.size() < 4) {
				for (int i = 0; i < 100; i++) {
					if (TowerTitan.save.save.army.size() > i) {
						shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
						shapeRenderer.rect(23 + (i % 4) * 32, armyY + iphoneXLeft + (i / 4) * 32, 9, 9);
						shapeRenderer.setColor(new Color(38 / 255f, 138 / 255f, 189 / 255f, 1));
						shapeRenderer.rect(24 + (i % 4) * 32, armyY + 1 + iphoneXLeft + (i / 4) * 32, 7, 7);
						shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
						shapeRenderer.rect(26 + (i % 4) * 32, armyY + 4 + iphoneXLeft + (i / 4) * 32, 3, 1);
						shapeRenderer.rect(27 + (i % 4) * 32, armyY + 3 + iphoneXLeft + (i / 4) * 32, 1, 3);
					} else {

					}
				}
			}
			shapeRenderer.end();
			shapeRenderer.begin(ShapeType.Filled);
			//draw party
			shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
			shapeRenderer.rect(0, 13 + iphoneXLeft, maxWidth, 36);
			shapeRenderer.setColor(new Color(90 / 255f, 90 / 255f, 90 / 255f, 1));
			shapeRenderer.rect(0, 46 + iphoneXLeft, maxWidth, 2);
			for (int i = 0; i < 4; i++) {
				Character c;
				if (TowerTitan.save.save.party.size() > i) {
					c = TowerTitan.save.save.party.get(i);
					drawCharacterIcon(c, i * 32, 13 + iphoneXLeft);
				} else {
					drawCharacterIcon(null, i * 32, 13 + iphoneXLeft);
				}
			}

			shapeRenderer.end();
			batcher.begin();
			for (int i = 0; i < 4; i++) {
				Character c;
				if (TowerTitan.save.save.party.size() > i) {
					c = TowerTitan.save.save.party.get(i);
					if (c.getJob().getDetailImage() != null) {
						batcher.draw(c.getJob().getDetailImage(), 3 + i * 32, 16 + iphoneXLeft);
					}
					switch (c.role) {
						case 1:
							batcher.draw(AssetLoader.tankIcon, i * 32, 31 + iphoneXLeft);
							break;
						case 2:
							batcher.draw(AssetLoader.dmgIcon, i * 32, 31 + iphoneXLeft);
							break;
						case 3:
							batcher.draw(AssetLoader.healIcon, i * 32, 31 + iphoneXLeft);
							break;
					}
					String levelText = "";
					if (c.getLevel() < 200) {
						levelText = "Lv." + String.valueOf(c.getLevel());
					} else {
						levelText = "Lv" + String.valueOf(c.getLevel());
					}
					drawTextOutline(levelText, 1 + i * 32, 39 + iphoneXLeft);
				}
			}

			batcher.end();
			shapeRenderer.begin(ShapeType.Filled);
			if (TowerTitan.save.save.party.size() > 1) {
				for (int i = 0; i < 4; i++) {
					if (TowerTitan.save.save.party.size() > i) {
						shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
						shapeRenderer.rect(23 + i * 32, 13 + iphoneXLeft, 9, 9);
						shapeRenderer.setColor(new Color(179 / 255f, 37 / 255f, 37 / 255f, 1));
						shapeRenderer.rect(24 + i * 32, 14 + iphoneXLeft, 7, 7);
						shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
						shapeRenderer.rect(26 + i * 32, 17 + iphoneXLeft, 3, 1);
					} else {
					}
				}
			}
			shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
			shapeRenderer.rect(0, 194 + hpad, maxWidth, 4);
			shapeRenderer.setColor(new Color(90 / 255f, 90 / 255f, 90 / 255f, 1));
			shapeRenderer.rect(0, 195 + hpad, maxWidth, 2);
			shapeRenderer.end();
		}
		else{
			shapeRenderer.begin(ShapeType.Filled);
			shapeRenderer.setColor(.4f,.4f,.4f,1);
			shapeRenderer.rect(0,0,maxWidth,maxHeight);
			if(myWorld.tab == 0) {
				if(myWorld.itemSlot == -1) {
					drawCharacterIcon(myWorld.view, 2, 15 + iphoneXLeft);
					shapeRenderer.setColor(.1f, .1f, .1f, 1);
					shapeRenderer.rect(36, 33 + iphoneXLeft, 89, 6);
					shapeRenderer.setColor(.2f, .2f, .2f, 1);
					shapeRenderer.rect(37, 34 + iphoneXLeft, 87, 4);
					shapeRenderer.setColor(.1f, .1f, .1f, 1);
					shapeRenderer.rect(0, 49 + iphoneXLeft, maxWidth, 89);
					shapeRenderer.setColor(.6f, .6f, .6f, 1);
					shapeRenderer.rect(0, 50 + iphoneXLeft, maxWidth, 87);

					shapeRenderer.setColor(Color.valueOf("cca329"));
					float levelRatio = TowerTitan.save.save.gold / (float) Character.getLevelCost(myWorld.view.getLevel() + 1);
					if (levelRatio > 1) {
						levelRatio = 1;
					}
					shapeRenderer.rect(37, 34 + iphoneXLeft, (int) Math.floor(levelRatio * 87), 4);

					for (int i = 0; i < 4; i++) {
						if(myWorld.view.items[i] == null){
							drawItemSquare(0,2,53 + iphoneXLeft + i * 21);
						}
						else{
							drawItemSquare(myWorld.view.items[i].rarity,2,53 + iphoneXLeft + i * 21);
						}
					}

					float max = -1;
					for (int i = 0; i < 4; i++) {
						if (myWorld.view.getStat(i) > max) {
							max = myWorld.view.getStat(i);
						}
					}

					float ratio = myWorld.view.getStat(0) / max;

					shapeRenderer.setColor(.1f, .1f, .1f, 1);
					shapeRenderer.rect(66, 142 + iphoneXLeft, 60, 6);
					shapeRenderer.setColor(.2f, .2f, .2f, 1);
					shapeRenderer.rect(67, 143 + iphoneXLeft, 58, 4);

					shapeRenderer.setColor(Color.valueOf("#cc2929"));
					shapeRenderer.rect(67, 143 + iphoneXLeft, 58 * ratio, 4);

					ratio = myWorld.view.getStat(1) / max;
					shapeRenderer.setColor(.1f, .1f, .1f, 1);
					shapeRenderer.rect(66, 154 + iphoneXLeft, 60, 6);
					shapeRenderer.setColor(.2f, .2f, .2f, 1);
					shapeRenderer.rect(67, 155 + iphoneXLeft, 58, 4);
					shapeRenderer.setColor(Color.valueOf("#e65000"));
					shapeRenderer.rect(67, 155 + iphoneXLeft, 58 * ratio, 4);

					ratio = myWorld.view.getStat(2) / max;
					shapeRenderer.setColor(.1f, .1f, .1f, 1);
					shapeRenderer.rect(66, 166 + iphoneXLeft, 60, 6);
					shapeRenderer.setColor(.2f, .2f, .2f, 1);
					shapeRenderer.rect(67, 167 + iphoneXLeft, 58, 4);
					shapeRenderer.setColor(Color.valueOf("#3676b3"));
					shapeRenderer.rect(67, 167 + iphoneXLeft, 58 * ratio, 4);

					ratio = myWorld.view.getStat(3) / max;
					shapeRenderer.setColor(.1f, .1f, .1f, 1);
					shapeRenderer.rect(66, 178 + iphoneXLeft, 60, 6);
					shapeRenderer.setColor(.2f, .2f, .2f, 1);
					shapeRenderer.rect(67, 179 + iphoneXLeft, 58, 4);
					shapeRenderer.setColor(Color.valueOf("#8936b3"));
					shapeRenderer.rect(67, 179 + iphoneXLeft, 58 * ratio, 4);
				}
				else {
					shapeRenderer.setColor(.6f, .6f, .6f, 1);
					shapeRenderer.rect(0, 13 + iphoneXLeft, maxWidth, Math.abs((13 + iphoneXLeft) - (198 + hpad)));
					shapeRenderer.setColor(.1f, .1f, .1f, 1);
					shapeRenderer.rect(0, 36 + iphoneXLeft, maxWidth, 1);
					if(myWorld.view.items[myWorld.itemSlot] == null){
						drawItemSquare(0,2,15 + iphoneXLeft);

					}
					else{
						drawItemSquare(myWorld.view.items[myWorld.itemSlot].rarity,2,15 + iphoneXLeft);
						shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
						shapeRenderer.rect(119, 13 + iphoneXLeft, 9, 9);
						shapeRenderer.setColor(new Color(179 / 255f, 37 / 255f, 37 / 255f, 1));
						shapeRenderer.rect(120, 14 + iphoneXLeft, 7, 7);
						shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
						shapeRenderer.rect(122, 17 + iphoneXLeft, 3, 1);
					}
					for (int i = 0; i < Math.min(6,myWorld.itemList.size() - 6 * myWorld.page); i++){
						shapeRenderer.setColor(.1f, .1f, .1f, 1);
						shapeRenderer.rect(0, 36 + iphoneXLeft + 23 * i, maxWidth, 24);
						shapeRenderer.setColor(.6f, .6f, .6f, 1);
						shapeRenderer.rect(0, 37 + iphoneXLeft + 23 * i, maxWidth, 22);
						drawItemSquare(myWorld.itemList.get(i + 6 * myWorld.page).rarity,2,39 + iphoneXLeft + 23 * i);

						shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
						shapeRenderer.rect(119, 36 + iphoneXLeft + 23 * i, 9, 9);
						shapeRenderer.setColor(new Color(38 / 255f, 138 / 255f, 189 / 255f, 1));
						shapeRenderer.rect(120, 37 + iphoneXLeft + 23 * i, 7, 7);
						shapeRenderer.setColor(new Color(15 / 255f, 15 / 255f, 15 / 255f, 1));
						shapeRenderer.rect(122, 40 + iphoneXLeft + 23 * i, 3, 1);
						shapeRenderer.rect(123, 39 + iphoneXLeft + 23 * i, 1, 3);
					}

				}

			}
			shapeRenderer.end();

			batcher.begin();

			if(myWorld.tab == 0) {
				if(myWorld.itemSlot == -1) {
					if (myWorld.view.getJob().getDetailImage() != null) {
						batcher.draw(myWorld.view.getJob().getDetailImage(), 5, 18 + iphoneXLeft);
					}
					switch (myWorld.view.role) {
						case 1:
							batcher.draw(AssetLoader.tankIcon, 37 + getTextX(myWorld.view.getName()) + 2, 15 + iphoneXLeft);
							break;
						case 2:
							batcher.draw(AssetLoader.dmgIcon, 37 + getTextX(myWorld.view.getName()) + 2, 15 + iphoneXLeft);
							break;
						case 3:
							batcher.draw(AssetLoader.healIcon, 37 + getTextX(myWorld.view.getName()) + 2, 15 + iphoneXLeft);
							break;
					}
					drawTextOutline(myWorld.view.getName(), 37, 16 + iphoneXLeft);
					drawTextOutline("Level " + myWorld.view.getLevel(), 37, 25 + iphoneXLeft);
					int cost = Math.max(0, Character.getLevelCost(myWorld.view.getLevel() + 1) - TowerTitan.save.save.gold);
					if (cost == 0) {
						batcher.draw(AssetLoader.levelUpIcon, 39 + getTextX("Level " + myWorld.view.getLevel()), 23.5f + iphoneXLeft);
					}
					drawTextOutline(String.valueOf(cost), 37, 41 + iphoneXLeft);
					for (int i = 0; i < 4; i++) {
						if (myWorld.view.items[i] != null) {
							drawItem(myWorld.view.items[i],3,54 + iphoneXLeft + i * 21);
						} else {
							batcher.draw(AssetLoader.levelUpIcon, 6.5f, 57.5f + iphoneXLeft + i * 21);
							String text = "No Helm";
							switch (i) {
								case 1:
									text = "No Cloak";
									break;
								case 2:
									text = "No Armor";
									break;
								case 3:
									text = "No Weapon";
									break;
							}
							drawTextOutline(text, 22, 54 + iphoneXLeft + i * 21);
						}
					}
					drawTextOutline("Hp: ", 3, 142 + iphoneXLeft);
					drawTextOutline("" + myWorld.view.getStat(0), 64 - (getTextX("" + myWorld.view.getStat(0))), 142 + iphoneXLeft);
					drawTextOutline("Pow: ", 3, 154 + iphoneXLeft);
					drawTextOutline("" + myWorld.view.getStat(1), 64 - (getTextX("" + myWorld.view.getStat(1))), 154 + iphoneXLeft);
					drawTextOutline("Def: ", 3, 166 + iphoneXLeft);
					drawTextOutline("" + myWorld.view.getStat(2), 64 - (getTextX("" + myWorld.view.getStat(2))), 166 + iphoneXLeft);
					drawTextOutline("Res: ", 3, 178 + iphoneXLeft);
					drawTextOutline("" + myWorld.view.getStat(3), 64 - (getTextX("" + myWorld.view.getStat(3))), 178 + iphoneXLeft);
				}
				else{
					if(myWorld.view.items[myWorld.itemSlot] == null){
						String text = "No Helm";
						switch (myWorld.itemSlot) {
							case 1:
								text = "No Cloak";
								break;
							case 2:
								text = "No Armor";
								break;
							case 3:
								text = "No Weapon";
								break;
						}
						drawTextOutline(text, 22,16 + iphoneXLeft);
					}
					else{
						drawItem(myWorld.view.items[myWorld.itemSlot],3,16 + iphoneXLeft);
					}
					for (int i = 0; i < Math.min(6,myWorld.itemList.size() - 6 * myWorld.page); i++){
						drawItem(myWorld.itemList.get(i + 6 * myWorld.page),3,40 + iphoneXLeft + 23 * i);
					}
					if(myWorld.itemList.size() - 6 * myWorld.page > 6){
						batcher.draw(AssetLoader.rightArrow,96,174 + hpad);
					}
					if(myWorld.page > 0){
						AssetLoader.rightArrow.flip(true,false);
						batcher.draw(AssetLoader.rightArrow,64,174 + hpad);
						AssetLoader.rightArrow.flip(true,false);
					}
					batcher.draw(AssetLoader.closeButton,0,174+hpad);
				}
			}
			batcher.draw(AssetLoader.backButton,0, 198 + hpad);
			if(myWorld.tab == 0){
				batcher.draw(AssetLoader.itemSelect, 50, 198 + hpad);
			}
			else{
				batcher.draw(AssetLoader.item, 50, 198 + hpad);
			}
			if(myWorld.tab == 1){
				batcher.draw(AssetLoader.moveSelect, 76, 198 + hpad);
			}
			else{
				batcher.draw(AssetLoader.move, 76, 198 + hpad);
			}
			if(myWorld.tab == 2){
				batcher.draw(AssetLoader.traitSelect, 102, 198 + hpad);
			}
			else{
				batcher.draw(AssetLoader.trait, 102, 198 + hpad);
			}
			batcher.end();
		}
	}
	public void drawItemSquare(int rarity, float x, float y){
		switch (rarity){
			case 1:
				shapeRenderer.setColor(Color.valueOf("#398033"));
				shapeRenderer.rect(x, y, 18, 18);
				shapeRenderer.setColor(Color.valueOf("#9dd998"));
				shapeRenderer.rect(x + 1, y + 1, 16, 16);
				break;
			case 2:
				shapeRenderer.setColor(Color.valueOf("#2e6599"));
				shapeRenderer.rect(x, y, 18, 18);
				shapeRenderer.setColor(Color.valueOf("#a1c4e6"));
				shapeRenderer.rect(x + 1, y + 1, 16, 16);
				break;
			case 3:
				shapeRenderer.setColor(Color.valueOf("#992e2e"));
				shapeRenderer.rect(x, y, 18, 18);
				shapeRenderer.setColor(Color.valueOf("#e6a1a1"));
				shapeRenderer.rect(x + 1, y + 1, 16, 16);
				break;
			case 4:
				shapeRenderer.setColor(Color.valueOf("#db961f"));
				shapeRenderer.rect(x, y, 18, 18);
				shapeRenderer.setColor(Color.valueOf("#ffcc80"));
				shapeRenderer.rect(x + 1, y + 1, 16, 16);
				break;
			default:
				shapeRenderer.setColor(.2f,.2f,.2f,1);
				shapeRenderer.rect(x, y, 18, 18);
				shapeRenderer.setColor(.7f,.7f,.7f,1);
				shapeRenderer.rect(x + 1, y + 1, 16, 16);
				break;
		}
	}

	public void drawItem(Item item, float x, float y){
		batcher.draw(item.getImage(), x + 1, y + 1);
		switch (item.role){
			case 1:
				batcher.draw(AssetLoader.tankIcon, x + 9, y+9);
				break;
			case 2:
				batcher.draw(AssetLoader.dmgIcon, x + 9, y+9);
				break;
			case 3:
				batcher.draw(AssetLoader.healIcon, x + 9, y+9);
				break;
		}
		x += 19;
		if(item.upgradeCount > 0){
			drawTextOutline(item.name + " +" + item.upgradeCount, x, y);
		}
		else {
			drawTextOutline(item.name, x, y);
		}
		if(item.message.length() <= 0 || (item.rarity == 4 && (runtime % 4 < 2 && (item.stats[1] != 0 || item.stats[4] != 0)))) {
			int startingStat = 0;
			int checkStat = 0;
			if(item.slot == 0 || item.slot == 2){
				startingStat = 2;
			}
			else if(item.slot == 3){
				startingStat = 1;
			}
			for (int i = 0; i < 4; i++) {
				checkStat = (startingStat + i) % 4;
				if (item.stats[checkStat] != 0) {
					Color c;
					switch (checkStat) {
						case 0:
							c = Color.valueOf("#e63030");
							drawTextOutline( "Hp:", x, y + 11,c);
							drawTextOutline(String.valueOf(item.stats[0]), x + 16, y + 11,c);
							x += 16 + getTextX(String.valueOf(item.stats[0])) + 2;
							break;
						case 1:
							c = Color.valueOf("#e66b2e");
							drawTextOutline("Pow:", x, y + 11,c);
							drawTextOutline(String.valueOf(item.stats[1]), x + 22, y + 11,c);
							x += 22 + getTextX(String.valueOf(item.stats[1])) + 2;
							break;
						case 2:
							c = Color.valueOf("#3f88cc");
							drawTextOutline("Def:", x, y + 11,c);
							drawTextOutline(String.valueOf(item.stats[2]), x + 22, y + 11,c);
							x += 22 + getTextX(String.valueOf(item.stats[2])) + 2;
							break;
						case 3:
							c = Color.valueOf("#9b3fcc");
							drawTextOutline("Res:", x, y + 11,c);
							drawTextOutline(String.valueOf(item.stats[3]), x + 22, y + 11,c);
							x += 22 + getTextX(String.valueOf(item.stats[3])) + 2;
							break;
					}
					//x += 39;
					font2.setColor(.1f, .1f, .1f, 1);
				}
			}
		}
		else{
			drawTextOutline(item.message, x, y + 11);
		}
	}
	public static int getTextX(String text){
		char[] array = text.toCharArray();
		int length = 0;
		for(int c = 0; c < array.length; c++){
			if(array[c] == '1' || array[c] == '.' || array[c] == 'i' || array[c] == 'I'){
				length += 3;
			}
			else{
				length += 6;
			}
		}
		return length;
	}
}