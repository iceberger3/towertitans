package world;


import java.util.ArrayList;

import game.TowerTitan;
import helpers.AssetLoader;
import helpers.CombatText;
import objects.Attack;
import objects.AttackDisplay;
import objects.AttackOverTime;
import objects.Buff;
import objects.Character;
import objects.Floor;
import objects.Item;
import objects.Unit;
import screens.GameScreen;


public class GameWorld {

    public TowerTitan game;
    public GameScreen parent;
    public int screen = 2;
    public float delta = 0;
    public ArrayList<Floor> floors;
    public ArrayList<Unit> party;
    public float runtime = 0;
    public float lastRound = -1;
    public Character attacking = null;
    public float attackingTime = 0;
    public ArrayList<Unit> attackQueue;
    public ArrayList<CombatText> floatingText = new ArrayList<CombatText>();
    public float reviveTime = 0;
    public boolean moving = false;
    public float movingTime = 0;
    public float scrollY = 0;
    public float lastY = -1;
    public float initY = -1;
    public Character view;
    public int tab = 0;
    public float itemDisplay = 0;
    public Item newItem = null;
    public int itemSlot = -1;
    public int page = 0;
    public ArrayList<Item> itemList;

	public GameWorld(TowerTitan g, int mapId, GameScreen p) {
        this.game = g;
        parent = p;

        reset();
	}

	public void reset(){
	    moving = false;
	    reviveTime = 0;
	    movingTime = 0;
	    attackingTime = 0;
	    runtime = 0;
	    lastRound = -1;
	    attacking = null;
	    newItem = null;
	    itemDisplay = 0;
	    floatingText = new ArrayList<>();
	    floors = new ArrayList<Floor>();
        for(int i = 0; i < 5; i++){
            floors.add(getNewFloor(TowerTitan.save.save.towerFloor + i, ((4 - i) * 40) - 43));
        }
        party = new ArrayList<>();
        for(int i = 0; i < TowerTitan.save.save.party.size(); i++){
            Character c = TowerTitan.save.save.party.get(i);
            c.reset();
            c.lastIndex = i - 1;
            c.currentHp = c.getStat(0);
            Unit u = new Unit(c,this);
            if(TowerTitan.save.save.towerFloor % 2 == 0){
                u.x = 48 - i * 16;
            }
            else{
                u.x = 64 + i * 16;
            }
            u.y = 133;
            u.character.displayHp = u.character.currentHp;
            party.add(u);
        }
        attackQueue = new ArrayList<>();
        attackQueue.addAll(party);
        attackQueue.addAll(floors.get(0).monsters);

    }

	public void resize(){

    }



	public void update(float delta) {
        this.delta = delta;

        updateFloatingText(delta);
        updateAnimations(delta);
        updateDisplayHp(delta);
        if(itemDisplay > 0){
            itemDisplay -= delta;
            if(itemDisplay < 0){
                newItem = null;
                itemDisplay = 0;
            }
        }
        if(reviveTime > 0){
            reviveTime -= delta;
            if(reviveTime <= 0){
                reviveTime = 0;
                for(Unit u: party){
                    u.character.reset();
                    u.character.lastIndex = party.indexOf(u) - 1;
                }
                if(floors.get(0).monsters.size() <= 0) {
                    moving = true;
                    movingTime = 9.5f;
                    attackQueue.clear();
                    attacking = null;
                    attackingTime = 0;
                    attackQueue.addAll(party);
                    attackQueue.addAll(floors.get(1).monsters);
                }
                else{
                    attackQueue.clear();
                    attacking = null;
                    attackingTime = 0;
                    attackQueue.addAll(party);
                    attackQueue.addAll(floors.get(0).monsters);
                }
            }
        }
        else if(floors.get(0).monsters.size() > 0) {
            this.runtime += delta;
            if (this.runtime > 100) {
                this.runtime -= 100;
                lastRound -= 100;
            }
            if (lastRound < Math.floor(runtime)) {
                lastRound = (int) Math.floor(runtime);
                addGold();
                if(attackQueue.get(0).character.stunTurns > 0){
                    stunned();
                }
                else {
                    battle();
                }
            }
            attackingTime += delta;
            if (attackingTime > .5 && attacking != null) {
                attackingTime = .5f;
                if(attacking.stunTurns > 0){
                    attacking.stunTurns -= 1;
                }
                attacking = null;
            }
        }
        else if(moving){
            movingTime -= delta * 4;
            if(movingTime <= 2.5f){
                for (Floor floor: floors){
                    floor.y += delta * 64;
                }
            }
            if(movingTime <= 0){
                movingTime = 0;
                moving = false;
                int f = 0;
                for (Floor floor: floors){
                    floor.y = 157 - f * 40;
                    f++;
                }
                if(floors.get(0).floorNum % 2 == 0){
                    int i = 0;
                    for(Unit u: party){
                        u.y = floors.get(1).y + 16;
                        u.x = 64 + i * 16;
                        i++;
                    }
                }
                else{
                    int i = 0;
                    for(Unit u: party){
                        u.y = floors.get(1).y + 16;
                        u.x = 48 - (i * 16);
                        i++;
                    }
                }
                moveToNextFloor();
            }
        }
    }

    public void updateDisplayHp(float delta){
        for (Unit u: party){
            if(u.character.displayHp > u.character.currentHp){
                u.character.displayHp -= delta * (u.character.getStat(0) - u.character.currentHp);
                if(u.character.displayHp < u.character.currentHp){
                    u.character.displayHp = u.character.currentHp;
                }
            }
        }
        for (Unit u: floors.get(0).monsters){
            if(u.character.displayHp > u.character.currentHp){
                u.character.displayHp -= delta * (u.character.getStat(0) - u.character.currentHp);
                if(u.character.displayHp < u.character.currentHp){
                    u.character.displayHp = u.character.currentHp;
                }
            }
        }
    }

    public void updateAnimations(float delta){
        for(Unit u: party){
            if(u.displays.size() > 0) {
                u.displays.get(0).time += delta;
                if(u.displays.get(0).time >= u.displays.get(0).animation.getAnimationDuration()){
                    u.displays.remove(0);
                }
            }
        }
        for(Unit u: floors.get(0).monsters){
            if(u.displays.size() > 0) {
                u.displays.get(0).time += delta;
                if(u.displays.get(0).time >= u.displays.get(0).animation.getAnimationDuration()){
                    u.displays.remove(0);
                }
            }
        }
    }

    public void updateFloatingText(float delta){
        int i = 0;
        while (i < floatingText.size()) {
            floatingText.get(i).update(delta);
            if (floatingText.get(i).time > 1) {
                floatingText.remove(i);
            } else {
                i++;
            }
        }
    }
    public void moveToNextFloor(){
	    floors.add(getNewFloor(floors.get(0).floorNum + 5,floors.get(0).y - 200));
	    floors.remove(0);
	    TowerTitan.save.save.towerFloor = floors.get(0).floorNum;
	    TowerTitan.save.save();
    }

    public void addGold(){
	    int gold = 1 + TowerTitan.save.save.towerFloor / 5;
	    TowerTitan.save.save.gold += gold;
	    if(TowerTitan.save.save.gold > 999999999){
            TowerTitan.save.save.gold = 999999999;
        }
    }

    public Floor getNewFloor(int floor, float y){
	    int floorTile = 0;
	    int floorBackground = 0;

	    if(floor <= 5){
	        floorTile = 0;
	        floorBackground = 0;
        }
	    return new Floor(floorBackground,floorTile,floor,y,this);
    }

    public void updateBuffs(Unit u){
        int b = 0;
        while(b < u.character.buffs.size()){
            u.character.buffs.get(b).turns -=1;
            if(u.character.buffs.get(b).turns <= 0){
                u.character.buffs.remove(b);
            }
            else{
                b++;
            }
        }
    }
    public void stunned(){
        Character c = attackQueue.get(0).character;
        attacking = c;
        attackingTime = 0;
        int x = 0;
        if(party.contains(attackQueue.get(0))){
            x = party.indexOf(attackQueue.get(0));
            if(floors.get(0).floorNum % 2 == 0){
                x = 48 - 16 * x;
            }
            else{
                x = 64 + 16 * x;
            }

        }
        else {
            x = floors.get(0).monsters.indexOf(attackQueue.get(0));
            if(floors.get(0).floorNum % 2 == 0){
                x = 112 + (x - floors.get(0).monsters.size()) * 16;
            }
            else{
                x = 0 - (x - floors.get(0).monsters.size()) * 16;
            }
        }
        CombatText ct = new CombatText("Stun", x, 135, 5);
        floatingText.add(ct);
        if(c.aots.size() > 0){
            int count = 0;
            int index = 0;
            while(index  < c.aots.size()) {
                AttackOverTime aot = c.aots.get(index);
                c.currentHp = Math.max(0, (int) (c.currentHp - aot.damage));
                ct = new CombatText(String.valueOf((int) aot.damage), x, 127 - count * 8, aot.getElement());
                floatingText.add(ct);
                count++;
                aot.turns -= 1;
                if(aot.turns <= 0){
                    c.aots.remove(aot);
                }
                else{
                    index++;
                }
            }
        }
        attackQueue.remove(0);
        updateTurn();
    }

    public void updateTurn(){
        Floor currentFloor = floors.get(0);
        if(attackQueue.size() == 0){
            for(Unit u: party){
                if(u.character.currentHp > 0){
                    attackQueue.add(u);
                }
            }
            for(Unit u: currentFloor.monsters){
                if(u.character.currentHp > 0){
                    attackQueue.add(u);
                }
            }
        }
        int deadCount = 0;
        for(Unit u: party){
            if(u.character.currentHp <= 0){
                deadCount++;
            }
        }
        if(currentFloor.monsters.size() <= 0){

            //add item
            if(TowerTitan.save.save.towerFloor % 2 == 1){
                newItem = Item.getMaterial(TowerTitan.save.save.towerFloor);
                int index = (TowerTitan.save.save.towerFloor / 20) % 20;
                TowerTitan.save.save.material[index] += 1;
                if(TowerTitan.save.save.material[index] > 999){
                    TowerTitan.save.save.material[index] = 999;
                }
            }
            else{
                newItem = Item.getItem(TowerTitan.save.save.towerFloor);
                TowerTitan.save.save.items.add(newItem);
            }
            TowerTitan.save.save();
            itemDisplay = 4;

            //check if heroes need to be revived
            if(deadCount > 0) {
                reviveTime = .7f;
            }
            else{
                for(Unit u: party){
                    u.character.reset();
                    u.character.lastIndex = party.indexOf(u) - 1;
                }
                moving = true;
                movingTime = 9.5f;
                attackQueue.clear();
                attacking = null;
                attackingTime = 0;
                attackQueue.addAll(party);
                attackQueue.addAll(floors.get(1).monsters);
            }
        }
        else if(deadCount == party.size()){
            reviveTime = .7f;
        }
    }

    public void battle(){
	    Floor currentFloor = floors.get(0);
        int partyCount = 0;
        for (Unit u: party){
            updateBuffs(u);
            u.character.displayHp = u.character.currentHp;
            if(u.character.currentHp <= 0){
                partyCount++;
                attackQueue.remove(u);
            }
        }
        int enemyCount = 0;
       while(enemyCount < currentFloor.monsters.size()){
           Unit u = currentFloor.monsters.get(enemyCount);
           updateBuffs(u);
           u.character.displayHp = u.character.currentHp;
           if(u.character.currentHp <= 0){
               currentFloor.monsters.remove(enemyCount);
               attackQueue.remove(u);
           }
           else{
               enemyCount++;
           }
       }
       if(attackQueue.size() > 0) {
           Character c = attackQueue.get(0).character;
           c.lastIndex += 1;
           if (c.lastIndex > 3) {
               c.lastIndex = 0;
           }
           Attack a = c.getJob().getAttack(c.lastIndex);
           attacking = c;
           attackingTime = 0;
           if (c.currentHp > 0 && currentFloor.monsters.size() > 0 && partyCount < party.size()) {
               if (party.contains(attackQueue.get(0))) {
                   if (a.targeting == -1) {
                       if (a.area == 1) {
                           Character target = c;
                           float hpRatio = 9999;
                           for (Unit u : party) {
                               float newRatio = u.character.currentHp / (float) u.character.getStat(0);
                               if (u.character.currentHp > 0 && newRatio < hpRatio) {
                                   target = u.character;
                                   hpRatio = newRatio;
                               }
                           }
                           heal(c, a, target);
                       } else if (a.area == 2) {
                           for (Unit u : party) {
                               if(u.character.currentHp > 0) {
                                   heal(c, a, u.character);
                               }
                           }

                       }
                   } else if (a.targeting == 0) {
                       if(a.area != 2){
                           neutral(c,a,c);
                       }
                       else{
                           for(Unit u: party){
                               neutral(c,a,u.character);
                           }
                       }

                   } else if (a.targeting == 1) {
                       if (a.area == 1) {
                           if (floors.get(0).monsters.size() > 0) {
                               Character enemy = currentFloor.monsters.get(0).character;
                               attack(c, a, enemy);
                           }
                       } else if (a.area == 2) {
                           int i = 0;
                           while (i < currentFloor.monsters.size()) {
                               Character enemy = currentFloor.monsters.get(i).character;
                               if (enemy.currentHp > 0) {
                                   attack(c, a, enemy);
                               }
                               i++;
                           }
                       }
                   }
               } else {
                   if (a.targeting == -1) {

                   } else if (a.targeting == 0) {
                       if(a.area != 2){
                           neutral(c,a,c);
                       }
                       else{
                           for(Unit u: currentFloor.monsters){
                               neutral(c,a,u.character);
                           }
                       }
                   } else if (a.targeting == 1) {
                       if (a.area == 1) {
                           if (party.size() > 0) {
                               Character enemy = null;
                               int i = 0;
                               while (enemy == null && i < party.size()) {
                                   if (party.get(i).character.currentHp > 0) {
                                       enemy = party.get(i).character;
                                   } else {
                                       i++;
                                   }
                               }
                               if (enemy != null) {
                                   attack(c, a, enemy);
                               }
                           }
                       } else if (a.area == 2) {
                           for (Unit u : party) {
                               if (u.character.currentHp > 0) {
                                   attack(c, a, u.character);
                               }
                           }
                       }
                   }
               }
               if(c.aots.size() > 0){
                   int count = 0;
                   int index = 0;
                   while(index  < c.aots.size()) {
                       AttackOverTime aot = c.aots.get(index);
                       c.currentHp = Math.max(0, (int) (c.currentHp - aot.damage));
                       int x = 0;
                       if (getIndex(c, party) != -1) {
                           x = getIndex(c, party);
                           if (floors.get(0).floorNum % 2 == 0) {
                               x = 48 - 16 * x;
                           } else {
                               x = 64 + 16 * x;
                           }

                       } else if (getIndex(c, floors.get(0).monsters) != -1) {
                           x = getIndex(c, floors.get(0).monsters);
                           if (floors.get(0).floorNum % 2 == 0) {
                               x = 112 + (x - floors.get(0).monsters.size()) * 16;
                           } else {
                               x = 0 - (x - floors.get(0).monsters.size()) * 16;
                           }
                       }
                       CombatText ct = new CombatText(String.valueOf((int) aot.damage), x, 135 - count * 8, aot.getElement());
                       floatingText.add(ct);
                       count++;
                       aot.turns -= 1;
                       if(aot.turns <= 0){
                           c.aots.remove(aot);
                       }
                       else{
                           index++;
                       }
                   }
               }
           }
           attackQueue.remove(0);
       }
        updateTurn();
    }

    public void heal(Character c, Attack a, Character target){
	    int healing = Math.round((c.getStat(0) / 4f) * (a.attackPower / 40f));
	    if(target.currentHp + healing > target.getStat(0)){
	        healing = target.getStat(0) - target.currentHp;
        }
	    target.currentHp = target.currentHp + healing;

	    int x = 0;
	    float startX = 0;
	    if(getIndex(target,party) != -1){
	        x = getIndex(target,party);
	        party.get(x).displays.add(new AttackDisplay(a));
	        if(floors.get(0).floorNum % 2 == 0){
                x = 48 - 16 * x;
            }
	        else{
                x = 64 + 16 * x;
            }
        }
	    else if(getIndex(target,floors.get(0).monsters) != -1){
            x = getIndex(target,floors.get(0).monsters);
            floors.get(0).monsters.get(x).displays.add(new AttackDisplay(a));
            if(floors.get(0).floorNum % 2 == 0){
                x = 112 + (x - floors.get(0).monsters.size()) * 16;
            }
            else{
                x = 0 - (x - floors.get(0).monsters.size()) * 16;
            }
        }
        CombatText ct = new CombatText("+" + String.valueOf(healing), x + 1, 135, 0);
	    floatingText.add(ct);
	    System.out.println(c.getName() + " healed " + target.getName() + " for " + healing);

	    applyEffect(c,a,target,x);
    }

    public void neutral(Character c, Attack a, Character target){
	    int x = 0;
        if(getIndex(target,party) != -1){
            x = getIndex(target,party);
            party.get(x).displays.add(new AttackDisplay(a));
            if(floors.get(0).floorNum % 2 == 0){
                x = 48 - 16 * x;
            }
            else{
                x = 64 + 16 * x;
            }
        }
        else if(getIndex(target,floors.get(0).monsters) != -1){
            x = getIndex(target,floors.get(0).monsters);
            floors.get(0).monsters.get(x).displays.add(new AttackDisplay(a));
            if(floors.get(0).floorNum % 2 == 0){
                x = 112 + (x - floors.get(0).monsters.size()) * 16;
            }
            else{
                x = 0 - (x - floors.get(0).monsters.size()) * 16;
            }
        }
        applyEffect(c, a, target, x);
    }

    public Unit getUnit(Character c){
	    Unit unit = party.get(0);
	    for(Unit u: party){
	        if(u.character == c){
	            unit = u;
            }
        }
        for(Unit u: floors.get(0).monsters){
            if(u.character == c){
                unit = u;
            }
        }
        return unit;
    }

    public void applyEffect(Character c, Attack a, Character target, int x){
	    if(a.buffs.size() > 0){
	        if(party.contains(c) == party.contains(target)) {
                for (Buff b : a.buffs) {
                    Buff newBuff = new Buff(b.type, b.effect);
                    newBuff.turns = party.size() + floors.get(0).monsters.size();
                    target.buffs.add(newBuff);
                }
            }
	        else{
	            for (Buff b : a.buffs) {
                    Buff newBuff = new Buff(b.type, b.effect);
                    newBuff.turns = party.size() + floors.get(0).monsters.size();
                    c.buffs.add(newBuff);
                }
            }
        }
	    Unit tUnit = getUnit(target);
	    if(a.attackPower != 0 || (a.buffs.size() > 0)) {
            if (a.enemyHealth > 0) {
                target.addBattleModifier(0, a.enemyHealth);
                CombatText ct = new CombatText("Hp", x + 1, 148, -1);
                if (tUnit.displays.size() > 0) {
                    ct.time -= .5f;
                }
                if (a.enemyHealth > 0) {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.healthUpAnimation));
                    ct.up = 1;
                }
                floatingText.add(ct);
            }
            if (a.enemyPower != 0) {
                target.addBattleModifier(1, a.enemyPower);
                CombatText ct = new CombatText("Pow", x + 1, 148, -1);
                if (tUnit.displays.size() > 0) {
                    ct.time -= .5f;
                }
                if (a.enemyPower > 0) {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.powerUpAnimation));
                    ct.up = 1;
                } else {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.downAnimation));
                    ct.up = -1;
                }
                floatingText.add(ct);
            }
            if (a.enemyDefense != 0) {
                target.addBattleModifier(2, a.enemyDefense);
                CombatText ct = new CombatText("Def", x + 1, 148, -1);
                if (tUnit.displays.size() > 0) {
                    ct.time -= .5f;
                }
                if (a.enemyDefense > 0) {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.defenseUpAnimation));
                    ct.up = 1;
                } else {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.downAnimation));
                    ct.up = -1;
                }
                floatingText.add(ct);
            }
            if (a.enemyResistance != 0) {
                target.addBattleModifier(3, a.enemyResistance);
                CombatText ct = new CombatText("Res", x + 1, 148, -1);
                if (tUnit.displays.size() > 0) {
                    ct.time -= .5f;
                }
                if (a.enemyResistance > 0) {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.resistanceUpAnimation));
                    ct.up = 1;
                } else {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.downAnimation));
                    ct.up = -1;
                }
                floatingText.add(ct);
            }
            if (a.selfHealth != 0) {
                c.addBattleModifier(0, a.selfHealth);
                CombatText ct = new CombatText("Hp", x + 1, 148, -1);
                if (tUnit.displays.size() > 0) {
                    ct.time -= .5f;
                }
                if (a.selfHealth > 0) {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.healthUpAnimation));
                    ct.up = 1;
                } else {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.downAnimation));
                    ct.up = -1;
                }
                floatingText.add(ct);
            }
            if (a.selfPower != 0) {
                c.addBattleModifier(1, a.selfPower);
                CombatText ct = new CombatText("Pow", x + 1, 148, -1);
                if (tUnit.displays.size() > 0) {
                    ct.time -= .5f;
                }
                if (a.selfPower > 0) {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.powerUpAnimation));
                    ct.up = 1;
                } else {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.downAnimation));
                    ct.up = -1;
                }
                floatingText.add(ct);
            }
            if (a.selfDefense != 0) {
                c.addBattleModifier(2, a.selfDefense);
                CombatText ct = new CombatText("Pow", x + 1, 148, -1);
                if (tUnit.displays.size() > 0) {
                    ct.time -= .5f;
                }
                if (a.selfDefense > 0) {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.defenseUpAnimation));
                    ct.up = 1;
                } else {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.downAnimation));
                    ct.up = -1;
                }
                floatingText.add(ct);
            }
            if (a.selfResistance != 0) {
                c.addBattleModifier(3, a.selfResistance);
                CombatText ct = new CombatText("Pow", x + 1, 148, -1);
                if (tUnit.displays.size() > 0) {
                    ct.time -= .5f;
                }
                if (a.selfResistance > 0) {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.resistanceUpAnimation));
                    ct.up = 1;
                } else {
                    getUnit(target).displays.add(new AttackDisplay(AssetLoader.downAnimation));
                    ct.up = -1;
                }
                floatingText.add(ct);
            }
        }
        if(a.stunChance > 0){
            int random = Character.randInt(1, 100);
            if(random <= a.stunChance * 100){
                target.stunTurns += 1;
            }
        }

    }

    public int getIndex(Character target, ArrayList<Unit> list){
	    int i = 0;
	    while(i < list.size()){
	        if(list.get(i).character == target){
	            return  i;
            }
	        else{
	            i++;
            }
        }
	    return -1;
    }

    public void attack(Character c, Attack a, Character enemy){
        int defenseIndex = 2; //defense
        if(a.type != -1){
            defenseIndex = 3; //resistance
        }

        float damage = (a.attackPower / 40f) * Math.max(1,c.getStat(1) - enemy.getStat(defenseIndex));
        for(Buff b: c.buffs){
            if(b.type == 0){ //damage buff
                damage *= b.effect;
            }
        }
        for(Buff b: enemy.buffs){
            if(b.type == 1 || (b.type == 2 && a.type == -1) || (b.type == 3 && a.type != -1)){ //protect buff
                damage *= b.effect;
            }
        }
        damage = (int) damage;
        damage = Math.max(damage,1);
        enemy.currentHp = Math.max(0,(int)(enemy.currentHp - damage));
        if(enemy.currentHp > 0 && a.aot != null){
            float aotDamage = Math.max(1,damage * (a.aot.getAp() / (float)a.attackPower));
            AttackOverTime newAot = a.aot.copy((int)damage);
            newAot.host = c;
            enemy.aots.add(newAot);
        }
        int x = 0;
        if(getIndex(enemy,party) != -1){
            x = getIndex(enemy,party);
            party.get(x).displays.add(new AttackDisplay(a));
            if(floors.get(0).floorNum % 2 == 0){
                x = 48 - 16 * x;
            }
            else{
                x = 64 + 16 * x;
            }

        }
        else if(getIndex(enemy,floors.get(0).monsters) != -1){
            x = getIndex(enemy,floors.get(0).monsters);
            floors.get(0).monsters.get(x).displays.add(new AttackDisplay(a));
            if(floors.get(0).floorNum % 2 == 0){
                x = 112 + (x - floors.get(0).monsters.size()) * 16;
            }
            else{
                x = 0 - (x - floors.get(0).monsters.size()) * 16;
            }
        }
        CombatText ct = new CombatText(String.valueOf((int)damage), x, 135, a.type);
        floatingText.add(ct);


        applyEffect(c,a,enemy,x);
    }

}
