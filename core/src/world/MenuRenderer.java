package world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.tools.bmfont.BitmapFontWriter;

public class MenuRenderer {

    private MenuWorld myWorld;
    private OrthographicCamera cam;
    private ShapeRenderer shapeRenderer;
    private SpriteBatch batcher;
    BitmapFont font, fontText;
    public float maxWidth = 224;
    public float maxHeight = 168;
    public boolean banner = true;

    private int midPointY;
    private int gameHeight;

    public MenuRenderer(MenuWorld world, int gameHeight, int midPointY) {
        generateFonts();
        maxWidth = Gdx.graphics.getWidth();
        maxHeight = Gdx.graphics.getHeight();
        float ratio = (float)maxWidth / (float)maxHeight;
        ratio /= (1920.0f/1080.0f);
        if(ratio > 1){
            maxWidth = 1920;
            maxHeight = 1080 / ratio;
        }
        else{
            maxWidth = 1920 * ratio;
            maxHeight = 1080;
        }
        cam = new OrthographicCamera();
        cam.setToOrtho(true, maxWidth, maxHeight);
        batcher = new SpriteBatch();
        // Attach batcher to camera
        batcher.setProjectionMatrix(cam.combined);
        myWorld = world;
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(cam.combined);
    }

    public void resize(){
        if(!banner) {
            maxWidth = myWorld.game.maxWidth;
            maxHeight = myWorld.game.maxHeight;
            cam.setToOrtho(true, maxWidth, maxHeight);
            batcher.setProjectionMatrix(cam.combined);
            shapeRenderer.setProjectionMatrix(cam.combined);
        }
    }

    public void render(float runtime) {
        // Fill the entire screen with black, to prevent potential flickering.
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        /*if(myWorld.bannerTimer < 0){
            batcher.begin();
            batcher.draw(AssetLoader.bannerImage,maxWidth / 2 - 960, maxHeight / 2 - 540);
            batcher.end();

            Gdx.gl20.glEnable(GL20.GL_BLEND);
            // Begin ShapeRenderer
            shapeRenderer.begin(ShapeType.Filled);
            // Draw Background color
            float shade = 0f;
            if(myWorld.bannerTimer >= -.5f){
                shade = (.25f + myWorld.bannerTimer) * 4;
            }
            shapeRenderer.setColor(0,0,0,shade);

            shapeRenderer.rect(0, 0, maxWidth, maxHeight);


            // End ShapeRenderer
            shapeRenderer.end();
            Gdx.gl20.glDisable(GL20.GL_BLEND);
            myWorld.rendered = true;
        }
        else if(myWorld.loaded){
            if(banner){
                banner = false;
                resize();
            }
            setup();
            if (myWorld.screen == 5) {
                drawTutorial();
            } else if (myWorld.screen == 7) {
                drawMusicPlayer();
            } else if (myWorld.screen == 4) {
                if (myWorld.deleteUnit != -1) {
                    drawDeleteUnit(runtime);
                } else if (myWorld.copyUnit != -1) {
                    drawCopyUnit(runtime);
                } else {
                    drawSelect(runtime);
                }
            } else if (myWorld.screen == 3) {
                drawNew();
            } else if (myWorld.screen == 0) {
                drawNew();
            } else {
                drawClassSelect(runtime);
            }
            if(myWorld.cursor != -1){
                drawCursorSelect(runtime);
            }
        }*/

    }
    private void generateFonts(){
        FreeTypeFontGenerator generator;
        FreeTypeFontGenerator.FreeTypeFontParameter parameter;
        try {
            font = new BitmapFont(Gdx.files.internal("fonts/font.fnt"), true);
            //System.out.println("Font loaded successfully");
        }catch(Exception e){
            generator = new FreeTypeFontGenerator(Gdx.files.internal("visitor1.ttf"));
            parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.flip = true;
            parameter.shadowOffsetX = 0;
            parameter.shadowOffsetY = 0;
            parameter.shadowColor = Color.BLACK;
            parameter.size = 10;
            parameter.packer = new PixmapPacker(512, 512, Pixmap.Format.RGBA8888, 2, false, new PixmapPacker.SkylineStrategy());
            font = generator.generateFont(parameter);
            font.setColor(.1f,.1f,.1f,1);
            FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(parameter);

            BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
            info.padding = new BitmapFontWriter.Padding(0, 0, 0, 0);
            BitmapFontWriter.writeFont(data, new String[] {"font.png"},
                    Gdx.files.internal("fonts/font.fnt"), info, 512, 512);
            BitmapFontWriter.writePixmaps(parameter.packer.getPages(), Gdx.files.internal("fonts"), "font");
            //System.out.println("Font generated");
        }
        try{
            fontText = new BitmapFont(Gdx.files.internal("fonts/fontText.fnt"), true);
            fontText.setColor(Color.valueOf("000000"));
            //System.out.println("Font1 loaded successfully");
        }catch(Exception e) {
            generator = new FreeTypeFontGenerator(Gdx.files.internal("m5x7.ttf"));
            parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 16;
            parameter.flip = true;
            parameter.shadowOffsetX = 0;
            parameter.shadowOffsetY = 0;
            parameter.shadowColor = Color.BLACK;
            parameter.packer = new PixmapPacker(512, 512, Pixmap.Format.RGBA8888, 2, false, new PixmapPacker.SkylineStrategy());
            fontText = generator.generateFont(parameter);
            fontText.setColor(Color.valueOf("000000"));

            //save big font
            FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(parameter);
            BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
            info.padding = new BitmapFontWriter.Padding(0, 0, 0, 0);
            BitmapFontWriter.writeFont(data, new String[]{"fontText.png"},
                    Gdx.files.internal("fonts/fontText.fnt"), info, 512, 512);
            BitmapFontWriter.writePixmaps(parameter.packer.getPages(), Gdx.files.internal("fonts"), "fontText");
            //System.out.println("FontBig generated");
        }
        fontText.setColor(.1f,.1f,.1f,1);
        fontText = font;
    }

    public void getColor(Color one, Color two, int color){
        switch (color){
            case 1:
                one.set(Color.valueOf("#bf4d4d"));
                two.set(Color.valueOf("#f2c2c2"));
                break;
            case 2:
            case 5:
                one.set(Color.valueOf("#4d99bf"));
                two.set(Color.valueOf("#b6def2"));
                break;
            case 3:
                one.set(Color.valueOf("#4ba642"));
                two.set(Color.valueOf("#bbe6b8"));
                break;
            case 4:
                one.set(Color.valueOf("#397fbf"));
                two.set(Color.valueOf("#b6d5f2"));
                break;
            default:
                one.set(Color.valueOf("#806840"));
                two.set(Color.valueOf("#e6c4a1"));
                break;
        }
    }

}