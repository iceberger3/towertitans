package world;

import game.TowerTitan;
import helpers.MyTextInputListener;
import objects.Character;


public class MenuWorld {
    public int currentScreen = 0; //0 -> main, 1 -> scores, 2 -> select world, 3 -> shop
    public TowerTitan game;
    public int cursor = -1;
    public int screen = 3;
    public int classId = 1;
    public int tab = 0;
    public int page = 0;
    public boolean nameFlag;
    public int head = 0;
    public int hair = 1;
    public int style = 0;
    public Character main,paladin,reaper,ranger,sage,oracle,ninja;
    public String name;
    public boolean male = true;
    public MyTextInputListener listener;
    public boolean textDialog = false;
    public boolean deleting = false;
    public boolean copying = false;
    public int deleteUnit = -1;
    public int copyUnit = -1;
    public float tutorial = 0;
    public float bannerTimer = -2;
    public boolean rendered = false;
    public static boolean loaded = false;
    public MenuWorld(int midPointY, TowerTitan g) {
        game = g;

        listener = new MyTextInputListener(this);
    }

    public void update(float delta) {
    }


}
