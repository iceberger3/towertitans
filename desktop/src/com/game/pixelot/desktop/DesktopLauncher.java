package com.game.pixelot.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;

import javax.swing.ImageIcon;

import game.TowerTitan;
import gservices.DesktopGServices;
import helpers.ConfigObject;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		ConfigObject settings = new ConfigObject();
		settings.load();
		config.title = "TowerTitan";
		config.useGL30 = false;
		config.resizable = true;
		config.width = 512;
		config.height = 896;
		if(settings.config.xDimension != 0 && settings.config.yDimension != 0){
			config.width = settings.config.xDimension;
			config.height = settings.config.yDimension;
		}
		config.addIcon("icon128.png", Files.FileType.Internal);
		config.addIcon("icon32.png", Files.FileType.Internal);
		config.addIcon("icon16.png", Files.FileType.Internal);
		setApplicationIcon();
		new LwjglApplication(new TowerTitan(new DesktopGServices(),false), config);
	}
	private static void setApplicationIcon() {
		try {
			Class<?> cls = Class.forName("com.apple.eawt.Application");
			Object application = cls.newInstance().getClass().getMethod("getApplication").invoke(null);

			FileHandle icon = Gdx.files.local("icon.png");
			application.getClass().getMethod("setDockIconImage", java.awt.Image.class)
					.invoke(application, new ImageIcon(icon.file().getAbsolutePath()).getImage());
		} catch (Exception e) {
			// nobody cares!
		}
	}
}
