package com.game.pixelot;

import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSError;
import org.robovm.apple.gamekit.GKAchievement;
import org.robovm.apple.gamekit.GKLeaderboard;
import org.robovm.apple.uikit.UIApplication;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

import java.util.ArrayList;

import game.TowerTitan;
import gservices.GServices;

public class IOSLauncher extends IOSApplication.Delegate implements GServices {
    private GameCenterManager gcManager;
    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();
        return new IOSApplication(new TowerTitan(this,false), config);
    }

    public static void main(String[] argv) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, IOSLauncher.class);
        pool.close();
    }
    @Override
    public void signIn() {
        createGC().login();
    }

    @Override
    public void signOut() {

    }

    @Override
    public void rateGame() {

    }

    @Override
    public void submitScore(long score,int id) {
        if(id == 1){
            createGC().reportScore("pixelotArenaLeaderboard",score);
        }
        else{
            createGC().reportScore("darkTower",score);
        }
    }

    @Override
    public void unlockAchievement(int id) {
        String achievement = "";
        switch (id){
            case 1 :
                achievement = "rangersTrial";
                break;
            case 2 :
                achievement = "level10";
                break;
            case 3 :
                achievement = "earthCrystal";
                break;
            case 4 :
                achievement = "protect20";
                break;
            case 5 :
                achievement = "defeatMercenaries";
                break;
            case 6 :
                achievement = "collectHeroes";
                break;
            case 7 :
                achievement = "criticalHits";
                break;
            case 8 :
                achievement = "level20";
                break;
            case 9 :
                achievement = "shadowCrystal";
                break;
            case 10 :
                achievement = "escapeSewers";
                break;
            case 11 :
                achievement = "heal5000";
                break;
            case 12 :
                achievement = "items100";
                break;
            case 13 :
                achievement = "defeatMonsters";
                break;
            case 14 :
                achievement = "defeatUniqueEnemies";
                break;
            case 15 :
                achievement = "level30";
                break;
            case 16 :
                achievement = "sagesTrial";
                break;
            case 17 :
                achievement = "restoreMyria";
                break;
            case 18 :
                achievement = "gold10000";
                break;
            case 19 :
                achievement = "level40";
                break;
            case 20 :
                achievement = "defeatTowerKing";
                break;
            case 21 :
                achievement = "level50";
                break;
            case 22 :
                achievement = "level60";
                break;
            case 23 :
                achievement = "level70";
                break;
            case 24 :
                achievement = "level80";
                break;
            case 25 :
                achievement = "level90";
                break;
            case 26 :
                achievement = "level100";
                break;
            case 27 :
                achievement = "waterCrystal";
                break;
            case 28 :
                achievement = "stormCrystal";
                break;
            case 29 :
                achievement = "upgradeItem";
                break;
            case 30 :
                achievement = "upgradeItems";
                break;
            case 31 :
                achievement = "orgoChief";
                break;
            case 32 :
                achievement = "mountMagmor";
                break;
            case 33 :
                achievement = "damage";
                break;
            case 34 :
                achievement = "arenaChallenge";
                break;
            case 35 :
                achievement = "prisma";
                break;
            case 36 :
                achievement = "promote";
                break;
            case 37 :
                achievement = "bosses";
                break;
            case 38 :
                achievement = "quests";
                break;
            case 39 :
                achievement = "greatYeti";
                break;
            case 40 :
                achievement = "lightCrystal";
                break;
            case 41 :
                achievement = "hiddenGrotto";
                break;
            case 42 :
                achievement = "fireCrystal";
                break;
            case 43 :
                achievement = "savePixelot";
                break;
            case 44 :
                achievement = "labyrinth";
                break;
            case 45 :
                achievement = "legendary";
                break;
            case 46 :
                achievement = "prisma2";
                break;
        }
        createGC().reportAchievement(achievement);
    }

    @Override
    public void showScores(int id) {
        createGC().loadLeaderboards();
        if(id == 1){
            createGC().showLeaderboardView("pixelotArenaLeaderboard");
        }
        else {
            createGC().showLeaderboardView("darkTower");
        }
    }

    @Override
    public boolean showAchievements() {
        createGC().loadAchievements();
        createGC().showAchievementsView();
        return false;
    }

    @Override
    public boolean isSignedIn() {
        return false;
    }
    public GameCenterManager createGC(){
        if (null == gcManager) {
            System.out.println("Initialising GameCenterManager");
            gcManager = new GameCenterManager(UIApplication.getSharedApplication().getKeyWindow(), new GameCenterListener() {
                @Override
                public void playerLoginFailed (NSError error) {
                    System.out.println("playerLoginFailed. error: " + error);
                }

                @Override
                public void playerLoginCompleted () {
                    System.out.println("playerLoginCompleted");
                }

                @Override
                public void achievementReportCompleted () {
                    System.out.println("achievementReportCompleted");
                }

                @Override
                public void achievementReportFailed (NSError error) {
                    System.out.println("achievementReportFailed. error: " + error);
                }

                @Override
                public void achievementsLoadCompleted (ArrayList<GKAchievement> achievements) {
                    System.out.println("achievementsLoadCompleted: " + achievements.size());
                }

                @Override
                public void achievementsLoadFailed (NSError error) {
                    System.out.println("achievementsLoadFailed. error: " + error);
                }

                @Override
                public void achievementsResetCompleted () {
                    System.out.println("achievementsResetCompleted");
                }

                @Override
                public void achievementsResetFailed (NSError error) {
                    System.out.println("achievementsResetFailed. error: " + error);
                }

                @Override
                public void scoreReportCompleted () {
                    System.out.println("scoreReportCompleted");
                }

                @Override
                public void scoreReportFailed (NSError error) {
                    System.out.println("scoreReportFailed. error: " + error);
                }

                @Override
                public void leaderboardsLoadCompleted (ArrayList<GKLeaderboard> scores) {
                    System.out.println("scoresLoadCompleted: " + scores.size());
                }

                @Override
                public void leaderboardsLoadFailed (NSError error) {
                    System.out.println("scoresLoadFailed. error: " + error);
                }

                @Override
                public void leaderboardViewDismissed () {
                    System.out.println("leaderboardViewDismissed");
                }

                @Override
                public void achievementViewDismissed () {
                    System.out.println("achievementViewDismissed");
                }
            });
        }
        return gcManager;
    }
}